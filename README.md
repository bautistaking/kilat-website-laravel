
## About Kidlat Awards 2021

Intro: 
THE titans of Philippine advertising came together in a virtual ceremony to recognize the most outstanding, innovative and groundbreaking creative work that had come out in the past two years through the much-awaited return of the Kidlat Awards.

The Kidlat Awards, held on November 26, was the annual creative show of the Association of Accredited Advertising Agencies of the Philippines (4As). In total, more than 600 entries were screened and judged by a list of heavyweights from a variety of fields. This year, 26 entries were honored with the Gold Kidlat, 41 entries took home the Silver Kidlat Award and 85 entries received the Bronze Kidlat Award.

The Grand Kidlat for Creative Technology and Media went to McClassroom by Leo Burnett Group Manila and McDonald's Philippines. McClassroom also won the Grand Kidlat for Creative Purpose. The Grand Kidlat in Creative Storytelling was "Story of Us" by Leo Burnett Group Manila and McDonald's Philippines. Gold by Arcade Film Factory and ARC Refreshments Corp. took home the Grand Kidlat in Craft.

Filmpabrika Inc. was named the Kidlat Film Production House of the Year. The Kidlat Audio Production House of the Year went to Soundesign Manila. The Advertiser of the Year was McDonald's Philippines and the Agency of the Year was TBWASantiago Mangada Puno.

Meanwhile, three new names have been added to the Kidlat Awards Hall of Fame. Andrew Petch, chairman and chief creative officer of Petch & Partners; Biboy Royong, executive creative director of Dentsu Jayme Syfu; and Trixie Diyco, executive creative director of Publicis JimenezBasic were all honored for their outstanding contributions to the local advertising industry.

Melvin Mangada, chairman of the 4As and chairman and chief creative officer (CCO) of TBWASantiago Mangada Puno, was named the Kidlat Lifetime Achievement Award.

Source: https://www.manilatimes.net/2021/12/22/public-square/phl-advertisings-best-work-recognized-at-2021-kidlat-awards/1826848

How to Install:

1. Open command line or terminal and locate the project directory
2. Run "composer install" to download all the Laravel packages
3. Run "npm install" to download all the frontend packages
4. Change the database credentials on the .env file
3. Run "php artisan migrate" to upload the database tables
4. Run "php artisan optimize:clear" to clear all the caches
5. Open the browser and type "localhost:8000" on the url navbar (note: it can be a different port)
