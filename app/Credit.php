<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Credit extends Model
{
    //
    protected $table = "credits";
    protected $fillable = [
        'name', 'role', 'entry_id'
    ];
}
