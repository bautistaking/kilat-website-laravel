<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserCategories extends Model
{
    protected $table = "user_categories";
    protected $fillable = [
        'user_id', 'category_id', 'subcategory_id'
    ];

    protected $appends = [
        'category_name',
        'sub_category_name',
    	'left_entries1',
    	'total_entries1',
        'left_entries2',
        'total_entries2',
        'is_submitted1',
        'is_submitted2',
    ];

    public function getCategoryNameAttribute()
    {
        $category = Category::find($this->category_id);
        return $category->name;
    }

    public function getSubCategoryNameAttribute()
    {
        $sub_category = SubCategory::find($this->subcategory_id);
        return $sub_category->name;
    }

    public function getTotalEntries1Attribute()
    {
        return Entry::where('category', $this->category_id)
        			->where('sub_category', $this->subcategory_id)
                    ->where('payment_status', 'Paid')
                    ->where('status', 'Approved')
        			->count();
    }

    public function getLeftEntries1Attribute()
    {
        $user_id = auth()->user()->id;

        return VotesRound1::when($user_id, function($query) use ($user_id) {
                return $query->where('votes_round1.user_id', $user_id);
            })
           ->where('entries.category', $this->category_id)
		   ->where('entries.sub_category', $this->subcategory_id)
           ->where('entries.payment_status', 'Paid')
           ->where('entries.status', 'Approved')
		   ->join('entries', 'votes_round1.entry_id', '=', 'entries.id')
		   ->count();
    }

    public function getTotalEntries2Attribute()
    {
        $round2_entries = Round2Entries::get()->pluck('entry_id');

        return Entry::where('category', $this->category_id)
                    ->where('sub_category', $this->subcategory_id)
                    ->where('payment_status', 'Paid')
                    ->where('status', 'Approved')
                    ->whereIn('id', $round2_entries)
                    ->count();
    }

    public function getLeftEntries2Attribute()
    {
        $user_id = auth()->user()->id;
        $round2_entries = Round2Entries::get()->pluck('entry_id');

        return VotesRound2::when($user_id, function($query) use ($user_id) {
                return $query->where('votes_round2.user_id', $user_id);
            })
            ->where('entries.category', $this->category_id)
            ->where('entries.sub_category', $this->subcategory_id)
            ->where('entries.payment_status', 'Paid')
            ->where('entries.status', 'Approved')
            ->whereIn('entries.id', $round2_entries)
            ->join('entries', 'votes_round2.entry_id', '=', 'entries.id')
            ->count();
    }

    public function getIsSubmitted1Attribute()
    {
        $user_id = auth()->user()->id;

        $votes_round1 =  VotesRound1::when($user_id, function($query) use ($user_id) {
                return $query->where('votes_round1.user_id', $user_id);
            })
           ->where('entries.category', $this->category_id)
           ->where('entries.sub_category', $this->subcategory_id)
           ->where('entries.payment_status', 'Paid')
           ->where('entries.status', 'Approved')
           ->join('entries', 'votes_round1.entry_id', '=', 'entries.id')
           ->value('submitted');

        return $votes_round1;
    }

    public function getIsSubmitted2Attribute()
    {
        $user_id = auth()->user()->id;

        $votes_round2 =  VotesRound2::when($user_id, function($query) use ($user_id) {
                return $query->where('votes_round2.user_id', $user_id);
            })
           ->where('entries.category', $this->category_id)
           ->where('entries.sub_category', $this->subcategory_id)
           ->where('entries.payment_status', 'Paid')
           ->where('entries.status', 'Approved')
           ->join('entries', 'votes_round2.entry_id', '=', 'entries.id')
           ->value('submitted');

        return $votes_round2;
    }

}
