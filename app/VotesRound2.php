<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class VotesRound2 extends Model
{
    //
    protected $table = "votes_round2";
    protected $fillable = [
    	'user_id', 'entry_id', 'status', 'reason', 'submitted'
    ];
}
