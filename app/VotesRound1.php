<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class VotesRound1 extends Model
{
    //
    protected $table = "votes_round1";
    protected $fillable = [
    	'user_id', 'entry_id', 'status', 'reason'
    ];
}
