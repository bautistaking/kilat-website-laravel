<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class File extends Model
{
    //
    protected $table = "files";
    protected $fillable = [
        'name', 'path', 'type', 'entry_id'
    ];

    public function entry()
    {
        return Entry::where('id','=', $this->entry_id)->first();
    }

    public function file_type()
    {
        $info = pathinfo(storage_path().$this->path);
        if(isset($info['extension'])) {
            $ext = $info['extension'];
        } else {
            $ext = null;
        }
        
        if($ext == "jpeg" || $ext == "jpg" || $ext == "png") {
            $val = "Image";
        } elseif ($ext == "mp4" || $ext == "flv" || $ext == "avi" || $ext == "wmv" || $ext == "mov" || $ext == "qt") {
            $val = "Video";
        } elseif ($ext == "mp3") {
            $val = "Audio";
        } else {
            $val = "Others";
        }
        return $val;
    }
}
