<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Vote extends Model
{
    //
    protected $table = "votes_round1";
    protected $fillable = [
        'instance_id', 'entry_uid', 'score', 'status', 'order'
    ];

    public function entry()
    {
        return Entry::where('uid','=', $this->entry_uid)->first();
    }
}
