<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Entry extends Model
{
    protected $table = "entries";
    public $timestamps = true;

    protected $fillable = [
        'user_id', 
        'uid', 
        'category', 
        'sub_category', 
        'type', 
        'entry_type_list',
        'entry_name', 
        'ad_details', 
        'advertiser', 
        'brand', 
        'agency', 
        'date_publication',
        'concept_board_dropzone',
        'case_study_video_dropzone',
        'written_case_dropzone',
        'asc_clearance_dropzone',
        'media_certification_dropzone',
        'client_certification_dropzone',
        'film_dropzone',
        'posters_dropzone',
        'audio_dropzone',
        'demo_film_dropzone',
        'english_translation_dropzone',
        'photo_piece_dropzone',
        'entry_dropzone',
        'campaign_summary',
        'background',
        'objectives',
        'strategy',
        'solution',
        'execution',
        'results',
        'describe_strategy',
        'creative_idea',
        'describe_execution',
        'member',
        'agency_contact', 
        'position', 
        'phone_number',
        'email', 
        'fee', 
        'payment_status',
        'status',
        'first_timer',
        // 'asc_cert', 
        // 'media', 
        // 'client_cert'
    ];

    protected $appends = [
        'category_name',
        'subcategory_name',
        'completed1',
        'completed2',
        'status_round1',
        'reason_round1',
        'status_round2',
        'reason_round2',
        'submitted_round1',
        'submitted_round2',
    ];

    public function user()
    {
        return User::where('id','=', $this->user_id)->first();
    }

    public function credits()
    {
        return Credit::where('entry_id','=', $this->id)->get();
    }

    public function files()
    {
        $concept_board         = File::where('entry_id','=', $this->id)->where('type', 'concept_board_dropzone')->get();
        $case_video            = File::where('entry_id','=', $this->id)->where('type', 'case_study_video_dropzone')->get();
        $written_case          = File::where('entry_id','=', $this->id)->where('type', 'written_case_dropzone')->get();
        $asc_clearance         = File::where('entry_id','=', $this->id)->where('type', 'asc_clearance_dropzone')->get();
        $media_certification   = File::where('entry_id','=', $this->id)->where('type', 'media_certification_dropzone')->get();
        $client_certification  = File::where('entry_id','=', $this->id)->where('type', 'client_certification_dropzone')->get();
        $film                  = File::where('entry_id','=', $this->id)->where('type', 'film_dropzone')->get();
        $posters               = File::where('entry_id','=', $this->id)->where('type', 'posters_dropzone')->get();
        $audio                 = File::where('entry_id','=', $this->id)->where('type', 'audio_dropzone')->get();
        $english_translation   = File::where('entry_id','=', $this->id)->where('type', 'english_translation_dropzone')->get();
        $demo_film             = File::where('entry_id','=', $this->id)->where('type', 'demo_film_dropzone')->get();
        $jpeg_piece            = File::where('entry_id','=', $this->id)->where('type', 'photo_piece_dropzone')->get();
        $entry_piece           = File::where('entry_id','=', $this->id)->where('type', 'entry_dropzone')->get();

        $files = [
            'concept_board'         => $concept_board,
            'case_video'            => $case_video,
            'written_case'          => $written_case,
            'asc_clearance'         => $asc_clearance,
            'media_certification'   => $media_certification,
            'client_certification'  => $client_certification,
            'film'                  => $film,
            'posters'               => $posters,
            'audio'                 => $audio,
            'english_translation'   => $english_translation,
            'demo_film'             => $demo_film,
            'jpeg_piece'            => $jpeg_piece,
            'entry_piece'           => $entry_piece
        ];

        return $files;
    }

    public function key_files()
    {
        return File::where('entry_id','=', $this->id)->where('type','key')->get();
    }

    public function category()
    {
        return Category::where('id','=', $this->category)->first();
    }

    public function reference()
    {
        return Reference::where('entry_id','=', $this->id)->get();
    }

    public function sub_category()
    {
        return SubCategory::where('id','=', $this->sub_category)->first();
    }

    public function status()
    {
        $status = "";
        if($this->status == "Rejected") {
            $status = "With Issues. <br/> Please contact 4As Admin";
        } 
        elseif ($this->payment_status == "Unpaid") {
            $status = "Draft. <br/> Pending Invoice Request";
        }
        elseif ($this->payment_status == "Requested" || $this->payment_status == "Requested for Invoice") {
            $status = "Pending Payment";
        }
        elseif ($this->payment_status == "Billing Sent") {
            $status = "Invoice Sent via Email";
        } 
        elseif($this->payment_status == "Paid" && $this->status == "Pending") {
            $status = "Paid <br/> Pending Entry Review";
        }
        elseif($this->payment_status == "Paid" && $this->status == "Approved") {
            $status = "Paid <br/> Confirmed for Judging";
        }
        return $status;
    }

    public function getCompleted1Attribute()
    {
        return VotesRound1::where('entry_id', $this->id)
                          ->where('user_id', auth()->user()->id)
                          ->first();
    }

    public function getCompleted2Attribute()
    {
        return VotesRound2::where('entry_id', $this->id)
                          ->where('user_id', auth()->user()->id)
                          ->first();
    }

    public function getStatusRound1Attribute()
    {
       $status = VotesRound1::where('entry_id', $this->id)
                            ->where('user_id', auth()->user()->id)
                            ->value('status');

       if ($status == 'flag') { return "Flag"; }
       else if ($status == 'abstain') { return "Abstain"; }
       else if ($status == 'out') { return "Out"; }
       else if ($status == 'in') { return "In"; }
    }

    public function getReasonRound1Attribute()
    {
       $reason = VotesRound1::where('entry_id', $this->id)
                            ->where('user_id', auth()->user()->id)
                            ->value('reason');

       // Flag
       if ($reason == 'wrong_category') { return " - "."Wrong category"; }
       else if ($reason == 'wrong_eligibility') { return " - "."Wrong eligibility date"; }
       else if ($reason == 'other_flag') { return " - ".$reason; }

       // Abstain
       else if ($reason == 'same_agency') { return " - "."I am from the same agency / network"; }
       else if ($reason == 'involved') { return " - "."I am involved in this project"; }
       else if ($reason == 'other_abstain') { return " - ".$reason; }
       else {
            if ($reason != null) {
                return " - ".$reason;
            }
       }
    }

    public function getSubmittedRound1Attribute()
    {
       return VotesRound1::where('entry_id', $this->id)
                         ->where('user_id', auth()->user()->id)
                         ->value('submitted');
    }

    public function getStatusRound2Attribute()
    {
       $status = VotesRound2::where('entry_id', $this->id)
                            ->where('user_id', auth()->user()->id)
                            ->value('status');

       if ($status == 'flag') { return "Flag"; }
       else if ($status == 'abstain') { return "Abstain"; }
       else if ($status == 'outofshow') { return "Out of Show"; }
       else if ($status == 'finalist') { return "Finalist"; }
       else if ($status == 'gold') { return "Gold"; }
       else if ($status == 'silver') { return "Silver"; }
       else if ($status == 'bronze') { return "Bronze"; }
    }

    public function getReasonRound2Attribute()
    {
       $reason = VotesRound2::where('entry_id', $this->id)
                             ->where('user_id', auth()->user()->id)
                             ->value('reason');

       // Flag
       if ($reason == 'wrong_category') { return " - "."Wrong category"; }
       else if ($reason == 'wrong_eligibility') { return " - "."Wrong eligibility date"; }
       else if ($reason == 'other_flag') { return " - ".$reason; }

       // Abstain
       else if ($reason == 'same_agency') { return " - "."I am from the same agency / network"; }
       else if ($reason == 'involved') { return " - "."I am involved in this project"; }
       else if ($reason == 'other_abstain') { return " - ".$reason; }
       else {
            if ($reason != null) {
                return " - ".$reason;
            }
       }
    }

    public function getSubmittedRound2Attribute()
    {
       return VotesRound2::where('entry_id', $this->id)
                         ->where('user_id', auth()->user()->id)
                         ->value('submitted');
    }

    public function getCategoryNameAttribute()
    {
       return Category::where('id', $this->category)->value('name');
    }

    public function getSubCategoryNameAttribute()
    {
       return SubCategory::where('id', $this->sub_category)->value('name');
    }
    

}
