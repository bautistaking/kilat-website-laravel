<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class VoteInstance extends Model
{
    //
    protected $table = "vote_instance";
    protected $fillable = [
        'user_id', 'category_id', 'status'
    ];

    public function category()
    {
        return Category::where('id','=', $this->category_id)->first();
    }

    public function votes()
    {
        return Vote::where('instance_id','=', $this->id)->get();
    }

    public function finished()
    {
        return Vote::where('instance_id','=', $this->id)->whereNotNull('score')->get();
    }

    public function judge()
    {
        return User::where('id','=', $this->user_id)->first();
    }
}
