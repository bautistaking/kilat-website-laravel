<?php

namespace App\ViewModels;

use Illuminate\Database\Eloquent\Model;
use \App\UserCategories;

class Judge extends Model
{
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'updated_at'
    ]; 

    /**
     * The table associated with the model.
     *
     * @var string
    */
    protected $table = 'users';

    /**
     * The primary key associated with the table.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * The attributes that should be cast.
     *
     * @var array
     */
    protected $casts = [
        'created_at' => 'datetime:Y-m-d H:i:s',
    ];

    public function getUserCategories()
    {
        return $this->hasMany('App\UserCategories', 'user_id');
    }

    public function getUserDetails()
    {
        return $this->hasOne('App\UserDetails', 'user_id');
    }

    /**
     * Append additiona info to the return data
     *
     * @var string
     */
    public $appends = [
        'user_categories',
        'juror_name',
        'juror_agency',
    ];

    public function getUserCategoriesAttribute() 
    {
        $user_categories = $this->getUserCategories()->get();
        if($user_categories)
            return $user_categories;

        return null;
    }

    public function getJurorNameAttribute() 
    {
        $juror = $this->getUserDetails()->first();
        if($juror)
            return $juror->juror_name;

        return null;
    }

    public function getJurorAgencyAttribute() 
    {
        $juror = $this->getUserDetails()->first();
        if($juror)
            return $juror->juror_agency;

        return null;
    }

}
