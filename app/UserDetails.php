<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserDetails extends Model
{
    protected $table = "user_details";
    protected $fillable = [
        'user_id', 'juror_name', 'juror_agency'
    ];
}
