<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ShowStatus extends Model
{
    protected $table = "show_status";
    protected $fillable = [
        'show_name',
        'active',
        'expired_at'
    ];

}
