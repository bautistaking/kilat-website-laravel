<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Round2Entries extends Model
{
    //
    protected $table = "round2_entries";

    protected $fillable = [ 'entry_id' ];
}
