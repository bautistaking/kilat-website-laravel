<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    //
    protected $table = "category";

    public function sub_categories()
    {
        return SubCategory::where('category_id','=', $this->id)->orderBy('order','asc')->get();
    }
}
