<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromArray;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;

use \App\User;
use \App\Entry;
use \App\Credit;
use \App\File;
use \App\Category;
use \App\SubCategory;
use \App\Reference;
use App\ShowStatus;
use App\VotesRound1;
use App\VotesRound2;
use App\Round2Entries;

class Round2EntriesExport implements FromCollection, WithHeadings
{
    /**
    * Headings
    */
    public function headings(): array
    {
        return [
            'ID',
            'Entry_Name',
            'Category',
            'Subcategory',
            'Files',
        ];
    }
    
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        $entries = Round2Entries::select('entries.id','entries.uid', 'entries.entry_name', 'category.name as cat_name', 'sub_category.name as sub_name','entries.status as entry_status')
                         ->where('entries.status', 'Approved')
                         ->where('entries.payment_status', 'Paid')
                         ->join('entries', 'round2_entries.entry_id','=','entries.id')
                         ->join('sub_category', 'entries.sub_category','=','sub_category.id')
                         ->join('category', 'entries.category','=','category.id')
                         ->orderBy('entries.entry_name', 'ASC')
                         ->get();

        $responseData = [];

        foreach ($entries as $entry)
        {
            $entry_checker = Entry::where('uid', $entry->uid)->first();
            $file_list = "";

            $files   = File::where('entry_id', $entry_checker->id)
                       ->whereIn('type', ['ASC Clearance', 'Client Certification', 'Media Certification of Performance'])
                       ->get();

            if (count($files) > 0) {
                foreach($files as $index => $file) {
                    if ($file != "") {
                        $file_list .= url('/')."/storage/".$file->path.",";
                    }
                }
            } else {
                if ($entry_checker->asc_clearance_dropzone != "") {
                    $file_list .= url('/')."/storage/".$entry_checker->asc_clearance_dropzone.",";
                }

                if ($entry_checker->media_certification_dropzone != "") {
                    $file_list .= url('/')."/storage/".$entry_checker->media_certification_dropzone.",";
                }

                if ($entry_checker->client_certification_dropzone != "") {
                    $file_list .= url('/')."/storage/".$entry_checker->client_certification_dropzone."";
                }

            }

            $responseData[] = [
                'uid'         => $entry->uid,
                'entry_name'  => $entry->entry_name,
                'cat_name'    => $entry->cat_name,
                'sub_name'    => $entry->sub_name,
                'files'       => $file_list,
            ];
        }

        return collect($responseData);     
    }
}
