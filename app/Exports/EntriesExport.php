<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromArray;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;

use \App\User;
use \App\Entry;

class EntriesExport implements FromCollection, WithHeadings
{
    /**
    * Headings
    */
    public function headings(): array
    {
        return [
            'ID',
            'Entry Name',
            'Ad Title',
            'Advertiser',
            'Brand',
            'Agency',
            'Date Publication',
            'Entry Type',
            'Category',
            'Sub Category',
            'Agency Contact',
            'Position',
            'Phone Number',
            'Email Address',
            'Payment Status',
            'Fee',
            '4AS Member',
            'Entry Status',
        ];
    }
    
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        $entries = Entry::orderBy('created_at','desc')->get();

        $responseData = [];

        foreach ($entries as $entry)
        {
            $responseData[] = [
                'uid'            => $entry->uid,
                'entry_name'     => $entry->entry_name,
                'ad_details'     => $entry->ad_details,
                'advertiser'     => $entry->advertiser,
                'brand'          => $entry->brand,
                'agency'         => $entry->agency,
                'date_publication' => $entry->date_publication,
                'type'           => $entry->type,
                'category'       => $entry->category()->name,
                'sub_category'   => $entry->sub_category()->name,
                'agency_contact' => $entry->agency_contact,
                'position'       => $entry->position,
                'phone_number'   => $entry->phone_number,
                'email'          => $entry->email,
                'payment_status' => $entry->payment_status,
                'fee'            => $entry->fee,
                'member'         => $entry->member,
                'status'         => $entry->status,
            ];
        }

        return collect($responseData);     
    }
}
