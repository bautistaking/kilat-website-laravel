<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromArray;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;

use \App\User;
use \App\Entry;
use \App\Credit;
use \App\File;
use \App\Category;
use \App\SubCategory;
use \App\Reference;
use App\ShowStatus;
use App\VotesRound1;
use App\VotesRound2;
use App\Round2Entries;

class ScoreSummaryRound2Export implements FromCollection, WithHeadings
{
    /**
    * Headings
    */
	public function headings(): array
    {
        return [
            'ID',
            'Entry_Name',
            'Category',
            'Subcategory',
            'Status',
            'Reason',
            'Vote',
            'Advertiser'
        ];
    }
    
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        $entries = VotesRound2::select('entries.id','entries.uid', 'entries.entry_name', 'category.name as cat_name', 'sub_category.name as sub_name','entries.status as entry_status', 'votes_round2.status as vote_status', 'votes_round2.reason', 'entries.advertiser')
                         ->selectRaw('COUNT(*) AS vote_count')
                         ->where('entries.status', 'Approved')
                         ->where('entries.payment_status', 'Paid')
                         ->join('entries', 'votes_round2.entry_id','=','entries.id')
                         ->join('sub_category', 'entries.sub_category','=','sub_category.id')
                         ->join('category', 'entries.category','=','category.id')
                         ->orderBy('entries.created_at', 'DESC')
                         ->groupBy('entries.id', 'votes_round2.status')
                         ->get();

        $responseData = [];

        foreach ($entries as $entry)
        {
            $responseData[] = [
                'uid'         => $entry->uid,
                'entry_name'  => $entry->entry_name,
                'cat_name'    => $entry->cat_name,
                'sub_name'    => $entry->sub_name,
                'vote_status' => $entry->vote_status,
                'reason'      => $entry->reason,
                'vote_count'  => $entry->vote_count,
                'advertiser'  => $entry->advertiser

            ];
        }

        return collect($responseData);     
    }
}
