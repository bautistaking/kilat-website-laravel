<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromArray;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;

use \App\User;
use \App\Entry;
use \App\Credit;
use \App\File;
use \App\Category;
use \App\SubCategory;
use \App\Reference;
use App\ShowStatus;
use App\VotesRound1;
use App\VotesRound2;
use App\Round2Entries;

class ScoreAllRound1Export implements FromCollection, WithHeadings
{
    /**
    * Headings
    */
    public function headings(): array
    {
        return [
            'ID',
            'Entry_Name',
            'Category',
            'Subcategory',
            'Status',
            'Reason',
            'Judge',
            'Vote Date',
            '',
            'Files',
            'Advertiser',
        ];
    }
    
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        $entries = VotesRound1::select('entries.id','entries.uid', 'entries.entry_name', 'category.name as cat_name', 'sub_category.name as sub_name','entries.status as entry_status', 'votes_round1.status as vote_status', 'votes_round1.reason', 'user_details.juror_name as juror', 'votes_round1.updated_at as vote_date', 'entries.advertiser')
                         ->where('entries.status', 'Approved')
                         ->where('entries.payment_status', 'Paid')
                         ->leftJoin('entries', 'votes_round1.entry_id','=','entries.id')
                         ->leftJoin('sub_category', 'entries.sub_category','=','sub_category.id')
                         ->leftJoin('category', 'entries.category','=','category.id')
                         ->leftJoin('user_details', 'votes_round1.user_id', '=', 'user_details.user_id')
                         ->orderBy('entries.entry_name', 'ASC')
                         ->get();

        $responseData = [];

        foreach ($entries as $entry)
        {   
            $entry_checker = Entry::where('uid', $entry->uid)->first();
            $file_list = "";

            if ($entry_checker) {
                $files   = File::where('entry_id', $entry_checker->id)->get();
                foreach($files as $index => $file) {
                    $file_list .= url('/')."/storage/".$file->path.",";
                }
            } else {
                $file_list .= url('/')."/storage/".$entry_checker->concept_board_dropzone.",";
                $file_list .= url('/')."/storage/".$entry_checker->case_study_video_dropzone.",";
                $file_list .= url('/')."/storage/".$entry_checker->written_case_dropzone.",";
                $file_list .= url('/')."/storage/".$entry_checker->asc_clearance_dropzone.",";
                $file_list .= url('/')."/storage/".$entry_checker->media_certification_dropzone.",";
                $file_list .= url('/')."/storage/".$entry_checker->client_certification_dropzone.",";
                $file_list .= url('/')."/storage/".$entry_checker->film_dropzone.",";
                $file_list .= url('/')."/storage/".$entry_checker->posters_dropzone.",";
                $file_list .= url('/')."/storage/".$entry_checker->audio_dropzone.",";
                $file_list .= url('/')."/storage/".$entry_checker->demo_film_dropzone.",";
                $file_list .= url('/')."/storage/".$entry_checker->english_translation_dropzone.",";
                $file_list .= url('/')."/storage/".$entry_checker->photo_piece_dropzone.",";
                $file_list .= url('/')."/storage/".$entry_checker->entry_dropzone;
            }

            $responseData[] = [
                'uid'         => $entry->uid,
                'entry_name'  => $entry->entry_name,
                'cat_name'    => $entry->cat_name,
                'sub_name'    => $entry->sub_name,
                'vote_status' => $entry->vote_status,
                'reason'      => $entry->reason,
                'juror'       => $entry->juror,
                'vote_date'   => $entry->vote_date,
                ''            => '',
                'files'       => $file_list,
                'advertiser'  => $entry->advertiser
            ];
        }

        return collect($responseData);     
    }
}
