<?php

namespace App\Imports;

use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithValidation;

use Illuminate\Validation\Rule;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;


use App\Entry;
use App\Round2Entries;

use Storage;

class EntriesImport implements ToCollection, WithHeadingRow 
{
    /**
    * @param Collection $collection
    */
    public function collection(Collection $rows)
    {
        foreach ($rows as $row) 
        {
            $entry = Entry::where('uid', $row['id'])
                              ->orWhere('entry_name', $row['entry_name'])
                              ->first();
            
            Round2Entries::updateOrCreate(['entry_id' => $entry->id]);
        }
    }
}
