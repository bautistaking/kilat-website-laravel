<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class SubmitEntry extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'category'                  => 'required',
            'type'                      => 'required',
            'type_list'                 => 'required',
            'entry_name'                => 'required',
            'ad_title'                  => 'required',
            'advertiser'                => 'required',
            'agency'                    => 'required',
            'first_publication_date'    => 'required',

            // creative
            'case_study_video'          => 'required',
            'case_study_board'          => 'required',
            'asc_clearance'             => 'required',
            'media_certification'       => 'required',

            // film
            'film'                      => 'required',
            'case_study_video_branded'  => 'required',
            'asc_clearance_film'        => 'required',
            'media_certification_film'  => 'required',

            // design
            'photo'                     => 'required',
            'concept_board'             => 'required', // optional
            'client_certification'      => 'required',

            'online_links'              => 'required',
            'campaign_summary'          => 'required',
            'objectives'                => 'required',
            'strategy'                  => 'required',
            'execution'                 => 'required',
            'results'                   => 'required',
            'contact_person'            => 'required',
            'position'                  => 'required',
            'phone_number'              => 'required',
            'email_address'             => 'required',
            'credit_roles'              => 'required',
        ];
    }

    // public function attributes()
    // {
        // return [
        //     'type_list' => 'type list',
        // ];
    // }
}
