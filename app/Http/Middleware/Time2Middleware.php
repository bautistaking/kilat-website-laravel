<?php

namespace App\Http\Middleware;

use Closure;
use DateTime;

class Time2Middleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $date_now = new DateTime(); // this format is string comparable
        // $date_expiry = new DateTime("2019-04-16 12:00 am");
        $date_expiry = new DateTime("2021-12-01 06:00 am");


        if ($date_now > $date_expiry) {
            return redirect('/dashboard');
        } else {        
            return $next($request);
        }
    }
}
