<?php

namespace App\Http\Middleware;

use Closure;
use DateTime;
use App\ShowStatus;

class TimeMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $date_now = new DateTime();
        $status = ShowStatus::first();
        $date_expiry = new DateTime($status->expired_at);

        if ($date_now > $date_expiry) {
            return redirect('/dashboard');
        } else {        
            return $next($request);
        }
    }
}
