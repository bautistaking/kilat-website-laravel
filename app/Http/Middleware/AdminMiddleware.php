<?php

namespace App\Http\Middleware;

use Illuminate\Support\Facades\Auth;

use Closure;

use App\User;

class AdminMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $user = Auth::user();
        if(empty($user))
        {
            return redirect('/admin/login');
        }
        if ($user->role == 'Admin')
        {
            return $next($request);
        }
        
        return redirect()->route('error');
    }
}
