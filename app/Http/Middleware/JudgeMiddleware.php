<?php

namespace App\Http\Middleware;

use Illuminate\Support\Facades\Auth;

use Closure;

use App\User;

class JudgeMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $user = Auth::user();
        if(empty($user))
        {
            return redirect('/');
        }
        if ($user->role == 'Judge')
        {
            return $next($request);
        }
        
        return redirect()->route('error');
    }
}
