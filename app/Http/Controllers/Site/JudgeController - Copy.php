<?php

namespace App\Http\Controllers\Site;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

// use \App\VoteInstance;
use App\Vote;
use App\Entry;
use App\Category;
use App\Subcategory;
use App\UserCategories;
use App\Round2Entries;
use App\VotesRound1;
// use App\VotesRound2;
// use \App\File;

class JudgeController extends Controller
{
    public function index()
    {   
        // Round 1
        $data['user_categories1'] = UserCategories::select('user_categories.*','category.name as cat_name', 'sub_category.name as sub_name')
            ->where('user_categories.user_id', 10) // auth()->user()->id
            ->join('sub_category', 'user_categories.subcategory_id', '=', 'sub_category.id')
            ->join('category', 'user_categories.category_id', '=', 'category.id')
            ->get(); 

        // Round 2
       

        // $data['user_categories2'] = UserCategories::select('user_categories.*','category.name as cat_name', 'sub_category.name as sub_name')
        //     ->whereIn('entry_id', $round2_entries)
        //     ->where('user_categories.user_id', 25) // auth()->user()->id
        //     ->join('sub_category', 'user_categories.subcategory_id', '=', 'sub_category.id')
        //     ->join('category', 'user_categories.category_id', '=', 'category.id')
        //     ->get();

        return view('juror.dashboard', $data);
    }

    public function viewRound1(Request $request)
    {
        $data['entries'] = Entry::where('entries.category', $request->cat_id)
                   ->where('entries.sub_category', $request->sub_id)
                   ->where('entries.payment_status', 'Paid')
                   ->where('entries.status', 'Approved')
                   // ->where('user_categories.user_id', 25)
                   // ->join('user_categories', 'entries.category', '=', 'user_categories.category_id')
                   ->get();

        $data['category'] = Category::where('id', $request->cat_id)->first();
        $data['subcategory'] = SubCategory::where('id', $request->sub_id)->first();

        return view('juror.view-round1', $data);
    }

    public function submitVoteRound1(Request $request)
    {
        $vote = VotesRound1::where('entry_id', $request->entry_id)
                      ->where('user_id', auth()->user()->id)
                      ->first();

        if ($vote) {
            $vote->user_id       = auth()->user()->id;
            $vote->entry_id      = $request->entry_id;
            $vote->status        = $request->status;
            $vote->reason        = $request->reason;
            $vote->save();
        } else {
            $vote                = new VotesRound1;
            $vote->user_id       = auth()->user()->id;
            $vote->entry_id      = $request->entry_id;
            $vote->status = $request->status;
            $vote->reason        = $request->reason;
            $vote->save();
        }
        
        return response()->json([
            'header'      => "Entry voted successfully!",
            'data'        => ['id' => $vote->id],
            'status'      => 'success',
            'status_code' => 200
        ]);
    }

    public function reviewRound1()
    {
        return view('juror.review-round1', []);
    }

    public function successRound1()
    {
        return view('juror.success-round1', []);
    }

    public function viewRound2(Request $request)
    {
         $round2_entries = Round2Entries::get()->pluck('entry_id');
         $data['entries'] = Entry::where('entries.category', $request->cat_id)
                   ->whereIn('id', $round2_entries)
                   ->where('entries.sub_category', $request->sub_id)
                   ->where('entries.payment_status', 'Paid')
                   ->where('entries.status', 'Approved')
                   // ->where('user_categories.user_id', 25)
                   // ->join('user_categories', 'entries.category', '=', 'user_categories.category_id')
                   ->get();

        $data['category'] = Category::where('id', $request->cat_id)->first();
        $data['subcategory'] = SubCategory::where('id', $request->sub_id)->first();

        return view('juror.view-round2', $data);
    }

    public function reviewRound2()
    {
        return view('juror.review-round2', []);
    }

    public function successRound2()
    {
        return view('juror.success-round2', []);
    }

    // old methods
    public function instructions()
    {
        return view('dashboard.judge.instructions');
    }

    public function list()
    {
        return view('dashboard.judge.list');
    }

    public function vote($id)
    {
        $user_id = Auth::user()->id;
        $data['vote'] = Vote::where('id',$id)->first();
        $data['prev'] = Vote::where('id', '<', $data['vote']->id)->where('instance_id',$data['vote']->instance_id)->orderBy('id','desc')->first();
        $data['next'] = Vote::where('id', '>', $data['vote']->id)->where('instance_id',$data['vote']->instance_id)->orderBy('id','asc')->first();
        return view('dashboard.judge.vote',$data);
    }

    public function file($id)
    {
        # code...
        $file = File::where("id",$id)->first();
        if(!empty ( $file )) {
            return response()->file(storage_path('app/' . $file->path), [
                'Content-Disposition' => 'inline; filename="'. $file->name .'"'
            ]);
        } else {
            abort(404);
        }
    }

    public function submitScore(Request $request)
    {
        $data = $request->all();
        $vote = Vote::where('id',$data['id'])->first();
        if(!isset($data['score']))
        {
            $data['score'] = null;
        } 
        $vote->score = $data['score'];
        $vote->save();
        $instance = VoteInstance::where('id',$vote->instance_id)->first();
        $instance->status = "Started";
        $instance->save();

        $next = Vote::where('id', '>', $vote->id)->where('instance_id',$vote->instance_id)->first();

        if(!empty($next)) {
            return redirect('/judge/vote/'.$next->id);
        } else {
            return redirect('/judge/dashboard/list');
        }
    }

    public function completeJudging(Request $request)
    {
        $data = $request->all();
        $user_id = Auth::user()->id;
        $instance = VoteInstance::where('id',$data['id'])->where('user_id',$user_id)->first();
        $instance->status = "Done";
        $instance->save();

        return redirect('/judge/dashboard/list');
    }
}
