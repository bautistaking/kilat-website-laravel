<?php

namespace App\Http\Controllers\Site;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

// use \App\VoteInstance;
use App\Vote;
use App\Entry;
use App\Category;
use App\SubCategory;
use App\Reference;
use App\UserCategories;
use App\Round2Entries;
use App\VotesRound1;
use App\VotesRound2;
// use \App\File;

class JudgeController extends Controller
{
    public function index()
    {   
        $round2_entries = Round2Entries::get()->pluck('entry_id');

        $data['user_categories1'] = UserCategories::select('user_categories.*','category.name as cat_name', 'sub_category.name as sub_name')
            ->where('user_categories.user_id', auth()->user()->id)
            ->join('sub_category', 'user_categories.subcategory_id', '=', 'sub_category.id')
            ->join('category', 'user_categories.category_id', '=', 'category.id')
            ->get();      

        $data['round2_entries'] = $round2_entries->count();
        $sub_categories = Entry::whereRaw('uid in (SELECT entry_id FROM round2_entries)')->groupBy('sub_category')->get()->pluck('sub_category');

        $data['user_categories2'] = UserCategories::select('user_categories.*','category.name as cat_name', 'sub_category.name as sub_name')
            ->whereIn('sub_category.id', $sub_categories)
            ->where('user_categories.user_id', 137)
            ->join('sub_category', 'user_categories.subcategory_id', '=', 'sub_category.id')
            ->join('category', 'user_categories.category_id', '=', 'category.id')
            ->get();

        $votes_round2 = VotesRound2::select('entries.category', 'entries.sub_category')
                                   ->where('votes_round2.user_id', auth()->user()->id)
                                   ->where('submitted', 1)
                                   ->join('entries', 'votes_round2.entry_id','=', 'entries.id')
                                   ->orderBy('votes_round2.updated_at', 'DESC')
                                   ->groupBy('entries.category', 'entries.sub_category')
                                   ->get();

        $category_id    =  $votes_round2->pluck('category');
        $subcategory_id =  $votes_round2->pluck('sub_category');

        $user_categories = UserCategories::whereNotIn('subcategory_id', $subcategory_id)
                                         ->whereIn('entries.uid', $round2_entries)
                                         ->join('entries', 'user_categories.subcategory_id','=', 'entries.sub_category')
                                         ->first();

        $data['next'] = $user_categories;

        return view('juror.dashboard', $data);
    }

    public function viewRound1(Request $request)
    {
        $data['entries'] = Entry::where('category', $request->cat_id)
                   ->where('sub_category', $request->sub_id)
                   ->where('payment_status', 'Paid')
                   ->where('status', 'Approved')
                   ->get();

        $data['completed_round1'] = Round2Entries::get();
        $data['category'] = Category::where('id', $request->cat_id)->first();
        $data['subcategory'] = SubCategory::where('id', $request->sub_id)->first();

        return view('juror.view-round1', $data);
    }

    public function voteRound1(Request $request)
    {
        $data['entries'] = Entry::select('entries.*', 'votes_round1.status as vote_status', 'votes_round1.reason')
                   ->where('entries.category', $request->cat_id)
                   ->where('entries.sub_category', $request->sub_id)
                   ->where('entries.payment_status', 'Paid')
                   ->where('entries.status', 'Approved')
                   ->leftJoin('votes_round1', function($join)
                         {
                            $join->on('entries.id', '=', 'votes_round1.entry_id')
                            ->where('votes_round1.user_id','=', auth()->user()->id);
                         })
                   ->orderBy('entries.created_at', 'ASC')
                   ->get();

        return response()->json([
            'header'      => "Entries successfully fetched!",
            'data'        => $data,
            'status'      => 'success',
            'status_code' => 200
        ]);
    }

    public function voteStatusRound1(Request $request)
    {
        $data['vote_status'] = VotesRound1::where('entry_id', $request->entry_id)
                                      ->where('user_id', auth()->user()->id)
                                      ->first();

        $data['references'] = Reference::where('entry_id', $request->entry_id)->get();

        return response()->json([
            'header'      => "Vote status successfully fetched!",
            'data'        => $data,
            'status'      => 'success',
            'status_code' => 200
        ]);
    }

    public function submitVoteRound1(Request $request)
    {
        $vote = VotesRound1::where('entry_id', $request->entry_id)
                      ->where('user_id', auth()->user()->id)
                      ->first();

        if ($request->status == "in" || $request->status == "out") {
            $request->reason = "";
        }

        if ($vote) {
            $vote->user_id       = auth()->user()->id;
            $vote->entry_id      = $request->entry_id;
            $vote->status        = $request->status;
            $vote->reason        = $request->reason;
            $vote->save();
        } else {
            $vote                = new VotesRound1;
            $vote->user_id       = auth()->user()->id;
            $vote->entry_id      = $request->entry_id;
            $vote->status        = $request->status;
            $vote->reason        = $request->reason;
            $vote->save();
        }

        // $round2_entries           = new Round2Entries;
        // $round2_entries->entry_id = $request->entry_id;
        // $round2_entries->save();
        
        return response()->json([
            'header'      => "Entry voted successfully!",
            'data'        => ['id' => $vote->id],
            'status'      => 'success',
            'status_code' => 200
        ]);
    }

    public function submitAllVoteRound1(Request $request)
    {
        $data = $request->all();

        $id = Auth::user()->id;
        $votes = VotesRound1::where('user_id',$id)
                            ->whereIn('entry_id', $data['entry_ids'])
                            ->update(['submitted' => 1]);

        return response()->json([
            'header'      => "Votes submitted successfully!",
            'data'        => [],
            'status'      => 'success',
            'status_code' => 200
        ]);
    }

    public function successRound1()
    {
        $votes_round1 = VotesRound1::select('entries.category', 'entries.sub_category')
                                   ->where('votes_round1.user_id', auth()->user()->id)
                                   ->where('submitted', 1)
                                   ->join('entries', 'votes_round1.entry_id','=', 'entries.id')
                                   ->orderBy('votes_round1.updated_at', 'DESC')
                                   ->groupBy('entries.category', 'entries.sub_category')
                                   ->get();

        $category_id    =  $votes_round1->pluck('category');
        $subcategory_id =  $votes_round1->pluck('sub_category');

        $user_categories = UserCategories::where('user_categories.user_id', auth()->user()->id)
                                         ->whereNotIn('subcategory_id', $subcategory_id)
                                         ->join('entries', 'user_categories.subcategory_id','=', 'entries.sub_category')
                                         ->first();

        return view('juror.success-round1', ['next' => $user_categories]);
    }

    public function viewRound2(Request $request)
    {
        $round2_entries = Round2Entries::get()->pluck('entry_id');
        
        $data['entries'] = Entry::where('entries.category', $request->cat_id)
                   ->where('entries.sub_category', $request->sub_id)
                   ->where('payment_status', 'Paid')
                   ->where('status', 'Approved')
                   ->whereIn('uid', $round2_entries)
                   ->get();

        $data['category'] = Category::where('id', $request->cat_id)->first();
        $data['subcategory'] = SubCategory::where('id', $request->sub_id)->first();

        return view('juror.view-round2', $data);
    }

    public function voteStatusRound2(Request $request)
    {
        $data['vote_status'] = VotesRound2::where('entry_id', $request->entry_id)
                                      ->where('user_id', auth()->user()->id)
                                      ->first();

        $data['references'] = Reference::where('entry_id', $request->entry_id)->get();

        return response()->json([
            'header'      => "Vote status successfully fetched!",
            'data'        => $data,
            'status'      => 'success',
            'status_code' => 200
        ]);
    }

    public function voteRound2(Request $request)
    {   
        $round2_entries = Round2Entries::get()->pluck('entry_id');

        $data['entries'] = Entry::select('entries.*', 'votes_round2.status as vote_status', 'votes_round2.reason')
                   ->whereIn('entries.id', $round2_entries)
                   ->where('entries.category', $request->cat_id)
                   ->where('entries.sub_category', $request->sub_id)
                   ->where('entries.payment_status', 'Paid')
                   ->where('entries.status', 'Approved')
                   ->leftJoin('votes_round2', function($join)
                         {
                            $join->on('entries.id', '=', 'votes_round2.entry_id')
                            ->where('votes_round2.user_id','=', auth()->user()->id);
                         })
                   ->orderBy('entries.created_at', 'ASC')
                   ->get();

        return response()->json([
            'header'      => "Entries successfully fetched!",
            'data'        => $data,
            'status'      => 'success',
            'status_code' => 200
        ]);
    }

    public function submitVoteRound2(Request $request)
    {
        $vote = VotesRound2::where('entry_id', $request->entry_id)
                      ->where('user_id', auth()->user()->id)
                      ->first();

        if ($request->status == "shortlist" || $request->status == "bronze" || $request->status == "silver" || $request->status == "gold") {
            $request->reason = "";
        }

        if ($vote) {
            $vote->user_id       = auth()->user()->id;
            $vote->entry_id      = $request->entry_id;
            $vote->status        = $request->status;
            $vote->reason        = $request->reason;
            $vote->save();
        } else {
            $vote                = new VotesRound2;
            $vote->user_id       = auth()->user()->id;
            $vote->entry_id      = $request->entry_id;
            $vote->status        = $request->status;
            $vote->reason        = $request->reason;
            $vote->save();
        }

        // $round2_entries           = new Round2Entries;
        // $round2_entries->entry_id = $request->entry_id;
        // $round2_entries->save();
        
        return response()->json([
            'header'      => "Entry voted successfully!",
            'data'        => ['id' => $vote->id],
            'status'      => 'success',
            'status_code' => 200
        ]);
    }

    public function submitAllVoteRound2(Request $request)
    {
        $data = $request->all();

        $id = Auth::user()->id;
        $votes = VotesRound2::where('user_id',$id)
                            ->whereIn('entry_id', $data['entry_ids'])
                            ->update(['submitted' => 1]);

        return response()->json([
            'header'      => "Votes submitted successfully!",
            'data'        => [],
            'status'      => 'success',
            'status_code' => 200
        ]);
    }

    // public function viewRound2(Request $request)
    // {
    //      $round2_entries = Round2Entries::get()->pluck('entry_id');
    //      $data['entries'] = Entry::where('entries.category', $request->cat_id)
    //                ->whereIn('id', $round2_entries)
    //                ->where('entries.sub_category', $request->sub_id)
    //                ->where('entries.payment_status', 'Paid')
    //                ->where('entries.status', 'Approved')
    //                // ->where('user_categories.user_id', 25)
    //                // ->join('user_categories', 'entries.category', '=', 'user_categories.category_id')
    //                ->get();

    //     $data['category'] = Category::where('id', $request->cat_id)->first();
    //     $data['subcategory'] = SubCategory::where('id', $request->sub_id)->first();

    //     return view('juror.view-round2', $data);
    // }

    public function successRound2()
    {
        $round2_entries = Round2Entries::get()->pluck('entry_id');

        $votes_round2 = VotesRound2::select('entries.category', 'entries.sub_category')
                                   ->where('votes_round2.user_id', auth()->user()->id)
                                   ->where('submitted', 1)
                                   ->join('entries', 'votes_round2.entry_id','=', 'entries.id')
                                   ->orderBy('votes_round2.updated_at', 'DESC')
                                   ->groupBy('entries.category', 'entries.sub_category')
                                   ->get();

        $category_id    =  $votes_round2->pluck('category');
        $subcategory_id =  $votes_round2->pluck('sub_category');

        $user_categories = UserCategories::where('user_categories.user_id', auth()->user()->id)
                                         ->whereNotIn('subcategory_id', $subcategory_id)
                                         ->whereIn('entries.id', $round2_entries)
                                         ->join('entries', 'user_categories.subcategory_id','=', 'entries.sub_category')
                                         ->first();

        return view('juror.success-round2', ['next' => $user_categories]);
    }

    // old methods
    public function instructions()
    {
        return view('dashboard.judge.instructions');
    }

    public function list()
    {
        return view('dashboard.judge.list');
    }

    public function vote($id)
    {
        $user_id = Auth::user()->id;
        $data['vote'] = Vote::where('id',$id)->first();
        $data['prev'] = Vote::where('id', '<', $data['vote']->id)->where('instance_id',$data['vote']->instance_id)->orderBy('id','desc')->first();
        $data['next'] = Vote::where('id', '>', $data['vote']->id)->where('instance_id',$data['vote']->instance_id)->orderBy('id','asc')->first();
        return view('dashboard.judge.vote',$data);
    }

    public function file($id)
    {
        # code...
        $file = File::where("id",$id)->first();
        if(!empty ( $file )) {
            return response()->file(storage_path('app/' . $file->path), [
                'Content-Disposition' => 'inline; filename="'. $file->name .'"'
            ]);
        } else {
            abort(404);
        }
    }

    public function submitScore(Request $request)
    {
        $data = $request->all();
        $vote = Vote::where('id',$data['id'])->first();
        if(!isset($data['score']))
        {
            $data['score'] = null;
        } 
        $vote->score = $data['score'];
        $vote->save();
        $instance = VoteInstance::where('id',$vote->instance_id)->first();
        $instance->status = "Started";
        $instance->save();

        $next = Vote::where('id', '>', $vote->id)->where('instance_id',$vote->instance_id)->first();

        if(!empty($next)) {
            return redirect('/judge/vote/'.$next->id);
        } else {
            return redirect('/judge/dashboard/list');
        }
    }

    public function completeJudging(Request $request)
    {
        $data = $request->all();
        $user_id = Auth::user()->id;
        $instance = VoteInstance::where('id',$data['id'])->where('user_id',$user_id)->first();
        $instance->status = "Done";
        $instance->save();

        return redirect('/judge/dashboard/list');
    }
}
