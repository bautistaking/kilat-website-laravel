<?php

namespace App\Http\Controllers\Site;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DateTime;
use App\ShowStatus;

class HomeController extends Controller
{
    public function index($modal = null)
    {
        // $data['modal'] = $modal;
        // $date_now = new DateTime();
        // $date_expiry = new DateTime("2021-12-01 06:00 am");
        // if ($date_now > $date_expiry) {
        //     $data['expired'] = true;
        // }
        
        // $show_status = ShowStatus::first();
        // $data['expired'] = $show_status->active == 1 ? false : true;
        // return redirect('/login');

        return view('site.home', []);

    }
}
