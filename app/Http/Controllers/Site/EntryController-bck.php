<?php

namespace App\Http\Controllers\Site;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Mail;
use App\Mail\MailRequestInvoice;
use Storage;
use DateTime;
use Carbon\Carbon;

use App\Http\Requests\SubmitEntry;
use \App\User;
use \App\Entry;
use \App\Credit;
use \App\File;
use \App\Category;
use \App\SubCategory;
use \App\Reference;
use App\ShowStatus;



class EntryController extends Controller
{
    public function dashboard()
    {
        // $date_now = new DateTime();
        // $date_expiry = new DateTime("2021-12-01 06:00 am");
        // $data['expired'] = false;
        // if ($date_now > $date_expiry) {
        //     $data['expired'] = true;
        // }

        $show_status = ShowStatus::first();
        $data['expired'] = $show_status->active == 1 ? false : true;
        $entries = Entry::where('user_id', auth()->user()->id)->orderBy('created_at', 'DESC')->get();

        return view('dashboard.main', ['entries' => $entries]);
    }

    public function submitEntry()
    {
        $data['edit']           = false;
        $data['categories']     = Category::orderBy('order', 'ASC')->get();
        $data['sub_categories'] = SubCategory::orderBy('name', 'ASC')->get();

        return view('dashboard.submit_entry', $data);
    }

    public function postSubmitEntry(Request $request)
    {
        $data = $request->all();

        if ($data['category'] == '1') {
            $custom_data = [
                // 'written_case_dropzone'          => 'required|mimes:jpg,jpeg',
                // 'asc_clearance_dropzone'         => 'required|mimes:jpg,jpeg,pdf',
                // 'media_certification_dropzone'   => 'required|mimes:jpg,jpeg,pdf',
                'client_certification_dropzone'  => 'required|mimes:jpg,jpeg,pdf',
            ];
        } else if ($data['category'] == '2' || $data['category'] == '3' || $data['category'] == '4' || ($data['category'] == '5' && ($data['sub_category'] == '50' || $data['sub_category'] == '51' || $data['sub_category'] == '55' || $data['sub_category'] == '56' || $data['sub_category'] == '58' || $data['sub_category'] == '59' || $data['sub_category'] == '60' || $data['sub_category'] == '61' || $data['sub_category'] == '62'))) {
            $custom_data = [
                // 'asc_clearance_dropzone'         => 'required|mimes:jpg,jpeg,pdf',
                // 'media_certification_dropzone'   => 'required|mimes:jpg,jpeg,pdf',
                'client_certification_dropzone'  => 'required|mimes:jpg,jpeg,pdf',
            ];
        } else if ($data['category'] == '5' && $data['sub_category'] == '1') {
            $custom_data = [
                // 'audio_dropzone'                 => 'required|mimes:mp3',
                // 'asc_clearance_dropzone'         => 'required|mimes:jpg,jpeg,pdf',
                // 'media_certification_dropzone'   => 'required|mimes:jpg,jpeg,pdf',
                // 'english_translation_dropzone'   => 'required|mimes:pdf',
            ];
        } else if ($data['category'] == '5' && ($data['sub_category'] == '35' || $data['sub_category'] == '36' || $data['sub_category'] == '37' || $data['sub_category'] == '38' || $data['sub_category'] == '39' || $data['sub_category'] == '40' || $data['sub_category'] == '41')) {
            $custom_data = [
                // 'asc_clearance_dropzone'         => 'required|mimes:jpg,jpeg,pdf',
                // 'media_certification_dropzone'   => 'required|mimes:jpg,jpeg,pdf',
                'client_certification_dropzone'  => 'required|mimes:jpg,jpeg,pdf',
            ];
        }  else if ($data['category'] == '5' && ($data['sub_category'] == '42' || $data['sub_category'] == '43' || $data['sub_category'] == '44' || $data['sub_category'] == '45' || $data['sub_category'] == '46' || $data['sub_category'] == '47' || $data['sub_category'] == '48' || $data['sub_category'] == '49')) {
            $custom_data = [
                'client_certification_dropzone'  => 'required|mimes:jpg,jpeg,pdf',
            ];
        }

        if ($data['type'] == "campaign") {
            $custom_data = [
                'entry_type_list' => 'required'
            ];
        } else {
            $data['entry_type_list'] = '';
            $custom_data = [];
        }

        $data_rules = [
            'category'         => 'required',
            'type'             => 'required',
            'entry_name'       => 'required',
            'ad_details'       => 'required',
            'advertiser'       => 'required',
            'agency'           => 'required',
            'date_publication' => 'required',            
            'online_links'     => 'required',
            'working_url'      => 'required',
            // 'campaign_summary' => 'required|max:301',
            // 'objectives'       => 'required|max:301',
            // 'strategy'         => 'required|max:301',
            // 'execution'        => 'required|max:301',
            // 'results'          => 'required|max:301',
            'agency_contact'   => 'required',
            'position'         => 'required',
            'phone_number'     => 'required',
            'email'            => 'required|email:rfc,dns',
        ];

        $data_rules = array_merge($data_rules, $custom_data);

        $validator = Validator::make($request->all(), $data_rules);

        if ($validator->fails()) {
            $errors = $validator->errors()->getMessages();

            return response()->json([
                'header' => "Oops!",
                'message'     => $errors,
                'status'      => 'error',
                'status_code' => 400
            ]);
        }

        $data['user_id'] = Auth::user()->id;
        $data['uid'] = $this->generateId();
        $data['date_publication'] = date('Y-m-d H:i:s', strtotime($data['date_publication']));


        if($request->hasFile('concept_board_dropzone')) {
            $path = $request->concept_board_dropzone->store('entries/'. $data['uid']);
            $data['concept_board_dropzone'] = $path;
        }

        if($request->hasFile('case_study_video_dropzone')) {
            $path = $request->case_study_video_dropzone->store('entries/'. $data['uid']);
            $data['case_study_video_dropzone'] = $path;
        }

        if($request->hasFile('written_case_dropzone')) {
            $path = $request->written_case_dropzone->store('entries/'. $data['uid']);
            $data['written_case_dropzone'] = $path;
        }

        if($request->hasFile('asc_clearance_dropzone')) {
            $path = $request->asc_clearance_dropzone->store('entries/'. $data['uid']);
            $data['asc_clearance_dropzone'] = $path;
        }

        if($request->hasFile('media_certification_dropzone')) {
            $path = $request->media_certification_dropzone->store('entries/'. $data['uid']);
            $data['media_certification_dropzone'] = $path;
        }

        if($request->hasFile('client_certification_dropzone')) {
            $path = $request->client_certification_dropzone->store('entries/'. $data['uid']);
            $data['client_certification_dropzone'] = $path;
        }

        if($request->hasFile('film_dropzone')) {
            $path = $request->film_dropzone->store('entries/'. $data['uid']);
            $data['film_dropzone'] = $path;
        }

        if($request->hasFile('posters_dropzone')) {
            $path = $request->posters_dropzone->store('entries/'. $data['uid']);
            $data['posters_dropzone'] = $path;
        }

        if($request->hasFile('audio_dropzone')) {
            $path = $request->audio_dropzone->store('entries/'. $data['uid']);
            $data['audio_dropzone'] = $path;
        }

        if($request->hasFile('demo_film_dropzone')) {
            $path = $request->demo_film_dropzone->store('entries/'. $data['uid']);
            $data['demo_film_dropzone'] = $path;
        }

        if($request->hasFile('english_translation_dropzone')) {
            $path = $request->english_translation_dropzone->store('entries/'. $data['uid']);
            $data['english_translation_dropzone'] = $path;
        }

        if($request->hasFile('photo_piece_dropzone')) {
            $path = $request->photo_piece_dropzone->store('entries/'. $data['uid']);
            $data['photo_piece_dropzone'] = $path;
        }

        if($request->hasFile('entry_dropzone')) {
            $path = $request->entry_dropzone->store('entries/'. $data['uid']);
            $data['entry_dropzone'] = $path;
        }

        // $vat_value = 0.12;
        if($request->member == "Yes") {
            $data['member'] = "Yes";
            if($request->type == "SINGLE") {
                // $data['fee'] = 5000 + (5000 * $vat_value);
                $data['fee'] = 5000;
            } else {
                $data['fee'] = 7000;
            }
        } else {
            $data['member'] = "No";
            if($request->type == "SINGLE") {
                $data['fee'] = 6500;
            } else {
                $data['fee'] = 8500;
            }
        }

        $data['created_at'] = date('Y-m-d H:i:s');
        $data['updated_at'] = date('Y-m-d H:i:s');
        
        $entry = Entry::create($data);

        foreach (json_decode($data['credits']) as $key => $val) {
            if($val->name != '' && $val->role != '') {
                $credit = Credit::create([
                    'name'     => $val->name, 
                    'role'     => $val->role,
                    'entry_id' => $entry->id
                ]);
            }
        }

        foreach (json_decode($data['online_links']) as $key => $val) {
            if($val->link != '') {
                $ref = Reference::create([
                    'link'     => $val->link,
                    'entry_id' => $entry->id
                ]);
            }
        }

        foreach (json_decode($data['working_url']) as $key => $val) {
            if($val->link != '') {
                $ref = Reference::create([
                    'link'     => $val->link,
                    'entry_id' => $entry->id
                ]);
            }
        }

        $data['entry'] = $entry;
        // return redirect('/dashboard/success/' . $entry->uid);

        return response()->json([
            'header'      => "Entry Submitted Successful!",
            'data'        => ['uid' => $entry->uid],
            'status'      => 'success',
            'status_code' => 200
        ]);
    }

    // autosave feature
    public function submitCategory(Request $request) {
        $data = $request->all();

        if ($data['type'] == "CAMPAIGN") {
            $custom_data = [
                // 'entry_type_list' => 'required'
            ];
        } else {
            $data['entry_type_list'] = '';
            $custom_data = [];
        }

        $data_rules = [
            'category'         => 'required',
            'type'             => 'required',
        ];

        $data_rules = array_merge($data_rules, $custom_data);

        $validator = Validator::make($request->all(), $data_rules);

        if ($validator->fails()) {
            $errors = $validator->errors()->getMessages();

            return response()->json([
                'header' => "Oops!",
                'message'     => $errors,
                'status'      => 'error',
                'status_code' => 400
            ]);
        }

        $entry = Entry::where('uid', $data['uid'])->get();

        $data['user_id'] = Auth::user()->id;
        if (count($entry) <= 0) {
            $data['uid'] = $this->generateId();
            $data['status'] = "Draft";
            $data['created_at'] = date('Y-m-d H:i:s');
            $entry = Entry::create($data);
        } else {
            $data['updated_at'] = date('Y-m-d H:i:s');
            $id = Auth::user()->id;
            $entry = Entry::where('user_id',$id)->where('uid', $data['uid'])->first();
            $entry->fill($data);
            $entry->save();
        }

        $data['entry'] = $entry;
        return response()->json([
            'header'      => "Entry category submitted successfully!",
            'data'        => ['uid' => $entry->uid],
            'status'      => 'success',
            'status_code' => 200
        ]);
    }

    public function submitCampaign(Request $request) {
        $data = $request->all();

        $id = Auth::user()->id;
        $entry = Entry::where('user_id',$id)->where('uid', $request->uid)->first();

        $data_rules = [
            'entry_name'       => 'required',
            'ad_details'       => 'required',
            'advertiser'       => 'required',
            'agency'           => 'required',
            'date_publication' => 'required',            
        ];

        $validator = Validator::make($request->all(), $data_rules);

        if ($validator->fails()) {
            $errors = $validator->errors()->getMessages();

            return response()->json([
                'header' => "Oops!",
                'message'     => $errors,
                'status'      => 'error',
                'status_code' => 400
            ]);
        }

        $data['date_publication'] = date('Y-m-d H:i:s', strtotime($data['date_publication']));

        $data['updated_at'] = date('Y-m-d H:i:s');

        $entry->fill($data);
        $entry->save();

        $data['entry'] = $entry;

        return response()->json([
            'header'      => "Entry campaign updated successfully!",
            'data'        => ['uid' => $entry->uid],
            'status'      => 'success',
            'status_code' => 200
        ]);
    }

    public function submitFile(Request $request) {
        $data = $request->all();
        $custom_data = array();
        $id = Auth::user()->id;
        $entry = Entry::where('user_id',$id)->where('uid', $request->uid)->first();
        if(empty($entry)) {
            return redirect('/dashboard');
        } else if ($data['category'] == '1') {
            $custom_data = [
               // 'client_certification_dropzone'  => 'nullable|mimes:jpg,jpeg,pdf',
            ];

        } else if ($data['category'] == '2' || $data['category'] == '3' || $data['category'] == '4' || ($data['category'] == '5' && ($data['sub_category'] == '50' || $data['sub_category'] == '51' || $data['sub_category'] == '55' || $data['sub_category'] == '56' || $data['sub_category'] == '58' || $data['sub_category'] == '59' || $data['sub_category'] == '60' || $data['sub_category'] == '61' || $data['sub_category'] == '62'))) {
            $custom_data = [
                //'client_certification_dropzone'  => 'nullable|mimes:jpg,jpeg,pdf',
            ];
        } else if ($data['category'] == '5' && $data['sub_category'] == '1') {
            $custom_data = [
            ];
        } else if ($data['category'] == '5' && ($data['sub_category'] == '35' || $data['sub_category'] == '36' || $data['sub_category'] == '37' || $data['sub_category'] == '38' || $data['sub_category'] == '39' || $data['sub_category'] == '40' || $data['sub_category'] == '41')) {
            $custom_data = [
               // 'client_certification_dropzone'  => 'nullable|mimes:jpg,jpeg,pdf',
            ];
        }  else if ($data['category'] == '5' && ($data['sub_category'] == '42' || $data['sub_category'] == '43' || $data['sub_category'] == '44' || $data['sub_category'] == '45' || $data['sub_category'] == '46' || $data['sub_category'] == '47' || $data['sub_category'] == '48' || $data['sub_category'] == '49')) {
            $custom_data = [
                //'client_certification_dropzone'  => 'nullable|mimes:jpg,jpeg,pdf',
            ];
        }

        $data_rules = [           
            // 'online_links'     => 'required',
            // 'working_url'      => 'required',
        ];

        $data_rules = array_merge($data_rules, $custom_data);

        $validator = Validator::make($request->all(), $data_rules);

        if ($validator->fails()) {
            $errors = $validator->errors()->getMessages();

            return response()->json([
                'header' => "Oops!",
                'message'     => $errors,
                'status'      => 'error',
                'status_code' => 400
            ]);
        }

        $data['user_id'] = Auth::user()->id;

        if($request->hasFile('concept_board_dropzone')) {
            if(file_exists(storage_path('app/public/'.$entry->concept_board_dropzone) && $entry->concept_board_dropzone != null)) {
                // unlink(storage_path('app/public/'.$entry->concept_board_dropzone));
            }

            $path = $request->concept_board_dropzone->store('entries/'. $data['uid']);
            $data['concept_board_dropzone'] = $path;
        } else {
            $data['concept_board_dropzone'] = $entry->concept_board_dropzone;
        }

        if($request->hasFile('case_study_video_dropzone')) {
            if(file_exists(storage_path('app/public/'.$entry->case_study_video_dropzone) && $entry->case_study_video_dropzone != null)) {
                // unlink(storage_path('app/public/'.$entry->case_study_video_dropzone));
            }

            $path = $request->case_study_video_dropzone->store('entries/'. $data['uid']);
            $data['case_study_video_dropzone'] = $path;
        } else {
            $data['case_study_video_dropzone'] = $entry->case_study_video_dropzone;
        }

        if($request->hasFile('written_case_dropzone')) {
            if(file_exists(storage_path('app/public/'.$entry->written_case_dropzone) && $entry->written_case_dropzone != null)) {
                // unlink(storage_path('app/public/'.$entry->written_case_dropzone));
            }

            $path = $request->written_case_dropzone->store('entries/'. $data['uid']);
            $data['written_case_dropzone'] = $path;
        } else {
            $data['written_case_dropzone'] = $entry->written_case_dropzone;
        }

        if($request->hasFile('asc_clearance_dropzone')) {
            if(file_exists(storage_path('app/public/'.$entry->asc_clearance_dropzone) && $entry->asc_clearance_dropzone != null)) {
                // unlink(storage_path('app/public/'.$entry->asc_clearance_dropzone));
            }

            $path = $request->asc_clearance_dropzone->store('entries/'. $data['uid']);
            $data['asc_clearance_dropzone'] = $path;
        } else {
            $data['asc_clearance_dropzone'] = $entry->asc_clearance_dropzone;
        }

        if($request->hasFile('media_certification_dropzone')) {
            if(file_exists(storage_path('app/public/'.$entry->media_certification_dropzone) && $entry->media_certification_dropzone != null)) {
                // unlink(storage_path('app/public/'.$entry->media_certification_dropzone));
            }

            $path = $request->media_certification_dropzone->store('entries/'. $data['uid']);
            $data['media_certification_dropzone'] = $path;
        } else {
            $data['media_certification_dropzone'] = $entry->media_certification_dropzone;
        }

        if($request->hasFile('client_certification_dropzone')) {
            if(file_exists(storage_path('app/public/'.$entry->client_certification_dropzone) && $entry->client_certification_dropzone != null)) {
                unlink(storage_path('app/public/'.$entry->client_certification_dropzone));
            }

            $path = $request->client_certification_dropzone->store('entries/'. $data['uid']);
            $data['client_certification_dropzone'] = $path;
        } else {
            $data['client_certification_dropzone'] = $entry->client_certification_dropzone;
        }

        if($request->hasFile('film_dropzone')) {
            if(file_exists(storage_path('app/public/'.$entry->film_dropzone) && $entry->film_dropzone != null)) {
                unlink(storage_path('app/public/'.$entry->film_dropzone));
            }

            $path = $request->film_dropzone->store('entries/'. $data['uid']);
            $data['film_dropzone'] = $path;
        } else {
            $data['film_dropzone'] = $entry->film_dropzone;
        }

        if($request->hasFile('posters_dropzone')) {
            if(file_exists(storage_path('app/public/'.$entry->posters_dropzone) && $entry->posters_dropzone != null)) {
                // unlink(storage_path('app/public/'.$entry->posters_dropzone));
            }

            $path = $request->posters_dropzone->store('entries/'. $data['uid']);
            $data['posters_dropzone'] = $path;
        } else {
            $data['posters_dropzone'] = $entry->posters_dropzone;
        }

        if($request->hasFile('audio_dropzone')) {
            if(file_exists(storage_path('app/public/'.$entry->audio_dropzone) && $entry->audio_dropzone != null)) {
                // unlink(storage_path('app/public/'.$entry->audio_dropzone));
            }

            $path = $request->audio_dropzone->store('entries/'. $data['uid']);
            $data['audio_dropzone'] = $path;
        } else {
            $data['audio_dropzone'] = $entry->audio_dropzone;
        }

        if($request->hasFile('demo_film_dropzone')) {
            if(file_exists(storage_path('app/public/'.$entry->demo_film_dropzone) && $entry->demo_film_dropzone != null)) {
                // unlink(storage_path('app/public/'.$entry->demo_film_dropzone));
            }

            $path = $request->demo_film_dropzone->store('entries/'. $data['uid']);
            $data['demo_film_dropzone'] = $path;
        } else {
            $data['demo_film_dropzone'] = $entry->demo_film_dropzone;
        }

        if($request->hasFile('english_translation_dropzone')) {
            if(file_exists(storage_path('app/public/'.$entry->english_translation_dropzone) && $entry->english_translation_dropzone != null)) {
                // unlink(storage_path('app/public/'.$entry->english_translation_dropzone));
            }

            $path = $request->english_translation_dropzone->store('entries/'. $data['uid']);
            $data['english_translation_dropzone'] = $path;
        } else {
            $data['english_translation_dropzone'] = $entry->english_translation_dropzone;
        }

        if($request->hasFile('photo_piece_dropzone')) {
            if(file_exists(storage_path('app/public/'.$entry->photo_piece_dropzone) && $entry->photo_piece_dropzone != null)) {
                // unlink(storage_path('app/public/'.$entry->photo_piece_dropzone));
            }

            $path = $request->photo_piece_dropzone->store('entries/'. $data['uid']);
            $data['photo_piece_dropzone'] = $path;
        } else {
            $data['photo_piece_dropzone'] = $entry->photo_piece_dropzone;
        }

        if($request->hasFile('entry_dropzone')) {
            if(file_exists(storage_path('app/public/'.$entry->entry_dropzone) && $entry->entry_dropzone != null)) {
                // unlink(storage_path('app/public/'.$entry->entry_dropzone));
            }

            $path = $request->entry_dropzone->store('entries/'. $data['uid']);
            $data['entry_dropzone'] = $path;
        } else {
            $data['entry_dropzone'] = $entry->entry_dropzone;
        }


        $data['updated_at'] = date('Y-m-d H:i:s');
        $entry->fill($data);
        $entry->save();

        Reference::where('entry_id', '=', $entry->id)->delete();
        foreach (json_decode($data['online_links']) as $key => $val) {
            if($val->link != '') {
                $ref = Reference::create([
                    'link'     => $val->link,
                    'entry_id' => $entry->id
                ]);
            }
        }

        // foreach (json_decode($data['working_url']) as $key => $val) {
        //     if($val->link != '') {
        //         $ref = Reference::create([
        //             'link'     => $val->link,
        //             'entry_id' => $entry->id
        //         ]);
        //     }
        // }


        $data['entry'] = $entry;

        return response()->json([
            'header'      => "Entry upload updated successfully!",
            'data'        => ['uid' => $entry->uid],
            'status'      => 'success',
            'status_code' => 200
        ]);
    }

    public function submitWritten(Request $request) {
        $data = $request->all();

        $id = Auth::user()->id;
        $entry = Entry::where('user_id',$id)->where('uid', $request->uid)->first();

        $data_rules = [
                
        ];

        $validator = Validator::make($request->all(), $data_rules);

        if ($validator->fails()) {
            $errors = $validator->errors()->getMessages();

            return response()->json([
                'header' => "Oops!",
                'message'     => $errors,
                'status'      => 'error',
                'status_code' => 400
            ]);
        }

        $data['updated_at'] = date('Y-m-d H:i:s');
        $entry->fill($data);
        $entry->save();

        $data['entry'] = $entry;

        return response()->json([
            'header'      => "Entry written submission updated successfully!",
            'data'        => ['uid' => $entry->uid],
            'status'      => 'success',
            'status_code' => 200
        ]);
    }

    public function submitAgency(Request $request) {
        $data = $request->all();

        $id = Auth::user()->id;
        $entry = Entry::where('user_id',$id)->where('uid', $request->uid)->first();

        $data_rules = [
            'agency_contact'   => 'required',
            'position'         => 'required',
            'phone_number'     => 'required',
            'email'            => 'required|email:rfc,dns',
        ];


        $validator = Validator::make($request->all(), $data_rules);

        if ($validator->fails()) {
            $errors = $validator->errors()->getMessages();

            return response()->json([
                'header' => "Oops!",
                'message'     => $errors,
                'status'      => 'error',
                'status_code' => 400
            ]);
        }

        if($request->member == "Yes") {
            $data['member'] = "Yes";
            if($request->type == "SINGLE") {
                // $data['fee'] = 5000 + (5000 * $vat_value);
                $data['fee'] = 5000;
            } else {
                $data['fee'] = 7000;
            }
        } else {
            $data['member'] = "No";
            if($request->type == "SINGLE") {
                $data['fee'] = 6500;
            } else {
                $data['fee'] = 8500;
            }
        }

        $data['updated_at'] = date('Y-m-d H:i:s');
        $entry->fill($data);
        $entry->save();

        $data['entry'] = $entry;

        Credit::where('entry_id',$entry->id)->delete();
        foreach (json_decode($data['credits']) as $key => $val) {
            if($val->name != '' && $val->role != '') {
                $credit = Credit::create([
                    'name'     => $val->name, 
                    'role'     => $val->role,
                    'entry_id' => $entry->id
                ]);
            }
        }

        return response()->json([
            'header'      => "Entry agency updated successfully!",
            'data'        => ['uid' => $entry->uid],
            'status'      => 'success',
            'status_code' => 200
        ]);
    }

    public function submitReview(Request $request) {
        $data = $request->all();

        $id = Auth::user()->id;
        $entry = Entry::where('user_id',$id)->where('uid', $request->uid)->first();
        $data['status'] = $request->status;
        $data['updated_at'] = date('Y-m-d H:i:s');
        $entry->fill($data);
        $entry->save();

        $data['entry'] = $entry;

        return response()->json([
            'header'      => "Entry agency updated successfully!",
            'data'        => ['uid' => $entry->uid],
            'status'      => 'success',
            'status_code' => 200
        ]);
    }
    // end autosave feature

    public function entryFileValidator()
    {
       
    }

    public function emptyFileChecker()
    {
    }

    public function submit(Request $request)
    {
        # code...
        $data = $request->all();

        $data['user_id'] = Auth::user()->id;
        $data['uid'] = $this->generateId();

        if(!empty ( $request->media )) {
            $path = $request->media->store('entries/'. $data['uid']);
            $data['media'] = $path;
        }

        if(!empty ( $request->client_cert )) {
            $path = $request->client_cert->store('entries/'. $data['uid']);
            $data['client_cert'] = $path;
        }

        if(!empty ( $request->asc_cert )) {
            $path = $request->asc_cert->store('entries/'. $data['uid']);
            $data['asc_cert'] = $path;
        }

        $vat_value = 0.12;
        if($request->member == "Yes") {
            $data['member'] = "Yes";
            if($request->type == "SINGLE") {
                // $data['fee'] = 5000 + (5000 * $vat_value);
                $data['fee'] = 5000;
            } else {
                $data['fee'] = 7000;
            }
        } else {
            $data['member'] = "No";
            if($request->type == "SINGLE") {
                $data['fee'] = 6500;
            } else {
                $data['fee'] = 8500;
            }
        }

        $entry = Entry::create($data);
        foreach ($data['credit_name'] as $key=>$c) {
            if($data['credit_name'][$key] != '') {
                $credit = Credit::create([
                    'name' => $data['credit_name'][$key], 
                    'role' => $data['credit_role'][$key],
                    'entry_id' => $entry->id
                ]);
            }
        }

        foreach ($data['reference'] as $key=>$r) {
            if($data['reference'][$key] != '') {
                $ref = Reference::create([
                    'link' => $data['reference'][$key],
                    'entry_id' => $entry->id
                ]);
            }
        }

        if(!empty ( $data['files'] )) {
            foreach ($data['files'] as $key=>$file) {
                if(!empty ( $file )) {
                    $path = $file->store('entries/'. $data['uid']);
                    $f = FILE::create([
                        'entry_id' => $entry->id,
                        'name' => $data['files_name'][$key],
                        'path' => $path, 
                        'type' => $data['file_type'][$key]
                    ]);
                }
            }
        }
        $data['entry'] = $entry;

        return redirect('/dashboard/success/' . $entry->uid);
    }

    public function myInvoice(Request $request)
    {
        $id       = Auth::user()->id;
        $entries  = Entry::where('user_id', $id)
                         ->join('category', 'entries.category', '=', 'category.id')
                         ->select('entries.id as entry_id', 'entries.uid', 'entries.entry_name','category.name', 'entries.fee', 'entries.payment_status')
                         ->orderBy('created_at', 'DESC')
                         ->get();

        return view('dashboard.myinvoice', ['entries' => $entries]);
    }

    public function postRequestInvoice(Request $request)
    {
        $entries = Entry::whereIn('uid', $request->ids)->get();
        $user = User::where('id', $entries[0]->user_id)->first();
        $data['entries'] = $entries;
        $data['total'] = 0;
        $data['single'] = 0;
        $data['campaign'] = 0;
        foreach ($entries as $e) {
            $e->payment_status = "Requested";
            $e->save();
            if($e->type == "SINGLE")
            {
             $data['single'] += 1;
            } else {
                $data['campaign'] += 1;
            }
            $data['total'] += $e->fee + ($e->fee * 0.12);
        }

        $details = [
            'sender'  => config('mail.from.address'),
            'name'    => $user->name, 
            'email'   => $user->email,
            // 'cc'      => ['4asp@pldtdsl.net', 'kidlatawards2019@gmail.com'],
            // 'bcc'     => 'liza.4asp@yahoo.com',
            'subject' => 'KIDLAT AWARDS '.date('Y').' '.$user->name.' , here are your payment instructions for ' . $entries->count() . ' entry(ies)',
            'data'      => $data,
            'view'    => 'email.invoice'
        ];

        Mail::to($user->email)->send(new MailRequestInvoice($details));


        // return Response::json(["status"=> true,"name" => Auth::user()->name]);
        return response()->json([
            'header'      => 'Invoice request sent!',
            'message'     => ['email' => 'Please check your email.'],
            'status'      => 'success',
            'status_code' => 200
        ]);
    }

    public function invoiceEmailSent()
    {   
        return view("dashboard.invoice_email_sent", []);
    } 

    public function myEntries()
    {
        $id = Auth::user()->id;
        $entries = Entry::where('user_id', $id)->orderBy('created_at', 'DESC')->get();
        return view('dashboard.my_entries', ['entries' => $entries]);
    }

    public function view($uid)
    {
        $id = Auth::user()->id;
        $data['entry']      = Entry::where('user_id',$id)->where('uid', $uid)->first();
        $data['references'] = Reference::where('entry_id', $data['entry']->id)->get();

        if(!empty($data['entry'])) {
            return view('dashboard.view', $data);
        } else {
            return redirect('/dashboard');
        }
    }

    public function delete(Request $request)
    {
        $entry = Entry::where('user_id', auth()->user()->id)->where('uid', $request->uid)->first();

        if(empty($entry)) {
            return redirect('/my-entries');
        } else {
            $entry->delete();
            $credits   = Credit::where('entry_id', $entry->id)->delete();
            $reference = Reference::where('entry_id', $entry->id)->delete();
            
            return response()->json([
                'header' => "Entry deleted successfully!",
                'message' => ['id' => $request->id],
                'status'      => 'success',
                'status_code' => 200
            ]);
        }
    }

    public function clone(Request $request)
    {

        $og_entry      = Entry::where('uid', $request->uid)->first();
        $og_credits    = Credit::where('entry_id', $og_entry->id)->get();
        $og_references = Reference::where('entry_id', $og_entry->id)->get();

        $generated_id  = $this->generateId();

        // $concept_board          = str_replace($og_entry->uid,$generated_id,$og_entry->concept_board_dropzone);
        // $case_study_video       = str_replace($og_entry->uid,$generated_id,$og_entry->case_study_video_dropzone);
        // $written_case           = str_replace($og_entry->uid,$generated_id,$og_entry->written_case_dropzone);
        // $asc_clearance          = str_replace($og_entry->uid,$generated_id,$og_entry->asc_clearance_dropzone);
        // $media_certification    = str_replace($og_entry->uid,$generated_id,$og_entry->media_certification_dropzone);
        // $client_certification   = str_replace($og_entry->uid,$generated_id,$og_entry->client_certification_dropzone);
        // $film                   = str_replace($og_entry->uid,$generated_id,$og_entry->film_dropzone);
        // $posters                = str_replace($og_entry->uid,$generated_id,$og_entry->posters_dropzone);
        // $audio                  = str_replace($og_entry->uid,$generated_id,$og_entry->audio_dropzone);
        // $demo_film              = str_replace($og_entry->uid,$generated_id,$og_entry->demo_film_dropzone);
        // $english_translation    = str_replace($og_entry->uid,$generated_id,$og_entry->english_translation_dropzone);
        // $photo_piece            = str_replace($og_entry->uid,$generated_id,$og_entry->photo_piece_dropzone);
        // $entry_photo            = str_replace($og_entry->uid,$generated_id,$og_entry->entry_dropzone);

        $entry                                  = new Entry;
        $entry->user_id                         = $og_entry->user_id;
        $entry->uid                             = $generated_id;
        $entry->category                        = $og_entry->category;
        $entry->sub_category                    = $og_entry->sub_category;
        $entry->type                            = $og_entry->type;
        $entry->entry_type_list                 = $og_entry->entry_type_list;
        $entry->entry_name                      = $og_entry->entry_name." - Copy";
        $entry->ad_details                      = $og_entry->ad_details;
        $entry->advertiser                      = $og_entry->advertiser;
        $entry->brand                           = $og_entry->brand;
        $entry->agency                          = $og_entry->agency;
        $entry->date_publication                = $og_entry->date_publication;
        $entry->campaign_summary                = $og_entry->campaign_summary;
        $entry->objectives                      = $og_entry->objectives;
        $entry->strategy                        = $og_entry->strategy;
        $entry->execution                       = $og_entry->execution;
        $entry->results                         = $og_entry->results;


        $entry->concept_board_dropzone          = $og_entry->concept_board_dropzone;
        $entry->case_study_video_dropzone       = $og_entry->case_study_video_dropzone;
        $entry->written_case_dropzone           = $og_entry->written_case_dropzone;
        $entry->asc_clearance_dropzone          = $og_entry->asc_clearance_dropzone;
        $entry->media_certification_dropzone    = $og_entry->media_certification_dropzone;
        $entry->client_certification_dropzone   = $og_entry->client_certification_dropzone;
        $entry->film_dropzone                   = $og_entry->film_dropzone;
        $entry->posters_dropzone                = $og_entry->posters_dropzone;
        $entry->audio_dropzone                  = $og_entry->audio_dropzone;
        $entry->demo_film_dropzone              = $og_entry->demo_film_dropzone;
        $entry->english_translation_dropzone    = $og_entry->english_translation_dropzone;
        $entry->photo_piece_dropzone            = $og_entry->photo_piece_dropzone;
        $entry->entry_dropzone                  = $og_entry->entry_dropzone;

        $entry->member                          = $og_entry->member;
        $entry->agency_contact                  = $og_entry->agency_contact;
        $entry->position                        = $og_entry->position;
        $entry->phone_number                    = $og_entry->phone_number;
        $entry->email                           = $og_entry->email;
        $entry->payment_status                  = "Unpaid";
        $entry->status                          = "Draft";
        $entry->fee                             = $og_entry->fee;
        $entry->save();

        foreach ($og_credits as $credit) {
            $credits = new Credit;
            $credits->entry_id = $entry->id;
            $credits->name = $credit->name;
            $credits->role = $credit->role;
            $credits->save();
        }

        foreach ($og_references as $reference) {
            $references = new Reference;
            $references->entry_id = $entry->id;
            $references->link = $reference->link;
            $references->save();
        }

        return response()->json([
            'header'      => "Entry clone successfully!",
            'message'     => ['uid' => $generated_id],
            'status'      => 'success',
            'status_code' => 200
        ]);

    }    

    public function edit(Request $request)
    {
        $data = $request->all();

        $id = Auth::user()->id;
        $entry = Entry::where('user_id',$id)->where('uid', $request->uid)->first();
        if(empty($entry)) {
            return redirect('/dashboard');
        } else if ($data['category'] == '1') {
            $custom_data = [
                // 'written_case_dropzone'          => 'nullable|mimes:jpg,jpeg',
                // 'asc_clearance_dropzone'         => 'nullable|mimes:jpg,jpeg,pdf',
                // 'media_certification_dropzone'   => 'nullable|mimes:jpg,jpeg,pdf',
                'client_certification_dropzone'  => 'nullable|mimes:jpg,jpeg,pdf',
            ];

        } else if ($data['category'] == '2' || $data['category'] == '3' || $data['category'] == '4' || ($data['category'] == '5' && ($data['sub_category'] == '50' || $data['sub_category'] == '51' || $data['sub_category'] == '55' || $data['sub_category'] == '56' || $data['sub_category'] == '58' || $data['sub_category'] == '59' || $data['sub_category'] == '60' || $data['sub_category'] == '61' || $data['sub_category'] == '62'))) {
            $custom_data = [
                // 'asc_clearance_dropzone'         => 'nullable|mimes:jpg,jpeg,pdf',
                // 'media_certification_dropzone'   => 'nullable|mimes:jpg,jpeg,pdf',
                'client_certification_dropzone'  => 'nullable|mimes:jpg,jpeg,pdf',
            ];
        } else if ($data['category'] == '5' && $data['sub_category'] == '1') {
            $custom_data = [
                // 'audio_dropzone'                 => 'nullable|mimes:mp3',
                // 'asc_clearance_dropzone'         => 'nullable|mimes:jpg,jpeg,pdf',
                // 'media_certification_dropzone'   => 'nullable|mimes:jpg,jpeg,pdf',
                // 'english_translation_dropzone'   => 'nullable|mimes:pdf',
            ];
        } else if ($data['category'] == '5' && ($data['sub_category'] == '35' || $data['sub_category'] == '36' || $data['sub_category'] == '37' || $data['sub_category'] == '38' || $data['sub_category'] == '39' || $data['sub_category'] == '40' || $data['sub_category'] == '41')) {
            $custom_data = [
                // 'asc_clearance_dropzone'         => 'nullable|mimes:jpg,jpeg,pdf',
                // 'media_certification_dropzone'   => 'nullable|mimes:jpg,jpeg,pdf',
                'client_certification_dropzone'  => 'nullable|mimes:jpg,jpeg,pdf',
            ];
        }  else if ($data['category'] == '5' && ($data['sub_category'] == '42' || $data['sub_category'] == '43' || $data['sub_category'] == '44' || $data['sub_category'] == '45' || $data['sub_category'] == '46' || $data['sub_category'] == '47' || $data['sub_category'] == '48' || $data['sub_category'] == '49')) {
            $custom_data = [
                'client_certification_dropzone'  => 'nullable|mimes:jpg,jpeg,pdf',
            ];
        }

        if ($data['type'] == "CAMPAIGN") {
            $custom_data = [
                'entry_type_list' => 'required'
            ];
        } else {
            $data['entry_type_list'] = '';
            $custom_data = [];
        }

        $data_rules = [
            'category'         => 'required',
            'type'             => 'required',
            'entry_name'       => 'required',
            'ad_details'       => 'required',
            'advertiser'       => 'required',
            'agency'           => 'required',
            'date_publication' => 'required',            
            'online_links'     => 'required',
            'working_url'      => 'required',
            'agency_contact'   => 'required',
            'position'         => 'required',
            'phone_number'     => 'required',
            'email'            => 'required|email:rfc,dns',
        ];

        $data_rules = array_merge($data_rules, $custom_data);

        $validator = Validator::make($request->all(), $data_rules);

        if ($validator->fails()) {
            $errors = $validator->errors()->getMessages();

            return response()->json([
                'header' => "Oops!",
                'message'     => $errors,
                'status'      => 'error',
                'status_code' => 400
            ]);
        }

        $data['user_id'] = Auth::user()->id;
        $data['date_publication'] = date('Y-m-d H:i:s', strtotime($data['date_publication']));

        if($request->hasFile('concept_board_dropzone')) {
            if(file_exists(storage_path('app/public/'.$entry->concept_board_dropzone) && $entry->concept_board_dropzone != null)) {
                unlink(storage_path('app/public/'.$entry->concept_board_dropzone));
            }

            $path = $request->concept_board_dropzone->store('entries/'. $data['uid']);
            $data['concept_board_dropzone'] = $path;
        } else {
            $data['concept_board_dropzone'] = $entry->concept_board_dropzone;
        }

        if($request->hasFile('case_study_video_dropzone')) {
            if(file_exists(storage_path('app/public/'.$entry->case_study_video_dropzone) && $entry->case_study_video_dropzone != null)) {
                unlink(storage_path('app/public/'.$entry->case_study_video_dropzone));
            }

            $path = $request->case_study_video_dropzone->store('entries/'. $data['uid']);
            $data['case_study_video_dropzone'] = $path;
        } else {
            $data['case_study_video_dropzone'] = $entry->case_study_video_dropzone;
        }

        if($request->hasFile('written_case_dropzone')) {
            if(file_exists(storage_path('app/public/'.$entry->written_case_dropzone) && $entry->written_case_dropzone != null)) {
                unlink(storage_path('app/public/'.$entry->written_case_dropzone));
            }

            $path = $request->written_case_dropzone->store('entries/'. $data['uid']);
            $data['written_case_dropzone'] = $path;
        } else {
            $data['written_case_dropzone'] = $entry->written_case_dropzone;
        }

        if($request->hasFile('asc_clearance_dropzone')) {
            if(file_exists(storage_path('app/public/'.$entry->asc_clearance_dropzone) && $entry->asc_clearance_dropzone != null)) {
                unlink(storage_path('app/public/'.$entry->asc_clearance_dropzone));
            }

            $path = $request->asc_clearance_dropzone->store('entries/'. $data['uid']);
            $data['asc_clearance_dropzone'] = $path;
        } else {
            $data['asc_clearance_dropzone'] = $entry->asc_clearance_dropzone;
        }

        if($request->hasFile('media_certification_dropzone')) {
            if(file_exists(storage_path('app/public/'.$entry->media_certification_dropzone) && $entry->media_certification_dropzone != null)) {
                unlink(storage_path('app/public/'.$entry->media_certification_dropzone));
            }

            $path = $request->media_certification_dropzone->store('entries/'. $data['uid']);
            $data['media_certification_dropzone'] = $path;
        } else {
            $data['media_certification_dropzone'] = $entry->media_certification_dropzone;
        }

        if($request->hasFile('client_certification_dropzone')) {
            if(file_exists(storage_path('app/public/'.$entry->client_certification_dropzone) && $entry->client_certification_dropzone != null)) {
                unlink(storage_path('app/public/'.$entry->client_certification_dropzone));
            }

            $path = $request->client_certification_dropzone->store('entries/'. $data['uid']);
            $data['client_certification_dropzone'] = $path;
        } else {
            $data['client_certification_dropzone'] = $entry->client_certification_dropzone;
        }

        if($request->hasFile('film_dropzone')) {
            if(file_exists(storage_path('app/public/'.$entry->film_dropzone) && $entry->film_dropzone != null)) {
                unlink(storage_path('app/public/'.$entry->film_dropzone));
            }

            $path = $request->film_dropzone->store('entries/'. $data['uid']);
            $data['film_dropzone'] = $path;
        } else {
            $data['film_dropzone'] = $entry->film_dropzone;
        }

        if($request->hasFile('posters_dropzone')) {
            if(file_exists(storage_path('app/public/'.$entry->posters_dropzone) && $entry->posters_dropzone != null)) {
                unlink(storage_path('app/public/'.$entry->posters_dropzone));
            }

            $path = $request->posters_dropzone->store('entries/'. $data['uid']);
            $data['posters_dropzone'] = $path;
        } else {
            $data['posters_dropzone'] = $entry->posters_dropzone;
        }

        if($request->hasFile('audio_dropzone')) {
            if(file_exists(storage_path('app/public/'.$entry->audio_dropzone) && $entry->audio_dropzone != null)) {
                unlink(storage_path('app/public/'.$entry->audio_dropzone));
            }

            $path = $request->audio_dropzone->store('entries/'. $data['uid']);
            $data['audio_dropzone'] = $path;
        } else {
            $data['audio_dropzone'] = $entry->audio_dropzone;
        }

        if($request->hasFile('demo_film_dropzone')) {
            if(file_exists(storage_path('app/public/'.$entry->demo_film_dropzone) && $entry->demo_film_dropzone != null)) {
                unlink(storage_path('app/public/'.$entry->demo_film_dropzone));
            }

            $path = $request->demo_film_dropzone->store('entries/'. $data['uid']);
            $data['demo_film_dropzone'] = $path;
        } else {
            $data['demo_film_dropzone'] = $entry->demo_film_dropzone;
        }

        if($request->hasFile('english_translation_dropzone')) {
            if(file_exists(storage_path('app/public/'.$entry->english_translation_dropzone) && $entry->english_translation_dropzone != null)) {
                unlink(storage_path('app/public/'.$entry->english_translation_dropzone));
            }

            $path = $request->english_translation_dropzone->store('entries/'. $data['uid']);
            $data['english_translation_dropzone'] = $path;
        } else {
            $data['english_translation_dropzone'] = $entry->english_translation_dropzone;
        }

        if($request->hasFile('photo_piece_dropzone')) {
            if(file_exists(storage_path('app/public/'.$entry->photo_piece_dropzone) && $entry->photo_piece_dropzone != null)) {
                unlink(storage_path('app/public/'.$entry->photo_piece_dropzone));
            }

            $path = $request->photo_piece_dropzone->store('entries/'. $data['uid']);
            $data['photo_piece_dropzone'] = $path;
        } else {
            $data['photo_piece_dropzone'] = $entry->photo_piece_dropzone;
        }

        if($request->hasFile('entry_dropzone')) {
            if(file_exists(storage_path('app/public/'.$entry->entry_dropzone) && $entry->entry_dropzone != null)) {
                unlink(storage_path('app/public/'.$entry->entry_dropzone));
            }

            $path = $request->entry_dropzone->store('entries/'. $data['uid']);
            $data['entry_dropzone'] = $path;
        } else {
            $data['entry_dropzone'] = $entry->entry_dropzone;
        }


        if($request->member == "Yes") {
            $data['member'] = "Yes";
            if($request->type == "SINGLE") {
                $data['fee'] = 7000;
            } else {
                $data['fee'] = 9000;
            }
        } else {
            $data['member'] = "No";
            if($request->type == "SINGLE") {
                $data['fee'] = 8000;
            } else {
                $data['fee'] = 10000;
            }
        }

        $data['updated_at'] = date('Y-m-d H:i:s');
        $entry->fill($data);
        $entry->save();

        Credit::where('entry_id',$entry->id)->delete();
        foreach (json_decode($data['credits']) as $key => $val) {
            if($val->name != '' && $val->role != '') {
                $credit = Credit::create([
                    'name'     => $val->name, 
                    'role'     => $val->role,
                    'entry_id' => $entry->id
                ]);
            }
        }

        Reference::where('entry_id',$entry->id)->delete();
        foreach (json_decode($data['online_links']) as $key => $val) {
            if($val->link != '') {
                $ref = Reference::create([
                    'link'     => $val->link,
                    'entry_id' => $entry->id
                ]);
            }
        }

        foreach (json_decode($data['working_url']) as $key => $val) {
            if($val->link != '') {
                $ref = Reference::create([
                    'link'     => $val->link,
                    'entry_id' => $entry->id
                ]);
            }
        }


        $data['entry'] = $entry;

        return response()->json([
            'header'      => "Entry Updated Successful!",
            'data'        => ['uid' => $entry->uid],
            'status'      => 'success',
            'status_code' => 200
        ]);
    }

    public function thankyou($uid)
    {
        $id             = Auth::user()->id;
        $entry          = Entry::where('user_id',$id)->where('uid', $uid)->first();

        if(!empty($entry)) {
            $category      = Category::where('id', $entry->category)->first();
            $sub_category   = SubCategory::where('id', $entry->sub_category)->first();

            if (!empty($sub_category)) {
                $data = [
                    'category' => $category->name." - ".$sub_category->name
                ];
            } else {
                $data = [
                    'category' => $category->name
                ];
            }

            return view('dashboard.thankyou', $data);
        } else {
            return redirect('/dashboard');
        }
    }

    public function editEntry($uid = null)
    {
        if (!empty($uid)) {
            $id = Auth::user()->id;
            $data['entry']   = Entry::where('user_id',$id)->where('uid', $uid)->first();
            if (empty($data['entry'])) {
                return redirect('/dashboard');
            }
        }

        $data['edit']           = true;
        $data['categories']     = Category::orderBy('order','asc')->get();
        $data['sub_categories'] = SubCategory::orderBy('order','asc')->get();
        return view('dashboard.edit_entry',$data);
    }

    public function getPreview(Request $request)
    {
        $entry   = Entry::where('user_id', auth()->user()->id)->where('uid', $request->uid)->first();
        $reference = Reference::where('entry_id', $entry->id)->get();
        return response()->json($reference);
    }

    public function getCredits(Request $request)
    {
        $entry   = Entry::where('user_id', auth()->user()->id)->where('uid', $request->uid)->first();
        $credits = Credit::where('entry_id', $entry->id)->get();
        return response()->json($credits);
    }

    public function minutesPassed(Request $request)
    {
        $entry   = Entry::where('user_id', auth()->user()->id)->where('uid', $request->uid)->first();
        $data['minutes_passed'] = $entry->updated_at->diffForHumans();

        return response()->json($data);
    }

    public function form($uid = null, $edit = false)
    {
        $data['edit'] = $edit;
        if (!empty($uid)) {
            $id = Auth::user()->id;
            $data['entry'] = Entry::where('user_id',$id)->where('uid', $uid)->first();
        }
        if($edit == 'edit') {
            if($data['entry']->payment_status != "Unpaid") {
                $data['entry'] = null;
            } else {
                $data['edit'] = true;
            }
        }
        $data['categories'] = Category::orderBy('order','asc')->get();
        return view('dashboard.form',$data);
    }



    public function export(Request $request)
    {
        $headers = array(
            "Content-type" => "text/csv",
            "Content-Disposition" => "attachment; filename=export_".date('m-d-Y_hia').".csv",
            "Pragma" => "no-cache",
            "Cache-Control" => "must-revalidate, post-check=0, pre-check=0",
            "Expires" => "0"
        );

        if($request->filter == "All") {
            $entries = Entry::orderBy('created_at','desc')->get();
        } else {
            $entries = Entry::orderBy('created_at','desc')->where('status',$request->filter)->get();
        }
        $columns = array('ID', 'ENTRY NAME', 'AD TITLE/S', 'ADVERTISER', 'BRAND', 'AGENCY', 'DATE OF FIRST PUBLICATION', 'ENTRY TYPE', 'CATEGORY', 'SUB CATEGORY', 'AGENCY CONTACT', 'POSITION', 'PHONE NUMBER', 'EMAIL ADDRESS', 'PAYMENT STATUS', 'DATE SUBMITTED', 'FEE', '4AS MEMBER?',  'CLIENT CERTIFICATION', 'MEDIA CERTIFICATION', 'ASC CERTIFICATION', 'ENTRY STATUS');

        $callback = function() use ($entries, $columns)
        {
            $file = fopen('php://output', 'w');
            fputcsv($file, $columns);

            foreach($entries as $entry) {
                $cert = "N/A";
                $media = "N/A";
                $asc = "N/A";
                if($entry->client_cert != null) {
                    $cert = "Yes";
                }
                if($entry->media != null) {
                    $media = "Yes";
                }
                if($entry->asc != null) {
                    $asc = "Yes";
                }
                fputcsv($file, array(
                    $entry->uid,
                    $entry->entry_name,
                    $entry->ad_details,
                    $entry->advertiser,
                    $entry->brand,
                    $entry->agency,
                    $entry->date_publication,
                    $entry->type,
                    $entry->category()->name,
                    $entry->sub_category()->name,
                    $entry->agency_contact,
                    $entry->position,
                    $entry->phone_number,
                    $entry->email,
                    $entry->payment_status,
                    $entry->created_at,
                    $entry->fee,
                    $entry->member,
                    $cert,
                    $media,
                    $asc,
                    $entry->status
                ));
            }
            fclose($file);
        };
        return Response::stream($callback, 200, $headers);
    }

    public function downloadFile($id)
    {
        # code...
        $user_id = Auth::user()->id;
        $file = File::where("id",$id)->first();
        if($file->entry()->user_id == $user_id) {
            if(!empty ( $file )) {
                return response()->file(storage_path('app/' . $file->path), [
                    'Content-Disposition' => 'inline; filename="'. $file->name .'"'
                ]);
            } else {
                abort(404);
            }
        } else {
            abort(404);
        }
    }

    public function certs($uid,$field)
    {
        # code...
        $user_id = Auth::user()->id;
        $entry = Entry::where('user_id',$user_id)->where('uid', $uid)->first();
        return response()->file(storage_path('app/' . $entry[$field]));
    }

    protected function generateId() {
        $number = mt_rand(10000, 99999); 

        // call the same function if the id exists already
        if ($this->entryIdExists($number)) {
            return generateUID();
        }

        // otherwise, it's valid and can be used
        return $number;
    }

    protected function entryIdExists($number) {
        return Entry::where('id', '=', $number)->exists();
    }
}
