<?php

namespace App\Http\Controllers\Site;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Mail;
use App\Mail\MailLogin;
use App\Mail\MailRegister;
use Hash;
use \App\User;
use App\ShowStatus;

class UserController extends Controller
{

    private $email;

    public function getRegister()
    {   
        if (Auth::check()) {
            return redirect('dashboard');
        } else {
            $show_status = ShowStatus::first();
            if ($show_status->active == 1) {
                return view("site.register");
            } else {
                 return redirect('login');
            }
        }
    }

    public function postRegister(Request $request)
    {
        $data = $request->all();
        $user = User::where('email',$data['email'])->first();
        $this->email = $data['email'];

        if(empty($user)) {
            $validator = Validator::make($request->all(), [
                // 'name' => 'required|string|max:255',
                'email' => 'required|unique:users|email',
            ]);
    
            if ($validator->fails()) {
                $errors = $validator->errors()->getMessages();

                return response()->json([
                    'header'      => "Oops!",
                    'message'     => $errors,
                    'status'      => 'error',
                    'status_code' => 400
                ]);
            }
            
            // $data['name']        = '';
            $token               = str_random(20);
            $data['password']    = $token;
            $data['login_token'] = md5($token);
            $data['role']        = 'User';
            $user                = User::create($data);

            $details = [
                'sender'  => config('mail.from.address'),
                'name'    => $user->email, 
                'email'   => $user->email, 
                'token'   => $token,
                'subject' => 'KIDLAT AWARDS '.date('Y').', your account has been created!',
                'view'    => 'email.registered'
            ];

            Mail::to($user->email)->send(new MailRegister($details));

            return response()->json([
                'header' => "Registration Successful!",
                'message' => ['email' => "Please check your email."],
                'status'      => 'success',
                'status_code' => 200
            ]);
        } else {
            return response()->json([
                'header' => "Duplicate",
                'message'     => ['email' => 'Email address already registered. Are you trying to sign in?'],
                'status'      => 'error',
                'status_code' => 409
            ]);
        }
        
    }

    public function registerEmailSent()
    {   
        return view("site.register_email_sent",[]);
    } 

    public function getLogin()
    {
        if (Auth::check()) {
            return redirect('dashboard');
        } else {
            $show_status = ShowStatus::first();
            $data['expired'] = $show_status->active == 1 ? false : true;

            return view("site.login", ['expired' => $data['expired']]);
        }
    }

    public function postLogin(Request $request)
    {
        $data = $request->all();
        $user = User::where('email',$data['email'])->first();
        $this->email = $data['email'];

        if(!empty($user)) {

             $validator = Validator::make($request->all(), [
                'email' => 'required|email',
            ]);
    
            if ($validator->fails()) {
                $errors = $validator->errors()->getMessages();

                return response()->json([
                    'header' => "Oops!",
                    'message'     => $errors,
                    'status'      => 'error',
                    'status_code' => 400
                ]);
            }

            $token = str_random(20);
            $user['login_token'] = md5($token);
            $user->save();

            $details = [
                'sender'  => config('mail.from.address'),
                'name'    => $user->name, 
                'email'   => $user->email, 
                'token'   => $token,
                'subject' => 'KIDLAT AWARDS '.date('Y').' '.$user->name.' , Easily Sign-in with the provided link!',
                'view'    => 'email.relogin'
            ];

            Mail::to($user->email)->send(new MailLogin($details));

            return response()->json([
                'header'      => 'Email sent',
                'message'     => ['email' => 'Please check your email.'],
                'status'      => 'success',
                'status_code' => 200
            ]);
        } else {
            return response()->json([
                'header'      => 'Oops...',
                // 'message'     => ['email' => 'Invalid email format. We need your email address to sign you in.'],
                'message'     => ['email' => 'Your email address is not yet registered. Please register first.'],
                'status'      => 'error',
                'status_code' => 400
            ]);
        }
        
    }

    public function loginEmailSent()
    {   
        return view("site.login_email_sent", []);
    } 

    // old method
    public function signUp(Request $request)
    {
        $data = $request->all();

        $user = User::where('email',$data['email'])->first();

        if(empty($user)) {
            if($data['signin'] == "1") {
                return back()->with('error', ['header' => "Oops!",'message' => "Email does not exists."]);
            }
            
            $validator = Validator::make($request->all(), [
                'name' => 'required|string|max:255',
                'email' => 'required|unique:users|email',
            ]);
    
            if ($validator->fails()) {
                $errors = $validator->errors()->getMessages();
                return back()->with('error', ['header' => "Oops!",'message' => "Something went wrong."]);
            }
    
            $token = str_random(20);
            $data['password'] = $token;
            $data['login_token'] = md5($token);
            $data['role'] = 'User';
            $user = User::create($data);

            $details = [
                'sender'  => config('mail.from.address'),
                'name'    => '', 
                'email'   => $user->email, 
                'token'   => $token,
                'subject' => 'KIDLAT AWARDS '.date('Y').', your account has been created!',
                'view'    => 'email.registered'
            ];

            Mail::to($user->email)->send(new MailRegister($details));
    
            return back()->with('success', 
                [
                    'header' => "Registration Successful!",
                    'message' => "Please check your email."
                ]
            );
        } else {
            $token = str_random(20);
            $user['login_token'] = md5($token);
            $user->save();

            $details = [
                'sender'  => config('mail.from.address'),
                'name'    => '', 
                'email'   => $user->email, 
                'token'   => $token,
                'subject' => 'KIDLAT AWARDS '.date('Y').' , Easily Sign-in with the provided link!',
                'view'    => 'email.relogin'
            ];

            Mail::to($user->email)->send(new MailLogin($details));

            return response()->json([
                'header'      => 'Welcome Back!',
                'message'     => 'Please check your email.',
                'status'      => 'success',
                'status_code' => 200
            ]);

        }
        
    }

    public function authenticate($token, $email)
    {
        $user = User::where('email',$email)->first();
        if(empty($user)) {
            return redirect('/login')->with('error', ['header' => "Oops!",'message' => "Invalid Authentication."]);
        } else {
            if (md5($token) == $user->login_token) {
                Auth::login($user);
                if(Auth::user()->role == 'User')
                    return redirect('/dashboard');
                if(Auth::user()->role == 'Judge')
                    return redirect('/juror/dashboard');
            } else {
                return redirect('/login')->with('error', ['header' => "Oops!",'message' => "Invalid Authentication."]);
            }
        }
    }

    public function agreePolicy(Request $request){
        $user = User::where('id',Auth::user()->id)->first();
        $user->policy = true;
        $user->save();
        return \Response::json(['success' => true]);
    }

    public function logout()
    {
        if (auth()->user()->role != "Admin") {
            Auth::logout();
            return redirect('https://www.kidlatawards.com/');
        } else {
            Auth::logout();
            return redirect('/admin/login');
        }
    }

}
