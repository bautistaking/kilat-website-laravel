<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Mail;
use App\Mail\MailJurorCredentials;
use App\Mail\MailJurorCredentialsAll;

use \App\User;
use \App\Entry;
use \App\File;
use \App\Category;
use \App\SubCategory;
use \App\VoteInstance;
use \App\Vote;
use \App\UserCategories;
use \App\UserDetails;
use DB;

class JudgeController extends Controller
{
    //
    public function index()
    {
        $data['judges']   = User::where('role','Judge')->orderBy('created_at','desc')->get();
        $data['categories']        = Category::orderBy('order', 'ASC')->get();
        $data['sub_categories']    = SubCategory::orderBy('name', 'ASC')->get();
        $multi_cat = [];

        $new_judge = [];
        foreach ($data['judges'] as $judge) {
            $multi_cat = DB::table('users')->select('users.id as user_id', 'users.name as user_name', 'users.email as user_email', 'category.id as catid', 'category.name as cat_name', 'sub_category.id as subid', 'sub_category.name as sub_name', 'users.created_at')
                ->where('users.id', $judge->id)
                ->join('user_categories', 'users.id', '=', 'user_categories.user_id')
                ->join('sub_category', 'user_categories.subcategory_id','=', 'sub_category.id')
                ->join('category', 'sub_category.category_id','=', 'category.id')
                ->get();

            array_push($new_judge, $multi_cat);
        }

        $data['multi_categories'] = $multi_cat;

        return view('admin.jurors',$data);
    }

    public function sendEmail(Request $request)
    {   
        $juror = User::where('email', $request->email)
                     ->where('role', 'Judge')
                     ->first();

        $token = str_random(20);
        $juror['login_token'] = md5($token);
        $juror->save();

        $details = [
            'sender'  => config('mail.from.address'),
            'name'    => $juror->name, 
            'email'   => $request->email,
            'token'   => $token,
            'subject' => 'Round 1 of Kidlat Awards 2021',
            'view'    => 'email.juror_credentials'
        ];

        Mail::to($juror->email)->send(new MailJurorCredentials($details));

        return response()->json([
            'header' => "Juror credentials sent successfully!",
            'message' => ['email' => $request->email],
            'status'      => 'success',
            'status_code' => 200
        ]);
    }

    public function sendEmailAll(Request $request)
    {   
        $jurors = User::where('role', 'Judge')->get();

        foreach ($jurors as $index => $juror) {
            $token = str_random(20);
            $juror['login_token'] = md5($token);
            $juror->save();

            $details = [
                'sender'  => config('mail.from.address'),
                'name'    => $juror->name, 
                'email'   => $juror->email,
                'token'   => $token,
                'subject' => 'Round 1 of Kidlat Awards 2021',
                'view'    => 'email.juror_credentials'
            ];

            Mail::to($juror->email)->send(new MailJurorCredentialsAll($details));
        }

        return response()->json([
            'header' => "Jurors credentials sent successfully!",
            'message' => ['email' => $request->email],
            'status'      => 'success',
            'status_code' => 200
        ]);
    }

    public function add(Request $request)
    {
        // $data['categories'] = Category::orderBy('order','asc')->get();
        // return view('admin.add-judge',$data);
        $data = $request->all();
        $user = User::where('email',$request->email)->first();

        if(empty($user)) {
            $validator = Validator::make($request->all(), [
                'email' => 'required|unique:users|email',
            ]);
    
            if ($validator->fails()) {
                $errors = $validator->errors()->getMessages();

                return response()->json([
                    'header' => "Oops!",
                    'message'     => $errors,
                    'status'      => 'error',
                    'status_code' => 400
                ]);
            }
        
            $juror  = new User;
            $juror->name  = $request->juror_name;
            $juror->email = $request->email;
            $juror->role  = "Judge";
            $juror->save();

            // user details table
            $user_details = new UserDetails;
            $user_details->user_id      = $juror->id;
            $user_details->juror_name   = $request->juror_name;
            $user_details->juror_agency = $request->juror_agency;
            $user_details->save();

            //user categories table
            // $user_categories = new UserCategories;
            // $user_categories->user_id        = $juror->id;
            // $user_categories->category_id    = $request->category_id;
            // $user_categories->subcategory_id = $request->subcategory_id;
            // $user_categories->save();

            UserCategories::where('user_id',$juror->id)->delete();
            foreach (json_decode($data['categories']) as $key => $val) {
                if($val->category_id != '' && $val->subcategory_id != '') {
                    $user_categories = UserCategories::create([
                        'user_id'       => $juror->id,
                        'category_id'   => $val->category_id, 
                        'subcategory_id'   => $val->subcategory_id,
                    ]);
                }
            }

            return response()->json([
                'header' => "Juror Registration Successful!",
                'message' => ['id' => $juror->id],
                'status'      => 'success',
                'status_code' => 200
            ]);
        } else {
            return response()->json([
                'header' => "Duplicate",
                'message'     => ['email' => 'Email address already registered'],
                'status'      => 'error',
                'status_code' => 409
            ]);
        }
    }

    public function edit(Request $request)
    {
        $data = $request->all();
        $user = User::where('email',$request->email)->first();

        $validator = Validator::make($request->all(), [
            'email' => 'required|email',
        ]);

        if ($validator->fails()) {
            $errors = $validator->errors()->getMessages();

            return response()->json([
                'header' => "Oops!",
                'message'     => $errors,
                'status'      => 'error',
                'status_code' => 400
            ]);
        }
    
        $juror  = User::where('id', $request->id)
        ->update([
            'name'  => $request->juror_name,
            'email' => $request->email,
            'role'  => "Judge"
        ]);

        // user details table
        UserDetails::where('user_id',$request->id)->delete();
        $user_details = new UserDetails;
        $user_details->user_id      = $request->id;
        $user_details->juror_name   = $request->juror_name;
        $user_details->juror_agency = $request->juror_agency;
        $user_details->save();

        //user categories table
        // UserCategories::where('user_id',$request->id)->delete();
        // $user_categories = new UserCategories;
        // $user_categories->user_id        = $request->id;
        // $user_categories->category_id    = $request->category_id;
        // $user_categories->subcategory_id = $request->subcategory_id;
        // $user_categories->save();

        UserCategories::where('user_id',$request->id)->delete();
        foreach (json_decode($data['categories']) as $key => $val) {
            if($val->category_id != '' && $val->subcategory_id != '') {
                $user_categories = UserCategories::create([
                    'user_id'       => $request->id,
                    'category_id'   => $val->category_id, 
                    'subcategory_id'   => $val->subcategory_id,
                ]);
            }
        }

        return response()->json([
            'header' => "Juror updated successfully!",
            'message' => ['id' => $request->id],
            'status'      => 'success',
            'status_code' => 200
        ]);
    }

    public function getJurorById(Request $request)
    {
        $juror = User::where('role', 'Judge')
            ->where('id', $request->id)
            ->first();

        $user_details     = UserDetails::where('user_id', $request->id)->first();
        $user_categories  = UserCategories::where('user_id', $request->id)->first();
        $multi_categories = DB::table('users')->select('category.id as catid', 'category.name as cat_name', 'sub_category.id as subid', 'sub_category.name as sub_name')
                            ->where('users.id', $request->id)
                            ->join('user_categories', 'users.id', '=', 'user_categories.user_id')
                            ->join('sub_category', 'user_categories.subcategory_id','=', 'sub_category.id')
                            ->join('category', 'sub_category.category_id','=', 'category.id')
                            ->get();

        $data['juror']            = $juror;
        $data['user_details']     = $user_details;
        $data['user_categories']  = $user_categories;
        $data['multi_categories'] = $multi_categories;

        return response()->json($data); 
    }

    public function delete(Request $request)
    {
        $user = User::where('id', $request->id)->delete();

        return response()->json([
            'header' => "Juror user deleted successfully!",
            'message' => ['id' => $request->id],
            'status'      => 'success',
            'status_code' => 200
        ]);
    }

    public function addPost(Request $request)
    {
        $data = $request->all();

        $validator = Validator::make($request->all(), [
            'name' => 'required|string|max:255',
            'email' => 'required|unique:users|email',
        ]);

        if ($validator->fails()) {
            $errors = $validator->errors()->getMessages();
            return redirect()->back()->withErrors($validator);
        }

        $token = str_random(20);
        $data['login_token'] = md5($token);
        $data['role'] = 'Judge';
        $user = User::create($data);

        $instanceData = [
            'user_id' => $user->id,
            'category_id' => $data['category']
        ];

        $vote_instance = VoteInstance::create($instanceData);

        $entries = Entry::where('category',$data['category'])->where('payment_status','Paid')->where('status','Approved')->get();

        foreach ($entries as $e) {
           $d = [
            'instance_id' => $vote_instance->id,
            'entry_uid' => $e->uid
           ];

           $vote = Vote::create($d);
        }

        $category = $vote_instance->category();

        Mail::send('email.judge', ['name' => $user->name, 'email' => $user->email, 'token' => $token, 'category' => $category], function ($message) use ($user)
        {
            $message->to($user->email);
            $message->sender(config('mail.from.address'));
            $message->subject('[KIDLAT AWARDS 2019] Judge Invitation');

            $headers = $message->getHeaders();
        });


        return back()->with('success', ['header' => "Judge Succesfully Added",'message' => ""]);
    }

    public function entryPreview($category,$id = null) {
        switch ($category) {
            case 'creative-effectiveness':
                $cat_id = 1;
                break;
            case 'creative-technology-media':
                $cat_id = 2;
                break;
            case 'creative-storytelling':
                $cat_id = 3;
                break;
            case 'creative-purpose':
                $cat_id = 4;
                break;
            case 'craft':
                $cat_id = 5;
                break;
            default:
                $cat_id = 1;
                break;
        }
        $data['category'] = $category;
        if($id == null) {
            $data['entry'] = Entry::where('category',$cat_id)->where('payment_status','Paid')->where('status','Approved')->first();
        } else {
            $data['entry'] = Entry::where('id',$id)->where('category',$cat_id)->where('payment_status','Paid')->where('status','Approved')->first();
        }
        $data['prev'] = Entry::where('id', '<', $data['entry']->id)->where('category',$cat_id)->where('payment_status','Paid')->where('status','Approved')->orderBy('id','desc')->first();
        $data['next'] = Entry::where('id', '>', $data['entry']->id)->where('category',$cat_id)->where('payment_status','Paid')->where('status','Approved')->orderBy('id','asc')->first();
        return view('admin.preview',$data);
    }

    public function summary($category = "creative-effectiveness") {
        switch ($category) {
            case 'creative-effectiveness':
                $data['cat_id'] = 1;
                break;
            case 'creative-technology-media':
                $data['cat_id'] = 2;
                break;
            case 'creative-storytelling':
                $data['cat_id'] = 3;
                break;
            case 'creative-purpose':
                $data['cat_id'] = 4;
                break;
            case 'craft':
                $data['cat_id'] = 5;
                break;
            default:
                $data['cat_id'] = 1;
                break;
        }
        $data['category'] = $category;
        $data['entries'] = Entry::where('category',$data['cat_id'])->where('payment_status','Paid')->where('status','Approved')->get();
        return view('admin.summary',$data);
    }

    
}
