<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Validator;
use Maatwebsite\Excel\Facades\Excel;
use App\Exports\ScoreSummaryRound1Export;
use App\Exports\ScoreAllRound1Export;
use App\Exports\ScoreSummaryRound2Export;
use App\Exports\ScoreAllRound2Export;
use Storage;

use \App\User;
use \App\Entry;
use \App\Credit;
use \App\File;
use \App\Category;
use \App\SubCategory;
use \App\Reference;
use App\ShowStatus;
use App\VotesRound1;
use App\VotesRound2;
use App\Round2Entries;
use DB;

class ScoreController extends Controller
{
    public function index()
    {
        $show_status = ShowStatus::first();
        $data['expired'] = $show_status->active == 1 ? false : true;

        $data['entries_round1'] = VotesRound1::select('entries.id','entries.uid', 'entries.entry_name', 'category.name as cat_name', 'sub_category.name as sub_name','entries.status as entry_status', 'votes_round1.status as vote_status', 'votes_round1.reason')
                         ->selectRaw('COUNT(*) AS vote_count')
                         ->where('entries.status', 'Approved')
                         ->where('entries.payment_status', 'Paid')
                         ->join('entries', 'votes_round1.entry_id','=','entries.id')
                         ->join('sub_category', 'entries.sub_category','=','sub_category.id')
                         ->join('category', 'entries.category','=','category.id')
                         ->orderBy('entries.created_at', 'DESC')
                         ->groupBy('entries.id', 'votes_round1.status')
                         ->get();

        // round 2
        $data['entries_round2'] = VotesRound2::select('entries.id','entries.uid', 'entries.entry_name', 'category.name as cat_name', 'sub_category.name as sub_name','entries.status as entry_status', 'votes_round2.status as vote_status', 'votes_round2.reason')
                         ->selectRaw('COUNT(*) AS vote_count')
                         ->where('entries.status', 'Approved')
                         ->where('entries.payment_status', 'Paid')
                         ->join('entries', 'votes_round2.entry_id','=','entries.id')
                         ->join('sub_category', 'entries.sub_category','=','sub_category.id')
                         ->join('category', 'entries.category','=','category.id')
                         ->orderBy('entries.created_at', 'DESC')
                         ->groupBy('entries.id', 'votes_round2.status')
                         ->get();

        return view('admin.scores', $data);
    }

    public function viewScore($uid)
    {
        $data['entry'] = Entry::where('status', 'Approved')->where('payment_status', 'Paid')->where('uid', $uid)->first();
        $data['references'] = Reference::where('entry_id', $data['entry']->id)->get();

        if(!empty($data['entry'])) {
            return view('admin.score_view', $data);
        } else {
            return redirect('/admin/scores');
        }
    }

    public function downloadScoreSummaryRound1()
    {
        $filename = "score_summary-round1-".date("Y-m-d").".csv";
        return Excel::download(new ScoreSummaryRound1Export, $filename);
    }

    public function downloadScoreAllRound1()
    {
        $filename = "score_all-round1-".date("Y-m-d").".csv";
        return Excel::download(new ScoreAllRound1Export, $filename);
    }

    public function downloadScoreSummaryRound2()
    {
        $filename = "score_summary-round2-".date("Y-m-d").".csv";
        return Excel::download(new ScoreSummaryRound2Export, $filename);
    }

    public function downloadScoreAllRound2()
    {
        $filename = "score_all-round2-".date("Y-m-d").".csv";
        return Excel::download(new ScoreAllRound2Export, $filename);
    }
    
}
