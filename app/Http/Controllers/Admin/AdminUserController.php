<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\Rule;
use App\Mail\MailAdminRegister;
use \App\User;

class AdminUserController extends Controller
{
    public function index()
    {
        $data['admin_users'] = User::whereIn('role',['Admin','Auditor'])
        	->orderBy('created_at','desc')
        	->get();

        return view('admin.admin_users',$data);
    }

    public function add(Request $request)
    {
        $data = $request->all();
        $user = User::where('email',$request->email)->first();

        if(empty($user)) {
            $validator = Validator::make($request->all(), [
                'email' => 'required|unique:users|email',
            ]);
    
            if ($validator->fails()) {
                $errors = $validator->errors()->getMessages();

                return response()->json([
                    'header' => "Oops!",
                    'message'     => $errors,
                    'status'      => 'error',
                    'status_code' => 400
                ]);
            }
            
            $data['password']   = Hash::make($request->password);
            $data['admin_type'] = $request->admin_type == "Admin" ? 1 : 2;
            $user               = User::create($data);

            $details = [
                'sender'   => config('mail.from.address'),
                // 'admin_url'=> '/webmaster-kidlat/login',
                'name'     => $user->name, 
                'email'    => $user->email, 
                'password' => $request->password,
                'subject'  => 'KIDLAT AWARDS '.date('Y').', your admin account has been created!',
                'view'     => 'email.admin_registered'
            ];

            Mail::to($user->email)->send(new MailAdminRegister($details));

            return response()->json([
                'header' => "Admin Registration Successful!",
                'message' => ['email' => "Please check your email."],
                'status'      => 'success',
                'status_code' => 200
            ]);
        } else {
            return response()->json([
                'header' => "Duplicate",
                'message'     => ['email' => 'Email address already registered. Are you trying to sign in?'],
                'status'      => 'error',
                'status_code' => 409
            ]);
        }
    }

    public function getAdminById(Request $request)
    {
        $admin = User::whereIn('role',['Admin','Auditor'])
            ->where('id', $request->id)
            ->orderBy('updated_at','desc')
            ->first();

        return response()->json($admin); 
    }

    public function edit(Request $request)
    {
        $data = $request->all();
        $user = User::where('id', $request->id)->first();

        $validator = Validator::make($request->all(), [
            'email'    => 'required|email',
            'password' => 'sometimes'
        ]);

        if ($validator->fails()) {
            $errors = $validator->errors()->getMessages();

            return response()->json([
                'header' => "Oops!",
                'message'     => $errors,
                'status'      => 'error',
                'status_code' => 400
            ]);
        }
        
        if ($request->password != "") {
            $data['password']   = Hash::make($request->password);
        } else {
            $data['password'] = $user->password;
        }

        $data['admin_type'] = $request->admin_type == "Admin" ? 1 : 2;

        $user->fill($data);
        $user->save();

        $details = [
            'sender'   => config('mail.from.address'),
            'name'     => $user->name, 
            'email'    => $user->email, 
            'password' => $request->password,
            'subject'  => 'KIDLAT AWARDS '.date('Y').', your admin account has been updated!',
            'view'     => 'email.admin_registered'
        ];

        Mail::to($user->email)->send(new MailAdminRegister($details));

        return response()->json([
        'header' => "Admin user updated successfully!",
        'message' => ['email' => "Please check your email."],
        'status'      => 'success',
        'status_code' => 200
        ]);
    }

    public function delete(Request $request)
    {
        $user = User::where('id', $request->id)->delete();

        return response()->json([
            'header' => "Admin user deleted successfully!",
            'message' => ['id' => $request->id],
            'status'      => 'success',
            'status_code' => 200
        ]);
    }

}
