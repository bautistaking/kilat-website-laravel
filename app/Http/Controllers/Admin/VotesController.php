<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Mail;

use \App\User;
use \App\Entry;
use \App\File;
use \App\Category;
use \App\VoteInstance;
use \App\Vote;

class VotesController extends Controller
{
    //
    public function index($cat = 1)
    {
        $data['cat'] = $cat;
        $data['instances'] = VoteInstance::where('category_id',$cat)->get();
        $data['category'] = Category::where('id',$cat)->first();
        $ids = [];
        foreach ($data['instances'] as $i) {
            array_push($ids,$i->id);
        }
        $data['entries'] = Vote::select('entry_uid')->distinct()->whereIn('instance_id', $ids)->get();
        // return \Response::json(['success' => $data['instances'][0]->votes]);
        return view('admin.votes',$data);
    }

    public function updateEntries(Request $request)
    {
        $data = $request->all();
        $category = Category::where('id',$data['category_id'])->first();
        $entries = Entry::where('category',$data['category_id'])->where('payment_status','Paid')->where('status','Approved')->get();
        $instances = VoteInstance::where('category_id',$data['category_id'])->get();
        foreach ($instances as $i) {
            $exisiting = [];
            $send_mail = false;
            foreach($i->votes() as $v) {
                array_push($exisiting,$v->entry_uid);
            }
            foreach($entries as $e) {
                if(!in_array($e->uid,$exisiting)) {
                    $send_mail = true;
                    $d = [
                        'instance_id' => $i->id,
                        'entry_uid' => $e->uid
                    ];
                    $vote = Vote::create($d);
                }
            }
            if($send_mail == true) {
                $i->status = 'Started';
                $i->save();
                $user = User::where('id',$i->user_id)->first();
                $token = str_random(20);
                $user['login_token'] = md5($token);
                $user->save();
                Mail::send('email.update-entries', ['name' => $user->name, 'email' => $user->email, 'token' => $token, 'category' => $category], function ($message) use ($user)
                {
                    $message->to($user->email);
                    $message->sender(config('mail.from.address'));
                    $message->subject('[KIDLAT AWARDS 2019] Additional entries for Judges scoring');

                    $headers = $message->getHeaders();
                });
            }
        }
        return redirect()->back()->with('success', ['header' => "Succesfully Updated Entries for Judges",'message' => ""]);;
    }

    public function export($cat = 1)
    {
        $instance = VoteInstance::where('category_id',$cat)->get();
        $ids = [];
        $category = Category::where('id',$cat)->first();
        foreach ($instance as $i) {
            array_push($ids,$i->id);
        }
        $entries = Vote::select('entry_uid')->distinct()->whereIn('instance_id', $ids)->get();
        $headers = array(
            "Content-type" => "text/csv",
            "Content-Disposition" => "attachment; filename=votes_".$category->name."_".date('m-d-Y_hia').".csv",
            "Pragma" => "no-cache",
            "Cache-Control" => "must-revalidate, post-check=0, pre-check=0",
            "Expires" => "0"
        );
        $columns = ['Entry Name', 'Entry UID', 'Entry Agency'];
        foreach ($instance as $i) {
            array_push($columns,$i->judge()->name);
        }
        $callback = function() use ($instance, $columns, $entries)
        {
            $file = fopen('php://output', 'w');
            fputcsv($file, $columns);
            foreach($entries as $e) {
                $fields = [$e->entry()->entry_name,$e->entry()->uid, $e->entry()->agency];
                foreach ($instance as $i) {
                    if($i->status == 'Done') {
                        foreach ($i->votes() as $v) {
                            if($e->entry_uid == $v->entry_uid) {
                                if($v->score == 0) {
                                    array_push($fields,'A');
                                } else {
                                    array_push($fields,$v->score);
                                }
                            }
                        }
                    } else {
                        array_push($fields,'Pending');
                    }
                }
                fputcsv($file, $fields);
            }
            fclose($file);
        };
        return Response::stream($callback, 200, $headers);
    }
}
