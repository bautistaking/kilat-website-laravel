<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Mail;
use App\Mail\MailJurorCredentials;
use App\Mail\MailJurorCredentialsAll;

use \App\User;
use \App\ViewModels\Judge;
use \App\Entry;
use \App\File;
use \App\Category;
use \App\SubCategory;
use \App\VoteInstance;
use \App\Vote;
use \App\UserCategories;
use \App\UserDetails;
use DB;

class JudgeController extends Controller
{
    //
    public function index()
    {
        $data['judges']   = Judge::where('role','Judge')->orderBy('created_at','desc')->get();
        $data['categories']        = Category::orderBy('order', 'ASC')->get();
        $data['sub_categories']    = SubCategory::orderBy('name', 'ASC')->get();
        
        return view('admin.jurors',$data);
    }

    public function sendEmail(Request $request)
    {   
        $juror = User::where('email', $request->email)
                     ->where('role', 'Judge')
                     ->first();

        $user_categories = UserCategories::where('user_id', $juror->id)
                           ->join('category', 'user_categories.category_id','=','category.id')
                           ->groupBy('category.name')
                           ->get();
                            // select('category.name as cat_name', 'sub_category.name as sub_name')
                           // ->join('sub_category', 'user_categories.subcategory_id','=','sub_category.id')
                           // ->join('category', 'sub_category.category_id','=','category.id')

        $categories = [];

        foreach ($user_categories as $category) {
            array_push($categories,  $category->name);
            // array_push($categories,  $category->cat_name." - ".$category->sub_name);
        }

        $token = str_random(20);
        $juror['login_token'] = md5($token);
        $juror['password'] = $token;
        $juror->save();

        $details = [
            'sender'     => config('mail.from.address'),
            'name'       => $juror->name, 
            'email'      => $request->email,
            'categories' => $categories,
            'token'      => $token,
            'subject'    => 'Round 1 of Kidlat Awards 2022',
            'view'       => 'email.juror_credentials'
        ];

        $bcc = '4asp@pldtdsl.net';
        Mail::to($juror->email)->bcc($bcc)->send(new MailJurorCredentials($details));

        return response()->json([
            'header' => "Juror credentials sent successfully!",
            'message' => ['email' => $request->email],
            'status'      => 'success',
            'status_code' => 200
        ]);
    }

    public function sendEmail2(Request $request)
    {   
        $juror = User::where('email', $request->email)
                     ->where('role', 'Judge')
                     ->first();

        $user_categories = UserCategories::where('user_id', $juror->id)
                           ->join('category', 'user_categories.category_id','=','category.id')
                           ->groupBy('category.name')
                           ->get();
                            // select('category.name as cat_name', 'sub_category.name as sub_name')
                           // ->join('sub_category', 'user_categories.subcategory_id','=','sub_category.id')
                           // ->join('category', 'sub_category.category_id','=','category.id')

        $categories = [];

        foreach ($user_categories as $category) {
            array_push($categories,  $category->name);
            // array_push($categories,  $category->cat_name." - ".$category->sub_name);
        }

        $token = str_random(20);
        $juror['login_token'] = md5($token);
        $juror->save();

        $details = [
            'sender'     => config('mail.from.address'),
            'name'       => $juror->name, 
            'email'      => $request->email,
            'categories' => $categories,
            'token'      => $token,
            'subject'    => 'Round 2 of Kidlat Awards 2022',
            'view'       => 'email.juror_credentials2'
        ];

        $bcc = '4asp@pldtdsl.net';
        Mail::to($juror->email)->bcc($bcc)->send(new MailJurorCredentials($details));

        return response()->json([
            'header' => "Juror credentials sent successfully!",
            'message' => ['email' => $request->email],
            'status'      => 'success',
            'status_code' => 200
        ]);
    }

    public function sendEmailAll(Request $request)
    {   
        $jurors = User::where('role', 'Judge')->get();

        foreach ($jurors as $index => $juror) {

            $user_categories = UserCategories::where('user_id', $juror->id)
                               ->join('category', 'user_categories.category_id','=','category.id')
                               ->groupBy('category.name')
                               ->get(); 

            $categories = [];

            foreach ($user_categories as $category) {
                // array_push($categories,  $category->cat_name." - ".$category->sub_name);
                array_push($categories,  $category->name);
            }

            $token = str_random(20);
            $juror->login_token = md5($token);
            $juror->save();

            $subject = "";
            $view = "";

            if ($request->rounds == 1) {
                $subject = 'Round 1 of Kidlat Awards 2022';
                $view    = 'email.juror_credentials_all';
            } else if ($request->rounds == 2) {
                $subject = 'Round 2 of Kidlat Awards 2022';
                $view    = 'email.juror_credentials_all2';
            }

            $details = [
                'sender'     => config('mail.from.address'),
                'name'       => $juror->name, 
                'email'      => $juror->email,
                'categories' => $categories,
                'token'      => $token,
                'subject'    => $subject,
                'view'       => $view
            ];

            $bcc = '4asp@pldtdsl.net';
            Mail::to($juror->email)->bcc($bcc)->send(new MailJurorCredentialsAll($details));
        }

        return response()->json([
            'header' => "Jurors credentials sent successfully!",
            'message' => ['email' => $request->email],
            'status'      => 'success',
            'status_code' => 200
        ]);
    }

    public function add(Request $request)
    {
        // $data['categories'] = Category::orderBy('order','asc')->get();
        // return view('admin.add-judge',$data);
        $data = $request->all();
        $user = User::where('email',$request->email)->first();

        if(empty($user)) {
            $validator = Validator::make($request->all(), [
                'email' => 'required|unique:users|email',
            ]);
    
            if ($validator->fails()) {
                $errors = $validator->errors()->getMessages();

                return response()->json([
                    'header' => "Oops!",
                    'message'     => $errors,
                    'status'      => 'error',
                    'status_code' => 400
                ]);
            }
        
            $juror  = new User;
            $juror->name  = $request->juror_name;
            $juror->email = $request->email;
            $juror->role  = "Judge";
            $juror->save();

            // user details table
            $user_details = new UserDetails;
            $user_details->user_id      = $juror->id;
            $user_details->juror_name   = $request->juror_name;
            $user_details->juror_agency = $request->juror_agency;
            $user_details->save();

            //user categories table
            // $user_categories = new UserCategories;
            // $user_categories->user_id        = $juror->id;
            // $user_categories->category_id    = $request->category_id;
            // $user_categories->subcategory_id = $request->subcategory_id;
            // $user_categories->save();

            UserCategories::where('user_id',$juror->id)->delete();
            foreach (json_decode($data['categories']) as $key => $val) {
                if($val->category_id != '' && $val->subcategory_id != '') {
                    $user_categories = UserCategories::create([
                        'user_id'       => $juror->id,
                        'category_id'   => $val->category_id, 
                        'subcategory_id'   => $val->subcategory_id,
                    ]);
                }
            }

            return response()->json([
                'header' => "Juror Registration Successful!",
                'message' => ['id' => $juror->id],
                'status'      => 'success',
                'status_code' => 200
            ]);
        } else {
            return response()->json([
                'header' => "Duplicate",
                'message'     => ['email' => 'Email address already registered'],
                'status'      => 'error',
                'status_code' => 409
            ]);
        }
    }

    public function edit(Request $request)
    {
        $data = $request->all();
        $user = User::find($request->id);


        if($user->email != $request->email) {
            $validator = Validator::make($request->all(), [
                'email' => 'required|email|unique:users,email,'.$user->id,
            ]);

            if ($validator->fails()) {
                $errors = $validator->errors()->getMessages();

                return response()->json([
                    'header' => "Oops!",
                    'message'     => $errors,
                    'status'      => 'error',
                    'status_code' => 400
                ]);
            }
        }
    
        $juror  = User::where('id', $request->id)
        ->update([
            'name'  => $request->juror_name,
            'email' => $request->email,
            'role'  => "Judge"
        ]);

        // user details table
        UserDetails::where('user_id',$request->id)->delete();
        $user_details = new UserDetails;
        $user_details->user_id      = $request->id;
        $user_details->juror_name   = $request->juror_name;
        $user_details->juror_agency = $request->juror_agency;
        $user_details->save();

        UserCategories::where('user_id',$request->id)->delete();
        foreach (json_decode($data['categories']) as $key => $val) {
            if($val->category_id != '' && $val->subcategory_id != '') {
                $user_categories = UserCategories::create([
                    'user_id'       => $request->id,
                    'category_id'   => $val->category_id, 
                    'subcategory_id'   => $val->subcategory_id,
                ]);
            }
        }

        return response()->json([
            'header' => "Juror updated successfully!",
            'message' => ['id' => $request->id],
            'status'      => 'success',
            'status_code' => 200
        ]);
        // } else {
        //     return response()->json([
        //         'header' => "Duplicate",
        //         'message'     => ['email' => 'Email address already registered'],
        //         'status'      => 'error',
        //         'status_code' => 409
        //     ]);
        // }
    }

    public function getJurorById(Request $request)
    {
        $juror = User::where('role', 'Judge')
            ->where('id', $request->id)
            ->first();

        $user_details     = UserDetails::where('user_id', $request->id)->first();
        $user_categories  = UserCategories::where('user_id', $request->id)->first();
        $multi_categories = DB::table('users')->select('category.id as catid', 'category.name as cat_name', 'sub_category.id as subid', 'sub_category.name as sub_name')
                            ->where('users.id', $request->id)
                            ->join('user_categories', 'users.id', '=', 'user_categories.user_id')
                            ->join('sub_category', 'user_categories.subcategory_id','=', 'sub_category.id')
                            ->join('category', 'sub_category.category_id','=', 'category.id')
                            ->get();

        $data['juror']            = $juror;
        $data['user_details']     = $user_details;
        $data['user_categories']  = $user_categories;
        $data['multi_categories'] = $multi_categories;

        return response()->json($data); 
    }

    public function delete(Request $request)
    {
        $user = User::where('id', $request->id)->delete();

        return response()->json([
            'header' => "Juror user deleted successfully!",
            'message' => ['id' => $request->id],
            'status'      => 'success',
            'status_code' => 200
        ]);
    }

    public function addPost(Request $request)
    {
        $data = $request->all();

        $validator = Validator::make($request->all(), [
            'name' => 'required|string|max:255',
            'email' => 'required|unique:users|email',
        ]);

        if ($validator->fails()) {
            $errors = $validator->errors()->getMessages();
            return redirect()->back()->withErrors($validator);
        }

        $token = str_random(20);
        $data['login_token'] = md5($token);
        $data['role'] = 'Judge';
        $user = User::create($data);

        $instanceData = [
            'user_id' => $user->id,
            'category_id' => $data['category']
        ];

        $vote_instance = VoteInstance::create($instanceData);

        $entries = Entry::where('category',$data['category'])->where('payment_status','Paid')->where('status','Approved')->get();

        foreach ($entries as $e) {
           $d = [
            'instance_id' => $vote_instance->id,
            'entry_uid' => $e->uid
           ];

           $vote = Vote::create($d);
        }

        $category = $vote_instance->category();

        Mail::send('email.judge', ['name' => $user->name, 'email' => $user->email, 'token' => $token, 'category' => $category], function ($message) use ($user)
        {
            $message->to($user->email);
            $message->sender(config('mail.from.address'));
            $message->subject('[KIDLAT AWARDS 2019] Judge Invitation');

            $headers = $message->getHeaders();
        });


        return back()->with('success', ['header' => "Judge Succesfully Added",'message' => ""]);
    }

    public function entryPreview($category,$id = null) {
        switch ($category) {
            case 'creative-effectiveness':
                $cat_id = 1;
                break;
            case 'creative-technology-media':
                $cat_id = 2;
                break;
            case 'creative-storytelling':
                $cat_id = 3;
                break;
            case 'creative-purpose':
                $cat_id = 4;
                break;
            case 'craft':
                $cat_id = 5;
                break;
            default:
                $cat_id = 1;
                break;
        }
        $data['category'] = $category;
        if($id == null) {
            $data['entry'] = Entry::where('category',$cat_id)->where('payment_status','Paid')->where('status','Approved')->first();
        } else {
            $data['entry'] = Entry::where('id',$id)->where('category',$cat_id)->where('payment_status','Paid')->where('status','Approved')->first();
        }
        $data['prev'] = Entry::where('id', '<', $data['entry']->id)->where('category',$cat_id)->where('payment_status','Paid')->where('status','Approved')->orderBy('id','desc')->first();
        $data['next'] = Entry::where('id', '>', $data['entry']->id)->where('category',$cat_id)->where('payment_status','Paid')->where('status','Approved')->orderBy('id','asc')->first();
        return view('admin.preview',$data);
    }

    public function summary($category = "creative-effectiveness") {
        switch ($category) {
            case 'creative-effectiveness':
                $data['cat_id'] = 1;
                break;
            case 'creative-technology-media':
                $data['cat_id'] = 2;
                break;
            case 'creative-storytelling':
                $data['cat_id'] = 3;
                break;
            case 'creative-purpose':
                $data['cat_id'] = 4;
                break;
            case 'craft':
                $data['cat_id'] = 5;
                break;
            default:
                $data['cat_id'] = 1;
                break;
        }
        $data['category'] = $category;
        $data['entries'] = Entry::where('category',$data['cat_id'])->where('payment_status','Paid')->where('status','Approved')->get();
        return view('admin.summary',$data);
    }

    public function voteRound1(Request $request)
    {
        $data['entries'] = Entry::select('entries.*', 'votes_round1.status as vote_status', 'votes_round1.reason')
                   ->where('entries.category', $request->cat_id)
                   ->where('entries.sub_category', $request->sub_id)
                   ->where('entries.payment_status', 'Paid')
                   ->where('entries.status', 'Approved')
                   ->leftJoin('votes_round1', function($join)
                         {
                            $join->on('entries.id', '=', 'votes_round1.entry_id')
                            ->where('votes_round1.user_id','=', auth()->user()->id);
                         })
                   ->orderBy('entries.created_at', 'ASC')
                   ->get();

        return response()->json([
            'header'      => "Entries successfully fetched!",
            'data'        => $data,
            'status'      => 'success',
            'status_code' => 200
        ]);
    }

    public function voteStatusRound1(Request $request)
    {
        $data['vote_status'] = VotesRound1::where('entry_id', $request->entry_id)
                                      ->where('user_id', auth()->user()->id)
                                      ->first();

        $data['references'] = Reference::where('entry_id', $request->entry_id)->get();

        return response()->json([
            'header'      => "Vote status successfully fetched!",
            'data'        => $data,
            'status'      => 'success',
            'status_code' => 200
        ]);
    }
    
}
