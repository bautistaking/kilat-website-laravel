<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Response;

use \App\User;
use \App\ShowStatus;


class ExpiryStatusController extends Controller
{
    public function setExpiry(Request $request)
    {
		$show_status = ShowStatus::where('id', 1)
			->update([
				'active' 	 => $request->active,
				'expired_at' => $request->expired_at
			]);

		return response()->json([
            'header'      => "Expiry Status updated successfully!",
            'status'      => 'success',
            'status_code' => 200
        ]);
    }

}
