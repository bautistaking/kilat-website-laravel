<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Mail;
use Maatwebsite\Excel\Facades\Excel;
use App\Exports\EntriesExport;
use App\Imports\EntriesImport;
use App\Exports\Round2EntriesExport;
use App\Mail\UpdatePayment;
use App\Mail\UpdateStatus;

use \App\User;
use \App\Entry;
use \App\Category;
use \App\SubCategory;
use \App\Reference;
use \App\Round2Entries;
use \App\File;

class EntryController extends Controller
{
    public function index($filter = null)
    {
        if($filter == "rejected") {
            $data['entries'] = Entry::where('status','Rejected')->orderBy('created_at','desc')->get();
        } elseif($filter == "draft") {
            $data['entries'] = Entry::where('payment_status','Unpaid')->orderBy('created_at','desc')->get();
        } elseif($filter == "pending_payment") {
            $data['entries'] = Entry::where('payment_status','Requested')->orderBy('created_at','desc')->get();
        } elseif($filter == "pending_approval") {
            $data['entries'] = Entry::where('payment_status','Paid')->where('status','Pending')->orderBy('created_at','desc')->get();
        } elseif($filter == "approved") {
            $data['entries'] = Entry::where('payment_status','Paid')->where('status','Approved')->orderBy('created_at','desc')->get();
        } else {
            $data['entries'] = Entry::orderBy('created_at','desc')->get();
        }
        $data['filter'] = $filter;
        return view('admin.entries',$data);
    }

    public function downloadEntries()
    {
        $filename = "entries-".date("Y-m-d").".csv";
        return Excel::download(new EntriesExport, $filename);
    }

    public function round2Entries($filter = null)
    {
        $data['entries'] = Round2Entries::select('entries.*','category.name as cat_name', 'sub_category.name as sub_name')
                           ->join('entries', 'round2_entries.entry_id', 'entries.id')
                           ->join('sub_category', 'entries.sub_category', 'sub_category.id')
                           ->join('category', 'entries.category', 'category.id')
                           ->get();

        return view('admin.import',$data);
    }

    public function importEntries(Request $request)
    {
        Excel::import(new EntriesImport, $request->file('entries'));
        return response()->json([
            'header'      => 'Import successfully!',
            'message'     => [],
            'status'      => 'success',
            'status_code' => 200
        ]);

    }

    public function exportRound2()
    {
        $filename = "round2-entries-".date("Y-m-d").".csv";
        return Excel::download(new Round2EntriesExport, $filename);
    }

    public function adminLogin() {
        if (!Auth::check()) {
            return view('admin.login');
        } else {
            return redirect('/admin/entries');
        }
    }

    public function login(Request $request)
    {
        if (Auth::attempt([
                'email' => $request->email, 
                'password' => $request->password,
                // 'role' => 'Admin',
            ])) {

            return response()->json([
                'header'      => 'Welcome Back!',
                'message'     => ['content' => 'Login Successful!'],
                'status'      => 'success',
                'status_code' => 200
            ]);
        }

        return response()->json([
            'header'      => 'Oops...',
            'message'     => ['content' => 'Incorrect email or password.'],
            'status'      => 'error',
            'status_code' => 400
        ]);
    }

    public function viewEntry($uid)
    {
        $data['entry'] = Entry::where('uid', $uid)->first();
        $data['references'] = Reference::where('entry_id', $data['entry']->id)->get();

        $data['file_concept'] = File::where('entry_id', $data['entry']->id)
                                    ->where('name', 'Concept Board')
                                    ->limit(3)
                                    ->orderBy('updated_at', 'DESC')
                                    ->get();

        $data['file_concept_json'] = json_encode($data['file_concept']);

        $data['file_film'] = File::where('entry_id', $data['entry']->id)
                                 ->where('name', 'Film')
                                 ->limit(3)
                                 ->orderBy('updated_at', 'DESC')
                                 ->get();

        $data['file_film_json'] = json_encode($data['file_film']);

        $data['file_posters'] = File::where('entry_id', $data['entry']->id)
                                    ->where('name', 'Posters')
                                    ->limit(3)
                                    ->orderBy('updated_at', 'DESC')
                                    ->get();

        $data['file_posters_json'] = json_encode($data['file_posters']);

        $data['file_audio'] = File::where('entry_id', $data['entry']->id)
                                  ->where('name', 'Audio')
                                  ->limit(3)
                                  ->orderBy('updated_at', 'DESC')
                                  ->get();

        $data['file_audio_json'] = json_encode($data['file_audio']);

        if(!empty($data['entry'])) {
            return view('admin.view', $data);
        } else {
            return redirect('/admin/entries');
        }
    }

    public function downloadFile($id)
    {
        # code...
        $file = File::where("id",$id)->first();
        if(!empty ( $file )) {
            return response()->file(storage_path('app/' . $file->path), [
                'Content-Disposition' => 'inline; filename="'. $file->name .'"'
            ]);
        } else {
            abort(404);
        }
    }

    public function certs($uid,$field)
    {
        # code...
        $entry = Entry::where('uid', $uid)->first();
        return response()->file(storage_path('app/' . $entry[$field]));
    }

    public function updatePayment(Request $request)
    {

        $entry = Entry::where("id",$request->id)->first();
        $entry->payment_status = $request->payment_status;
        $entry->save();

        $user = User::where('id', $entry->user_id)->first();

        // $details = [
        //     'sender'  => config('mail.from.address'),
        //     'name'    => $user->name, 
        //     'email'   => $entry->email,
        //     'fee'     => $entry->fee + ($entry->fee * 0.12),
        //     'subject' => 'KIDLAT AWARDS '.date('Y').' '.$user->name.' , Payment Confirmation',
        //     'view'    => 'email.entry_payment'
        // ];

        // Mail::to($user->email)->send(new UpdatePayment($details));

        return response()->json([
            'header'      => 'Updated!',
            'message'     => 'Successfully updated payment status.',
            'status'      => 'success',
            'status_code' => 200
        ]);
    }

    public function updateStatus(Request $request)
    {

        $entry = Entry::where("id",$request->id)->first();
        $entry->status = $request->status;
        $entry->save();

        $user = User::where('id', $entry->user_id)->first();

        if ($entry->status == "Approved") {
            $entry->status = "Approved";
        } else if ($entry->status == "Reject") {
            $entry->status = "Rejected";
        }

        // $details = [
        //     'sender'  => config('mail.from.address'),
        //     'name'    => $user->name, 
        //     'email'   => $entry->email,
        //     'status'  => $entry->status,
        //     'subject' => 'KIDLAT AWARDS '.date('Y').' '.$user->name.' , Entry Status',
        //     'view'    => 'email.entry_status'
        // ];

        // Mail::to($user->email)->send(new UpdatePayment($details));

        return response()->json([
            'header'      => 'Updated!',
            'message'     => 'Successfully updated entry status.',
            'status'      => 'success',
            'status_code' => 200
        ]);
    }

    public function exportCredits(Request $request)
    {
        $headers = array(
            "Content-type" => "text/csv",
            "Content-Disposition" => "attachment; filename=export_credits_".date('m-d-Y_hia').".csv",
            "Pragma" => "no-cache",
            "Cache-Control" => "must-revalidate, post-check=0, pre-check=0",
            "Expires" => "0"
        );

        if($request->category == "0") {
            $entries = Entry::orderBy('created_at','desc')->where('payment_status','Paid')->where('status','Approved')->get();
        } else {
            $entries = Entry::orderBy('created_at','desc')->where('payment_status','Paid')->where('status','Approved')->where('category',$request->category)->get();
        }

        $columns = array('ID', 'CATEGORY', 'SUB CATEGORY', 'ENTRY NAME', 'NAME', 'POSITION', 'ENTRY STATUS');

        $callback = function() use ($entries, $columns)
        {
            $file = fopen('php://output', 'w');
            fputcsv($file, $columns);

            foreach($entries as $entry) {
                foreach ($entry->credits() as $c) {
                    fputcsv($file, array($entry->uid,$entry->category()->name,$entry->sub_category()->name,$entry->entry_name,$c->name,$c->role, $entry->status));
                }
                
            }
            fclose($file);
        };
        return Response::stream($callback, 200, $headers);
    }

}
