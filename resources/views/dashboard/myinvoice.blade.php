@extends('base')  
@push('styles')
    <style>
        .spinner-grow,
        .loading-label {
            display: none;
        }

        .invoice .form-check {
            display: -webkit-box !important;
        }
    </style>
@endpush
@section('content')
    @include('dashboard.components.header')
    <div class="wrapper-default pt72">
        <div class="container-fluid minHeight py72">
            <div class="invoice d-flex flex-column justify-content-center">
                @if (count($entries) > 0)
                    <h1 class="page-title text-center">
                        Your Invoice
                    </h1>
                    <p class="desc mb-0 text-center">
                        Below is the breakdown of your Kidlat Awards 2022 entry. Click on <br/>
                        the button below so we can send a copy of this to your email!
                    </p>
                    <p class="text-center">
                        <div id="invalid-invoice-request" class="invalid-feedback text-center">Please select entry first to continue.</div>
                    </p>
                    <div class="invoice-table">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th scope="col">Entry</th>
                                    <th scope="col">Category</th>
                                    <th scope="col" width="175">Status</th>
                                    <th scope="col" width="175">Fee VAT EX</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($entries as $entry)
                                    <tr>
                                        <td> 
                                            <div class="form-check form-check-inline">
                                                <input class="form-check-input custom-checkbox" type="checkbox" value="{{$entry->uid}}" data-fee="{{$entry->fee}}" id="checkFee{{$entry->entry_id}}" {{ $entry->payment_status != 'Unpaid' ? 'disabled' : 'checked' }}>
                                                <label class="form-check-label" for="checkFee{{$entry->entry_id}}">{{$entry->entry_name}}</label>
                                            </div> 
                                        </td>
                                        <td>{{ucwords(strtolower($entry->name))}}</td>
                                        <td>{{$entry->payment_status}}</td>
                                        <td class="text-right">PHP {{number_format($entry->fee, 2, ".", ",")}}</td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                    <div class="d-flex flex-column justify-content-end">
                        <div class="d-flex align-items-center justify-content-end total">
                            @if (count($entries) > 1)
                                Total Amount (<div class="total_entry" style="font-size:16px;font-weight:normal">0</div>&nbsp;entries)
                            @else
                            Total Amount (<div class="total_entry" style="font-size:16px;font-weight:normal">0</div>&nbsp;entry)
                            @endif
                            <span class="total_amount">PHP 0.00</span>
                        </div>
                        <div class="d-flex align-items-center justify-content-end vat-label" style="margin-right:122px">
                            <span>Inclusive of 12% VAT</span>
                        </div>
                        <div class="d-flex justify-content-end mt-5">
                            <a href="javascript:void(0)" id="btn_confirm">
                                <button type="button" class="progress-button text-uppercase" style="min-width:284px" data-style="slide-down" data-horizontal>
                                    <span class="confirm-invoice-label link-color">Confirm invoice request</span>
                                </button>
                            </a>
                        </div>
                    </div>
                @else
                    <h1 class="page-title">
                        Your Invoice
                    </h1>
                    <div class="pt72">
                        <div class="container-fluid minHeight py72">
                            <div class="state dark d-flex flex-column justify-content-center align-items-center">
                                <img class="mb-4" src="{{url('/images/no-entries.png')}}" alt="">
                                <h1 class="title mb-2 text-center fw-bold">
                                No invoice yet
                                </h1>
                                <p class="desc mb-0 text-center">
                                    Your invoice will be shown here after submitting an entry.<br/>
                                    Would you like to submit now?
                                </p>
                                <a href="{{url('/submit-entry')}}">
                                    <button type="button" class="btn btn-primary primary lg mt-5" style="min-width:223px">Submit an entry</button>
                                </a>
                            </div>
                        </div>
                    </div>
                @endif
            </div>            
        </div>
    </div>
    @include('dashboard.components.footer')
@push('custom-js')
<script>
    $(document).ready(activateQuery())

    function activateQuery() {

        calculateTotal()

        $("input[type=checkbox]").change(function(){
            if ($('.total_amount').text() == "PHP 0.00") {
                $('.vat-label').css('margin-right', '168px')
            } else {
                $('.vat-label').css('margin-right', '122px')
            }

            calculateTotal()
        })

        function calculateTotal() {
            let sum = 0
            $("input[type=checkbox]:checked").each(function(){
              sum += parseInt($(this).attr('data-fee'))
            })

            sum = sum + (sum * parseFloat(0.12))
            let count_checked = $("[type='checkbox']:checked").length


            $('.total_entry').text(count_checked)
            $('.total_amount').text("PHP " + sum.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",") + ".00")
        }

        let counter = 0
        $(document).on('click', '#btn_confirm', function() {
            $.LoadingOverlay("show", {
                image: '{{url("/images/loading1.png")}}'
            },100);

            // $('.confirm-invoice-label').hide()
            // $('.spinner-grow').addClass('d-inline-block')

            if ($('.total_amount').text() == "PHP 0.00") {
                $.LoadingOverlay("hide");

                // $('.confirm-invoice-label').show()
                // $('.spinner-grow').removeClass('d-inline-block')

                $(this).attr('href', 'javascript:void(0)')

                $('#invalid-invoice-request').show()

                // let html = ""
                // html += "<ul class='list-group'>"
                // html += "<li class='list-group-item text-danger'>Please select entry first to continue.</li>"
                // html += "</ul>"

                // Swal.fire({
                //     icon: 'warning',
                //     title: 'Warning',
                //     html: html,
                //     confirmButtonText: 'Close',
                //     confirmButonColor: 'btn-primary'
                // })
            } else {
                counter++;

                if (counter <= 1) {
                    let ids = [];
                    $('input[type=checkbox]:checked').each(function () {
                        ids.push($(this).val());
                    });

                    let params = {
                        '_token': '{{ csrf_token() }}',
                        'ids'   : ids
                    }

                    fetch("/request-invoice", {
                        method: 'POST',
                        headers: {
                            'Content-Type': 'application/json'
                        },
                        body: JSON.stringify(params)
                    })
                    .then(response => response.json())
                    .then((e) => {
                        if (e.status_code == 200) {
                            $.LoadingOverlay("hide");
                            localStorage.setItem("email", '{{Auth::user()->email}}')
                            setTimeout(function() {
                                location.href = "/invoice-email-sent"
                            }, 1000)
                        }
                    })
                    .catch((error) => {
                    })
                }

            }
        })
    }
</script>
@endpush
@endsection