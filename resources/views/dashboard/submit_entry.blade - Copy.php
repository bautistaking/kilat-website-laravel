@extends('base')  
@push('styles')
    <style>
        html {
            scroll-behavior: smooth;
        }

        .next-loading-label {
            display: none;
        }

        .entry-loader {
            display: none;
            color: rgba(215, 163, 51, 1) !important;
        }

        .spinner-grow,
        .loading-label {
            display: none;
        }

        .cb_container,
        .cv_container,
        .wc_container,
        .ac_container,
        .mc_container,
        .cl_container,
        .fi_container,
        .po_container,
        .au_container,
        .wu_container,
        .ol_container,
        .df_container,
        .et_container,
        .pp_container,
        .jp_container {
            display: none;
        }

        .delete-role {
            position: absolute;
            margin-left: 441px;
            margin-top: -51px;
            font-size: 30px;
            color: #d7a333;
        }

        .delete-link,
        .working-delete-link {
            position: absolute;
            margin-left: 580px;
            margin-top: -50px;
            font-size: 30px;
            color: #d7a333;
        }

        .delete-link:hover,
        .working-delete-link:hover,
        .delete-role:hover {
            color: #223448;
        }

        #btn_submit {
            display: none;
        }

        .list-group-item {
            font-size: 12px;
            text-align: left;
        }

        video {
            max-width: 272px;
            height: auto;
        }

        .dz-error-message {
            margin-top: 20px !important;
        }

        .file-preview-entry > .review-line {
            display: none;
        }

        .category-saved-label,
        .campaign-saved-label,
        .entry-saved-label,
        .written-saved-label,
        .agency-saved-label,
        .all-saved-label,
        .saved-x-mins-label {
            display: none;
        }

        #btn_draft {
            display: none;
        }
    </style>
@endpush

@section('content')
    @include('dashboard.components.header')
    <div class="bg">
        <div class="wrapper-default pt72">
            <div class="container-fluid py72">
                <div class="d-flex align-items-center justify-content-between mb-3">
                    <a href="{{url('/dashboard')}}" class="top-action">
                        <i class="bi bi-chevron-left me-2"></i> <span>Back to home</span>
                    </a>
                    <div class="top-action all-saved-label">
                        <i class="bi bi-check2 me-2"></i> <span>All changes saved</span>
                    </div>
                    <div class="top-action saved-x-mins-label">
                        <i class="bi bi-check2 me-2"></i> <span>Saved <span class="mins-label"></span></span>
                    </div>
                </div>
                <h1 class="page-title mb-4 pb-1 text-center">Submit an entry</h1>
                <div class="entry-tabs">
                        <ul class="nav nav-tabs" id="myTab" role="tablist">
                            <li class="nav-item" role="presentation">
                                <!-- add 'done' class if tab is done -->
                                <button class="nav-link active" id="category-tab" data-bs-toggle="tab" data-bs-target="#category" type="button" role="tab" aria-controls="category" aria-selected="true">Category details</button>
                            </li>
                            <li class="nav-item" role="presentation">
                                <button class="nav-link disabled" id="campaign-tab" data-bs-toggle="tab" data-bs-target="#campaign" type="button" role="tab" aria-controls="campaign" aria-selected="false">Campaign details</button>
                            </li>
                            <li class="nav-item" role="presentation">
                                <button class="nav-link disabled" id="entry-tab" data-bs-toggle="tab" data-bs-target="#entry" type="button" role="tab" aria-controls="entry" aria-selected="false">Entry upload</button>
                            </li>
                            <li class="nav-item" role="presentation">
                                <button class="nav-link disabled" id="written-tab" data-bs-toggle="tab" data-bs-target="#written" type="button" role="tab" aria-controls="written" aria-selected="false">Written submission</button>
                            </li>
                            <li class="nav-item" role="presentation">
                                <button class="nav-link disabled" id="agency-tab" data-bs-toggle="tab" data-bs-target="#agency" type="button" role="tab" aria-controls="agency" aria-selected="false">Agency details</button>
                            </li>
                            <li class="nav-item" role="presentation">
                                <button class="nav-link disabled" id="review-tab" data-bs-toggle="tab" data-bs-target="#review" type="button" role="tab" aria-controls="review" aria-selected="false">Review entry</button>
                            </li>
                        </ul>
                        <div class="tab-content" id="myTabContent">
                            <div class="tab-pane fade show active" id="category" role="tabpanel" aria-labelledby="category-tab">
                                <div class="tab-body">
                                    <div class="mb-5">
                                        <h2 class="heading-title">Category details <span class="link-color category-saved-label">(Saved)</span></h2>
                                        <p class="heading-desc">
                                            When you pick the right category, your chances of getting struck by lightning increases. Pick wisely!
                                        </p>
                                    </div>
                                    <div class="form">
                                        <div class="d-flex justify-content-between">
                                            <div>
                                                <div class="mb-4">
                                                    <label for="category_name" class="form-label fw-500 mb-2">Choose a category</label>
                                                    <select class="form-select form-select-lg custom-select lg" style="width: 390px" id="category_name" aria-label=".form-select-lg example">
                                                        <option selected value="0">SELECT CATEGORY</option>
                                                        @foreach($categories as $category)
                                                        <option value="{{ $category->id }}">{{ $category->name }}</option>
                                                        @endforeach
                                                    </select>
                                                    <div id="invalid-category" class="invalid-feedback">Category is required.</div>
                                                </div>
                                                <div class="mt-1">
                                                    <label for="" class="form-label fw-500">Entry type</label>
                                                    <div class="mb-4">
                                                        <div class="form-check d-flex align-items-center" style="margin-bottom:20px;">
                                                            <input class="form-check-input custom-checkbox" type="radio" name="entry_type" id="entry_type_single" value="SINGLE" data-kb="entry_type">
                                                            <label class="form-check-label fs-18" style="margin-left:14px;" for="entry_type_single">
                                                                Single
                                                            </label>
                                                        </div>
                                                        <div class="form-check d-flex align-items-center">
                                                            <input class="form-check-input custom-checkbox" type="radio" name="entry_type" id="entry_type_campaign" value="CAMPAIGN" data-kb="entry_type">
                                                            <label class="form-check-label fs-18" style="margin-left:14px;" for="entry_type_campaign">
                                                                Campaign (List down all media components)
                                                            </label>
                                                        </div>
                                                        <div id="invalid-entry-type" class="invalid-feedback">Entry type is required.</div>
                                                    </div>
                                                    <textarea class="form-control" id="entry_type_list" rows="3" style="resize: none; width: 480px; text-align: left;" placeholder="E.g. print ad, banner ad, Facebook post" data-kb="entry_type_list" required></textarea>
                                                    <div id="invalid-entry-type-list" class="invalid-feedback">Entry type list is required.</div>
                                                </div>
                                            </div>
                                            <div class="mb-3">
                                                <label for="subcategory" class="form-label fw-500 mb-2">Choose a subcategory</label>
                                                <select class="form-select form-select-lg lg" id="subcategory" style="width: 390px" aria-label=".form-select-lg example" data-kb="subcategory">
                                                    <option selected value="0">SELECT SUB-CATEGORY</option>
                                                    @foreach ($sub_categories as $sub)
                                                    <option class="sub_cat sub_cat_{{ $sub->category_id }}" style="display:none;" value="{{ $sub->id }}">{{ $sub->name }}</option>
                                                    @endforeach
                                                </select>
                                                <div id="invalid-subcategory" class="invalid-feedback">Subcategory is required.</div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane fade show" id="campaign" role="tabpanel" aria-labelledby="campaign-tab">
                                <div class="tab-body">
                                    <div class="mb-5">
                                        <h2 class="heading-title">Campaign details <span class="link-color campaign-saved-label">(Saved)</span></h2>
                                        <p class="heading-desc">
                                            Tell us everything we need to know about your campaign.
                                        </p>
                                    </div>
                                    <div class="form">
                                        <div class="d-flex justify-content-between mb-5" style="column-gap: 25px">
                                            <div style="width: 100%">
                                                <label for="entry_name" class="form-label fw-500 mb-2">Entry name</label>
                                                <input type="text" class="form-control form-control-lg lg fs-18" id="entry_name" placeholder="Your entry name here" data-kb="entry_name">
                                                <div id="invalid-entry-name" class="invalid-feedback">Entry name is required.</div>
                                            </div>
                                            <div style="width: 100%">
                                                <label for="ad_title" class="form-label fw-500 mb-2">Ad title/s</label>
                                                <input type="text" class="form-control form-control-lg lg fs-18" id="ad_title" placeholder="Your ad title here" data-kb="ad_title">
                                                <div id="invalid-ad-title" class="invalid-feedback">Ad title/s is required.</div>
                                            </div>
                                        </div>
                                        <div class="d-flex justify-content-between mb-5" style="column-gap: 25px">
                                            <div style="width: 100%">
                                                <label for="advertiser" class="form-label fw-500 mb-2">Advertiser</label>
                                                <input type="text" class="form-control form-control-lg lg fs-18" id="advertiser" placeholder="Your advertiser here" data-kb="advertiser">
                                                <div id="invalid-advertiser" class="invalid-feedback">Advertiser is required.</div>
                                            </div>
                                            <div style="width: 100%">
                                                <label for="brand" class="form-label fw-500 mb-2">Brand</label>
                                                <input type="text" class="form-control form-control-lg lg fs-18" id="brand" placeholder="Your brand here" data-kb="brand">
                                                <div id="invalid-brand" class="invalid-feedback">Brand is required.</div>
                                            </div>
                                        </div>
                                        <div class="d-flex justify-content-between mb-5" style="column-gap: 25px">
                                            <div style="width: 100%">
                                                <label for="agency_name" class="form-label fw-500 mb-2">Agency</label>
                                                <input type="text" class="form-control form-control-lg lg fs-18" id="agency_name" placeholder="Your agency here" data-kb="agency_name">
                                                <div id="invalid-agency-name" class="invalid-feedback">Agency name is required.</div>
                                            </div>
                                            <div style="width: 100%">
                                                <label for="first_publication_date" class="form-label fw-500 mb-2">Date of first publication</label>
                                                <div class="input-group">
                                                    <span class="input-group-text border-end-0 bg-transparent"><i class="bi bi-calendar" style="color: #61798E;font-size:20px"></i></span>
                                                    <input type="date" class="border-start-0 form-control form-control-lg lg fs-18" id="first_publication_date" placeholder="mm/dd/yyyy" data-kb="first_publication_date">
                                                    <div id="invalid-first-publication-date" class="invalid-feedback">Date of first publication is required.</div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="entry" role="tabpanel" aria-labelledby="entry-tab">
                                <div class="tab-body">
                                    <div class="mb-5">
                                        <h2 class="heading-title">Entry upload <span class="link-color entry-saved-label">(Saved)</span></h2>
                                        <p class="heading-desc">
                                            Include materials that can help you build a strong case.
                                        </p>
                                    </div>

                                    @include('dashboard.entry_upload.files')
                                </div>
                            </div>
                            <div class="tab-pane fade" id="written" role="tabpanel" aria-labelledby="written-tab">   
                                <div class="tab-body">
                                    <div class="mb-5">
                                        <h2 class="heading-title">Written submission <span class="link-color written-saved-label">(Saved)</span></h2>
                                        <p class="heading-desc">
                                            Creative effectiveness is all about the measurable impact of creativity on business results. To help the jury appreciate your entry, please fill up the written submission as succinctly and as clearly as possible.
                                        </p>
                                    </div>
                                    <div class="form">
                                        <div class="mb-5">
                                            <div class="mb-3">
                                                <label for="campaign_summary" class="form-label fw-500 mb-0">Campaign Summary (300 words max)</label><br/>
                                                <p class="span">Provide a summary of the campaign's objectives, strategy, execution and results.</p>
                                            </div>
                                            <textarea class="form-control fs-18 textarea-ws" id="campaign_summary" rows="9" style="resize: none; width: 100%; text-align: left;" data-kb="campaign_summary"></textarea>
                                            <span class="charcount summary_count">0/300</span>
                                            <div id="invalid-campaign-summary" class="invalid-feedback"></div>
                                        </div>
                                        <div class="mb-5">
                                            <div class="mb-3">
                                                <label for="objectives" class="form-label fw-500 mb-0">Objectives (300 words max)</label><br/>
                                                <p class="span">What were the objectives of the creative work?</p>
                                            </div>
                                            <textarea class="form-control fs-18 textarea-ws" id="objectives" rows="9" style="resize: none; width: 100%; text-align: left;" data-kb="objectives" ></textarea>
                                            <span class="charcount objectives_count">0/300</span>
                                            <div id="invalid-objectives" class="invalid-feedback"></div>
                                        </div>
                                        <div class="mb-5">
                                            <div class="mb-3">
                                                <label for="strategy" class="form-label fw-500 mb-0">Strategy (300 words max)</label><br/>
                                                <p class="span">Share the thinking behind the work. What were the insights on the business, product, <br/>culture, channels and/or target audience that allowed you to arrive at the critical strategy?</p>
                                            </div>
                                            <textarea class="form-control fs-18 textarea-ws" id="strategy" rows="9" style="resize: none; width: 100%; text-align: left;" data-kb="strategy" ></textarea>
                                            <span class="charcount strategy_count">0/300</span>
                                            <div id="invalid-strategy" class="invalid-feedback"></div>
                                        </div>
                                        <div class="mb-5">
                                            <div class="mb-3">
                                                <label for="execution" class="form-label fw-500 mb-0">Execution (300 words max)</label><br/>
                                                <p class="span">Describe and explain the resulting work. How did the creative execution <br/>amplify and enhance effectiveness?</p>
                                            </div>
                                            <textarea class="form-control fs-18 textarea-ws" id="execution" rows="9" style="resize: none; width: 100%; text-align: left;" data-kb="execution" ></textarea>
                                            <span class="charcount execution_count">0/300</span>
                                            <div id="invalid-execution" class="invalid-feedback"></div>
                                        </div>
                                        <div class="mb-5">
                                            <div class="mb-3">
                                                <label for="results" class="form-label fw-500 mb-0">Results (300 words max)</label><br/>
                                                <p class="span">Why do you think this case is a stellar example of creative effectiveness? Share how the work successfully achieved <br/>the business objectives. Provide specific results and data, make sure these are attributable to the campaign.</p>
                                            </div>
                                            <textarea class="form-control fs-18 textarea-ws" id="results" rows="9" style="resize: none; width: 100%; text-align: left;" data-kb="results"></textarea>
                                            <span class="charcount results_count">0/300</span>
                                            <div id="invalid-results" class="invalid-feedback"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="agency" role="tabpanel" aria-labelledby="agency-tab">
                                <div class="tab-body">
                                    <div class="mb-5">
                                        <h2 class="heading-title">Agency details <span class="link-color agency-saved-label">(Saved)</span></h2>
                                        <p class="heading-desc">
                                            Let us know who to contact should the need for clarifications arises.
                                        </p>
                                    </div>
                                    <div class="form">
                                        <div class="d-flex justify-content-between" style="column-gap: 25px">
                                            <div class="mt-1">
                                                <label for="" class="form-label fw-500">4As membership</label>
                                                <div class="mb-4">
                                                    <div class="form-check d-flex align-items-center" style="margin-bottom:20px;width:100%">
                                                        <input class="form-check-input custom-checkbox" type="radio" name="member" id="member_yes" value="Yes">
                                                        <label class="form-check-label fs-18" style="margin-left:14px;" for="entry_type_single">
                                                            I'm a member
                                                        </label>
                                                    </div>
                                                    <div class="form-check d-flex align-items-center">
                                                        <input class="form-check-input custom-checkbox" type="radio" name="member" id="member_no" value="No">
                                                        <label class="form-check-label fs-18" style="margin-left:14px;" for="entry_type_campaign">
                                                            I'm a non-member
                                                        </label>
                                                    </div>
                                                    <div id="invalid-member" class="invalid-feedback">Member type is required.</div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="d-flex justify-content-between" style="column-gap: 25px">
                                            <div class="mb-5" style="width:50%;">
                                                <label for="contact_person" class="form-label fw-500 mb-2">Name of agency contact person</label>
                                                <input type="text" class="form-control form-control-lg lg fs-18" id="contact_person" placeholder="Your name here" data-kb="contact_person">
                                                <div id="invalid-contact-person" class="invalid-feedback">Contact person is required.</div>
                                            </div>
                                            <div class="mb-5" style="width:50%;">
                                                <label for="position" class="form-label fw-500 mb-2">Position</label>
                                                <input type="text" class="form-control form-control-lg lg fs-18" id="position" placeholder="Your position here" data-kb="position">
                                                <div id="invalid-position" class="invalid-feedback">Position is required.</div>
                                            </div>
                                        </div>
                                        <div class="d-flex justify-content-between" style="column-gap: 25px">
                                            <div class="mb-5" style="width:50%;">
                                                <label for="phone_number" class="form-label fw-500 mb-2">Phone Number</label>
                                                <input type="number" class="form-control form-control-lg lg fs-18" id="phone_number" value="+63 " data-kb="phone_number">
                                                <div id="invalid-phone-number" class="invalid-feedback">Phone number is required.</div>
                                            </div>
                                            <div class="mb-5" style="width:50%;">
                                                <label for="email_address" class="form-label fw-500 mb-2">Email Address</label>
                                                <input type="text" class="form-control form-control-lg lg fs-18" id="email_address" placeholder="name@name.com" data-kb="email_address" value="{{auth()->user()->email}}">
                                                <div id="invalid-email-address" class="invalid-feedback"></div>
                                            </div>
                                        </div>
                                        <div class="d-flex justify-content-between" style="column-gap: 25px">
                                            <div class="mt-1">
                                                <div class="mb-4">
                                                    <div class="form-check d-flex align-items-center" style="margin-bottom:20px;width:100%">
                                                        <input class="form-check-input custom-checkbox" type="checkbox" name="first_timer" id="first_timer" value="1">
                                                        <label class="form-check-label fs-18" style="margin-left:14px;" for="entry_type_single">
                                                            1st Time to Kidlat Awards
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="divider" style="margin: 70px 0"></div>
                                    <div class="mb-5">
                                        <h2 class="heading-title">Credits</h2>
                                        <p class="heading-desc">
                                            Tell us who are the people involved in this project and their respective roles.
                                        </p>
                                    </div>
                                    <div class="form credits-container">
                                        <div class="d-flex justify-content-between mb-4 credit-roles" style="column-gap: 25px">
                                            <div style="width: 100%">
                                                <label for="" class="form-label mb-2">Name</label>
                                                <input type="text" class="form-control form-control-lg lg fs-18" id="role_name0" name="role_name[]" placeholder="Name" data-kb="credit_name0">
                                            </div>
                                            <div style="width: 100%">
                                                <label for="" class="form-label mb-2">Role</label>
                                                <input type="text" class="form-control form-control-lg lg fs-18" id="role0" name="role[]" placeholder="Role" data-kb="credit_role0">
                                                <a href="javascript:void(0)" class="delete-role"><i class="fas fa-times"></i></a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="mt-5 mx-auto text-center">
                                        <button class="btn btn-outline-primary primary brown small mt-3 btn_more_roles text-uppercase" style="min-width:191px">Add more rows</button>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="review" role="tabpanel" aria-labelledby="review-tab">
                                
                                <div class="tab-body review" style="scroll-behavior: smooth;">
                                    <div class="mb-5">
                                        <h2 class="page-title text-center mb-0">Double check everything before you proceed. Leave no one behind!</h2>
                                        <div class="links d-flex justify-content-center">
                                            <a href="#category-details">Category details</a>
                                            <a href="#campaign-details">Campaign details</a>
                                            <a href="#entry-upload">Entry upload</a>
                                            <a href="#written-submission" id="preview-ws-link">Written submission</a>
                                            <a href="#agency-details">Agency details</a>
                                        </div>
                                    </div>
                                    <div id="category-details" class="review-card mb-5">
                                        <div class="d-flex align-items-start justify-content-between mb-3">
                                            <div>
                                                <h3>Category details</h3>
                                                <p>When you pick the right category, your chances of getting struck by lightning increases. Pick wisely!</p>
                                            </div>
                                            <a href="javascript:void(0)" class="action d-flex align-items-center edit-category">
                                                <i class="bi bi-pencil-square me-2"></i>
                                                <span>Edit details</span>
                                            </a>
                                        </div>
                                        <div class="review-line review-line-category">
                                            <div class="label">Category</div>
                                            <div class="value"></div>
                                        </div>
                                        <div class="review-line review-line-subcategory">
                                            <div class="label">Subcategory</div>
                                            <div class="value text-uppercase" data-kb="subcategory"></div>
                                        </div>
                                        <div class="review-line">
                                            <div class="label">Entry type</div>
                                            <div class="value rl_entry_type" data-kb="entry_type"></div>
                                        </div>
                                        <div class="review-line review-line-entry_type_list">
                                            <div class="label">Entry type list</div>
                                            <div class="value"></div>
                                        </div>
                                    </div>
                                    <div id="campaign-details" class="review-card mb-5">
                                        <div class="d-flex align-items-start justify-content-between mb-3">
                                            <div>
                                                <h3>Campaign details</h3>
                                                <p>Tell us everything we need to know about your campaign.</p>
                                            </div>
                                            <a href="#" class="action d-flex align-items-center edit-campaign">
                                                <i class="bi bi-pencil-square me-2"></i>
                                                <span>Edit details</span>
                                            </a>
                                        </div>
                                        <div class="review-line">
                                            <div class="label">Entry name</div>
                                            <div class="value" data-kb="entry_name"></div>
                                        </div>
                                        <div class="review-line">
                                            <div class="label">Ad title/s</div>
                                            <div class="value" data-kb="ad_title"></div>
                                        </div>
                                        <div class="review-line">
                                            <div class="label">Advertiser</div>
                                            <div class="value" data-kb="advertiser"></div>
                                        </div>
                                        <div class="review-line">
                                            <div class="label">Brand</div>
                                            <div class="value" data-kb="brand"></div>
                                        </div>
                                        <div class="review-line">
                                            <div class="label">Agency</div>
                                            <div class="value" data-kb="agency_name"></div>
                                        </div>--
                                        <div class="review-line">
                                            <div class="label">Date of first publication</div>
                                            <div class="value" data-kb="first_publication_date"></div>
                                        </div>
                                    </div>

                                    <div id="entry-upload" class="review-card mb-5">
                                        <div class="d-flex align-items-start justify-content-between mb-3">
                                            <div>
                                                <h3>Entry upload</h3>
                                                <p>Include materials that can help you build a strong case.</p>
                                            </div>
                                            <a href="#" class="action d-flex align-items-center edit-entry">
                                                <i class="bi bi-pencil-square me-2"></i>
                                                <span>Edit details</span>
                                            </a>
                                        </div>
                                        <div class="file-preview-entry">
                                            <div class="review-line">
                                                <div class="label">Concept board</div>
                                                <div class="value">
                                                    <div class="img mt-2">
                                                        <img id="preview_concept_board_dropzone" src="/images/image.png" alt="">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="review-line">
                                                <div class="label">Case video</div>
                                                <div class="value">
                                                    <div class="img mt-2">
                                                        <video id="preview_case_study_video_dropzone" controls>
                                                            <source src="/images/video.png" type="video/mp4">
                                                            Your browser does not support the video tag.
                                                        </video>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="review-line">
                                                <div class="label">Written Case Study</div>
                                                <div class="value">
                                                    <div class="img mt-2">
                                                        <img id="preview_written_case_study_dropzone" src="/images/image.png" alt="">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="review-line">
                                                <div class="label">ASC Clearance</div>
                                                <div class="value">
                                                    <!-- add condition if img or pdf display -->
                                                    <div class="img mt-2">
                                                        <img id="preview_asc_clearance_dropzone" src="/images/image.png" alt="">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="review-line">
                                                <div class="label">Media Certification of Performance</div>
                                                <div class="value">
                                                    <div class="img mt-2">
                                                        <!-- add condition if img or pdf display -->
                                                        <img id="preview_media_certification_dropzone" src="/images/image.png" alt="">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="review-line">
                                                <div class="label">Client Certification</div>
                                                <div class="value">
                                                    <div class="img mt-2">
                                                        <!-- add condition if img or pdf display -->
                                                        <img id="preview_client_certification_dropzone" src="/images/image.png" alt="">
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="review-line">
                                                <div class="label">Film</div>
                                                <div class="value">
                                                    <div class="img mt-2">
                                                        <video id="preview_film_dropzone" controls>
                                                            <source src="/images/video.png" type="video/mp4">
                                                            Your browser does not support the video tag.
                                                        </video>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="review-line">
                                                <div class="label">Posters</div>
                                                <div class="value">
                                                    <div class="img mt-2">
                                                        <img id="preview_posters_dropzone" src="/images/image.png" alt="">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="review-line">
                                                <div class="label">Audio</div>
                                                <div class="value">
                                                    <audio id="preview_audio_dropzone" controls>
                                                        <source src="#" type="audio/mpeg">
                                                        Your browser does not support the audio element.
                                                    </audio>
                                                </div>
                                            </div>
                                            <div class="review-line">
                                                <div class="label">Working URL</div>
                                                <div class="value">
                                                    <div class="preview_working_url_container"></div>
                                                </div>
                                            </div>
                                            <div class="review-line">
                                                <div class="label">Online links</div>
                                                <div class="value">
                                                    <div class="preview_online_link_container"></div>
                                                </div>
                                            </div>
                                            <div class="review-line">
                                                <div class="label">Demo Film (if video)</div>
                                                <div class="value">
                                                    <div class="img mt-2">
                                                        <video id="preview_demo_film_dropzone" controls>
                                                            <source src="/images/video.png" type="video/mp4">
                                                            Your browser does not support the video tag.
                                                        </video>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="review-line">
                                                <div class="label">English Translation if needed</div>
                                                <div class="value">
                                                    <div class="img mt-2">
                                                        <embed type="application/pdf" 
                                                        id="preview_english_translation_dropzone"
                                                        src=""
                                                        frameBorder="0"
                                                        height="100%"
                                                        width="100%">                                                        
                                                        </embed>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="review-line">
                                                <div class="label">JPEG of the Piece</div>
                                                <div class="value">
                                                    <div class="img mt-2">
                                                        <img id="preview_photo_piece_dropzone" src="/images/image.png" alt="">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="review-line">
                                                <div class="label">JPEG of Entry</div>
                                                <div class="value">
                                                    <div class="img mt-2">
                                                        <img id="preview_entry_dropzone" src="/images/image.png" alt="">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div id="written-submission" class="review-card mb-5">
                                        <div class="d-flex align-items-start justify-content-between mb-3">
                                            <div>
                                                <h3>Written submission</h3>
                                                <p>Write everything that needs to be said about your work.</p>
                                            </div>
                                            <a href="#" class="action d-flex align-items-center edit-written-submission">
                                                <i class="bi bi-pencil-square me-2"></i>
                                                <span>Edit details</span>
                                            </a>
                                        </div>
                                        <div class="review-line flex-column">
                                            <h3>Creative effectiveness</h3>
                                            <p>
                                                Creative Effectiveness celebrates the measurable impact of creativity on business results.<br/>
                                                To help the jury appreciate your entry, please fill up the written submission as succinctly and clearly as possible.
                                            </p>
                                        </div>
                                        <div class="review-line flex-column">
                                            <h3>Campaign Summary (<span class="p_summary_count">0</span> words)</h3>
                                            <p data-kb="campaign_summary"></p>
                                        </div>
                                        <div class="review-line flex-column">
                                            <h3>Objectives (<span class="p_objectives_count">0</span> words)</h3>
                                            <p data-kb="objectives"></p>
                                        </div>
                                        <div class="review-line flex-column">
                                            <h3>Strategy (<span class="p_strategy_count">0</span> words)</h3>
                                            <p data-kb="strategy"></p>
                                        </div>
                                        <div class="review-line flex-column">
                                            <h3>Execution (<span class="p_execution_count">0</span> words)</h3>
                                            <p data-kb="execution"></p>
                                        </div>
                                        <div class="review-line flex-column">
                                            <h3>Results (<span class="p_results_count">0</span> words)</h3>
                                            <p data-kb="results"></p>
                                        </div>
                                    </div>
                                    <div id="agency-details" class="review-card">
                                        <div class="d-flex align-items-start justify-content-between mb-3">
                                            <div>
                                                <h3>Agency details</h3>
                                                <p>Let us know who to contact should the need for clarifications arises.</p>
                                            </div>
                                            <a href="#" class="action d-flex align-items-center edit-agency">
                                                <i class="bi bi-pencil-square me-2"></i>
                                                <span>Edit details</span>
                                            </a>
                                        </div>
                                        <div class="review-line">
                                            <div class="label">4As membership</div>
                                            <div class="value rl_member" data-kb="member"></div>
                                        </div>
                                        <div class="review-line">
                                            <div class="label">Name of agency contact person</div>
                                            <div class="value" data-kb="contact_person"></div>
                                        </div>
                                        <div class="review-line">
                                            <div class="label">Position</div>
                                            <div class="value" data-kb="position"></div>
                                        </div>
                                        <div class="review-line">
                                            <div class="label">Phone number</div>
                                            <div class="value" data-kb="phone_number"></div>
                                        </div>
                                        <div class="review-line">
                                            <div class="label">Email address</div>
                                            <div class="value" data-kb="email_address"></div>
                                        </div>
                                        <div class="review-line">
                                            <div class="label">1st Time to Kidlat Awards</div>
                                            <div class="value" data-kb="first_timer"></div>
                                        </div>
                                        <div class="review-line">
                                            <div class="label">Credits</div>
                                            <div class="value">
                                                <div class="preview_credits_container"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-actions d-flex align-items-center justify-content-between">
                            <a href="javascript:void(0)" class="top-action btn_previous">
                                <i class="bi bi-chevron-left"></i><span class="ms-2">Back</span>
                            </a>
                            <a href="javascript:void(0)" class="top-action btn_next">
                                <span class="me-2">Save & Continue</span> <i class="bi bi-chevron-right"></i>
                                <span class="next-loading-label link-color">Saving&nbsp;</span>
                                <div class="spinner-border text-warning spinner-border-sm entry-loader" style="" role="status">
                                    <span class="visually-hidden">Loading...</span>
                                </div>
                            </a>
                            <button class="btn btn-outline-primary primary small text-uppercase link-color" id="btn_draft" style="min-width:175px;position:absolute;margin-left:617px">
                                <span class="spinner-grow spinner-grow-sm" role="status"></span>
                                <span class="spinner-grow spinner-grow-sm" role="status"></span>
                                <span class="spinner-grow spinner-grow-sm" role="status"></span>
                                <span class="draft-label">Save as draft</span>
                            </button>
                            <button class="btn btn-primary primary small text-uppercase" id="btn_submit" style="min-width:175px">
                                <span class="spinner-grow spinner-grow-sm" role="status"></span>
                                <span class="spinner-grow spinner-grow-sm" role="status"></span>
                                <span class="spinner-grow spinner-grow-sm" role="status"></span>
                                <span class="submit-label">Submit entry</span>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @include('dashboard.components.footer')

@push('custom-js')
<script>
    Dropzone.autoDiscover = false;
    var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
    var entry_uid = 0;
    var url_link_id = 1
    var working_url_link_id = 1
    var role_name_id = 1
    var role_id = 1

    $( document ).ready(function() {

        activateDropzone();
        dropzoneFileSizeLabel();

        // CATEGORY SELECTED
        $('#category_name').on('change', function() {
            $('.sub_cat').hide();
            $('.sub_cat_'+this.value).show();
        });

        // DISABLED 
        $("input[name='entry_type']").on('change', function() {
            if($(this).val() == 'CAMPAIGN') {
                $('#entry_type_list').removeAttr("disabled");   
            }
            else {
                $('#entry_type_list').attr("disabled", "disabled");   
            }
        });

        $('.btn_next').on('click', function() {
            $('#btn_submit').hide();
            $('#btn_draft').hide();

            if ($('#category-tab').hasClass('active')) {
                // tab 1 validation
                var checker = 0
                if ($('#category_name').val() == "NONE") {
                    checker++;
                    $('#category_name').addClass('is-invalid')
                    $('#invalid-category').show()
                } else {
                    $('#category_name').removeClass('is-invalid')
                    $('#invalid-category').hide()
                }

                if ($('#subcategory').val() == "0") {
                    checker++;
                    $('#subcategory').addClass('is-invalid')
                     $('#invalid-subcategory').show()
                } else {
                    $('#subcategory').removeClass('is-invalid')
                     $('#invalid-subcategory').hide()
                }

                if (!$('input[name="entry_type"]').is(':checked')) {
                    checker++;
                    $('input[name="entry_type"]').addClass('is-invalid')
                    $('#invalid-entry-type').show()
                } else {
                    $('input[name="entry_type"]').removeClass('is-invalid')
                    $('#invalid-entry-type').hide()
                }

                if ($('input[name="entry_type"]:checked').val() == "campaign") {
                    if ($('#entry_type_list').val() == "") {
                        checker++;
                        $('#entry_type_list').addClass('is-invalid')
                         $('#invalid-entry-type-list').show()
                    } else {
                        $('#entry_type_list').removeClass('is-invalid')
                        $('#invalid-entry-type-list').hide()
                    }
                } else {
                    $('#entry_type_list').removeClass('is-invalid')
                    $('#invalid-entry-type-list').hide()
                }

                if (checker == 0) {
                    $('#campaign-tab').removeClass('disabled')
                    submitCategory()
                    setTimeout(function() {
                        activateCampaign()
                    }, 100)
                } else {
                    $('#campaign-tab').addClass('disabled')
                }
            } 
            else if ($('#campaign-tab').hasClass('active')) {
                // tab 2 validation
                var checker = 0
                if ($('#entry_name').val() == "") {
                    checker++;
                    $('#entry_name').addClass('is-invalid')
                    $('#invalid-entry-name').show()
                } else {
                    $('#entry_name').removeClass('is-invalid')
                    $('#invalid-entry-name').hide()
                }

                if ($('#ad_title').val() == "") {
                    checker++;
                    $('#ad_title').addClass('is-invalid')
                    $('#invalid-ad-title').show()
                } else {
                    $('#ad_title').removeClass('is-invalid')
                    $('#invalid-ad-title').hide()
                }

                if ($('#advertiser').val() == "") {
                    checker++;
                    $('#advertiser').addClass('is-invalid')
                    $('#invalid-advertiser').show()
                } else {
                    $('#advertiser').removeClass('is-invalid')
                    $('#invalid-advertiser').hide()
                }

                if ($('#brand').val() == "") {
                    checker++;
                    $('#brand').addClass('is-invalid')
                    $('#invalid-brand').show()
                } else {
                    $('#brand').removeClass('is-invalid')
                    $('#invalid-brand').hide()
                }

                if ($('#agency_name').val() == "") {
                    checker++;
                    $('#agency_name').addClass('is-invalid')
                    $('#invalid-agency-name').show()
                } else {
                    $('#agency_name').removeClass('is-invalid')
                    $('#invalid-agency-name').hide()
                }

                if ($('#first_publication_date').val() == "") {
                    checker++;
                    $('#first_publication_date').addClass('is-invalid')
                    $('#invalid-first-publication-date').show()
                } else {
                    $('#first_publication_date').removeClass('is-invalid')
                    $('#invalid-first-publication-date').hide()
                }

                if (checker == 0) {
                    $('#entry-tab').removeClass('disabled')
                    submitCampaign()
                    setTimeout(function() {
                        activateEntry()
                    }, 100)
                } else {
                    $('#entry-tab').addClass('disabled')
                }
            } 
            else if ($('#entry-tab').hasClass('active')) {
                // tab 3 validation
                var checker = 0
                if (checker == 0) {
                    $('#written-tab').removeClass('disabled')
                    submitFile()
                    setTimeout(function() {
                        activateWritten()
                    }, 100)
                } else {
                    $('#written-tab').addClass('disabled')
                }
            }
            else if ($('#written-tab').hasClass('active')) {
                // tab 4 validation
                var checker = 0

                $('.p_summary_count').text($('.summary_count').text().split('/')[0])
                $('.p_objectives_count').text($('.objectives_count').text().split('/')[0])
                $('.p_strategy_count').text($('.strategy_count').text().split('/')[0])
                $('.p_execution_count').text($('.execution_count').text().split('/')[0])
                $('.p_results_count').text($('.results_count').text().split('/')[0])


                if ($('#campaign_summary').val() == "") {
                    checker++;
                    $('#campaign_summary').addClass('is-invalid')
                    $('#invalid-campaign-summary').text('Campaign Summary is required.').show()
                } else {
                    if ($('#campaign_summary').next().text().indexOf('-') > -1) {
                        checker++;
                        $('#campaign_summary').addClass('is-invalid')
                        $('#invalid-campaign-summary').text('Campaign Summary exceeded the 300 max word limit.').show()
                    } else {
                        $('#campaign_summary').removeClass('is-invalid')
                        $('#invalid-campaign-summary').hide()
                    }
                }

                if ($('#objectives').val() == "") {
                    checker++;
                    $('#objectives').addClass('is-invalid')
                    $('#invalid-objectives').text('Objectives is required.').show()
                } else {
                    if ($('#objectives').next().text().indexOf('-') > -1) {
                        checker++;
                        $('#objectives').addClass('is-invalid')
                        $('#invalid-objectives').text('Objectives exceeded the 300 max word limit.').show()
                    } else {
                        $('#objectives').removeClass('is-invalid')
                        $('#invalid-objectives').hide()
                    }
                }

                if ($('#strategy').val() == "") {
                    checker++;
                    $('#strategy').addClass('is-invalid')
                    $('#invalid-strategy').text('Strategy is required.').show()
                } else {
                    if ($('#strategy').next().text().indexOf('-') > -1) {
                        checker++;
                        $('#strategy').addClass('is-invalid')
                        $('#invalid-strategy').text('Strategy exceeded the 300 max word limit.').show()
                    } else {
                        $('#strategy').removeClass('is-invalid')
                        $('#invalid-strategy').hide()
                    }
                }

                if ($('#execution').val() == "") {
                    checker++;
                    $('#execution').addClass('is-invalid')
                    $('#invalid-execution').text('Execution is required.').show()
                } else {
                    if ($('#execution').next().text().indexOf('-') > -1) {
                        checker++;
                        $('#execution').addClass('is-invalid')
                        $('#invalid-execution').text('Execution exceeded the 300 max word limit.').show()
                    } else {
                        $('#execution').removeClass('is-invalid')
                        $('#invalid-execution').hide()
                    }
                }

                if ($('#results').val() == "") {
                    checker++;
                    $('#results').addClass('is-invalid')
                    $('#invalid-results').text('Result is required.').show()
                } else {
                    if ($('#results').next().text().indexOf('-') > -1) {
                        checker++;
                        $('#results').addClass('is-invalid')
                        $('#invalid-results').text('Results exceeded the 300 max word limit.').show()
                    } else {
                        $('#results').removeClass('is-invalid')
                        $('#invalid-results').hide()
                    }
                }

                if (checker == 0) {
                    $('#agency-tab').removeClass('disabled')
                    submitWritten()
                    setTimeout(function() {
                        activateAgencyDetails()
                    }, 100)
                } else {
                    $('#agency-tab').addClass('disabled')
                }
            } 
            else if ($('#agency-tab').hasClass('active')) {
                // tab 5 validation
                reviewFile()
                getLinks()
                setTimeout(function() {
                    getCredits()
                }, 1000)

                if (typeof($('.client_certification_dropzone').get(0).dropzone.files[0]) != "undefined") {
                    $('#btn_submit').show()
                    $('#btn_draft').show()
                } else {
                    $('#btn_submit').hide()
                    $('#btn_draft').show()
                }

                var checker = 0
                if (!$('input[name="member"]').is(':checked')) {
                    checker++;
                    $('input[name="member"]').addClass('is-invalid')
                    $('#invalid-member').show()
                } else {
                    $('input[name="member"]').removeClass('is-invalid')
                    $('#invalid-member').hide()
                }

                if ($('#contact_person').val() == "") {
                    checker++;
                    $('#contact_person').addClass('is-invalid')
                    $('#invalid-contact-person').show()
                } else {
                    $('#contact_person').removeClass('is-invalid')
                    $('#invalid-contact-person').hide()
                }

                if ($('#position').val() == "") {
                    checker++;
                    $('#position').addClass('is-invalid')
                    $('#invalid-position').show()
                } else {
                    $('#position').removeClass('is-invalid')
                    $('#invalid-position').hide()
                }

                if ($('#phone_number').val() == "") {
                    checker++;
                    $('#phone_number').addClass('is-invalid')
                    $('#invalid-phone-number').show()
                } else {
                    $('#phone_number').removeClass('is-invalid')
                    $('#invalid-phone-number').hide()
                }

                if ($('#email_address').val() == "") {
                    checker++;
                    $('#email_address').addClass('is-invalid')
                    $('#invalid-email-address').text('Email address is required.').show()
                } else {
                    var email_filter = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
                    if (!email_filter.test($('#email_address').val())) {
                        checker++;
                        $('#email_address').addClass('is-invalid')
                        $('#invalid-email-address').text('Email address is invalid.').show()
                    } else {
                        $('#email_address').removeClass('is-invalid')
                        $('#invalid-email-address').hide()

                    }

                }

                if (checker == 0) {
                    $('#review-tab').removeClass('disabled')
                    submitAgency()
                    setTimeout(function() {
                        reviewEntry()
                    }, 100)
                } else {
                    $('#review-tab').addClass('disabled')
                }
            }

            window.scrollTo(0, 0);

            $('.review-line-category .value').text($( "#category_name option:selected" ).text()) 
            $('.review-line-subcategory .value').text($( "#subcategory option:selected" ).text()) 
            $('.rl_entry_type').text($("input[name='entry_type']:checked").val())
            $('.review-line-entry_type_list .value').text($( "#entry_type_list" ).val())             
            $('.review-line .value[data-kb="email_address"]').text($('#email_address').val())          

            var first_timer = (!$('input[name="first_timer"]:checked').val()) ? 'No' : 'Yes';
            $('.review-line .value[data-kb="first_timer"]').text(first_timer)

        });

        $('#btn_submit').on('click', function() {
            submitReview();
        });

    });

    function getMinutesAgo() {
        let param = {
            'uid': entry_uid
        }

        fetch("/minutes-passed", {
            method: 'POST',
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(param)
        })
        .then(response => response.json())
        .then((data) => {
            $('.saved-x-mins-label').show()
            $('.mins-label').text(data.minutes_passed) 
        })
    }

    function submitCategory() {
        $('.next-loading-label').show()
        $('.entry-loader').show()

        let form_data = new FormData()

        form_data.append('_token', '{{ csrf_token() }}')
        form_data.append('uid', entry_uid)
        form_data.append('category', $('#category_name').val())
        form_data.append('sub_category', $('#subcategory').val())
        form_data.append('type', $('input[name="entry_type"]:checked').val())
        form_data.append('entry_type_list', $('#entry_type_list').val())

        fetch("/submit-category", {
            method: 'POST',
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            body: form_data
        })
        .then(response => response.json())
        .then((e) => {
            if (e.status_code == 200) {
                entry_uid = e.data.uid;
                $('.next').addClass('disabled').attr('disabled', true);
                $.LoadingOverlay("hide");

                $('.next-loading-label').hide()
                $('.entry-loader').hide()
                $('#category-tab').addClass('done')

                setTimeout(function() {
                    $('.next').removeClass('disabled').attr('disabled', false);
                    getMinutesAgo()
                }, 100)
            }
        })
    }

    function submitCampaign() {
        $('.next-loading-label').show()
        $('.entry-loader').show()

        let form_data = new FormData()

        form_data.append('_token', '{{ csrf_token() }}')
        form_data.append('uid', entry_uid)
        form_data.append('entry_name', $('#entry_name').val())
        form_data.append('ad_details', $('#ad_title').val())
        form_data.append('advertiser', $('#advertiser').val())
        form_data.append('brand', $('#brand').val())
        form_data.append('agency', $('#agency_name').val())
        form_data.append('date_publication', $('#first_publication_date').val())

        fetch("/submit-campaign", {
            method: 'POST',
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            body: form_data
        })
        .then(response => response.json())
        .then((e) => {
            if (e.status_code == 200) {
                entry_uid = e.data.uid;
                $('.next').addClass('disabled').attr('disabled', true);
                $.LoadingOverlay("hide");

                $('.next-loading-label').hide()
                $('.entry-loader').hide()
                $('#campaign-tab').addClass('done')

                setTimeout(function() {
                    $('.next').removeClass('disabled').attr('disabled', false);
                }, 100)
            }
        })
    }

    // ALL FUNCTIONS START HERE
    function submitReview() {
        $.LoadingOverlay("show", {
            image: '{{url("/images/loading1.png")}}'
        },100);

        $('.submit-label').hide()
        $('#btn_submit .spinner-grow').addClass('d-inline-block')

        let form_data = new FormData()

        form_data.append('_token', '{{ csrf_token() }}')
        form_data.append('uid', entry_uid)
        form_data.append('status', "Pending")

        fetch("/submit-review", {
            method: 'POST',
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            body: form_data
        })
        .then(response => response.json())
        .then((e) => {
            if (e.status_code == 200) {
                entry_uid = e.data.uid;
                $('.next').addClass('disabled').attr('disabled', true);
                $.LoadingOverlay("hide");
                // $('.category-saved-label').show()
                $('#review-tab').addClass('done')

                setTimeout(function() {
                    if (typeof $('.client_certification_dropzone').get(0).dropzone.files[0] != "undefined") {
                        location.href = "/dashboard/success/"+e.data.uid
                    } else {
                        location.href = "/dashboard"
                    }
                }, 2000)
            }
        })
    }

    function reviewEntry() {
        remove_class()
        $('#review-tab').addClass('active')
        $('#review').addClass('active show')
    }

    function activateDropzone() {        
        case_study_video_dropzone = new Dropzone('.case_study_video_dropzone', {
            clickable: '.file-text > p, .dropzone > img',
            addRemoveLinks: true,
            autoProcessQueue: true,
            uploadMultiple: false,
            parallelUploads: 1,
            maxFiles: 1,
            maxFilesize: 150,
            dictDefaultMessage: "",
            paramName: 'case_study_video_dropzone',
            clickable: true,
            acceptedFiles: 'video/mp4',
            url: '/submit-file',
            headers: {
                'x-csrf-token': CSRF_TOKEN,
            },
            init: function () {
                this.on("addedfile", function (data) {
                    extensionChecker(data)
                    let file_url = window.URL.createObjectURL($('.case_study_video_dropzone').get(0).dropzone.files[0]);
                    $("#preview_case_study_video_dropzone").attr("src", file_url);
                });
                this.on('sending', function (file, xhr, formData) {
                    submitFile('case_study_video_dropzone')
                });
            },
            reset: function () {
                this.removeAllFiles(true);
            }
        });

        concept_board_dropzone = new Dropzone('.concept_board_dropzone', {
            addRemoveLinks: true,
            autoProcessQueue: true,
            uploadMultiple: false,
            parallelUploads: 1,
            maxFiles: 1,
            maxFilesize: 10,
            dictDefaultMessage: "",
            paramName: 'concept_board_dropzone',
            clickable: true,
            acceptedFiles: 'image/jpeg',
            url: '{{url("/submit-file")}}',
            headers: {
                'x-csrf-token': CSRF_TOKEN,
            },
            init: function () {
                this.on("addedfile", function (data) {
                    extensionChecker(data)
                    let file_url = window.URL.createObjectURL($('.concept_board_dropzone').get(0).dropzone.files[0]);
                    $("#preview_concept_board_dropzone").attr("src", file_url);
                });
                this.on('sending', function (file, xhr, formData) {
                    submitFile('concept_board_dropzone')
                });
            },
            reset: function () {
                this.removeAllFiles(true);
            }
        });
        
        written_case_study_dropzone = new Dropzone('.written_case_study_dropzone', {
            clickable: '.file-text > p, .dropzone > img',
            addRemoveLinks: true,
            autoProcessQueue: true,
            uploadMultiple: false,
            parallelUploads: 1,
            maxFiles: 1,
            maxFilesize: 10,
            dictDefaultMessage: "",
            paramName: 'written_case_study_dropzone',
            clickable: true,
            acceptedFiles: 'image/jpeg',
            url: '/submit-file',
            headers: {
                'x-csrf-token': CSRF_TOKEN,
            },
            init: function () {
                this.on("addedfile", function (data) {
                    extensionChecker(data)
                    let file_url = window.URL.createObjectURL($('.written_case_study_dropzone').get(0).dropzone.files[0]);
                    $("#preview_written_case_study_dropzone").attr("src", file_url);

                });
                this.on('sending', function (file, xhr, formData) {
                    submitFile('written_case_study_dropzone')
                });
            },
            reset: function () {
                this.removeAllFiles(true);
            }
        });

        asc_clearance_dropzone = new Dropzone('.asc_clearance_dropzone', {
            addRemoveLinks: true,
            autoProcessQueue: true,
            uploadMultiple: false,
            parallelUploads: 1,
            maxFiles: 1,
            maxFilesize: 10,
            dictDefaultMessage: "",
            paramName: 'asc_clearance_dropzone',
            clickable: true,
            acceptedFiles: 'image/jpeg, application/pdf',
            url: '/submit-file',
            headers: {
                'x-csrf-token': CSRF_TOKEN,
            },
            init: function () {
                this.on("addedfile", function (data) {
                    extensionChecker(data)
                    let file_url = window.URL.createObjectURL($('.asc_clearance_dropzone').get(0).dropzone.files[0]);
                    $("#preview_asc_clearance_dropzone").attr("src", file_url);
                });
                this.on('sending', function (file, xhr, formData) {
                    submitFile('asc_clearance_dropzone')
                });
            },
            reset: function () {
                this.removeAllFiles(true);
            }
        });

        media_certification_dropzone = new Dropzone('.media_certification_dropzone', {
            addRemoveLinks: true,
            autoProcessQueue: true,
            uploadMultiple: false,
            parallelUploads: 1,
            maxFiles: 1,
            maxFilesize: 10,
            dictDefaultMessage: "",
            paramName: 'media_certification_dropzone',
            clickable: true,
            acceptedFiles: 'image/jpeg, application/pdf',
            url: '/submit-file',
            headers: {
                'x-csrf-token': CSRF_TOKEN,
            },
            init: function () {
                this.on("addedfile", function (data) {
                    extensionChecker(data)
                    let file_url = window.URL.createObjectURL($('.media_certification_dropzone').get(0).dropzone.files[0]);
                    $("#preview_media_certification_dropzone").attr("src", file_url);
                });
                this.on('sending', function (file, xhr, formData) {
                    submitFile('media_certification_dropzone')
                });
            },
            reset: function () {
                this.removeAllFiles(true);
            }
        });

        client_certification_dropzone = new Dropzone('.client_certification_dropzone', {
            addRemoveLinks: true,
            autoProcessQueue: true,
            uploadMultiple: false,
            parallelUploads: 1,
            maxFiles: 1,
            maxFilesize: 10,
            dictDefaultMessage: "",
            paramName: 'client_certification_dropzone',
            clickable: true,
            acceptedFiles: 'image/jpeg, application/pdf',
            url: '/submit-file',
            headers: {
                'x-csrf-token': CSRF_TOKEN,
            },
            init: function () {
                this.on("addedfile", function (data) {
                    extensionChecker(data)
                    let file_url = window.URL.createObjectURL($('.client_certification_dropzone').get(0).dropzone.files[0]);
                    $("#preview_client_certification_dropzone").attr("src", file_url);
                });
                this.on('sending', function (file, xhr, formData) {
                    submitFile('client_certification_dropzone')
                });
            },
            reset: function () {
                this.removeAllFiles(true);
            }
        });

        film_dropzone = new Dropzone('.film_dropzone', {
            addRemoveLinks: true,
            autoProcessQueue: true,
            uploadMultiple: false,
            parallelUploads: 1,
            maxFiles: 1,
            maxFilesize: 150,
            dictDefaultMessage: "",
            paramName: 'film_dropzone',
            clickable: true,
            acceptedFiles: 'video/mp4',
            url: '/submit-file',
            headers: {
                'x-csrf-token': CSRF_TOKEN,
            },
            init: function () {
                this.on("addedfile", function (data) {
                    extensionChecker(data)
                    let file_url = window.URL.createObjectURL($('.film_dropzone').get(0).dropzone.files[0]);
                    $("#preview_film_dropzone").attr("src", file_url);
                });
                this.on('sending', function (file, xhr, formData) {
                    submitFile('film_dropzone')
                });
            },
            reset: function () {
                this.removeAllFiles(true);
            }
        });

        posters_dropzone = new Dropzone('.posters_dropzone', {
            addRemoveLinks: true,
            autoProcessQueue: true,
            uploadMultiple: false,
            parallelUploads: 1,
            maxFiles: 1,
            maxFilesize: 10,
            dictDefaultMessage: "",
            paramName: 'posters_dropzone',
            clickable: true,
            acceptedFiles: 'image/jpeg',
            url: '/submit-file',
            headers: {
                'x-csrf-token': CSRF_TOKEN,
            },
            init: function () {
                this.on("addedfile", function (data) {
                    extensionChecker(data)
                    let file_url = window.URL.createObjectURL($('.posters_dropzone').get(0).dropzone.files[0]);
                    $("#preview_posters_dropzone").attr("src", file_url);
                });
                this.on('sending', function (file, xhr, formData) {
                    submitFile('posters_dropzone')
                });
            },
            reset: function () {
                this.removeAllFiles(true);
            }
        });

        audio_dropzone = new Dropzone('.audio_dropzone', {
            addRemoveLinks: true,
            autoProcessQueue: true,
            uploadMultiple: false,
            parallelUploads: 1,
            maxFiles: 1,
            maxFilesize: 10,
            dictDefaultMessage: "",
            paramName: 'audio_dropzone',
            clickable: true,
            acceptedFiles: 'audio/mpeg',
            url: '/submit-file',
            headers: {
                'x-csrf-token': CSRF_TOKEN,
            },
            init: function () {
                this.on("addedfile", function (data) {
                    extensionChecker(data)
                    let file_url = window.URL.createObjectURL($('.audio_dropzone').get(0).dropzone.files[0]);
                    $("#preview_audio_dropzone").attr("src", file_url);
                });
                this.on('sending', function (file, xhr, formData) {
                    submitFile('audio_dropzone')
                });
            },
            reset: function () {
                this.removeAllFiles(true);
            }
        });

        english_translation_dropzone = new Dropzone('.english_translation_dropzone', {
            addRemoveLinks: true,
            autoProcessQueue: true,
            uploadMultiple: false,
            parallelUploads: 1,
            maxFiles: 1,
            maxFilesize: 10,
            dictDefaultMessage: "",
            paramName: 'english_translation_dropzone',
            clickable: true,
            acceptedFiles: 'application/pdf',
            url: '/submit-file',
            headers: {
                'x-csrf-token': CSRF_TOKEN,
            },
            init: function () {
                this.on("addedfile", function (data) {
                    extensionChecker(data)
                    let file_url = window.URL.createObjectURL($('.english_translation_dropzone').get(0).dropzone.files[0]);
                    $("#preview_english_translation_dropzone").attr("src", file_url);
                });
                this.on('sending', function (file, xhr, formData) {
                    submitFile('english_translation_dropzone')
                });
            },
            reset: function () {
                this.removeAllFiles(true);
            }
        });

        demo_film_dropzone = new Dropzone('.demo_film_dropzone', {
            addRemoveLinks: true,
            autoProcessQueue: true,
            uploadMultiple: false,
            parallelUploads: 1,
            maxFiles: 1,
            maxFilesize: 150,
            dictDefaultMessage: "",
            paramName: 'demo_film_dropzone',
            clickable: true,
            acceptedFiles: 'video/mp4',
            url: '/submit-file',
            headers: {
                'x-csrf-token': CSRF_TOKEN,
            },
            init: function () {
                this.on("addedfile", function (data) {
                    extensionChecker(data)
                    let file_url = window.URL.createObjectURL($('.demo_film_dropzone').get(0).dropzone.files[0]);
                    $("#preview_demo_film_dropzone").attr("src", file_url);
                });
                this.on('sending', function (file, xhr, formData) {
                    submitFile('demo_film_dropzone')
                });
            },
            reset: function () {
                this.removeAllFiles(true);
            }
        });

        photo_piece_dropzone = new Dropzone('.photo_piece_dropzone', {
            addRemoveLinks: true,
            autoProcessQueue: true,
            uploadMultiple: false,
            parallelUploads: 1,
            maxFiles: 1,
            maxFilesize: 10,
            dictDefaultMessage: "",
            paramName: 'photo_piece_dropzone',
            clickable: true,
            acceptedFiles: 'image/jpeg',
            url: '/submit-file',
            headers: {
                'x-csrf-token': CSRF_TOKEN,
            },
            init: function () {
                this.on("addedfile", function (data) {
                    extensionChecker(data)
                    let file_url = window.URL.createObjectURL($('.photo_piece_dropzone').get(0).dropzone.files[0]);
                    $("#preview_photo_piece_dropzone").attr("src", file_url);
                });
                this.on('sending', function (file, xhr, formData) {
                    submitFile('photo_piece_dropzone')
                });
            },
            reset: function () {
                this.removeAllFiles(true);
            }
        });

        entry_dropzone = new Dropzone('.entry_dropzone', {
            addRemoveLinks: true,
            autoProcessQueue: true,
            uploadMultiple: false,
            parallelUploads: 1,
            maxFiles: 1,
            maxFilesize: 10,
            dictDefaultMessage: "",
            paramName: 'entry_dropzone',
            clickable: true,
            acceptedFiles: 'image/jpeg',
            url: '/submit-file',
            headers: {
                'x-csrf-token': CSRF_TOKEN,
            },
            init: function () {
                this.on("addedfile", function (data) {
                    extensionChecker(data)
                    let file_url = window.URL.createObjectURL($('.entry_dropzone').get(0).dropzone.files[0]);
                    $("#preview_entry_dropzone").attr("src", file_url);
                });
                this.on('sending', function (file, xhr, formData) {
                    submitFile('entry_dropzone')
                });
            },
            reset: function () {
                this.removeAllFiles(true);
            }
        });
    }

    function dropzoneFileSizeLabel() {
        var mbsize_10 = '<p class="mb-0">' +
            'Drop your file(s) here or <span class="fw-bold">browse</span><br>' +
            'The total file/s should not exceed <span class="fw-bold">10MB</span>' +
            '</p>'

        var mbsize_150 = '<p class="mb-0">' +
            'Drop your file(s) here or <span class="fw-bold">browse</span><br>' +
            'The total file/s should not exceed <span class="fw-bold">150MB</span>' +
            '</p>'

        $('.concept_board_dropzone > .file-text > p').html(mbsize_10)
        $('.case_study_video_dropzone > .file-text > p ').html(mbsize_150)
        $('.written_case_study_dropzone > .file-text > p ').html(mbsize_10)
        $('.asc_clearance_dropzone > .file-text > p ').html(mbsize_10)
        $('.client_certification_dropzone > .file-text > p ').html(mbsize_10)
        $('.media_certification_dropzone > .file-text > p ').html(mbsize_10)
        $('.film_dropzone > .file-text > p ').html(mbsize_150)
        $('.posters_dropzone > .file-text > p ').html(mbsize_10)
        $('.audio_dropzone > .file-text > p ').html(mbsize_10)
        $('.english_translation_dropzone > .file-text > p ').html(mbsize_10)
        $('.demo_film_dropzone > .file-text > p ').html(mbsize_150)
        $('.photo_piece_dropzone > .file-text > p ').html(mbsize_10)
        $('.entry_dropzone > .file-text > p ').html(mbsize_10)
    }

    function remove_class() {
        $('.nav-link').removeClass('active')
        $('.tab-pane').removeClass('active')
    }

    function activateCategory() {
        remove_class()
        $('#category-tab').addClass('active')
        $('#category').addClass('active show')
    }

    function activateCampaign() {
        remove_class()
        $('#campaign-tab').addClass('active')
        $('#campaign').addClass('active show')
    }

    function activateEntry() {
        remove_class()
        $('#entry-tab').addClass('active')
        $('#entry').addClass('active show')
        
        var category_name = $( "#category_name option:selected" ).text();
        var sub_category_name = $( "#subcategory option:selected" ).text();
        $('.entry-card').hide()

        if(category_name == "CLASSIC CREATIVITY") {
            $('.classic_container').show()
        }
        else if(category_name == 'CREATIVITY IN CRAFT' 
            && (sub_category_name == 'AUDIO: BEST COPYWRITING' 
            || sub_category_name == 'AUDIO: BEST DIRECTION' 
            || sub_category_name == 'AUDIO: BEST MUSIC / SOUND DESIGN' 
            || sub_category_name == 'AUDIO: BEST VOICE PERFORMANCE')) {
                $('.audio_container').show()
        }
        else if(category_name == 'CREATIVITY IN CRAFT' 
            && (sub_category_name == 'DESIGN/ OUTDOOR/ PRINT: BEST ART DIRECTION' 
            || sub_category_name == 'DESIGN/ OUTDOOR/ PRINT: BEST CHARACTER DESIGN' 
            || sub_category_name == 'DESIGN/ OUTDOOR/ PRINT: BEST DIGITAL IMAGING' 
            || sub_category_name == 'DESIGN/ OUTDOOR/ PRINT: BEST ILLUSTRATION'
            || sub_category_name == 'DESIGN/ OUTDOOR/ PRINT: BEST PHOTOGRAPHY'
            || sub_category_name == 'DESIGN/ OUTDOOR/ PRINT: BEST TYPOGRAPHY'
            || sub_category_name == 'DESIGN/ OUTDOOR/ PRINT: BEST COPYWRITING')) {
                $('.design_container').show()
                $('.outdoor_container').show()
        }
        else if(category_name == 'CREATIVITY IN CRAFT' 
            && (sub_category_name == 'DIGITAL (EXCLUDING FILM): BEST ART DIRECTION' 
            || sub_category_name == 'DIGITAL (EXCLUDING FILM): BEST COPYWRITING' 
            || sub_category_name == 'DIGITAL: BEST UX / UI')) {
                $('.digital_c_container').show()
        }
        else if(category_name == 'CREATIVITY IN CRAFT' 
            && (sub_category_name == 'FILM: BEST ANIMATION' 
            || sub_category_name == 'FILM: BEST ART DIRECTION' 
            || sub_category_name == 'FILM: BEST AUDIO CRAFT'
            || sub_category_name == 'FILM: BEST CINEMATOGRAPHY'
            || sub_category_name == 'FILM: BEST DIRECTION'
            || sub_category_name == 'FILM: BEST EDITING'
            || sub_category_name == 'FILM: BEST PRODUCTION DESIGN'
            || sub_category_name == 'FILM: BEST VISUAL EFFECTS AND DIGITAL IMAGING')) {
                $('.film_c_container').show()
        }
        else if(category_name == 'CREATIVITY IN ENGAGEMENT') {
                $('.engagement_container').show()
        }
        else if(category_name == 'CREATIVITY IN ENTERTAINMENT') {
                $('.engagement_container').show()
        }
        else if(category_name == 'CREATIVITY IN EXPERIENCE') {
                $('.engagement_container').show()
        }
        else if(category_name == 'CREATIVITY IN STRATEGY & EFFECTIVENESS') {
                $('.effective_container').show()
        }
        else if(category_name == 'CREATIVITY FOR GOOD') {
                $('.engagement_container').show()
        }

    }

    function activateWritten() {
        remove_class()
        $('#written-tab').addClass('active')
        $('#written').addClass('active show')
    }

    function activateAgencyDetails() {
        remove_class()
        $('#agency-tab').addClass('active')
        $('#agency').addClass('active show')
    }

    $(document).on('click', '.edit-category', function() {
        $('#btn_submit').removeClass('d-block')
        $('.btn_next').show()
        activateCategory() 
    })
    $(document).on('click', '.edit-campaign', function() { 
        $('#btn_submit').removeClass('d-block')
        $('.btn_next').show()
        activateCampaign() 
    })
    $(document).on('click', '.edit-entry', function() { 
        $('#btn_submit').removeClass('d-block')
        $('.btn_next').show()
        activateEntry() 
    })
    $(document).on('click', '.edit-agency', function() { 
        $('#btn_submit').removeClass('d-block')
        $('.btn_next').show()
        activateAgencyDetails() 
    })

    $(document).on('click', '.edit-written-submission', function() { 
        $('#btn_submit').removeClass('d-block')
        $('.btn_next').show()
        activateWritten() 
    })

    $(document).on('click','.btn_more_link', function() {
        $('.online-link-container').append(
            '<div><input type="email" class="form-control form-control-lg lg fs-18 mt-3" id="online-link'+ url_link_id++ +'" name="online_link[]" placeholder="https://" style="width: 622px;"><a href="javascript:void(0)" class="delete-link"><i class="fas fa-times"></i></a></div>'
        )
    })

    $(document).on('click','.btn_more_working_link', function() {
        $('.working-link-container').append(
            '<div><input type="email" class="form-control form-control-lg lg fs-18 mt-3" id="working_url'+ working_url_link_id++ +'" name="working_url[]" placeholder="https://" style="width: 622px;"><a href="javascript:void(0)" class="working-delete-link"><i class="fas fa-times"></i></a></div>'
        )
    })

    $(document).on('click','.btn_more_roles', function() {
        $('.credits-container').append(
            '<div class="d-flex justify-content-between mb-4 credit-roles" style="column-gap: 25px"><div style="width: 100%"><label for="" class="form-label mb-2">Name</label><input type="text" class="form-control form-control-lg lg fs-18" id="role_name'+ role_name_id++ +'" name="role_name[]" placeholder="Name" data-kb="credit_name'+role_name_id+'"></div><div style="width: 100%"><label for="" class="form-label mb-2">Role</label><input type="text" class="form-control form-control-lg lg fs-18" id="role'+ role_id++ +'" name="role[]" placeholder="Role" data-kb="credit_role'+role_id+'"><a href="javascript:void(0)" class="delete-role"><i class="fas fa-times"></i></a></div></div>'
        )
    })

    $(document).on('click', '.delete-role', function() {
        $(this).parent().parent().remove()
    })

    $(document).on('click', '.delete-link', function() {
        $(this).parent().remove()
    })

    $(document).on('click', '.working-delete-link', function() {
        $(this).parent().remove()
    })

    $(document).on('keyup', '.textarea-ws', function(e) {
        var text = $(this).val();
        var numWords = 0;
        for (var i = 0; i < text.length; i++) {
            var currentCharacter = text[i];
            if (currentCharacter == " ") {
                numWords += 1;
            }
        }

        numWords += 1;
        if (numWords > 300) {
            e.preventDefault()
        }

        if (numWords > 300) {
            $(this).next().text("-"+numWords+"/300")
            $(this).addClass('is-invalid')
        } else {
            $(this).removeClass('is-invalid')
            $(this).next().text(numWords+"/300")
        }
    })

    function submitFile(dropzone='') {
        let links = []
        $('input[name="online_link[]"').each(function() {
            links.push({
                link: this.value
            })
        })

        let working_links = []
        $('input[name="working_url[]"').each(function() {
            working_links.push({
                link: this.value
            })
        })

        $('.next-loading-label').show()
        $('.entry-loader').show()

        let form_data = new FormData()

        form_data.append('_token', '{{ csrf_token() }}')
        form_data.append('uid', entry_uid)
        if(dropzone == 'concept_board_dropzone')
            form_data.append('concept_board_dropzone', $('.concept_board_dropzone').get(0).dropzone.files[0])
        if(dropzone == 'case_study_video_dropzone')
            form_data.append('case_study_video_dropzone', $('.case_study_video_dropzone').get(0).dropzone.files[0])
        if(dropzone == 'written_case_study_dropzone')
            form_data.append('written_case_study_dropzone', $('.written_case_study_dropzone').get(0).dropzone.files[0])
        if(dropzone == 'asc_clearance_dropzone')
            form_data.append('asc_clearance_dropzone', $('.asc_clearance_dropzone').get(0).dropzone.files[0])
        if(dropzone == 'media_certification_dropzone')
            form_data.append('media_certification_dropzone', $('.media_certification_dropzone').get(0).dropzone.files[0])
        if(dropzone == 'client_certification_dropzone')
           form_data.append('client_certification_dropzone', $('.client_certification_dropzone').get(0).dropzone.files[0])
        if(dropzone == 'film_dropzone')
            form_data.append('film_dropzone', $('.film_dropzone').get(0).dropzone.files[0])
        if(dropzone == 'posters_dropzone')
            form_data.append('posters_dropzone', $('.posters_dropzone').get(0).dropzone.files[0])
        if(dropzone == 'audio_dropzone')
            form_data.append('audio_dropzone', $('.audio_dropzone').get(0).dropzone.files[0])
        if(dropzone == 'demo_film_dropzone')
            form_data.append('demo_film_dropzone', $('.demo_film_dropzone').get(0).dropzone.files[0])
        if(dropzone == 'english_translation_dropzone')
            form_data.append('english_translation_dropzone', $('.english_translation_dropzone').get(0).dropzone.files[0])
        if(dropzone == 'photo_piece_dropzone')
            form_data.append('photo_piece_dropzone', $('.photo_piece_dropzone').get(0).dropzone.files[0])
        if(dropzone == 'entry_dropzone')
            form_data.append('entry_dropzone', $('.entry_dropzone').get(0).dropzone.files[0])

        form_data.append('online_links', JSON.stringify(links))
        form_data.append('working_url', JSON.stringify(working_links))

        fetch("/submit-file", {
            method: 'POST',
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            body: form_data
        })
        .then(response => response.json())
        .then((e) => {
            if (e.status_code == 200) {
                entry_uid = e.data.uid;
                $('.next').addClass('disabled').attr('disabled', true);
                $.LoadingOverlay("hide");

                $('.next-loading-label').hide()
                $('.entry-loader').hide()
                $('#entry-tab').addClass('done')

                setTimeout(function() {
                    $('.next').removeClass('disabled').attr('disabled', false);
                }, 100)
            }
        })
    }
    // All FUNCTIONS END HERE    
    function submitWritten() {
        $('.next-loading-label').show()
        $('.entry-loader').show()

        let form_data = new FormData()

        form_data.append('_token', '{{ csrf_token() }}')
        form_data.append('uid', entry_uid)

        form_data.append('campaign_summary', $('#campaign_summary').val())
        form_data.append('objectives', $('#objectives').val())
        form_data.append('strategy', $('#strategy').val())
        form_data.append('execution', $('#execution').val())
        form_data.append('results', $('#results').val())

        fetch("/submit-written", {
            method: 'POST',
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            body: form_data
        })
        .then(response => response.json())
        .then((e) => {
            if (e.status_code == 200) {
                entry_uid = e.data.uid;
                $('.next').addClass('disabled').attr('disabled', true);
                $.LoadingOverlay("hide");

                $('.next-loading-label').hide()
                $('.entry-loader').hide()
                $('#written-tab').addClass('done')

                setTimeout(function() {
                    $('.next').removeClass('disabled').attr('disabled', false);
                }, 100)
            }
        })
    }

    function submitAgency() {
        $('.next-loading-label').show()
        $('.entry-loader').show()

        let credits     = []
        let credit_name = []
        let credit_role = []

        $('input[name="role_name[]"').each(function() {
            credit_name.push(this.value)
        })

        $('input[name="role[]"').each(function() {
            credit_role.push(this.value)
        })

        for (let i = 0; i < credit_name.length; i++) {
            credits.push({
                name: credit_name[i],
                role: credit_role[i]
            })
        }

        let form_data = new FormData()

        var first_timer = (!$('input[name="first_timer"]:checked').val()) ? 0 : 1;

        form_data.append('_token', '{{ csrf_token() }}')
        form_data.append('uid', entry_uid)
        form_data.append('type', $('input[name="entry_type"]:checked').val())
        form_data.append('member', $('input[name="member"]:checked').val())
        form_data.append('agency_contact', $('#contact_person').val())
        form_data.append('position', $('#position').val())
        form_data.append('phone_number', $('#phone_number').val())
        form_data.append('email', $('#email_address').val())
        form_data.append('first_timer', first_timer)
        form_data.append('credits', JSON.stringify(credits))

        fetch("/submit-agency", {
            method: 'POST',
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            body: form_data
        })
        .then(response => response.json())
        .then((e) => {
            if (e.status_code == 200) {
                entry_uid = e.data.uid;
                $('.next').addClass('disabled').attr('disabled', true);
                $.LoadingOverlay("hide");

                $('.next-loading-label').hide()
                $('.entry-loader').hide()
                $('#agency-tab').addClass('done')

                $('.all-saved-label').show()
                $('.saved-x-mins-label').hide()
                $('.btn_next').hide()
                setTimeout(function() {
                    $('.next').removeClass('disabled').attr('disabled', false);
                }, 100)
            }
        })
    }

    function getLinks() {
        $('.next-loading-label').show()
        $('.entry-loader').show()

        let param = {
            'uid': entry_uid
        }

        fetch("/preview-entry", {
            method: 'POST',
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(param)
        })
        .then(response => response.json())
        .then((data) => {
            var html = "";
            $.each(data, function(index, ref) {
                    html += '<div class="mb-1">'
                        html += '<span class="fs-18 fw-500">'+ref.link+'</span><br/>'
                    html += '</div>'
            })

            $('.preview_online_link_container').html(html)
        })
    }

    function getCredits() {
        $('.next-loading-label').show()
        $('.entry-loader').show()

        let param = {
            'uid': entry_uid
        }

        fetch("/preview-credits", {
            method: 'POST',
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(param)
        })
        .then(response => response.json())
        .then((data) => {
            var html = "";
            $.each(data, function(index, credit) {
                html += '<div class="mb-4">'
                    html += '<span class="fs-18 fw-500">'+credit.name+'</span><br/>'
                    html += '<span class="fs-18 fw-400">'+credit.role+'</span><br/></br>'
                html += '</div>'
            })

            $('.preview_credits_container').html(html)
        })
    }

    function extensionChecker(data) {
        let ext = data.name.split('.').pop();

        if (ext == "jpeg" || ext == "jpg") {
            $(data.previewElement).find(".dz-image img").attr("src", "{{url('/images/jpeg.png')}}");
        } else if (ext == "pdf") {
            $(data.previewElement).find(".dz-image img").attr("src", "{{url('/images/pdf.png')}}");
        } else if (ext.indexOf("doc") != -1) {
            $(data.previewElement).find(".dz-image img").attr("src", "{{url('/images/doc.png')}}");
        } else if (ext.indexOf("xls") != -1 || ext.indexOf("xlsx") != -1) {
            $(data.previewElement).find(".dz-image img").attr("src", "{{url('/images/xlsx.png')}}");
        } else if (ext.indexOf("mp4") != -1) {
            $(data.previewElement).find(".dz-image img").attr("src", "{{url('/images/mp4.png')}}");
        } else if (ext.indexOf("mp3") != -1) {
            $(data.previewElement).find(".dz-image img").attr("src", "{{url('/images/mp3.png')}}");
        }
    }

    $(document).on('keyup', '.textarea-ws', function(e) {
        var text = $(this).val();
        var numWords = 0;
        for (var i = 0; i < text.length; i++) {
            var currentCharacter = text[i];
            if (currentCharacter == " ") {
                numWords += 1;
            }
        }

        numWords += 1;
        if (numWords > 300) {
            e.preventDefault()
        }

        if (numWords > 300) {
            $(this).next().text("-"+numWords+"/300")
            $(this).addClass('is-invalid')
        } else {
            $(this).removeClass('is-invalid')
            $(this).next().text(numWords+"/300")
        }
    })

    function reviewFile() {
        $('.file-preview-entry > .review-line').removeClass('d-flex')
        $('.file-preview-entry > .review-line:nth-child(1)').addClass('d-flex')
        $('.file-preview-entry > .review-line:nth-child(2)').addClass('d-flex')
        $('.file-preview-entry > .review-line:nth-child(4)').addClass('d-flex')
        $('.file-preview-entry > .review-line:nth-child(6)').addClass('d-flex')
        $('.file-preview-entry > .review-line:nth-child(7)').addClass('d-flex')
        $('.file-preview-entry > .review-line:nth-child(8)').addClass('d-flex')
        $('.file-preview-entry > .review-line:nth-child(9)').addClass('d-flex')
        $('.file-preview-entry > .review-line:nth-child(10)').addClass('d-flex')
        $('.file-preview-entry > .review-line:nth-child(11)').addClass('d-flex')
        $('.file-preview-entry > .review-line:nth-child(12)').addClass('d-flex')
        $('.file-preview-entry > .review-line:nth-child(13)').addClass('d-flex')
        $('.file-preview-entry > .review-line:nth-child(14)').addClass('d-flex')
        $('.file-preview-entry > .review-line:nth-child(15)').addClass('d-flex')
    }

</script>
@endpush
@endsection