@extends('base')  
@push('styles')
    <style>
        .spinner-grow,
        .loading-label {
            display: none;
        }

        .custom-card {
            display: -webkit-box !important;
        }

        .btn-edit {
            background: #223448;
            color: #fff;
        }
    </style>
@endpush
@section('content')
    @include('dashboard.components.header')
        @if (count($entries) > 0)
            <div style="margin-top: 200px" class="mb-5">
                <div class="entries flex-column d-flex">
                    <div class="d-flex align-items-center justify-content-between">
                        <h1 class="page-title mb-0">
                        Your Entries
                        </h1>
                        <a href="{{url('/my-invoice')}}" class="top-action">
                            <span>View your invoice</span> <i class="bi bi-chevron-right"></i>
                        </a>
                    </div>
                    @foreach ($entries as $entry)
                        <div class="entries-cards">
                            <div class="custom-card">
                                <div class="custom-card-col">
                                    <div class="custom-card-col-info">
                                        <h2>{{$entry->category()->name}} | {{$entry->status}} | {{$entry->payment_status}}</h2>
                                        <h2>{{$entry->entry_name}}</h2>
                                        <p>{{$entry->ad_details}}</p>
                                    </div>
                                </div>
                                <!-- <button class="btn btn-outline-primary primary small me-3 link-color" id="btn_clone" style="min-width:156px; height: 48px;" data-uid="{{$entry->uid}}">
                                    <span>Clone entry</span>
                                </button> -->
                                <a href="/view/entry/{{$entry->uid}}" style="color:#fff;">
                                    <button type="button" class="btn btn-primary primary small me-3" style="min-width:156px; height: 48px;">View entry</button>
                                </a>
                                <div class="btn-group" role="group">
                                    <a href="/entry/edit/{{$entry->uid}}" class="btn btn-secondary btn-edit" data-id="{{$entry->uid}}">
                                        <i class="far fa-edit"></i>
                                    </a>
                                    <!-- <a href="javascript:void(0)" class="btn btn-danger btn_delete" data-uid="{{$entry->uid}}" data-bs-toggle="modal" data-bs-target="#deleteEntryModal">
                                        <i class="far fa-trash-alt"></i>
                                    </a> -->
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        @else
            <div class="wrapper-default mb-5">
                <div class="container-fluid py72">
                    <div class="entries flex-column d-flex mb-5 mt-5">
                        <h1 class="page-title mb-0">
                        Your Entries
                        </h1>
                    </div>
                    <div class="state dark d-flex flex-column justify-content-center align-items-center">
                        <img class="mb-4" src="/images/no-entries.png" alt="">
                        <h1 class="title mb-2 text-center fw-bold">
                        No entries yet
                        </h1>
                        <p class="desc mb-0 text-center">
                            Would you like to submit now?
                        </p>
                        <a href="{{url('/submit-entry')}}">
                            <button type="button" class="btn btn-primary primary lg mt-5" style="min-width:223px">Submit an entry</button>
                        </a>
                    </div>
                </div>
            </div>
        @endif

        <div class="voting-modal modal fade" id="deleteEntryModal" tabindex="-1" aria-labelledby="flag" aria-hidden="true">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header d-flex">
                <h4 style="margin-top:-20px">Are you sure you want to delete?</h4>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
              </div>
              <div class="modal-body p-0 pt-4">
                   <div class="d-flex justify-content-center">
                        <button class="btn btn-outline-primary brown primary small me-3 text-uppercase brown" style="min-width:156px; height: 48px;"  data-bs-dismiss="modal" aria-label="Close">
                            <span class="delete-label">Cancel</span>
                        </button>
                        <button id="btn_remove" class="btn btn-primary primary small me-3 text-uppercase" style="min-width:156px; height: 48px;">
                            <span class="spinner-grow spinner-grow-sm" role="status"></span>
                            <span class="spinner-grow spinner-grow-sm" role="status"></span>
                            <span class="spinner-grow spinner-grow-sm" role="status"></span>
                            <span class="delete-label">Confirm</span>
                        </button>
                    </div>
              </div>
            </div>
          </div>
        </div>
    @include('dashboard.components.footer')
@push('custom-js')
<script>
    $(document).ready(activateQuery())

    function activateQuery() {

    }

    var entry_uid = 0;
    $(document).on('click', '.btn_delete', function() {
        entry_uid = $(this).attr('data-uid')
    })

    $(document).on('click', '#btn_remove', function() {
        $.LoadingOverlay("show", {
            image: '{{url("/images/loading1.png")}}'
        },100);

        $('.delete-label').hide()
        $('.spinner-grow').addClass('d-inline-block')

        $(this).attr('disabled', true)

        let params = {
            '_token'  : '{{ csrf_token() }}',
            'uid'     : entry_uid
        }

        fetch("{{url('/entry/delete')}}", {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(params)
        })
        .then(response => response.json())
        .then((e) => {

            if (e.status_code == 200) {
                $.LoadingOverlay("hide");
                $('#deleteEntryModal').toggle('modal')
                setTimeout(function() {
                    location.href = "{{url('/my-entries')}}"
                }, 100)
            }
        })

    })

    $(document).on('click', '#btn_clone', function() {
        $.LoadingOverlay("show", {
            image: '{{url("/images/loading1.png")}}'
        },100);

        $('.clone-label').hide()
        $('.spinner-grow').addClass('d-inline-block')

        $(this).attr('disabled', true)

        entry_uid = $(this).attr('data-uid')

        let params = {
            '_token'  : '{{ csrf_token() }}',
            'uid'     : entry_uid
        }

        fetch("{{url('/entry/clone')}}", {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(params)
        })
        .then(response => response.json())
        .then((e) => {

            if (e.status_code == 200) {
                $.LoadingOverlay("hide");
                setTimeout(function() {
                    location.href = "/entry/edit/"+e.message.uid
                }, 100)
            }
        })

    })
</script>
@endpush
@endsection