@extends('base')

@section('content')
<div class="dashboard">
  @include('dashboard.components.header')
  @include('dashboard.components.sub-nav',['crumb' => ''])
  @if(count(Auth::user()->entries()) < 1)
  <div class="card text-center padded">
    <h2 class="greetings">Hello, <strong>{{ Auth::user()->name }}!</strong></h2>
    <div class="spacer"></div>
    <p class="light">Welcome to Kidlat Awards 2019.</p>
    <div class="spacer"></div>
    <a href="{{ url('/dashboard/form') }}" class="waves-effect waves-dark btn pink">Submit your first entry</a>
  </div>
  @else 
  <div class="card text-center padded">
    <h2 class="greetings">Hello, <strong>{{ Auth::user()->name }}!</strong></h2>
    <div class="spacer"></div>
    <p class="light">Welcome to Kidlat Awards 2019.</p>
    
    <?php
     $total = 0;
     foreach(Auth::user()->entries() as $e){
       $total += $e->fee;
     }
    ?>
    <p class="light">You have a total of <strong>{{count(Auth::user()->entries())}}</strong> entry(ies) with a total of <strong>PHP {{number_format($total, 2)}} + 12% VAT</strong>.</p>
    <div class="spacer"></div>
    <div class="mw-100 overflow-auto">
      <table class="striped centered">
        <thead>
          <tr>
              <th>ID</th>
              <th>Entry Name</th>
              <th>Brand</th>
              <th>Status</th>
              <th>Entry</th>
              <th>Category</th>
              <th>Fee</th>
              <th>Action</th>
          </tr>
        </thead>

        <tbody>
          @foreach (Auth::user()->entries() as $e)
          <tr>
            <td class="uid flex align-center">
              <label class="uid-cb hidden">
                <input type="checkbox" class="filled-in pink" name="invoice_id" value="{{$e->uid}}" {{ $e->payment_status === 'Unpaid' ? '' : 'disabled checked' }} />
                <span>&nbsp;</span>
              </label>
              {{$e->uid}}</td>
            <td>{{$e->entry_name}}</td>
            <td>{{$e->brand}}</td>
            <td class="text-color {{  $e->payment_status === 'Unpaid' ? '-gray' : ($e->payment_status === 'Requested' ? '-red' : ($e->status === 'Approved' ? '-green' : '-orange')) }}">{!!$e->status()!!}</td>
            <td>{{$e->type}}</td>
            <td>{{$e->category()->name}}</td>
            <td>{{number_format($e->fee)}}</td>
            <td>
              <a class='dropdown2-trigger flex align-center btn inverted pink' href='#' data-target='dropdown-{{$e->uid}}'>Actions <i class="material-icons right">expand_more</i></a>
              <ul id='dropdown-{{$e->uid}}' class='dropdown-content'>
                <li>
                  <a href="{{ url('/view/entry') }}/{{$e->uid}}" target="_blank" class="text-color -pink"><i class="material-icons">visibility</i>View</a>
                </li>
                @if($e->payment_status == "Unpaid" && $expired == false)
                <li><a href="{{url('/dashboard/form')}}/{{$e->uid}}/edit" class="text-color -green"><i class="material-icons">edit</i>Edit</a></li>
                <li>
                  <a href="#!" class="text-color -red submit"><i class="material-icons">delete</i>Delete</a>
                  <form method="POST" action="{{ url('/delete/entry') }}">
                    @csrf
                    <input type="hidden" name="id" value="{{$e->id}}">
                  </form>
                </li>
                @endif
              </ul>
            </td>
          </tr>
          @endforeach
          
        </tbody>
      </table>
    </div>

      <div class="spacer"></div>
      @if($expired == false)
      <div class="default-actions">
        <div>
          <button class="waves-effect waves-dark btn-flat underlined mx-1 invoice-request">Select Entry(ies) for Invoice Request</button>
        </div>
        <div class="spacer"></div>
        <div>
          <a href="{{ url('dashboard/form') }}" class="waves-effect waves-dark btn pink mx-1">Add an Entry</a>
        </div>
      </div>

      <div class="request-actions hidden">
        <div>
          <button class="waves-effect waves-dark btn-flat underlined mx-1 cancel-request">Cancel</button>
        </div>  
        <div class="spacer"></div>
        <div>
          <button class="waves-effect waves-dark btn pink mx-1 confirm-request">Confirm Invoice Request</button>
        </div>
      </div>
      @else
      <div>
        <a class="waves-effect waves-dark btn-flat underlined mx-1 modal-trigger" href="#rules">Submission of entries is now closed. Review the Competition Rules here</a>
      </div>
      @endif
  </div>
  @endif
  <div id="rules" class="modal modal-fixed-footer">
    <div class="modal-content">
      @include('site.components.rules')
    </div>
    <div class="modal-footer">
      <a href="#!" class="modal-close waves-effect waves-dark btn pink">Close</a>
    </div>
  </div>
</div>
@include('dashboard.components.footer')
@if(Auth::user()->policy == false)
@push('custom-js')
<script>
  $( document ).ready(function() {
    Swal.fire({
        type: 'warning',
        title: "Privacy Policy",
        allowOutsideClick: false,
        html: '<p>By using kidlatawards.com.ph, you agree with our privacy policy to have your data collected, processed, and retained by 4As Philippines for all lawful purposes. You can also View Privacy Policy <a href="https://www.freeprivacypolicy.com/privacy/view/2dcfb4d870e27f0cff36389f8997d8bd" target="_blank">here</a>.</p>',
        confirmButtonText: 'Accept and Proceed'
      }).then((result) => {
        if (result.value) {
          $.ajax({
              type: "POST",
              url: '/user/agreePolicy',
              headers: {
                  'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
              }
          });
        };
      });
    });
</script>
@endpush
@endif
@endsection