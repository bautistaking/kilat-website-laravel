@extends('base')

@section('content')
<div class="dashboard">
  @include('dashboard.components.header')
  @include('dashboard.components.sub-nav',['crumb' => 'Kidlat Entry Form 2019'])
  <div class="card text-center">
    <div class="section scrollspy">
        <div class="row">
          <div class="col s12">
            <form action="@if($edit == true) {{ url('/edit/entry') }} @else {{ url('/submit/entry') }} @endif" method="post"  enctype="multipart/form-data">
            @csrf
            @if($edit == true)
            <input type="hidden" name="uid" value="{{$entry->uid}}">
            @endif
            <ul class="stepper horizontal" id="stepper">

              @include('dashboard.steps.step1')

              @include('dashboard.steps.step2')

              @include('dashboard.steps.step3')

              @include('dashboard.steps.step4')

              @include('dashboard.steps.step5')

              @include('dashboard.steps.step6')

            </ul>
            </form>
          </div>
        </div>
    </div>
  </div>
</div>
<div id="rules" class="modal modal-fixed-footer">
  <div class="modal-content">
    @include('site.components.rules')
  </div>
  <div class="modal-footer">
    <a href="#!" class="modal-close waves-effect waves-dark btn pink">Close</a>
  </div>
</div>
@include('dashboard.components.footer')
@push('dependencies-js')
  <script src="https://unpkg.com/materialize-stepper@3.0.0/dist/js/mstepper.min.js"></script>
@endpush
@push('custom-js')
  <script src="{{ mix('js/form-wizard.js') }}"></script>
  <script>
   function validateField(destroyFeedback, form, activeStepContent) {
      if($('.date-field').val() == '') {
        Swal.fire({
            imageUrl: "/images/Error.svg",
            imageWidth: 100,
            imageHeight: 124,
            title: "Ooops! Looks like you missed something important.",
            text: "Kindly fill up all the required fields. Thank you!"
        });
        destroyFeedback(false);
      } else {
        destroyFeedback(true);
      }
      
   }
</script>
@endpush
@endsection