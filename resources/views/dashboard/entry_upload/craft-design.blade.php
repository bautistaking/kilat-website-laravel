<div class="craft-design">
    <div class="entry-card mb-4">
        <h3>Photo of the Design</h3>
        <div class="d-flex mb-3" style="column-gap: 25px;">
            <ul class="hori-list-ul ps-4 mb-4" style="list-style-type: disc;">
                <li>JPEG format</li>
                <li>300 dpi</li>
                <li>Longest side measuring approx. 420mm</li>
                <li>Put English translation if needed</li>
            </ul>
        </div>
        <div class="file-upload">
            <div class="d-flex align-items-center justify-content-center photo_dropzone_cd dropzone" style="column-gap: 25px;">
                <img src="/images/file-upload-icon.png" width="40" height="51" alt="">
                <div class="file-text">
                    <p class="mb-0">
                        Drop your file(s) here or <span class="fw-bold">browse</span><br/>
                        Maximum file size of <span class="fw-bold">xxMB</span>
                    </p>
                </div>
            </div>
            <div id="invalid-photo-dropzone-cd" class="invalid-feedback"></div>
        </div>
    </div>
    <div class="entry-card mb-4">
        <h3>Concept Board (Optional)</h3>
        <div class="d-flex mb-3" style="column-gap: 25px;">
            <ul class="hori-list-ul ps-4 mb-4" style="list-style-type: disc;">
                <li>With explanation of its design merits</li>
                <li>JPEG format</li>
                <li>300 dpi</li>
                <li>Longest side measuring approx. 420mm</li>
                <li>Put English translation if needed</li>
            </ul>
        </div>
        <div class="file-upload">
            <div class="d-flex align-items-center justify-content-center concept_board_dropzone_cd dropzone" style="column-gap: 25px;">
                <img src="/images/file-upload-icon.png" width="40" height="51" alt="">
                <div class="file-text">
                    <p class="mb-0">
                        Drop your file(s) here or <span class="fw-bold">browse</span><br/>
                        Maximum file size of <span class="fw-bold">xxMB</span>
                    </p>
                </div>
            </div>
            <div id="invalid-concept-board-dropzone-cd" class="invalid-feedback"></div>
        </div>
    </div>
    <div class="entry-card mb-4">
        <h3>Client Certification</h3>
        <div class="mb-3">
            <ul class="hori-list-ul ps-4 mb-4" style="list-style-type: disc;">
                <li>Should be signed by a senior-ranking client (use Pro-Forma)</li>
            </ul>
        </div>
        <div class="file-upload">
            <div class="d-flex align-items-center justify-content-center client_certification_dropzone_cd dropzone" style="column-gap: 25px;">
                <img src="/images/file-upload-icon.png" width="40" height="51" alt="">
                <div class="file-text">
                    <p class="mb-0">
                        Drop your file(s) here or <span class="fw-bold">browse</span><br/>
                        Maximum file size of <span class="fw-bold">xxMB</span>
                    </p>
                </div>
            </div>
            <div id="invalid-client-certification-dropzone-cd" class="invalid-feedback"></div>
        </div>
    </div>
    <div class="entry-card mb-4">
        <h3 class="mb-4">Online links if available</h3>
        @if ($edit == false)
            <div class="form online-link-container">
                <div>
                    <input type="text" class="form-control form-control-lg lg fs-18 fst-italic" id="online-link0" name="online_link[]" placeholder="https://" style="width: 622px;">
                    <a href="javascript:void(0)" class="delete-link"><i class="fas fa-times"></i></a>
                </div>
            </div>
        @else 
            <div class="form online-link-container">
            @foreach($entry->reference() as $r)
                @if($loop->index == 0)
                    <div>
                        <input type="text" class="form-control form-control-lg lg fs-18 fst-italic" id="online-link0" name="online_link[]" placeholder="https://" style="width: 622px;" value="{{$r->link}}">
                        <a href="javascript:void(0)" class="delete-link"><i class="fas fa-times"></i></a>
                    </div>
                @else
                    <div>
                        <input type="text" class="form-control form-control-lg lg fs-18 fst-italic mt-3" id="online-link{{$loop->index}}" name="online_link[]" placeholder="https://" style="width: 622px;" value="{{$r->link}}">
                        <a href="javascript:void(0)" class="delete-link"><i class="fas fa-times"></i></a>
                    </div>
                @endif
            @endforeach
            </div>
        @endif
        <button class="btn btn-outline-primary primary brown small mt-4 btn_more_link text-uppercase" style="min-width:191px">Add more links</button>
    </div>
</div>