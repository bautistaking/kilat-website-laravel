<div class="creative-storytelling">
    <div class="entry-card mb-4">
        <h3>Concept Board</h3>
        <div class="d-flex mb-3" style="column-gap: 25px;">
            <ul class="hori-list-ul ps-4 mb-4" style="list-style-type: disc;">
                <li>With explanation of its design merits</li>
                <li>JPEG format</li>
                <li>300 dpi</li>
                <li>Longest side measuring approx. 420mm</li>
                <li>Put English translation if needed</li>
            </ul>
        </div>
        <div class="file-upload">
            <div class="d-flex align-items-center justify-content-center concept_board_dropzone_cs dropzone" style="column-gap: 25px;">
                <img src="/images/file-upload-icon.png" width="40" height="51" alt="">
                <div class="file-text">
                    <p class="mb-0">
                        Drop your file(s) here or <span class="fw-bold">browse</span><br/>
                        Maximum file size of <span class="fw-bold">xxMB</span>
                    </p>
                </div>
            </div>
        </div>
        <div id="invalid-concept-board-dropzone-cs" class="invalid-feedback"></div>
    </div>
    <div class="entry-card mb-4">
        <h3>Case Video</h3>
        <div class="d-flex mb-3" style="column-gap: 25px;">
            <ul class="hori-list-ul ps-4 mb-4" style="list-style-type: disc;">
                <li>Maximum of 2 minutes</li>
                <li>MP4 format</li>
                <li>NTSC 720x486 at 29.97 fps</li>
                <li>Minimum of 8 mbps or 1080p/720p 23.976 fps</li>
            </ul>
            <ul class="hori-list-ul ps-4 mb-4" style="list-style-type: disc;">
                <li>Minimum of 16 mbps</li>
                <li>H.264 compression</li>
                <li>Audio 44.1 kHz stereo</li>
                <li>1080p recommended</li>
            </ul>
        </div>
        <div class="file-upload">
            <div class="d-flex align-items-center justify-content-center case_video_dropzone_cs dropzone" style="column-gap: 25px;">
                <img src="/images/file-upload-icon.png" width="40" height="51" alt="">
                <div class="file-text">
                    <p class="mb-0">
                        Drop your file(s) here or <span class="fw-bold">browse</span><br/>
                        Maximum file size of <span class="fw-bold">xxMB</span>
                    </p>
                </div>
            </div>
        </div>
        <div id="invalid-case-video-dropzone-cs" class="invalid-feedback"></div>
    </div>
    <div class="entry-card mb-4">
        <h3>Film</h3>
        <div class="d-flex mb-3" style="column-gap: 25px;">
            <ul class="hori-list-ul ps-4 mb-4" style="list-style-type: disc;">
                <li>Maximum of 2 minutes</li>
                <li>MP4 format</li>
                <li>NTSC 720x486 at 29.97 fps</li>
                <li>Minimum of 8 mbps or 1080p/720p 23.976 fps</li>
            </ul>
            <ul class="hori-list-ul ps-4 mb-4" style="list-style-type: disc;">
                <li>Minimum of 16 mbps</li>
                <li>H.264 compression</li>
                <li>Audio 44.1 kHz stereo</li>
                <li>1080p recommended</li>
            </ul>
        </div>
        <div class="file-upload">
            <div class="d-flex align-items-center justify-content-center film_dropzone_cs dropzone" style="column-gap: 25px;">
                <img src="/images/file-upload-icon.png" width="40" height="51" alt="">
                <div class="file-text">
                    <p class="mb-0">
                        Drop your file(s) here or <span class="fw-bold">browse</span><br/>
                        Maximum file size of <span class="fw-bold">xxMB</span>
                    </p>
                </div>
            </div>
        </div>
        <div id="invalid-film-dropzone-cs" class="invalid-feedback"></div>
    </div>
    <div class="entry-card mb-4">
        <h3>Posters / Press / Static Digital Content</h3>
        <div class="mb-3">
            <ul class="hori-list-ul ps-4 mb-4" style="list-style-type: disc;">
                <li>JPEG format</li>
                <li>300 dpi</li>
                <li>Longest side measuring approx. 420mm</li>
                <li>Put English translation if needed</li>
            </ul>
        </div>
        <div class="file-upload">
            <div class="d-flex align-items-center justify-content-center posters_dropzone_cs dropzone" style="column-gap: 25px;">
                <img src="/images/file-upload-icon.png" width="40" height="51" alt="">
                <div class="file-text">
                    <p class="mb-0">
                        Drop your file(s) here or <span class="fw-bold">browse</span><br/>
                        Maximum file size of <span class="fw-bold">xxMB</span>
                    </p>
                </div>
            </div>
        </div>
        <div id="invalid-posters-dropzone-cs" class="invalid-feedback"></div>
    </div>
    <div class="entry-card mb-4">
        <h3>Audio</h3>
        <div class="mb-3">
            <ul class="hori-list-ul ps-4 mb-4" style="list-style-type: disc;">
                <li>MP3 format</li>
                <li>64 Kbps BitRate</li>
                <li>AAC 44.1 khz stereo</li>
            </ul>
        </div>
        <div class="file-upload">
            <div class="d-flex align-items-center justify-content-center audio_dropzone_cs dropzone" style="column-gap: 25px;">
                <img src="/images/file-upload-icon.png" width="40" height="51" alt="">
                <div class="file-text">
                    <p class="mb-0">
                        Drop your file(s) here or <span class="fw-bold">browse</span><br/>
                        Maximum file size of <span class="fw-bold">xxMB</span>
                    </p>
                </div>
            </div>
        </div>
        <div id="invalid-audio-dropzone-cs" class="invalid-feedback"></div>
    </div>
    <div class="entry-card mb-4">
        <h3 class="mb-4">Working URL</h3>
        @if ($edit == false)
            <div class="form working-link-container">
                <div>
                    <input type="text" class="form-control form-control-lg lg fs-18 fst-italic" id="working_url0" name="working_url[]" placeholder="https://" style="width: 622px;">
                    <a href="javascript:void(0)" class="working-delete-link"><i class="fas fa-times"></i></a>
                </div>
            </div>
        @else 
            <div class="form working-link-container">
            @foreach($entry->reference() as $r)
                @if($loop->index == 0)
                    <div>
                        <input type="text" class="form-control form-control-lg lg fs-18 fst-italic" id="online-working_url0" name="working_url[]" placeholder="https://" style="width: 622px;" value="{{$r->link}}">
                        <a href="javascript:void(0)" class="working-delete-link"><i class="fas fa-times"></i></a>
                    </div>
                @else
                    <div>
                        <input type="text" class="form-control form-control-lg lg fs-18 fst-italic mt-3" id="working_url0{{$loop->index}}" name="working_url[]" placeholder="https://" style="width: 622px;" value="{{$r->link}}">
                        <a href="javascript:void(0)" class="working-delete-link"><i class="fas fa-times"></i></a>
                    </div>
                @endif
            @endforeach
            </div>
        @endif
        <button class="btn btn-outline-primary primary brown small mt-4 btn_more_working_link text-uppercase" style="min-width:191px">Add more links</button>
    </div>
    <div class="entry-card mb-4">
        <h3>ASC Clearance</h3>
        <div class="mb-3">
            <ul class="hori-list-ul ps-4 mb-4" style="list-style-type: disc;">
                <li>JPEG format</li>
            </ul>
        </div>
        <div class="file-upload">
            <div class="d-flex align-items-center justify-content-center asc_clearance_dropzone_cs dropzone" style="column-gap: 25px;">
                <img src="/images/file-upload-icon.png" width="40" height="51" alt="">
                <div class="file-text">
                    <p class="mb-0">
                        Drop your file(s) here or <span class="fw-bold">browse</span><br/>
                        Maximum file size of <span class="fw-bold">xxMB</span>
                    </p>
                </div>
            </div>
        </div>
        <div id="invalid-asc-clearance-dropzone-cs" class="invalid-feedback"></div>
    </div>
    <div class="entry-card mb-4">
        <h3>Media Certification of Performance</h3>
        <div class="mb-3">
            <ul class="hori-list-ul ps-4 mb-4" style="list-style-type: disc;">
                <li>JPEG format</li>
            </ul>
        </div>
        <div class="file-upload">
            <div class="d-flex align-items-center justify-content-center media_certification_dropzone_cs dropzone" style="column-gap: 25px;">
                <img src="/images/file-upload-icon.png" width="40" height="51" alt="">
                <div class="file-text">
                    <p class="mb-0">
                        Drop your file(s) here or <span class="fw-bold">browse</span><br/>
                        Maximum file size of <span class="fw-bold">xxMB</span>
                    </p>
                </div>
            </div>
        </div>
        <div id="invalid-media-certification-dropzone-cs" class="invalid-feedback"></div>
    </div>
    <div class="entry-card mb-4">
        <h3>Client Certification</h3>
        <div class="mb-3">
            <ul class="hori-list-ul ps-4 mb-4" style="list-style-type: disc;">
                <li>Should be signed by a senior-ranking client (use Pro-Forma)</li>
            </ul>
        </div>
        <div class="file-upload">
            <div class="d-flex align-items-center justify-content-center client_certification_dropzone_cs dropzone" style="column-gap: 25px;">
                <img src="/images/file-upload-icon.png" width="40" height="51" alt="">
                <div class="file-text">
                    <p class="mb-0">
                        Drop your file(s) here or <span class="fw-bold">browse</span><br/>
                        Maximum file size of <span class="fw-bold">xxMB</span>
                    </p>
                </div>
            </div>
        </div>
        <div id="invalid-client-certification-dropzone-cs" class="invalid-feedback"></div>
    </div>
</div>