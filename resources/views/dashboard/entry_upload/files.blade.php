<div class="entry-card mb-4 classic_container engagement_container effective_container">
    <h3>Case Video</h3>
    <p><strong>(As needed based on your category)</strong></p>
    <div class="d-flex mb-3" style="column-gap: 25px;">
        <ul class="hori-list-ul ps-4 mb-4" style="list-style-type: disc;">
            <li>2 minutes maximum</li>
            <li>MP4</li>
            <li>NTSC 720 x 486 at 29.97 fps</li>
            <li>Minimum of 8 mbps or 1080p/720p 23.976 fps</li>
        </ul>
        <ul class="hori-list-ul ps-4 mb-4" style="list-style-type: disc;">
            <li>Minimum of 16mbps</li>
            <li>H.264 compression</li>
            <li>Audio 44.1 kHz stereo</li>
            <li>1080p recommended</li>
        </ul>
    </div>
    <div class="file-upload">
        <div class="d-flex align-items-center justify-content-center case_study_video_dropzone dropzone" style="column-gap: 25px;">
            <img src="/images/file-upload-icon.png" width="40" height="51" alt="">
            <div class="file-text">
                <p class="mb-0">
                    Drop your file(s) here or <span class="fw-bold">browse</span><br/>
                    Maximum file size of <span class="fw-bold">xxMB</span>
                </p>
            </div>
        </div>
    </div>
    <div id="invalid-case-study-video-dropzone" class="invalid-feedback"></div>
</div>

<div class="entry-card mb-4 design_container classic_container outdoor_container digital_c_container engagement_container effective_container">
    <h3>Concept Board</h3>
    <p><strong>(As needed based on your category)</strong></p>
    <div class="d-flex mb-3" style="column-gap: 25px;">
        <ul class="hori-list-ul ps-4 mb-4" style="list-style-type: disc;">
            <li>With explanation of its design merits</li>
            <li>JPEG format</li>
            <li>300 dpi</li>
            <li>Longest side measuring approx. 420mm</li>
            <li>Put English translation if needed</li>
        </ul>
    </div>
    <div class="file-upload">
        <div class="d-flex align-items-center justify-content-center concept_board_dropzone dropzone" style="column-gap: 25px;">
            <img src="/images/file-upload-icon.png" width="40" height="51" alt="">
            <div class="file-text">
                <p class="mb-0">
                    Drop your file(s) here or <span class="fw-bold">browse</span><br/>
                    Maximum file size of <span class="fw-bold">xxMB</span>
                </p>
            </div>
        </div>
        <div id="invalid-concept-board-dropzone" class="invalid-feedback"></div>
    </div>
</div>

<div class="entry-card mb-4 classic_container engagement_container">
    <h3>Posters</h3>
    <p><strong>(As needed based on your category)</strong></p>
    <div class="mb-3">
        <ul class="hori-list-ul ps-4 mb-4" style="list-style-type: disc;">
            <li>JPEG format</li>
            <li>300 dpi</li>
            <li>Longest side measuring approx. 420mm</li>
            <li>Put English translation if needed</li>
        </ul>
    </div>
    <div class="file-upload">
        <div class="d-flex align-items-center justify-content-center posters_dropzone dropzone" style="column-gap: 25px;">
            <img src="/images/file-upload-icon.png" width="40" height="51" alt="">
            <div class="file-text">
                <p class="mb-0">
                    Drop your file(s) here or <span class="fw-bold">browse</span><br/>
                    Maximum file size of <span class="fw-bold">xxMB</span>
                </p>
            </div>
        </div>
    </div>
    <div id="invalid-posters-dropzone" class="invalid-feedback"></div>
</div>

<div class="entry-card mb-4 classic_container audio_container engagement_container">
    <h3>Audio</h3>
    <p><strong>(As needed based on your category)</strong></p>
    <div class="mb-3">
        <ul class="hori-list-ul ps-4 mb-4" style="list-style-type: disc;">
            <li>MP3 format</li>
            <li>64 Kbps BitRate</li>
            <li>AAC 44.1 khz stereo</li>
        </ul>
    </div>
    <div class="file-upload">
        <div class="d-flex align-items-center justify-content-center audio_dropzone dropzone" style="column-gap: 25px;">
            <img src="/images/file-upload-icon.png" width="40" height="51" alt="">
            <div class="file-text">
                <p class="mb-0">
                    Drop your file(s) here or <span class="fw-bold">browse</span><br/>
                    Maximum file size of <span class="fw-bold">xxMB</span>
                </p>
            </div>
        </div>
    </div>
    <div id="invalid-audio-dropzone" class="invalid-feedback"></div>
</div>

<div class="entry-card mb-4 classic_container film_c_container engagement_container">
    <h3>Film</h3>
    <p><strong>(As needed based on your category)</strong></p>
    <div class="d-flex mb-3" style="column-gap: 25px;">
        <ul class="hori-list-ul ps-4 mb-4" style="list-style-type: disc;">
            <li>MP4 format</li>
            <li>NTSC 720 x 486 at 29.97 fps, minimum 8mbps or 1080p/720p 23.976fps, minimum 16 mbps (1080p recommended)</li>
            <li>H.264 compression</li>
            <li>44.1 khz stereo audio</li>
        </ul>
    </div>
    <div class="file-upload">
        <div class="d-flex align-items-center justify-content-center film_dropzone dropzone" style="column-gap: 25px;">
            <img src="/images/file-upload-icon.png" width="40" height="51" alt="">
            <div class="file-text">
                <p class="mb-0">
                    Drop your file(s) here or <span class="fw-bold">browse</span><br/>
                    Maximum file size of <span class="fw-bold">xxMB</span>
                </p>
            </div>
        </div>
        <div id="invalid-film-dropzone" class="invalid-feedback"></div>
    </div>
</div>

<div class="entry-card mb-4 design_container classic_container audio_container outdoor_container film_c_container engagement_container effective_container">
    <h3>ASC Clearance</h3>
    <p><strong>(As needed based on your category)</strong></p>
    <div class="mb-3">
        <ul class="hori-list-ul ps-4 mb-4" style="list-style-type: disc;">
            <li>JPEG/PDF format</li>
        </ul>
    </div>
    <div class="file-upload">
        <div class="d-flex align-items-center justify-content-center asc_clearance_dropzone dropzone" style="column-gap: 25px;">
            <img src="/images/file-upload-icon.png" width="40" height="51" alt="">
            <div class="file-text">
                <p class="mb-0">
                    Drop your file(s) here or <span class="fw-bold">browse</span><br/>
                    Maximum file size of <span class="fw-bold">xxMB</span>
                </p>
            </div>
        </div>
        <div id="invalid-asc-clearance-dropzone" class="invalid-feedback"></div>
    </div>
</div>

<div class="entry-card mb-4 design_container classic_container audio_container outdoor_container film_c_container engagement_container effective_container">
    <h3>Media Certification of Performance</h3>
    <p><strong>(As needed based on your category)</strong></p>
    <div class="mb-3">
        <ul class="hori-list-ul ps-4 mb-4" style="list-style-type: disc;">
            <li>JPEG/PDF format</li>
            <!-- <li>Required</li> -->
        </ul>
    </div>
    <div class="file-upload">
        <div class="d-flex align-items-center justify-content-center media_certification_dropzone dropzone" style="column-gap: 25px;">
            <img src="/images/file-upload-icon.png" width="40" height="51" alt="">
            <div class="file-text">
                <p class="mb-0">
                    Drop your file(s) here or <span class="fw-bold">browse</span><br/>
                    Maximum file size of <span class="fw-bold">xxMB</span>
                </p>
            </div>
        </div>
        <div id="invalid-media-certification-dropzone" class="invalid-feedback"></div>
    </div>
</div>

<div class="entry-card mb-4 classic_container audio_container design_container outdoor_container digital_c_container film_c_container engagement_container effective_container">
    <h3>Client Certification</h3>
    <div class="mb-3">
        <ul class="hori-list-ul ps-4 mb-4" style="list-style-type: disc;">
            <li>Should be signed by a senior-ranking client (use Pro-Forma)</li>
            <li>JPEG/PDF format</li>
        </ul>
    </div>
    <div class="file-upload">
        <div class="d-flex align-items-center justify-content-center client_certification_dropzone dropzone" style="column-gap: 25px;">
            <img src="/images/file-upload-icon.png" width="40" height="51" alt="">
            <div class="file-text">
                <p class="mb-0">
                    Drop your file(s) here or <span class="fw-bold">browse</span><br/>
                    Maximum file size of <span class="fw-bold">xxMB</span>
                </p>
            </div>
        </div>
        <div id="invalid-client-certification-dropzone" class="invalid-feedback"></div>
    </div>
</div>

<div class="entry-card mb-4 classic_container audio_container design_container digital_c_container film_c_container engagement_container effective_container">
    <h3 class="mb-4">Campaign components, Youtube Links & Working URLs</h3>
    <p><strong>(As needed based on your category)</strong></p>
    @if ($edit == false)
        <div class="form online-link-container">
            <div>
                <input type="text" class="form-control form-control-lg lg fs-18 fst-italic" id="online-link0" name="online_link[]" placeholder="https://" style="width: 622px;">
                <a href="javascript:void(0)" class="delete-link"><i class="fas fa-times"></i></a>
            </div>
        </div>
    @else 
        <div class="form online-link-container">
        @foreach($entry->reference() as $r)
            @if($loop->index == 0)
                <div>
                    <input type="text" class="form-control form-control-lg lg fs-18 fst-italic" id="online-link0" name="online_link[]" placeholder="https://" style="width: 622px;" value="{{$r->link}}">
                    <a href="javascript:void(0)" class="delete-link"><i class="fas fa-times"></i></a>
                </div>
            @else
                <div>
                    <input type="text" class="form-control form-control-lg lg fs-18 fst-italic mt-3" id="online-link{{$loop->index}}" name="online_link[]" placeholder="https://" style="width: 622px;" value="{{$r->link}}">
                    <a href="javascript:void(0)" class="delete-link"><i class="fas fa-times"></i></a>
                </div>
            @endif
        @endforeach
        </div>
    @endif
    <button class="btn btn-outline-primary primary brown small mt-4 btn_more_link text-uppercase" style="min-width:191px">Add more links</button>
</div>

<div class="entry-card mb-4 wc_container">
    <h3>Written Case Study</h3>
    <p><strong>(As needed based on your category)</strong></p>
    <div class="mb-3">
        <ul class="hori-list-ul ps-4 mb-4" style="list-style-type: disc;">
            <li>JPEG format</li>
        </ul>
    </div>
    <div class="file-upload">
        <div class="d-flex align-items-center justify-content-center written_case_study_dropzone dropzone" style="column-gap: 25px;">
            <img src="/images/file-upload-icon.png" width="40" height="51" alt="">
            <div class="file-text">
                <p class="mb-0">
                    Drop your file(s) here or <span class="fw-bold">browse</span><br/>
                    Maximum file size of <span class="fw-bold">xxMB</span>
                </p>
            </div>
        </div>
    </div>
    <div id="invalid-written-case-dropzone" class="invalid-feedback"></div>
</div>

<!-- <div class="entry-card mb-4 design_container outdoor_container digital_c_container ">
    <h3 class="mb-4">Working URL</h3>
    <p><strong>(As needed based on your category)</strong></p>
    @if ($edit == false)
    <div class="form working-link-container">
        <div>
            <input type="text" class="form-control form-control-lg lg fs-18 fst-italic" id="working_url0" name="working_url[]" placeholder="https://" style="width: 622px;">
            <a href="javascript:void(0)" class="working-delete-link"><i class="fas fa-times"></i></a>
        </div>
    </div>
    @else
    <div class="form working-link-container">
        @foreach($entry->reference() as $r)
        @if($loop->index == 0)
        <div>
            <input type="text" class="form-control form-control-lg lg fs-18 fst-italic" id="online-working_url0" name="working_url[]" placeholder="https://" style="width: 622px;" value="{{$r->link}}">
            <a href="javascript:void(0)" class="working-delete-link"><i class="fas fa-times"></i></a>
        </div>
        @else
        <div>
            <input type="text" class="form-control form-control-lg lg fs-18 fst-italic mt-3" id="working_url0{{$loop->index}}" name="working_url[]" placeholder="https://" style="width: 622px;" value="{{$r->link}}">
            <a href="javascript:void(0)" class="working-delete-link"><i class="fas fa-times"></i></a>
        </div>
        @endif
        @endforeach
    </div>
    @endif
    <button class="btn btn-outline-primary primary brown small mt-4 btn_more_working_link text-uppercase" style="min-width:191px">Add more links</button>
</div> -->

<div class="entry-card mb-4 df_container">
    <h3>Demo Film</h3>
    <p><strong>(As needed based on your category)</strong></p>
    <div class="d-flex mb-3" style="column-gap: 25px;">
        <ul class="hori-list-ul ps-4 mb-4" style="list-style-type: disc;">
            <li>MP4 format</li>
            <li>NTSC 720x486 at 29.97 fps</li>
            <li>Minimum of 8 mbps or 1080p/720p 23.976 fps</li>
            <li>Minimum of 16 mbps</li>
        </ul>
        <ul class="hori-list-ul ps-4 mb-4" style="list-style-type: disc;">
            <li>H.264 compression</li>
            <li>Audio 44.1 kHz stereo</li>
            <li>1080p recommended</li>
            <li>Put English subtitles if needed</li>
            <li>Maximum of 2 minutes</li>
        </ul>
    </div>
    <div class="file-upload">
        <div class="d-flex align-items-center justify-content-center demo_film_dropzone dropzone" style="column-gap: 25px;">
            <img src="/images/file-upload-icon.png" width="40" height="51" alt="">
            <div class="file-text">
                <p class="mb-0">
                    Drop your file(s) here or <span class="fw-bold">browse</span><br/>
                    Maximum file size of <span class="fw-bold">xxMB</span>
                </p>
            </div>
        </div>
    </div>
    <div id="invalid-demo-film-dropzone" class="invalid-feedback"></div>
</div>

<div class="entry-card mb-4 audio_container">
    <h3>English Translation</h3>
    <p><strong>(As needed based on your category)</strong></p>
    <div class="mb-3">
        <ul class="hori-list-ul ps-4 mb-4" style="list-style-type: disc;">
            <li>PDF format</li>
        </ul>
    </div>
    <div class="file-upload">
        <div class="d-flex align-items-center justify-content-center english_translation_dropzone dropzone" style="column-gap: 25px;">
            <img src="/images/file-upload-icon.png" width="40" height="51" alt="">
            <div class="file-text">
                <p class="mb-0">
                    Drop your file(s) here or <span class="fw-bold">browse</span><br/>
                    Maximum file size of <span class="fw-bold">xxMB</span>
                </p>
            </div>
        </div>
        <div id="invalid-english-translation-dropzone" class="invalid-feedback"></div>
    </div>
</div>

<div class="entry-card mb-4 design_container outdoor_container digital_c_container">
    <h3>JPEG of the Piece</h3>
    <p><strong>(As needed based on your category)</strong></p>
    <div class="d-flex mb-3" style="column-gap: 25px;">
        <ul class="hori-list-ul ps-4 mb-4" style="list-style-type: disc;">
            <li>JPEG format</li>
            <li>300 dpi</li>
            <li>Longest side measuring approx. 420mm</li>
            <li>Put English translation if needed</li>
        </ul>
    </div>
    <div class="file-upload">
        <div class="d-flex align-items-center justify-content-center photo_piece_dropzone dropzone" style="column-gap: 25px;">
            <img src="/images/file-upload-icon.png" width="40" height="51" alt="">
            <div class="file-text">
                <p class="mb-0">
                    Drop your file(s) here or <span class="fw-bold">browse</span><br/>
                    Maximum file size of <span class="fw-bold">xxMB</span>
                </p>
            </div>
        </div>
    </div>
    <div id="invalid-photo-piece-dropzone" class="invalid-feedback"></div>
</div>

<div class="entry-card mb-4 jp_container">
    <h3>Entry Piece</h3>
    <p><strong>(As needed based on your category)</strong></p>
    <div class="d-flex mb-3" style="column-gap: 25px;">
        <ul class="hori-list-ul ps-4 mb-4" style="list-style-type: disc;">
            <li>JPEG format</li>
            <li>300 dpi</li>
            <li>Longest side measuring approx. 420mm</li>
            <li>Put English translation if needed</li>
        </ul>
    </div>
    <div class="file-upload">
        <div class="d-flex align-items-center justify-content-center entry_dropzone dropzone" style="column-gap: 25px;">
            <img src="/images/file-upload-icon.png" width="40" height="51" alt="">
            <div class="file-text">
                <p class="mb-0">
                    Drop your file(s) here or <span class="fw-bold">browse</span><br/>
                    Maximum file size of <span class="fw-bold">xxMB</span>
                </p>
            </div>
        </div>
    </div>
    <div id="invalid-entry-dropzone" class="invalid-feedback"></div>
</div>