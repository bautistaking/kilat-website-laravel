@extends('base')  
@push('styles')
    <style>
        .spinner-grow,
        .loading-label {
            display: none;
        }

       /* .file-preview-entry > .review-line {
            display: none;
        }*/

        @if ($entry->category == '1')
            .file-preview-entry > .review-line:nth-child(1),
            .file-preview-entry > .review-line:nth-child(2),
            .file-preview-entry > .review-line:nth-child(3),
            .file-preview-entry > .review-line:nth-child(4),
            .file-preview-entry > .review-line:nth-child(5),
            .file-preview-entry > .review-line:nth-child(6),
            .file-preview-entry > .review-line:nth-child(7),
            .file-preview-entry > .review-line:nth-child(8),
            .file-preview-entry > .review-line:nth-child(9),
            .file-preview-entry > .review-line:nth-child(11) {
                display: flex;
            }

            .file-preview-entry > .review-line:nth-child(10),
            .file-preview-entry > .review-line:nth-child(12),
            .file-preview-entry > .review-line:nth-child(13),
            .file-preview-entry > .review-line:nth-child(14),
            .file-preview-entry > .review-line:nth-child(15) {
                display: none;
            }
        @elseif ($entry->category == '2')
            .file-preview-entry > .review-line:nth-child(1),
            .file-preview-entry > .review-line:nth-child(2),
            .file-preview-entry > .review-line:nth-child(4),
            .file-preview-entry > .review-line:nth-child(5),
            .file-preview-entry > .review-line:nth-child(6),
            .file-preview-entry > .review-line:nth-child(7),
            .file-preview-entry > .review-line:nth-child(8),
            .file-preview-entry > .review-line:nth-child(9),
            .file-preview-entry > .review-line:nth-child(11) {
                display: flex;
            }

            .file-preview-entry > .review-line:nth-child(3),
            .file-preview-entry > .review-line:nth-child(10),
            .file-preview-entry > .review-line:nth-child(12),
            .file-preview-entry > .review-line:nth-child(13),
            .file-preview-entry > .review-line:nth-child(14),
            .file-preview-entry > .review-line:nth-child(15) {
                display: none;
            }
        @elseif ($entry->category == '3' || $entry->category == '4')
            .file-preview-entry > .review-line:nth-child(1),
            .file-preview-entry > .review-line:nth-child(2),
            .file-preview-entry > .review-line:nth-child(4),
            .file-preview-entry > .review-line:nth-child(5),
            .file-preview-entry > .review-line:nth-child(6),
            .file-preview-entry > .review-line:nth-child(7),
            .file-preview-entry > .review-line:nth-child(8),
            .file-preview-entry > .review-line:nth-child(9),
            .file-preview-entry > .review-line:nth-child(11),
            .file-preview-entry > .review-line:nth-child(12) {
                display: flex;
            }

            .file-preview-entry > .review-line:nth-child(3),
            .file-preview-entry > .review-line:nth-child(10),
            .file-preview-entry > .review-line:nth-child(13),
            .file-preview-entry > .review-line:nth-child(14),
            .file-preview-entry > .review-line:nth-child(15) {
                display: none;
            }
        @elseif ($entry->category == '5' || (
            $entry->sub_category == '34' ||
            $entry->sub_category == '52' ||
            $entry->sub_category == '53' ||
            $entry->sub_category == '54'))
            .file-preview-entry > .review-line:nth-child(1),
            .file-preview-entry > .review-line:nth-child(2),
            .file-preview-entry > .review-line:nth-child(4),
            .file-preview-entry > .review-line:nth-child(5),
            .file-preview-entry > .review-line:nth-child(7),
            .file-preview-entry > .review-line:nth-child(8),
            .file-preview-entry > .review-line:nth-child(9),
            .file-preview-entry > .review-line:nth-child(11),
            .file-preview-entry > .review-line:nth-child(13) {
                display: flex;
            }

            .file-preview-entry > .review-line:nth-child(6),
            .file-preview-entry > .review-line:nth-child(3),
            .file-preview-entry > .review-line:nth-child(10),
            .file-preview-entry > .review-line:nth-child(12),
            .file-preview-entry > .review-line:nth-child(14),
            .file-preview-entry > .review-line:nth-child(15) {
                display: none;
            }
        @elseif ($entry->category == '5' || (
            $entry->sub_category == '42' ||
            $entry->sub_category == '43' ||
            $entry->sub_category == '44' ||
            $entry->sub_category == '45' ||
            $entry->sub_category == '46' ||
            $entry->sub_category == '47' ||
            $entry->sub_category == '48' ||
            $entry->sub_category == '49' ))
            .file-preview-entry > .review-line:nth-child(1),
            .file-preview-entry > .review-line:nth-child(2),
            .file-preview-entry > .review-line:nth-child(6),
            .file-preview-entry > .review-line:nth-child(7),
            .file-preview-entry > .review-line:nth-child(8),
            .file-preview-entry > .review-line:nth-child(9),
            .file-preview-entry > .review-line:nth-child(10),
            .file-preview-entry > .review-line:nth-child(11),
            .file-preview-entry > .review-line:nth-child(12){
                display: flex;
            }

            .file-preview-entry > .review-line:nth-child(4),
            .file-preview-entry > .review-line:nth-child(5),
            .file-preview-entry > .review-line:nth-child(3),
            .file-preview-entry > .review-line:nth-child(13),
            .file-preview-entry > .review-line:nth-child(14),
            .file-preview-entry > .review-line:nth-child(15) {
                display: none;
            }
        @elseif ($entry->category == '5' || (
            $entry->sub_category == '50' ||
            $entry->sub_category == '51' ||
            $entry->sub_category == '56' ||
            $entry->sub_category == '58' ||
            $entry->sub_category == '59' ||
            $entry->sub_category == '60' ||
            $entry->sub_category == '61' ||
            $entry->sub_category == '62' ))
            .file-preview-entry > .review-line:nth-child(1),
            .file-preview-entry > .review-line:nth-child(2),
            .file-preview-entry > .review-line:nth-child(6),
            .file-preview-entry > .review-line:nth-child(7),
            .file-preview-entry > .review-line:nth-child(8),
            .file-preview-entry > .review-line:nth-child(9),
            .file-preview-entry > .review-line:nth-child(10),
            .file-preview-entry > .review-line:nth-child(11),
            .file-preview-entry > .review-line:nth-child(12){
                display: flex;
            }

            .file-preview-entry > .review-line:nth-child(4),
            .file-preview-entry > .review-line:nth-child(5),
            .file-preview-entry > .review-line:nth-child(3),
            .file-preview-entry > .review-line:nth-child(13),
            .file-preview-entry > .review-line:nth-child(14),
            .file-preview-entry > .review-line:nth-child(15) {
                display: none;
            }
        @else 
            .file-preview-entry > .review-line:nth-child(1),
            .file-preview-entry > .review-line:nth-child(2),
            .file-preview-entry > .review-line:nth-child(4),
            .file-preview-entry > .review-line:nth-child(5),
            .file-preview-entry > .review-line:nth-child(6),
            .file-preview-entry > .review-line:nth-child(7),
            .file-preview-entry > .review-line:nth-child(8),
            .file-preview-entry > .review-line:nth-child(9),
            .file-preview-entry > .review-line:nth-child(11),
            .file-preview-entry > .review-line:nth-child(12){
                display: flex;
            }

            .file-preview-entry > .review-line:nth-child(3),
            .file-preview-entry > .review-line:nth-child(10),
            .file-preview-entry > .review-line:nth-child(13),
            .file-preview-entry > .review-line:nth-child(14),
            .file-preview-entry > .review-line:nth-child(15) {
                display: none;
            }
        @endif

        .status {
            color: #fff;
            margin-top: 15px;
            font-size: 14px;
        }

        .edit-link {
            color: #fff;
            text-decoration: none;
        }

        .edit-link > i {
            color: #d7a333;
            font-size: 20px;
        }

        .edit-link:hover {
            color: #d7a333;
        }

        video {
            max-width: 272px;
            height: auto;
        }
    </style>
@endpush
@section('content')
    @include('dashboard.components.header')
        <div class="bg">
            <div class="wrapper-default pt72">
                <div class="container-fluid py72">
                    <div class="d-flex align-items-center justify-content-between mb-3">
                        <a href="/dashboard" class="top-action">
                            <i class="bi bi-chevron-left me-2"></i> <span>Go Back</span>
                        </a>
                        <p class="status">{{$entry->status}} | {{$entry->payment_status}}</p>
                    </div>
                    <div class="d-flex align-items-center justify-content-end mb-3">
                        <a class="edit-link" href="{{url('/entry/edit/'.$entry->uid)}}">
                            <i class="bi bi-pencil-square me-2"></i>Edit details
                        </a>
                    </div>
                    <h1 class="page-title text-center mb-0">{{$entry->entry_name}}</h1>
                    <div class="tabs entry">
                        <ul class="nav nav-pills" id="myTab" role="tablist">
                            <li class="nav-item" role="presentation">
                                <button class="nav-link active" id="pills-category-tab" data-bs-toggle="pill" data-bs-target="#pills-category" type="button" role="tab" aria-controls="pills-category" aria-selected="true">Category details</button>
                            </li>
                            <li class="nav-item" role="presentation">
                                <button class="nav-link" id="pills-campaign-tab" data-bs-toggle="pill" data-bs-target="#pills-campaign" type="button" role="tab" aria-controls="pills-campaign" aria-selected="false">Campaign details</button>
                            </li>
                            <li class="nav-item" role="presentation">
                                <button class="nav-link" id="pills-entry -tab" data-bs-toggle="pill" data-bs-target="#pills-entry " type="button" role="tab" aria-controls="pills-entry " aria-selected="false">Entry upload</button>
                            </li>
                            @if ($entry->category == 1)
                                <li class="nav-item" role="presentation">
                                    <button class="nav-link" id="pills-written-tab" data-bs-toggle="pill" data-bs-target="#pills-written" type="button" role="tab" aria-controls="pills-written" aria-selected="false">Written submission</button>
                                </li>
                            @endif
                            <li class="nav-item" role="presentation">
                                <button class="nav-link" id="pills-entry -tab" data-bs-toggle="pill" data-bs-target="#pills-agency " type="button" role="tab" aria-controls="pills-entry " aria-selected="false">Agency details</button>
                            </li>
                        </ul>
                        <div class="tab-content" id="myTabContent">
                            <div class="tab-pane fade show active" id="pills-category" role="tabpanel" aria-labelledby="pills-category-tab">
                                <div class="review-card">
                                    <div class="d-flex align-items-start justify-content-between my-3">
                                        <h3>Category details</h3>
                                    </div>
                                    <div class="review-line">
                                        <div class="label">Category</div>
                                        <div class="value">{{$entry->category()->name}}</div>
                                    </div>
                                    <div class="review-line">
                                        <div class="label">Subcategory</div>
                                        <div class="value text-uppercase">{{$entry->sub_category()->name}}</div>
                                    </div>
                                    <div class="review-line">
                                        <div class="label">Entry type</div>
                                        <div class="value">{{$entry->type}}</div>
                                    </div>
                                    <div class="review-line">
                                        <div class="label">Entry type list</div>
                                        <div class="value">{{$entry->entry_type_list}}</div>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="pills-campaign" role="tabpanel" aria-labelledby="pills-campaign-tab">
                                <div class="review-card">
                                    <div class="d-flex align-items-start justify-content-between my-3">
                                        <h3>Campaign details</h3>
                                    </div>
                                    <div class="review-line">
                                        <div class="label">Entry name</div>
                                        <div class="value">{{$entry->entry_name}}</div>
                                    </div>
                                    <div class="review-line">
                                        <div class="label">Ad title/s</div>
                                        <div class="value">{{$entry->ad_details}}</div>
                                    </div>
                                    <div class="review-line">
                                        <div class="label">Advertiser</div>
                                        <div class="value">{{$entry->advertiser}}</div>
                                    </div>
                                    <div class="review-line">
                                        <div class="label">Brand</div>
                                        <div class="value">{{$entry->brand}}</div>
                                    </div>
                                    <div class="review-line">
                                        <div class="label">Agency</div>
                                        <div class="value">{{$entry->agency}}</div>
                                    </div>
                                    <div class="review-line">
                                        <div class="label">Date of first publication</div>
                                        <div class="value">{{date('F j, Y', strtotime($entry->date_publication))}}</div>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="pills-entry" role="tabpanel" aria-labelledby="pills-entry-tab">
                                <div class="review-card">
                                    <div class="d-flex align-items-start justify-content-between mb-3 my-3">
                                        <div>
                                            <h3>Entry upload</h3>
                                        </div>
                                    </div>
                                    <div class="file-preview-entry">
                                        <div class="review-line">
                                            <div class="label">Concept board</div>
                                            <div class="value">
                                                <div class="img mt-2">
                                                    @if($entry->concept_board_dropzone != null)
                                                        @if(count($file_concept) > 0)
                                                            @foreach($file_concept as $fc)
                                                                <img class="mb-2" src="{{asset('storage/'.$fc->path)}}">
                                                            @endforeach
                                                        @else 
                                                            <img id="preview_concept_board_dropzone" src="{{asset('storage/'.$entry->concept_board_dropzone)}}" alt="">
                                                        @endif
                                                    @else
                                                        <p>N/A</p>
                                                    @endif
                                                </div>
                                            </div>
                                        </div>
                                        <div class="review-line">
                                            <div class="label">Case video</div>
                                            <div class="value">
                                                <div class="img mt-2">
                                                    @if($entry->case_study_video_dropzone != null)
                                                    <video id="preview_case_study_video_dropzone" controls>
                                                        <source src="{{asset('storage/'.$entry->case_study_video_dropzone)}}">
                                                        Your browser does not support the video tag.
                                                    </video>
                                                    @else
                                                        <p>N/A</p>
                                                    @endif
                                                </div>
                                            </div>
                                        </div>
                                        <div class="review-line">
                                            <div class="label">Written Case Study</div>
                                            <div class="value">
                                                <div class="img mt-2">
                                                    @if($entry->written_case_dropzone == null)
                                                        <p>N/A</p>
                                                    @elseif($entry->written_case_dropzone != null && strstr($entry->written_case_dropzone, '.') == '.pdf')
                                                        <a href="{{asset('storage/'.$entry->written_case_dropzone)}}" target="_blank">
                                                            <img src="{{asset('images/pdf.png')}}">
                                                        </a>
                                                    @else
                                                    <img id="written_case_dropzone" src="{{asset('storage/'.$entry->written_case_dropzone)}}" alt="">
                                                    @endif
                                                </div>
                                            </div>
                                        </div>
                                        <div class="review-line">
                                            <div class="label">ASC Clearance</div>
                                            <div class="value">
                                                <div class="img mt-2">
                                                    @if($entry->asc_clearance_dropzone == null)
                                                        <p>N/A</p>
                                                    @elseif($entry->asc_clearance_dropzone != null && strstr($entry->asc_clearance_dropzone, '.') == '.pdf')
                                                        <a href="{{asset('storage/'.$entry->asc_clearance_dropzone)}}" target="_blank">
                                                            <img src="{{asset('images/pdf.png')}}">
                                                        </a>
                                                    @else
                                                        <img id="preview_asc_clearance_dropzone" src="{{asset('storage/'.$entry->asc_clearance_dropzone)}}" alt="">
                                                    @endif
                                                </div>
                                            </div>
                                        </div>
                                        <div class="review-line">
                                            <div class="label">Media Certification of Performance</div>
                                            <div class="value">
                                                <div class="img mt-2">
                                                    @if($entry->media_certification_dropzone == null)
                                                        <p>N/A</p>
                                                    @elseif($entry->media_certification_dropzone != null && strstr($entry->media_certification_dropzone, '.') == '.pdf')
                                                        <a href="{{asset('storage/'.$entry->media_certification_dropzone)}}" target="_blank">
                                                            <img src="{{asset('images/pdf.png')}}">
                                                        </a>
                                                    @else
                                                    <img id="preview_media_certification_dropzone" src="{{asset('storage/'.$entry->media_certification_dropzone)}}" alt="">
                                                    @endif
                                                </div>
                                            </div>
                                        </div>
                                        <div class="review-line">
                                            <div class="label">Client Certification</div>
                                            <div class="value">
                                                <div class="img mt-2">
                                                    @if($entry->client_certification_dropzone == null)
                                                        <p>N/A</p>
                                                    @elseif($entry->client_certification_dropzone != null && strstr($entry->client_certification_dropzone, '.') == '.pdf')
                                                        <a href="{{asset('storage/'.$entry->client_certification_dropzone)}}" target="_blank">
                                                            <img src="{{asset('images/pdf.png')}}">
                                                        </a>
                                                    @else
                                                    <img id="preview_client_certification_dropzone" src="{{asset('storage/'.$entry->client_certification_dropzone)}}" alt="">
                                                    @endif
                                                </div>
                                            </div>
                                        </div>
                                        <div class="review-line">
                                            <div class="label">Film</div>
                                            <div class="value">
                                                <div class="img mt-2">
                                                    @if($entry->film_dropzone != null)

                                                        @if(count($file_film) > 0)
                                                            @foreach($file_film as $ff)
                                                                <video class="mb-2" controls>
                                                                    <source src="{{asset('storage/'.$ff->path)}}">
                                                                    Your browser does not support the video tag.
                                                                </video>
                                                            @endforeach
                                                        @else 
                                                            <video id="preview_film_dropzone" controls>
                                                                <source src="{{asset('storage/'.$entry->film_dropzone)}}">
                                                                Your browser does not support the video tag.
                                                            </video>
                                                        @endif
                                                    @else
                                                        <p>N/A</p>
                                                    @endif
                                                </div>
                                            </div>
                                        </div>
                                        <div class="review-line">
                                            <div class="label">Posters</div>
                                            <div class="value">
                                                <div class="img mt-2">
                                                    @if($entry->posters_dropzone != null)
                                                        @if(count($file_posters) > 0)
                                                            @foreach($file_posters as $fp)
                                                                <img class="mb-2" src="{{asset('storage/'.$fp->path)}}">
                                                            @endforeach
                                                        @else 
                                                            <img id="preview_posters_dropzone" src="{{asset('storage/'.$entry->posters_dropzone)}}" alt="">
                                                        @endif
                                                    @else
                                                        <p>N/A</p>
                                                    @endif
                                                </div>
                                            </div>
                                        </div>
                                        <div class="review-line">
                                            <div class="label">Audio</div>
                                            <div class="value">
                                                @if($entry->audio_dropzone != null)
                                                    @if(count($file_audio) > 0)
                                                        @foreach($file_audio as $fa)
                                                            <audio controls>
                                                                <source src="{{asset('storage/'.$fa->path)}}" type="audio/mpeg">
                                                                Your browser does not support the audio tag.
                                                            </audio>
                                                        @endforeach
                                                    @else 
                                                        <audio id="preview_audio_dropzone" controls>
                                                            <source src="{{asset('storage/'.$entry->audio_dropzone)}}" type="audio/mpeg">
                                                            Your browser does not support the audio tag.
                                                        </audio>
                                                    @endif
                                                </audio>
                                                @else
                                                    <p>N/A</p>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="review-line">
                                            <div class="label">Working URL</div>
                                            <div class="value">
                                                @if(count($references) > 0)
                                                    @foreach($references as $r)
                                                    <div class="mb-2">
                                                        <span class="fs-18 fw-500">{{$r->link}}</span>
                                                    </div>
                                                    @endforeach
                                                @else 
                                                    <div class="mb-2">
                                                        <p>N/A</p>
                                                    </div>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="review-line">
                                            <div class="label">Online links</div>
                                            <div class="value">
                                                @if(count($references) > 0)
                                                    @foreach($references as $r)
                                                        <div class="mb-2">
                                                            <span class="fs-18 fw-500">{{$r->link}}</span>
                                                        </div>
                                                    @endforeach
                                                @else 
                                                    <div class="mb-2">
                                                        <p>N/A</p>
                                                    </div>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="review-line">
                                            <div class="label">Demo Film</div>
                                            <div class="value">
                                                <div class="img mt-2">
                                                    @if($entry->demo_film_dropzone != null)
                                                    <video id="preview_demo_film_dropzone" controls>
                                                        <source src="{{asset('storage/'.$entry->demo_film_dropzone)}}">
                                                        Your browser does not support the video tag.
                                                    </video>
                                                    @else
                                                        <p>N/A</p>
                                                    @endif
                                                </div>
                                            </div>
                                        </div>
                                        <div class="review-line">
                                            <div class="label">English Translation if needed</div>
                                            <div class="value">
                                                <div class="img mt-2">
                                                    @if($entry->english_translation_dropzone != null)
                                                    <a class="link-color" href="{{asset('storage/'.$entry->english_translation_dropzone)}}">Download English Translation PDF</a>
                                                    @else
                                                        <p>N/A</p>
                                                    @endif
                                                </div>
                                            </div>
                                        </div>
                                        <div class="review-line">
                                            <div class="label">JPEG of the Piece</div>
                                            <div class="value">
                                                <div class="img mt-2">
                                                    @if($entry->photo_piece_dropzone != null)
                                                    <img id="preview_photo_piece_dropzone" src="{{asset('storage/'.$entry->photo_piece_dropzone)}}" alt="">
                                                    @else
                                                        <p>N/A</p>
                                                    @endif
                                                </div>
                                            </div>
                                        </div>
                                        <div class="review-line">
                                            <div class="label">Entry Piece</div>
                                            <div class="value">
                                                <div class="img mt-2">
                                                    @if($entry->entry_dropzone != null)
                                                    <img id="preview_entry_dropzone" src="{{asset('storage/'.$entry->entry_dropzone)}}" alt="">
                                                    @else
                                                        <p>N/A</p>
                                                    @endif
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div style="border-bottom:1px solid rgba(201,210,218,0.5)"></div>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="pills-written" role="tabpanel" aria-labelledby="pills-written-tab">
                                <div class="review-card">
                                    <div class="d-flex align-items-start justify-content-between my-3">
                                        <h3>Written submission</h3>
                                    </div>
                                    <div class="review-line flex-column">
                                        <h3>Creative effectiveness</h3>
                                        <p>
                                            Creative Effectiveness celebrates the measurable impact of creativity on business results.<br/>
                                            To help the jury appreciate your entry, please fill up the written submission as succinctly and clearly as possible.
                                        </p>
                                    </div>
                                    <div class="review-line flex-column">
                                        <h3>Campaign Summary ({{str_word_count($entry->campaign_summary)}})</h3>
                                        <p>
                                            {{$entry->campaign_summary}}
                                        </p>
                                    </div>
                                    <div class="review-line flex-column">
                                        <h3>Objectives ({{str_word_count($entry->objectives)}})</h3>
                                        <p>
                                            {{$entry->objectives}}
                                        </p>
                                    </div>
                                    <div class="review-line flex-column">
                                        <h3>Strategy ({{str_word_count($entry->strategy)}})</h3>
                                        <p>
                                            {{$entry->strategy}}
                                        </p>
                                    </div>
                                    <div class="review-line flex-column">
                                        <h3>Execution ({{str_word_count($entry->execution)}})</h3>
                                        <p>
                                            {{$entry->execution}}
                                        </p>
                                    </div>
                                    <div class="review-line flex-column">
                                        <h3>Results ({{str_word_count($entry->results)}})</h3>
                                        <p>
                                            {{$entry->results}}
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="pills-agency" role="tabpanel" aria-labelledby="pills-campaign-tab">
                                <div class="review-card">
                                    <div class="d-flex align-items-start justify-content-between my-3">
                                        <h3>Agency details</h3>
                                    </div>
                                    <div class="review-line">
                                        <div class="label">Member</div>
                                        <div class="value">{{$entry->member}}</div>
                                    </div>
                                    <div class="review-line">
                                        <div class="label">Name of agency contact person</div>
                                        <div class="value">{{$entry->agency_contact}}</div>
                                    </div>
                                    <div class="review-line">
                                        <div class="label">Position</div>
                                        <div class="value">{{$entry->position}}</div>
                                    </div>
                                    <div class="review-line">
                                        <div class="label">Phone number</div>
                                        <div class="value">{{$entry->phone_number}}</div>
                                    </div>
                                    <div class="review-line">
                                        <div class="label">Email address</div>
                                        <div class="value">{{$entry->email}}</div>
                                    </div>
                                    <div class="review-line">
                                        @if(!empty($entry->credits()))
                                        <div class="label">Credits</div>
                                        <div class="value">
                                            @foreach($entry->credits() as $credit)
                                                <div class="mb-4">
                                                    <span class="fs-18 fw-500">{{$credit->name}}</span> <br/>
                                                    <span class="fw-400">{{$credit->role}}</span>
                                                </div>
                                            @endforeach
                                        </div>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @include('dashboard.components.footer')
@push('custom-js')
<script>
    $(document).ready(activateQuery())

    function activateQuery() {

    }
</script>
@endpush
@endsection