<div class="sub-nav z-depth-1">
  <div class="container">
    <div>
      <a href="{{url('/judge/dashboard/list')}}"><i class="material-icons text-color -purple">home</i></a>
      @if($crumb != '')
      <i class="material-icons">arrow_forward_ios</i> {{$crumb}}
      @endif
    </div>

    <a class='dropdown-trigger flex align-center text-color -black' data-target='logout'>
      Hello,&nbsp;<strong>{{ Auth::user()->name }}</strong><i class="material-icons right text-color -pink">expand_more</i>
    </a>

    <!-- Dropdown Structure -->
    <ul id='logout' class='dropdown-content'>
      <li><a href="{{ url('/logout') }}">Logout</a></li>
    </ul>

  </div>
</div>