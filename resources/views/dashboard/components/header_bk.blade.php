<header>
    <div class="header fixed-top">
        <div class="wrapper-default">
            <nav class="navbar navbar-expand navbar-dark">
                <div class="container-fluid">
                    <a class="navbar-brand pt-0 pb-0" href="/dashboard">
                        <img src="{{url('/images/kidlat-logo-horizontal.png')}}" alt="" width="145" height="56">
                    </a>
                    <div class="d-flex align-items-center">
                        <div class="navbar-nav mr-3">
                            <a class="nav-link active" aria-current="page" href="/dashboard">Home</a>
                            <a class="nav-link" href="{{url('/my-entries')}}">Your Entries</a>
                            <a class="nav-link" href="{{url('/my-invoice')}}">Your Invoice</a>
                            <a class="nav-link profile-link" href="javascript:void(0)">
                                @if (auth()->user()->name != "")
                                    {{auth()->user()->name}}
                                @else
                                    <span>CreativeFest 2022</span>
                                @endif
                                <i class="fas fa-external-link-alt"></i>
                            </a>
                        </div>
                        <a href="{{url('/documents/kidlat_rules.pdf')}}" target="_blank">
                            <button type="button" class="btn btn-outline-primary primary small ms-3 text-uppercase" style="min-width:182px">Download rules</button>
                        </a>
                        <a href="{{url('/submit-entry')}}">
                            <button type="button" class="btn btn-primary primary small ms-3 text-uppercase" style="min-width:185px">Submit an entry</button>
                        </a>
                    </div>
                </div>
            </nav>
        </div>
    </div>
</header>