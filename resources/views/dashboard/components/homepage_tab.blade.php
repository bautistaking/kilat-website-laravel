<style>
    .tabs .tab-content .hori-list.divider {
        padding-bottom: 20px;
    }

    .sub-header span {
        display: block;
        text-align: justify;
    }

    #myTabContent p,
    #myTabContent span {
        color: #3B4652 !important;
    }

    .table-fee {
        width: 100%;
        text-align: center;
        border-radius: 1em;
        border-collapse: separate;
        border-spacing: 0;
        min-width: 350px;
    }

    .table-fee th,
    .table-fee td {
        padding: 5px;
    }

    .table-fee tr > th:nth-child(2),
    .table-fee tr > th:nth-child(3),
    .table-fee tr > td:nth-child(1) {
        font-weight: bold;
        color: #5d2800;
    }

    .table-fee tr {
        font-size: 16px;
    }


    table tr th,
    table tr td {
      border-right: 1px solid #bbb;
      border-bottom: 1px solid #bbb;
      padding: 5px;
    }

    table tr th:first-child,
    table tr td:first-child {
      border-left: 1px solid #bbb;
    }
    table tr th {
      border-top: 1px solid #bbb;
    }


    /* top-left border-radius */
    table tr:first-child th:first-child {
      border-top-left-radius: 20px;
    }

    /* top-right border-radius */
    table tr:first-child th:last-child {
      border-top-right-radius: 20px;
    }

    /* bottom-left border-radius */
    table tr:last-child td:first-child {
      border-bottom-left-radius: 20px;
    }

    /* bottom-right border-radius */
    table tr:last-child td:last-child {
      border-bottom-right-radius: 20px;
    }
</style>
<div class="tabs">
    <!-- Nav tabs -->
    <ul class="nav nav-pills" id="myTab" role="tablist">
        <li class="nav-item" role="presentation">
            <button class="nav-link active" id="pills-rules-tab" data-bs-toggle="pill" data-bs-target="#pills-rules" type="button" role="tab" aria-controls="pills-rules" aria-selected="false">Competition Rules</button>
        </li>
        <li class="nav-item" role="presentation">
            <button class="nav-link" id="pills-fees-tab" data-bs-toggle="pill" data-bs-target="#pills-fees" type="button" role="tab" aria-controls="pills-fees" aria-selected="false">How to Enter and Entry Fees</button>
        </li>
        <li class="nav-item" role="presentation">
            <button class="nav-link" id="pills-awards-tab" data-bs-toggle="pill" data-bs-target="#pills-awards" type="button" role="tab" aria-controls="pills-awards" aria-selected="false">Special Awards and Rankings</button>
        </li>
        <li class="nav-item" role="presentation">
            <button class="nav-link" id="pills-categories-tab" data-bs-toggle="pill" data-bs-target="#pills-categories" type="button" role="tab" aria-controls="pills-categories" aria-selected="true">Categories</button>
        </li>
        <li class="nav-item" role="presentation">
            <button class="nav-link" id="pills-requirements-tab" data-bs-toggle="pill" data-bs-target="#pills-requirements" type="button" role="tab" aria-controls="pills-requirements" aria-selected="false">Requirements</button>
        </li>
    </ul>
    <div class="tab-content" id="myTabContent">
        <div class="tab-pane fade  show active" id="pills-rules" role="tabpanel" aria-labelledby="pills-rules-tab">
            <div class="hori-list mb-4 divider">
                <h2 class="heading-title mb-4">KIDLAT AWARDS 2022</h2>
                <p class="sub-header">
                    The Annual Creative Show of the Association of Accredited Advertising Agencies of the Philippines the official Philippine partner of the Cannes Lions.
                </p>
            </div>
            <div class="hori-list mb-4 divider">
                <h2 class="heading-title mb-4">Important dates</h2>
                <p class="sub-header mb-3">
                    <span class="mb-1">Eligibility — March 1, 2021 to December 15, 2022</span>
                    <span class="mb-1">Call for Entries/Kidlat Launch — Week of November 22, 2022</span>                
                </p>
            </div>
            <div class="hori-list mb-4 divider">
                <h2 class="heading-title mb-4">Deadline of submission of entries</h2>
                <p class="sub-header mb-3">
                    <span class="mb-1">Early Bird: January 24, 2023</span>
                    <span class="mb-1">Last Bird: January 27, 2023</span>
                </p>
            </div>
            <div class="hori-list mb-4 divider" style="border-bottom:0px; !important">
                <h2 class="heading-title mb-4">Rules</h2>
                <p class="sub-header mb-3">
                    <span>1. The Kidlat Awards is open to all 4As and non-4As member agencies based in the Philippines. This includes creative and digital agencies, production houses, media, PR, and design shops</span><br>

                    <span>2. Only entries made by Philippine-based agencies, production houses, media, PR and design shops that have first aired, gone live, been posted online, or made available for public download between March 1, 2021 to December 15, 2022 are qualified for the 2022 Kidlat Awards. If implementation of an entry exceeds this period, the majority of the campaign must fall within this time to qualify. Private screenings or previews are not eligible.   </span><br>

                    <span>*For Creative Effectiveness entries the eligibility period is from January 1, 2019 to February 28, 2021 to accommodate results.</span><br>

                    <span>3. Work can be entered in as many categories if relevant and qualified. </span><br>

                    <span>4. To be eligible, a piece of work must be accompanied by a fully accomplished entry form (available on <a href="http://www.kidlatawards.com">www.kidlatawards.com</a>) complete support documents, material requirements and payment. To help in preparing an entry, please refer to the Technical Requirements for Submission section. </span><br>

                    <span>5. All entries must have been created within the context of a normal paying contract with a client who has covered all media and production costs, except in the case of self-promotion and non-profit ads. A client certification will be required, signed by an officer with the rank of Assistant Brand Manager or higher. Please use the “Client Certification Pro-Forma.” </span><br>

                    <span>6. Prior permission from the client/owner of the rights of the work should be sought before entering any piece into the show. </span><br>

                    <span>7. A. Entries are subject to prescreening. The organizers have the right to refuse entries which offend national or religious sentiment, public taste or which breach any applicable laws, 4A’s by-laws or ASC rules. The Jury can vote to recommend the transfer of a material to another category during the shortlist round. The Kidlat Creative Committee, however, has the final word on reclassification. </span><br>

                    <span>7. 7.	B. The organizers have the right to refuse entries from agencies suspended by the 4A’s if said entries were released within the suspension period. </span><br>

                    <span>8. Work entered in previous Kidlat shows are not qualified even if its implementation date falls within the dates of eligibility except for Creativity in Strategy & Effectiveness.</span><br>

                    <span>9. A campaign or parts thereof entered in previous Kidlat shows shall not qualify for the current judging period. For long running campaigns of more than two years, only the material that was aired or published within the judging period can be entered.</span><br>

                    <span>10. The organizers may contact the client for queries related to any entry during the pre-screening or judging process, should any question about the implementation or presentation of the work arise. </span><br>

                    <span>11. There must be no indication on the actual entry identifying the entrant agency. For videos, no mention or any identifying visual of the agency is allowed on the material. For jpegs and mounted boards, there must be no such information seen on the material. Necessarily, however, this rule does not apply to self-promotion entries.</span>
                </p>
            </div>
        </div>
        <div class="tab-pane fade" id="pills-fees" role="tabpanel" aria-labelledby="pills-fees-tab">
            <div class="hori-list mb-4 divider">
                <h2 class="heading-title mb-4">How to enter</h2>
                <p class="sub-header mb-3">
                    <span>1. Register your agency at www.kidlatawards.com.</span><br>

                    <span>2. Choose the categories and upload your work. Please be guided by the technical requirements provided below for all entries.</span><br>

                    <span>3. Review your entries and the total amount to be paid.</span><br>

                    <span>4. Pay the 4As Philippines via check or bank payment on or before the last day of entry submission.*</span><br>

                    <span>5. Upon payment, you will receive an email to confirm payment and an acknowledgment of your entries. </span><br>

                    <span>*Uploaded entries will only be accepted in the system AFTER payment. 
                            For the entries to be valid, payments must be cleared within the prescreening period. </span><br>
                </p>
            </div>
            <h2 class="heading-title mb-4">Entry fees</h2>
            <table class="table-fee">
                <tr>
                    <th rowspan="2"></th>
                    <th colspan="2">Early Bird<br/>January 24, 2023</th>
                    <th colspan="2">Last Bird<br/>January 27, 2023</th>
                    <!-- <th colspan="2">Late Bird<br/>Jan.27, 2023</th> -->
                </tr>
                <tr>
                    <th>Single</th>
                    <th>Campaign</th>
                    <th>Single</th>
                    <th>Campaign</th>
                    <!-- <th>Single</th>
                    <th>Campaign</th> -->
                </tr>
                <tr>
                    <td>4As member</td>
                    <td>P5,000<br/>+12% VAT</td>
                    <td>P7,000<br/>+12% VAT</td>
                    <td>P6,000<br/>+12% VAT</td>
                    <td>P8,000<br/>+12% VAT</td>
                    <!-- <td>P7,000<br/>+12% VAT</td>
                    <td>P9,000<br/>+12% VAT</td> -->
                </tr>
                <tr>
                    <td>Non-4As member</td>
                    <td>P6,500<br/>+12% VAT</td>
                    <td>P8,500<br/>+12% VAT</td>
                    <td>P7,500<br/>+12% VAT</td>
                    <td>P9,500<br/>+12% VAT</td>
                    <!-- <td>P8,500<br/>+12% VAT</td>
                    <td>P10,500<br/>+12% Vat</td> -->
                </tr>
            </table><br/><br/>
            <div class="hori-list mb-4 divider">
                <h2 class="heading-title mb-4">First-Timer Discount</h2>
                <p class="sub-header mb-3">
                    <span>If this is your company’s first time to enter in Kidlat Awards a 10% discount will be given to the entry fee.</span>
                </p>
            </div>
        </div>
        <div class="tab-pane fade" id="pills-awards" role="tabpanel" aria-labelledby="pills-awards-tab">
            <div class="hori-list mb-4">
                <h2 class="heading-title mb-4">Special Awards and Rankings</h2>
                <p class="sub-header mb-3">

                    <span> 1. <strong>GRAND KIDLAT </strong>– OnOne Grand Kidlat winner may be chosen for each of the following categories below. The Grand Kildat winner must be chosen from among the gold winners by unanimous vote of their respective juries. </span>

                    <ul>
                        <li>Classic Creativity</li>
                        <li>Creativity in Engagement</li>
                        <li>Creativity in Entertainment</li>
                        <li>Creativity in Experience</li>
                    </ul>
                    <br>

                    <span>
                        2. <strong>KIDLAT SINAG </strong> – One Kidlat Sinag winner may be chosen from among the gold winners of the Creativity for Good category through unanimous vote by all Creativity for Good jury members. This award does not count in the overall award points for the winner’s agency network. 
                    </span><br><br>
                     
                    <span>
                        3. <strong>KIDLAT AGENCY OF THE YEAR</strong> – Awarded to the agency with the most number of points in the Kidlat categories of Classic Creativity, Creativity in Engagement, Creativity in Entertainment, Creativity in Experience, Creativity in Strategy & Effectivity, and the following Craft subcategories:
                    </span><br><br>

                    <span><strong>B.01. Audio: Best Copywriting </strong></span><br>
                    <span><strong>B.02. Design/Outdoor/Print: Best Art Direction </strong></span><br>
                    <span><strong>B.03. Design/Outdoor/Print: Best Character Design </strong></span><br>
                    <span><strong>B.04. Design/Outdoor/Print: Best Copywriting </strong></span><br>
                    <span><strong>B.05. Design/Outdoor/Print: Best Digital Imaging </strong></span><br>
                    <span><strong>B.06. Design/Outdoor/Print: Best Illustration </strong></span><br>
                    <span><strong>B.07. Design/Outdoor/Print: Best Photography </strong></span><br>
                    <span><strong>B.08. Design/Outdoor/Print: Best Typography </strong></span><br>
                    <span><strong>B.09. Digital (excluding Film): Best Art Direction </strong></span><br>
                    <span><strong>B.10. Digital (excluding Film): Best Copywriting </strong></span><br>
                    <span><strong>B.11. Digital (excluding Film): Best Character Design * </strong></span><br>
                    <span><strong>B.12. Digital (excluding Film): Best Digital Imaging * </strong></span><br>
                    <span><strong>B.13. Digital (excluding Film): Best Illustration </strong></span><br>
                    <span><strong>B.14. Digital (excluding Film): Best Photography </strong></span><br>
                    <span><strong>B.15. Digital (excluding Film): Best Typography </strong></span><br>
                    <span><strong>B.16. Digital: Best UX/UI </strong></span><br>
                    <span><strong>B.17. Film: Best Art Direction </strong></span><br>
                    <span><strong>B.18. Film: Best Copywriting </strong></span><br>
                     
                    <span>4. <strong>KIDLAT ADVERTISER OF THE YEAR</strong> – Awarded to the client with the most number of points, excluding those from the Craft category.</span><br><br>
                     
                    <span>5. <strong>KIDLAT AUDIO PRODUCTION HOUSE OF THE YEAR</strong> – Awarded to the audio production house with the most number of points in the following Craft subcategories:</span><br><br>

                    <span><strong>B.19. Audio: Best Direction </strong></span><br>
                    <span><strong>B.20. Audio: Best Music/Sound Design </strong></span><br>
                    <span><strong>B.21. Audio: Best Voice Performance </strong></span><br>
                    <span><strong>B.22. Film: Best Audio Craft </strong></span><br>
                     
                    <span>6. <strong>KIDLAT FILM PRODUCTION HOUSE OF THE YEAR</strong> – Awarded to the film production house with the most number of points in the following Craft subcategories: </span><br><br>

                    <span><strong>B.23. Film: Best Animation E24. Film: Best Cinematography </strong></span><br>
                    <span><strong>B.25. Film: Best Direction </strong></span><br>
                    <span><strong>B.26. Film: Best Editing </strong></span><br>
                    <span><strong>B.27. Film: Best Production Design </strong></span><br>
                    <span><strong>B.28. Film: Best Visual Effects and Digital Imaging  </strong></span><br>

                    <span>The winners of the Kidlat Agency of the Year, Kidlat Advertiser of the Year, and Kidlat Audio Production House of the Year, and Kidlat Film Production House of the Year will be determined by tallying the cumulative wins of the entrants after judging is done, based on the following point system:</span><br><br>

                    <span>Grand Kidlat: 5 pts.</span><br>
                    <span>Gold: 15 pts.</span><br>
                    <span>Silver: 7 pts.</span><br>
                    <span>Bronze: 3 pts.</span><br>
                    <span>Shortlist: 1 pt.</span><br>
                </p>
            </div>
        </div>
        <div class="tab-pane fade" id="pills-categories" role="tabpanel" aria-labelledby="pills-categories-tab">
            <div class="hori-list mb-4 divider">
                <h2 class="heading-title mb-4">Categories</h2>
                <p class="sub-header mb-3">
                    <span>There are 7 main categories</span>
                    <span><strong>⦁	Classic Creativity</strong></span>
                    <span><strong>⦁	Creativity in Craft </strong></span>
                    <span><strong>⦁	Creativity in Engagement</strong></span>
                    <span><strong>⦁	Creativity in Entertainment</strong></span>
                    <span><strong>⦁	Creativity in Experience</strong></span>
                    <span><strong>⦁	Creativity in Strategy & Effectiveness </strong></span>
                    <span><strong>⦁	Creativity for Good</strong></span>
                </p>
            </div>
            <div class="hori-list mb-4 divider">
                <h2 class="heading-title mb-4">A. Classic Creativity</h2>
                <p class="sub-header mb-3">
                    <span>
                        Celebrating creative excellence through classic marketing mediums. Open to single and campaign entries (two or more pieces). Components of the campaign can also be entered as separate single entries. 
                    </span><br><br>

                    <span><strong>A.01. Outdoor/Ambient </strong></span>
                    <span>Big and small-scale OOH including posters, billboards, outdoor LED screens, installations, performance art, murals, free-standing structures, ambient ads, and transit ads. </span><br>
                     
                    <span><strong>A.02. Print & Publication </strong></span>
                    <span>Print and press work published in dailies, books, and magazines.</span><br>
                     
                    <span><strong>A03. Film</strong></span>
                    <span>Any video or film that aired on television, in cinemas, or in digital and mobile media platforms.</span><br>
                     
                    <span><strong>A04. Audio</strong></span>
                    <span>All audio media content. This includes ads broadcast over public radio, podcasts, and innovative use of audio.  </span><br> 
                     
                    <span><strong>A.06. Design </strong></span>
                    <span>Work that uses visual craftsmanship to define a brand or communicate its key messages. These include brand identity materials, communication design, product design, packaging, interactive design, environmental design, and other design-driven pieces of work.  </span><br>
                </p>
            </div>
            <div class="hori-list mb-4 divider">
                <h2 class="heading-title mb-4">B. Creativity in Craft </h2>
                <p class="sub-header mb-3">
                    <span>
                        Each entry will be judged specifically on the quality of its craftsmanship. Only original, produced work will be accepted. Absolutely no purchased material (i.e. stock footage, purchased music).     
                    </span><br><br>

                    <span><strong>B.01 Audio: Best Copywriting </strong></span>
                    <span><strong>B.02. Design/Outdoor/Print: Best Art Direction </strong></span>
                    <span><strong>B.03. Design/Outdoor/Print: Best Character Design </strong></span>
                    <span><strong>B.04. Design/Outdoor/Print: Best Copywriting </strong></span>
                    <span><strong>B.05. Design/Outdoor/Print: Best Digital Imaging </strong></span>
                    <span><strong>B.06. Design/Outdoor/Print: Best Illustration </strong></span>
                    <span><strong>B.07. Design/Outdoor/Print: Best Photography </strong></span>
                    <span><strong>B.08. Design/Outdoor/Print: Best Typography </strong></span>
                    <span><strong>B.09. Digital (excluding Film): Best Art Direction </strong></span>
                    <span><strong>B.10. Digital (excluding Film): Best Copywriting </strong></span>
                    <span><strong>B.11. Digital (excluding Film): Best Character Design </strong></span>
                    <span><strong>B.12. Digital (excluding Film): Best Digital Imaging </strong></span>
                    <span><strong>B.13. Digital (excluding Film): Best Illustration </strong></span>
                    <span><strong>B.14. Digital (excluding Film): Best Photography </strong></span>
                    <span><strong>B.15. Digital (excluding Film): Best Typography </strong></span>
                    <span><strong>B.16. Digital: Best UX/UI </strong></span>
                    <span><strong>B.17. Film: Best Art Direction </strong></span>
                    <span><strong>B.18. Film: Best Copywriting </strong></span>
                    <span><strong>B.19.  Audio: Best Direction  </strong></span>
                    <span><strong>B.20. Audio: Best Music/Sound Design  </strong></span>
                    <span><strong>B.21. Audio: Best Voice Performance  </strong></span>
                    <span><strong>B.22. Film: Best Audio Craft  </strong></span>
                    <span><strong>B.23. Film: Best Animation </strong></span>
                    <span><strong>B.24. Film: Best Cinematography </strong></span>
                    <span><strong>B.25. Film: Best Direction </strong></span>
                    <span><strong>B.26. Film: Best Editing </strong></span>
                    <span><strong>B.27. Film: Best Production Design </strong></span>
                    <span><strong>B.28. Film: Best Visual Effects and Digital Imaging </strong></span>
                </p>
            </div>
            <div class="hori-list mb-4 divider">
                <h2 class="heading-title mb-4">C. Creativity in Engagement </h2>
                <p class="sub-header mb-3">
                    <span>
                        Celebrating insightful creativity that captivates at every touchpoint. Creating authentic interaction and immersive experiences that engage consumers and impact culture.
                    </span><br><br>

                    <span><strong>C.01. Creative B2B </strong></span>
                    <span>Celebrates game-changing creativity and effectiveness in work for products and services that are purchased by professionals on behalf of businesses.</span><br>
                     
                    <span><strong>C.02. Creative Data</strong></span>
                    <span>Celebrates the interplay of ideas and information. Entries will need to demonstrate how the work was enhanced or driven by the creative use, interpretation, analysis or application of data. The creative use of data must sit at the core of the idea and the results / impact must be clear and robust.</span><br>
                     
                    <span><strong>C.04. Media</strong></span>
                    <span>Celebrates the context of creativity. Entries will need to demonstrate an inspiring and innovative implementation of media ideas; work which is enhanced and amplified by a game-changing channel strategy.</span><br>
                     
                    <span><strong>C.05. PR</strong></span>
                    <span>Celebrates the craft of strategic and creative communication. Entries will need to demonstrate how original thinking, transformative insight and a strategy rooted in earned has influenced opinion and driven business, societal, and/or cultural change. Work with storytelling at its core, which established, protected and enhanced reputation and business of an organization or brand.</span><br>
                     
                    <span><strong>C.06. Social & Influencer</strong></span>
                    <span>Celebrates creative social thinking and strategic influencer marketing solutions. Entries will need to demonstrate how levels of engagement, social reach and the creative use of social media, brand ambassadors and influencers led to commercial success.</span><br>
                </p>
            </div>
            <div class="hori-list mb-4 divider">
                <h2 class="heading-title mb-4">D. Creativity in Entertainment</h2>
                <p class="sub-header mb-3">
                    <span>
                        Celebrates creativity that turns content into culture. Entries will need to demonstrate ideas that are unskippable; work which captivates in order to cut-through, communicated a brand message or connected with consumers in a new way.
                    </span><br><br>

                    <span><strong>D.01. Audio-visual Branded Content </strong></span>
                    <span>Includes Fiction, Non-fiction, Documentaries or Series made for VR, AR, Live Broadcast/Live Streaming, Audio Content such as podcast and other audio platforms, Brand Integration and Sponsorships/Partnerships</span><br>
                     
                    <span><strong>D.02. Talent</strong></span>
                    <span>Entertainment that features or is developed in collaboration with talent. Entries should demonstrate how the talent’s influence was leveraged to amplify a brand's message.</span><br>
                     
                    <span><strong>D.03. Gaming</strong></span>
                    <span>Games and gaming initiatives that communicate a brand, product or artist through the creative production, promotion and distribution of content with gaming at the core. </span><br>

                    <span><strong>D.04. Sports and Esports </strong></span>
                    <span>Rewarding examples of sports entertainment and initiatives across different platforms, including esports. </span><br>

                    <span><strong>D.05. Innovation</strong></span>
                    <span>Innovative branded content that pushes the boundaries of the industry utilizing forward thinking ideas to engage with the chosen audience.  </span><br>
                </p>
            </div>
            <div class="hori-list mb-4 divider"  style="border-bottom:0px !important;">
                <h2 class="heading-title mb-4">E. Creativity in Experience</h2>
                <p class="sub-header mb-3">
                    <span>
                        The Brand Experience & Activation category celebrate creative, comprehensive brand building through the next level use of experience design, activation, immersive, retail and 360° customer engagement. Entries should demonstrate how the customer journey, experience of the brand and optimization of every touch point led to increased brand affinity and commercial success.
                    </span><br><br>

                    <span><strong>E.01 Touchpoints & Technology</strong></span>
                    <span>The use of tech and multiple touchpoints across a brand experience or activation. Ex. Work that harnesses social media, mobile app, portable device, mobile tech to enhance a live experience or activation. Or Work that uses branded websites or microsites or creative use of AR / VR, voice activation to drive engagement. This also includes Branded Games, Digital Installations, Interactive Brand Videos.</span><br>

                    <span><strong>E.02. Brand Experience</strong></span>
                    <span>Any live, brand experience, retail experience or activation that was held at a consumer or B2B event. This may include, but is not limited to installations, product demos, trade shows, expos & pop-ups.  </span><br>

                    <span><strong>E.03. Guerrilla Marketing & Stunts </strong></span>
                    <span>Any brand experience or activation using guerrilla marketing, short / one-off live executions, street teams, publicity stunts and street stunts to drive customer engagement. </span><br>

                    <span><strong>E.04. Sponsorship & Brand Partnership</strong></span>
                    <span>Partnerships / sponsorships that create immediate and long-term brand experiences or activations. Entries will be judged on how effective the partnership / sponsorship was.</span><br>

                    <span><strong>E.05. Creative Commerce</strong></span>
                    <span>Celebrates the innovative and creative approach to online and offline commerce, payment solutions and transactional journeys. Entries will need to demonstrate how innovation and optimization at any point of the end-to-end customer journey led to increased consumer engagement and commercial success.</span><br>

                    <span><strong>E.06. Innovation</strong></span>
                    <span>Celebrates ground-breaking innovation, technology and problem solving. Standalone technological solutions including tools, products, models, platforms and other forms of adtech will also be recognized, as well as creative campaigns utilizing new technology.</span><br>
                </p>
            </div>
            <div class="hori-list mb-4 divider"  style="border-bottom:0px !important;">
                <h2 class="heading-title mb-4">F. Creativity in Strategy & Effectiveness</h2>
                <p class="sub-header mb-3">
                    <span>
                        Celebrating commercial effectiveness, strategic planning and creative application of solutions to unlock growth and measure impact.
                    </span><br><br>

                    <span><strong>F.01. Creative Effectiveness</strong></span>
                    <span>Celebrates the measurable impact of creative work. Entries into this category will need to demonstrate how an effective strategy rooted in creativity has met its chosen business objectives, how it generated positive customer outcomes and drove sustainable business impact over time. A number of criteria will be considered during judging and weighted as follows: 25% idea; 25% strategy; 50% impact and results.</span><br>

                    <span><strong>F.02. Creative Strategy</strong></span>
                    <span>Celebrates the idea behind the idea, how strategic planning can redefine a brand, reinvent its business, and influence consumers or wider culture. Entries will need to demonstrate exceptional interpretation of the business/brand challenge, breakthrough thinking and transformational problem-solving that led to compelling creative strategy. A number of criteria will be considered during judging and weighted as follows: 30% interpretation of business / brand challenge, 30% insight / breakthrough thinking, 20% creative idea, 20% outcome / results.</span><br>
                </p>
            </div>
            <div class="hori-list mb-4 divider"  style="border-bottom:0px !important;">
                <h2 class="heading-title mb-4">G. Creativity for Good </h2>
                <p class="sub-header mb-3">
                    <span>
                        This category recognizes that creativity can be a beacon of hope and change in this world. Open to single and campaign entries (two or more pieces). Components of the campaign can also be entered as separate single entries. 
                    </span><br><br>

                    <span><strong>G.01. Corporate Social Responsibility  </strong></span>
                    <span>Non-product or service-based social responsibility campaigns by brands, aimed at addressing social, ethical, and environmental issues. </span><br>

                    <span><strong>G.02. Non-Profit, Charity and Government  </strong></span>
                    <span>Advertising for government, public information, military, charities, and non-profit organizations. </span><br>

                    <span><strong>G.03. Public Services & Cause Appeals   </strong></span>
                    <span>Anti-smoking, anti-drug & other addictions, anti-drunk-driving, road & public safety awareness, health & hygiene awareness, political & religious messages, unions & associations, environmental awareness, human rights awareness, animal rights awareness, education, racial, ethnic & disability awareness, gender equality, volunteers & donation appeals, NGOs.  </span><br>
                </p>
            </div>
        </div>
        <div class="tab-pane fade" id="pills-requirements" role="tabpanel" aria-labelledby="pills-requirements-tab">
            <div class="hori-list mb-4 divider">
                <h2 class="heading-title mb-4">Technical Requirements for Submission </h2>
                <p class="sub-header mb-3">
                    Please prepare the following to make uploading of entries easier and faster
                </p>

            </div>
            <div class="hori-list mb-5 divider">
                <h2 class="heading-title mb-4">CLASSIC CREATIVE</h2>
                <h2 class="heading-title mb-4">CREATIVITY IN ENGAGEMENT</h2>
                <h2 class="heading-title mb-4">CREATIVITY IN ENTERTAINMENT</h2>
                <h2 class="heading-title mb-4">CREATIVITY IN EXPERIENCE</h2>

                <p class="sub-header mb-3">Depending on the subcategory, you may choose to submit your entry in one or more of the following formats that best represent the merit and excellence of your entry:</p>
                <ul class="hori-list-ul ps-4 mb-4" style="list-style-type: disc;">
                    <li>
                        <p class="ml-3 mb-0"><span class="label">JPEG of Concept Board</span>300 dpi, longest side measuring approximately 420 mm.</p>
                    </li>
                    <li>
                        <p class="ml-3 mb-0"><span class="label">Case Video</span>, with the following specifications:</p>

                        <span>- 2 minutes maximum</span><br>
                        <span>- MP4</span><br>
                        <span>- NTSC 720 x 486 at 29.97 fps, minimum 8mbps or 1080p/720p 23.976fps, minimum 16 mbps (1080p recommended)</span><br>
                        <span>- H.264 compression</span><br>
                        <span>- 44.1 khz stereo audio</span><br>
                        <span>- File size must not exceed 150MB</span><br><br>
                    </li>
                    <li>
                        <p class="ml-3 mb-0"><span class="label">Film</span>, with the following specifications:</p>

                        <span>- MP4</span><br>
                        <span>- NTSC 720 x 486 at 29.97 fps, minimum 8mbps or 1080p/720p 23.976fps, minimum 16 mbps (1080p recommended)</span><br>
                        <span>- H.264 compression</span><br>
                        <span>- 44.1 khz stereo audio</span><br>
                        <span>- File size must not exceed 150MB; for longer films that may exceed the maximum upload size, the entrant may provide an unlisted YouTube link so judges can view the work at the ideal resolution</span><br>
                        <span>- Put English subtitles if needed </span><br><br>
                    </li>
                    <li>
                        <p class="ml-3 mb-0"><span class="label">JPEG of Posters</span>300 dpi, longest side measuring approximately 420 mm.</p>
                        <span>- Put English subtitles if needed </span><br><br>
                    </li>
                    <li>
                        <p class="ml-3 mb-0"><span class="label">Audio</span></p>
                        <span>- MP3, 64 Kbps BitRate, AAC 44.1 khz stereo </span><br><br>
                    </li>
                    <li>
                        <p class="ml-3 mb-0"><span class="label">Working URL and Demo Film </span> (2 mins max) for digital entries </p>
                    </li>
                </ul>
                <p class="sub-header mb-1">Required documents:</p>
                <ul class="hori-list-ul ps-4 mb-0" style="list-style-type: disc;">
                    <li>
                        <p class="ml-3 mb-0"><span class="label">JPEG of Client Certification signed by a senior-ranking client (use Pro-Forma) </span></p>
                    </li>
                    <li>
                        <p class="ml-3 mb-0"><span class="label">JPEG of ASC Clearance if applicable </span></p>
                    </li>
                    <li>
                        <p class="ml-3 mb-0"><span class="label">JPEG of Media Certification of Performance if applicable </p>
                    </li>
                </ul>
            </div>
            <div class="hori-list mb-5 divider">
                <h2 class="heading-title mb-4">CREATIVITY IN STRATEGY</h2>
                <p class="sub-header mb-3">Entries for Creativity in Strategy should include the following:</p>
                <ul class="hori-list-ul ps-4 mb-4" style="list-style-type: disc;">
                    <li>
                        <p class="ml-3 mb-0"><span class="label">JPEG of Concept Board</span> (300 dpi, longest side measuring approximately 420 mm.)</p>
                    </li>
                    <li>
                        <p class="ml-3 mb-0"><span class="label">Case Video</span>, with the following specifications:</p>

                        <span>- 2 minutes maximum</span><br>
                        <span>- MP4</span><br>
                        <span>- NTSC 720 x 486 at 29.97 fps, minimum 8mbps or 1080p/720p 23.976fps, minimum 16 mbps (1080p recommended)</span><br>
                        <span>- H.264 compression</span><br>
                        <span>- 44.1 khz stereo audio</span><br>
                        <span>- File size must not exceed 150MB</span><br><br>
                    </li>
                    <li>
                        <p class="ml-3 mb-0"><span class="label">Written Case Study</span> as part of the online entry submission  </p>
                    </li>
                </ul>
                <p class="sub-header mb-1">Required documents:</p>
                <ul class="hori-list-ul ps-4 mb-0" style="list-style-type: disc;">
                    <li>
                        <p class="ml-3 mb-0"><span class="label">JPEG of Client Certification signed by a senior-ranking client (use Pro-Forma) </span></p>
                    </li>
                    <li>
                        <p class="ml-3 mb-0"><span class="label">JPEG of ASC Clearance if applicable </span></p>
                    </li>
                    <li>
                        <p class="ml-3 mb-0"><span class="label">JPEG of Media Certification of Performance if applicable </p>
                    </li>
                </ul>
            </div>
            <div class="hori-list mb-5 divider">
                <h2 class="heading-title mb-4">CREATIVITY FOR GOOD </h2>
                <p class="sub-header mb-3">Depending on the subcategory, you may choose to submit your entry in one or more of the following formats that best represent the merit and excellence of your entry: </p>
                <ul class="hori-list-ul ps-4 mb-4" style="list-style-type: disc;">
                    <li>
                        <p class="ml-3 mb-0"><span class="label">JPEG of Concept Board</span> (300 dpi, longest side measuring approximately 420 mm.)</p>
                    </li>
                    <li>
                        <p class="ml-3 mb-0"><strong>Case Video</strong>, with the following specifications:</p><br>
                        <span>- 2 minutes maximum</span><br>
                        <span>- MP4</span><br>
                        <span>- NTSC 720 x 486 at 29.97 fps, minimum 8mbps or 1080p/720p 23.976fps, minimum 16 mbps (1080p recommended)</span><br>
                        <span>- H.264 compression</span><br>
                        <span>- 44.1 khz stereo audio</span><br>
                        <span>- File size must not exceed 150MB</span><br><br>
                    </li>
                    <li>
                        <p class="ml-3 mb-0"><span class="label">Film</span> with the following specifications.</p>
                        <span>- MP4</span><br>
                        <span>- NTSC 720 x 486 at 29.97 fps, minimum 8mbps or 1080p/720p 23.976fps, minimum <span>16 mbps (1080p recommended)</span><br>
                        <span>- H.264 compression</span><br>
                        <span>- 44.1 khz stereo audio</span><br>
                        <span>- File size must not exceed 150MB; for longer films that may exceed the maximum upload size, the entrant may provide an unlisted YouTube link so judges can view the work at the ideal resolution Put English subtitles if needed </span><br>
                    </li>
                    <li>
                        <p class="ml-3 mb-0"><span class="label">JPEG of Posters </span> (300 dpi, longest side measuring approximately 420 mm.) with English translation if needed</p>
                    </li>
                    <li>
                        <p class="ml-3 mb-0"><span class="label">Audio</span> (MP3, 64 Kbps BitRate, AAC 44.1 khz stereo)</p>
                    </li>
                    <li>
                        <p class="ml-3 mb-0"><span class="label">Working URL and Demo Film</span> (2 mins max) for digital entries</p>
                    </li>
                </ul>
                <p class="sub-header mb-1">Required documents:</p>
                <ul class="hori-list-ul ps-4 mb-0" style="list-style-type: disc;">
                    <li>
                        <p class="ml-3 mb-0"><span class="label">JPEG of Client Certification signed by a senior-ranking client (use Pro-Forma) </span></p>
                    </li>
                    <li>
                        <p class="ml-3 mb-0"><span class="label">JPEG of ASC Clearance if applicable </span></p>
                    </li>
                    <li>
                        <p class="ml-3 mb-0"><span class="label">JPEG of Media Certification of Performance if applicable </p>
                    </li>
                </ul>
            </div>
            <div class="hori-list mb-5 divider">
                <h2 class="heading-title mb-4">CRAFT</h2>
                <h2 class="heading-title mb-4">AUDIO CRAFT </h2>
                <p class="sub-header mb-3">Best Copywriting, Direction, Music/Sound Design, Voice Performance</p>
                <ul class="hori-list-ul ps-4 mb-4" style="list-style-type: disc;">
                    <li>
                        <p class="ml-3 mb-0"><span class="label">MP3</span> (64 Kbps BitRate, AAC 44.1 khz stereo) </p>
                    </li>
                    <li>
                        <p class="ml-3 mb-0"><span class="label">English Translation (in PDF) if needed </span></p>
                    </li>
                </ul>
                <p class="sub-header mb-1">Required documents:</p>
                <ul class="hori-list-ul ps-4 mb-0" style="list-style-type: disc;">
                    <li>
                        <p class="ml-3 mb-0"><span class="label">JPEG of Client Certification signed by a senior-ranking client (use Pro-Forma) </span></p>
                    </li>
                    <li>
                        <p class="ml-3 mb-0"><span class="label">JPEG of ASC Clearance if applicable </span></p>
                    </li>
                    <li>
                        <p class="ml-3 mb-0"><span class="label">JPEG of Media Certification of Performance if applicable </p>
                    </li>
                </ul>
            </div>
            <div class="hori-list mb-5 divider">
                <h2 class="heading-title mb-4">DESIGN CRAFT  </h2>
                <p class="sub-header mb-3">Best Art Direction, Character Design, Best Copywriting, Best Digital Imaging, Best Illustration, Best Photography, Best Typography </p>
                <ul class="hori-list-ul ps-4 mb-4" style="list-style-type: disc;">
                    <li>
                        <p class="ml-3 mb-0"><span class="label">JPEG of the piece</span> (300 dpi, longest side measuring approximately 420 mm.) with English translation if needed </p>
                    </li>
                    <li>
                        <p class="ml-3 mb-0"><span class="label">JPEG of Concept Board </span>of the entry (300 dpi, longest side measuring approximately 420 mm.) that further explains its design merits, with English translation if needed </p>
                    </li>
                </ul>
                <p class="sub-header mb-1">Required documents:</p>
                <ul class="hori-list-ul ps-4 mb-0" style="list-style-type: disc;">
                    <li>
                        <p class="ml-3 mb-0"><span class="label">JPEG of Client Certification signed by a senior-ranking client (use Pro-Forma) </span></p>
                    </li>
                </ul>
            </div>
            <div class="hori-list mb-5 divider">
                <h2 class="heading-title mb-4">OUTDOOR CRAFT </h2>
                <p class="sub-header mb-3">Best Art Direction, Character Design, Best Copywriting, Best Digital Imaging, Best Illustration, Best Photography, Best Typography </p>
                <ul class="hori-list-ul ps-4 mb-4" style="list-style-type: disc;">
                    <li>
                        <p class="ml-3 mb-0"><span class="label">JPEG of the piece</span> (300 dpi, longest side measuring approximately 420 mm.) with English translation if needed </p>
                    </li>
                    <li>
                        <p class="ml-3 mb-0"><span class="label">JPEG of Concept Board </span>of the entry (300 dpi, longest side measuring approximately 420 mm.) that further explains its design merits, with English translation if needed </p>
                    </li>
                </ul>
                <p class="sub-header mb-1">Required documents:</p>
                <ul class="hori-list-ul ps-4 mb-0" style="list-style-type: disc;">
                    <li>
                        <p class="ml-3 mb-0"><span class="label">JPEG of Client Certification signed by a senior-ranking client (use Pro-Forma) </span></p>
                    </li>
                    <li>
                        <p class="ml-3 mb-0"><span class="label">JPEG of ASC Clearance if applicable </span></p>
                    </li>
                    <li>
                        <p class="ml-3 mb-0"><span class="label">JPEG of Media Certification of Performance if applicable </p>
                    </li>
                </ul>
            </div>
            <div class="hori-list mb-5 divider">
                <h2 class="heading-title mb-4">PRINT CRAFT </h2>
                <p class="sub-header mb-3">Best Art Direction, Character Design, Best Copywriting, Best Digital Imaging, Best Illustration, Best Photography, Best Typography </p>
                <ul class="hori-list-ul ps-4 mb-4" style="list-style-type: disc;">
                    <li>
                        <p class="ml-3 mb-0"><span class="label">JPEG of the piece</span> (300 dpi, longest side measuring approximately 420 mm.) with English translation if needed </p>
                    </li>
                    <li>
                        <p class="ml-3 mb-0"><span class="label">JPEG of Concept Board </span>of the entry (300 dpi, longest side measuring approximately 420 mm.) that further explains its design merits, with English translation if needed </p>
                    </li>
                </ul>
                <p class="sub-header mb-1">Required documents:</p>
                <ul class="hori-list-ul ps-4 mb-0" style="list-style-type: disc;">
                    <li>
                        <p class="ml-3 mb-0"><span class="label">JPEG of Client Certification signed by a senior-ranking client (use Pro-Forma) </span></p>
                    </li>
                    <li>
                        <p class="ml-3 mb-0"><span class="label">JPEG of Media Certification of Performance if applicable </p>
                    </li>
                </ul>
            </div>
            <div class="hori-list mb-5 divider">
                <h2 class="heading-title mb-4">DIGITAL CRAFT </h2>
                <p class="sub-header mb-3">Best in Art Direction/Design, Best Copywriting, Best UX/UI </p>
                <ul class="hori-list-ul ps-4 mb-4" style="list-style-type: disc;">
                    <li>
                        <p class="ml-3 mb-0"><span class="label">Working URL and Demo Film or JPEG of the piece if static</span></p>
                    </li>
                    <li>
                        <p class="ml-3 mb-0"><span class="label">JPEG of Concept Board </span>of the entry (300 dpi, longest side measuring approximately 420 mm.) that further explains its design merits, with English translation if needed </p>
                    </li>
                </ul>
                <p class="sub-header mb-1">Required documents:</p>
                <ul class="hori-list-ul ps-4 mb-0" style="list-style-type: disc;">
                    <li>
                        <p class="ml-3 mb-0"><span class="label">JPEG of Client Certification signed by a senior-ranking client (use Pro-Forma) </span></p>
                    </li>
                </ul>
            </div>
            <div class="hori-list mb-5 divider">
                <h2 class="heading-title mb-4">FILM CRAFT</h2>
                <p class="sub-header mb-3">Best Animation, Best Art Direction, Best Audio Craft, Best Cinematography, Best Copywriting, Best Direction, Best Editing, Best Production Design, Best Visual Effects and Digital Imaging </p>
                <ul class="hori-list-ul ps-4 mb-4" style="list-style-type: disc;">
                    <li>
                        <p class="ml-3 mb-0"><span class="label">Film</span> with the following specifications.</p>
                        <span>- MP4</span><br>
                        <span>- NTSC 720 x 486 at 29.97 fps, minimum 8mbps or 1080p/720p 23.976fps, minimum <span>16 mbps (1080p recommended)</span><br>
                        <span>- H.264 compression</span><br>
                        <span>- 44.1 khz stereo audio</span><br>
                        <span>- File size must not exceed 150MB; for longer films that may exceed the maximum upload size, the entrant may provide an unlisted YouTube link so judges can view the work at the ideal resolution Put English subtitles if needed </span><br>
                    </li>
                    <li>
                        <p class="ml-3 mb-0"><strong>For Branded Film Content, a Case Video</strong>, with the following specifications:</p><br>
                        <span>- 2 minutes maximum</span><br>
                        <span>- MP4</span><br>
                        <span>- NTSC 720 x 486 at 29.97 fps, minimum 8mbps or 1080p/720p 23.976fps, minimum 16 mbps (1080p recommended)</span><br>
                        <span>- H.264 compression</span><br>
                        <span>- 44.1 khz stereo audio</span><br>
                        <span>- File size must not exceed 150MB</span><br><br>
                    </li>
                    <li>
                        <p class="ml-3 mb-0"><span class="label">For production subcategories, before-and-after videos are welcome but must be included in the 2-minute case video. </span></p>
                    </li>
                </ul>
                <p class="sub-header mb-1">Required documents:</p>
                <ul class="hori-list-ul ps-4 mb-0" style="list-style-type: disc;">
                    <li>
                        <p class="ml-3 mb-0"><span class="label">JPEG of Client Certification signed by a senior-ranking client (use Pro-Forma) </span></p>
                    </li>
                    <li>
                        <p class="ml-3 mb-0"><span class="label">JPEG of ASC Clearance if applicable </span></p>
                    </li>
                    <li>
                        <p class="ml-3 mb-0"><span class="label">JPEG of Media Certification of Performance if applicable </p>
                    </li>
                </ul>
            </div>




        </div>
    </div>
</div>