<div class="guidelines">

    <h6 class="label__category m-0"></h6>
    <label class="label__sub_category"></label>

    <div class="spacer"></div>

    <div class="cat-1 hidden">
      <label>Required documents:</label>

      <ol class="small">
        <li>
          Jpeg of Concept Board in which results of idea are indicated (300 dpi, longest side measuring approximately 420 mm.)
        </li>
        <li>
          Case Video in which results of idea are indicated (2 mins. maximum, MP4,
          NTSC 720 x 486 at 29.97 fps, minimum 8mbps or 1080p/720p 23.976fps,
          minimum 16 mbps, H.264 compression, Audio 44.1 khz stereo. 1080p
          recommended.)
        </li>
        <li>
          Jpeg of ASC Clearance
        </li>
        <li>
          Jpeg of Media Certification of Performance
        </li>
        <li>
          Jpeg of Client Certification signed by a senior-ranking client (use
          Pro-Forma)
        </li>
      </ol>
    </div>

    <div class="cat-2 hidden">
      <label>Required documents:</label>

      <ol class="small">
        <li>
          Jpeg of Concept Board (300 dpi, longest side measuring approximately 420 mm.) 
        </li>
        <li>
          Case Video (2 mins. maximum, MP4, NTSC 720 x 486 at 29.97 fps, minimum 8mbps or 1080p/720p 23.976fps, minimum 16 mbps, H.264 compression, Audio 44.1 khz stereo. 1080p recommended.) 
        </li>
        <li>
          Jpeg of Client Certification signed by a senior-ranking client (use Pro-Forma) 
        </li>
        <li>
          Jpeg of ASC Clearance if applicable 
        </li>
        <li>
          Jpeg of Media Certification of Performance if applicable
        </li>
      </ol>
    </div>

    <div class="cat-3 hidden">
      <label>Required documents:</label>

      <ol class="small">
        <li>
          Jpeg of Concept Board (300 dpi, longest side measuring approximately 420mm.)
        </li>
        <li>
          Case Video (2 mins. maximum, MP4, NTSC 720 x 486 at 29.97 fps,
          minimum 8mbps or 1080p/720p 23.976fps, minimum 16 mbps, H.264
          compression, Audio 44.1 khz stereo. 1080p recommended.) 
        </li>
        <li>
          Film (MP4, NTSC 720 x 486 at 29.97 fps, minimum 8mbps or 1080p/720p
          23.976fps, minimum 16 mbps, H.264 compression, Audio 44.1 khz stereo.
          1080p recommended.) Put English subtitles if needed. 
        </li>
        <li>
          Jpeg of Posters (300 dpi, longest side measuring approximately 420 mm.)
          with English translation if needed
        </li>
        <li>
          Audio (MP3, 64 Kbps BitRate, AAC 44.1 khz stereo)
        </li>
        <li>
          Working URL and Demo Film (2 mins max) for digital entries
        </li>
        <li>
          Jpeg of Client Certification signed by a senior-ranking client (use Pro-Forma) 
        </li>
        <li>
          Jpeg of ASC Clearance if applicable 
        </li>
        <li>
          Jpeg of Media Certification of Performance if applicable
        </li>
      </ol>
    </div>

    <div class="cat-4 hidden">
      <label>Required documents:</label>

      <ol class="small">
        <li>
          Jpeg of Concept Board (300 dpi, longest side measuring approximately 420mm.)
        </li>
        <li>
          Case Video (2 mins. maximum, MP4, NTSC 720 x 486 at 29.97 fps,
          minimum 8mbps or 1080p/720p 23.976fps, minimum 16 mbps, H.264
          compression, Audio 44.1 khz stereo. 1080p recommended.) 
        </li>
        <li>
          Film (MP4, NTSC 720 x 486 at 29.97 fps, minimum 8mbps or 1080p/720p
          23.976fps, minimum 16 mbps, H.264 compression, Audio 44.1 khz stereo.
          1080p recommended.) Put English subtitles if needed. 
        </li>
        <li>
          Jpeg of Posters (300 dpi, longest side measuring approximately 420 mm.)
          with English translation if needed
        </li>
        <li>
          Audio (MP3, 64 Kbps BitRate, AAC 44.1 khz stereo)
        </li>
        <li>
          Working URL and Demo Film (2 mins max) for digital entries
        </li>
        <li>
          Jpeg of Client Certification signed by a senior-ranking client (use Pro-Forma) 
        </li>
        <li>
          Jpeg of ASC Clearance if applicable 
        </li>
        <li>
          Jpeg of Media Certification of Performance if applicable
        </li>
      </ol>
    </div>

    <div class="cat-5">

      <ol type="A" class="small">
        <li>AUDIO - Best Copywriting, Direction, Music/Sound Design, Voice Performance Required documents:
          <ol class="small">
            <li>MP3 (64 Kbps BitRate, AAC 44.1 khz stereo)</li>
            <li>Jpeg of ASC Clearance</li>
            <li>Jpeg of Media Certification of Performance Jpeg of Client Certification signed by a senior-ranking client (use Pro-Forma)</li>
            <li>English Translation (in Word) if needed</li>
          </ol>
        </li>
        <li>DESIGN - Best Art Direction, Character Design, Best Copywriting, Best Digital
            Imaging, Best Illustration, Best Photography, Best Typography
            Required documents:
            <ol class="small">
              <li>Jpeg of Client Certification (use Pro-Forma)</li>
              <li>
                Jpeg of Concept Board of the Design piece (300 dpi, longest side measuring approximately 420 mm.) with English translation if needed
              </li>
              <li>
                Jpeg of Poster (300 dpi, longest side measuring approximately 420 mm.) with English translation if needed
              </li>
            </ol>
        </li>
        <li>Best Art Direction, Character Design, Best Copywriting, Best Digital
            Imaging, Best Illustration, Best Photography, Best Typography
            Required documents:
            <ol class="small">
              <li>Jpeg of ASC Clearance if applicable</li>
              <li>Jpeg of Media Certification of Performance if applicable</li>
              <li>Jpeg of Client Certification signed by a senior-ranking client (use Pro-Forma)</li>
              <li>Jpeg of Concept Board (300 dpi, longest side measuring approximately 420mm.) with English translation if needed</li>
              <li>Jpeg of Poster (300 dpi, longest side measuring approximately 420 mm.) with English translation if needed </li>
            </ol>
        </li>
        <li>PRINT - Best Art Direction, Character Design, Best Copywriting, Best Digital
            Imaging, Best Illustration, Best Photography, Best Typography
            Required documents:
            <ol class="small">
              <li>Jpeg of Media Certification of Performance</li>
              <li>Jpeg of Client Certification signed by a senior-ranking client (use Pro-Forma)</li>
              <li>Jpeg of entry (300 dpi, longest side measuring approximately 420 mm.) with English translation if needed</li>
            </ol>
        </li>
        <li>DIGITAL - Best in Art Direction/Design, Best Copywriting, Best UX/UI
            Working URL and Demo Film
            Required documents:
            <ol class="small">
              <li>Jpeg of Client Certification signed by a senior-ranking client (use Pro-Forma)</li>
            </ol>
        </li>
        <li>FILM - Best Animation, Best Art Direction, Best Audio Craft, Best
            Cinematography, Best Copywriting, Best Direction, Best Editing, Best Production
            Design, Best Visual Effects and Digital Imaging
            <ol class="small">
              <li>
                MP4 (NTSC 720 x 486 at 29.97 fps, minimum 8mbps or 1080p/720p
                23.976fps, minimum 16 mbps, H.264 compression, Audio 44.1 khz stereo.
                1080p recommended.) Put English subtitles if needed.
              </li>
              <li>
                For Branded Film Content: Case Video (2 mins. maximum, MP4, NTSC
                720 x 486 at 29.97 fps, minimum 8mbps or 1080p/720p 23.976fps,
                minimum 16 mbps, H.264 compression, Audio 44.1 khz stereo. 1080p
                recommended.)
              </li>
              <li>
                For production sub categories, before-and-after videos are welcome but
                must be included in the 2-minute case video.
              </li>
            </ol>
        </li>
      </ol>
    </div>

  </div>