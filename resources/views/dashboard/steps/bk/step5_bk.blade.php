<li class="step">
    <div class="step-title waves-effect waves-dark">File Upload <i class="material-icons">arrow_forward_ios</i></div>
    <div class="step-content">
      <div class="row">
        <div class="input-field col s12 m8 offset-m2">
          
          <div class="flex justify-between align-center">
            <h5 class="title">File Upload</h5>
            <a href="#rules" class="waves-effect waves-dark btn-flat underlined mx-1 modal-trigger">Review Entry Requirements</a>           
          </div>

          <div class="spacer"></div>

          

          @include('dashboard.components.guidelines')

          <div class="spacer"></div>

          <h6 class="title">KEY REQUIREMENTS</h6>
          <div class="spacer"></div>
          <div class="key-files-container">

            @if(isset($entry) && $edit == true)
              @if($entry->files()->count() < 1)
                @for ($i = 0; $i < 3; $i++)
                <div class="file-field input-field">
                  <div class="btn pink">
                    <span>Click to Attach File</span>
                    <input type="hidden" name="pre_file[]" value="no">
                    <input type="hidden" name="f_id[]" value="">
                    <input type="hidden" name="file_type[]" value="key">
                    <input type="file" name="files[]" accept=".doc,.docx,application/msword,application/vnd.openxmlformats-officedocument.wordprocessingml.document,.jpg,.jpeg,.png,.pdf,.mp3,.mp4,.avi,.flv,.wmv,.mov">
                  </div>
                  <div class="file-path-wrapper">
                    <input class="file-path validate" name="files_name[]" type="text">
                  </div>
                  <a class="btn-floating btn-large waves-effect waves-light red delete-node"><i class="material-icons">delete</i></a>
                </div>
                @endfor
                @else
                  @foreach($entry->files() as $f)
                  @if($f->type == 'key')
                  <div class="file-field input-field">
                    <div class="btn pink">
                      <span>Click to Attach File</span>
                      <input type="hidden" name="pre_file[]" value="yes">
                      <input type="hidden" name="f_id[]" value="{{$f->id}}">
                      <input type="hidden" name="file_type[]" value="key">
                      <input type="file" name="files[]" accept=".doc,.docx,application/msword,application/vnd.openxmlformats-officedocument.wordprocessingml.document,.jpg,.jpeg,.png,.pdf,.mp3,.mp4,.avi,.flv,.wmv,.mov">
                    </div>
                    <div class="file-path-wrapper">
                      <input class="file-path validate" name="files_name[]" value="{{$f->name}}" type="text">
                    </div>
                    <a class="btn-floating btn-large waves-effect waves-light red delete-node"><i class="material-icons">delete</i></a>
                  </div>
                  @endif
                  @endforeach
              @endif
            @else
              @for ($i = 0; $i < 3; $i++)
              <div class="file-field input-field">
                <div class="btn pink">
                  <span>Click to Attach File</span>
                  <input type="hidden" name="pre_file[]" value="no">
                  <input type="hidden" name="f_id[]" value="">
                  <input type="hidden" name="file_type[]" value="key">
                  <input type="file" name="files[]" accept=".doc,.docx,application/msword,application/vnd.openxmlformats-officedocument.wordprocessingml.document,.jpg,.jpeg,.png,.pdf,.mp3,.mp4,.avi,.flv,.wmv,.mov">
                </div>
                <div class="file-path-wrapper">
                  <input class="file-path validate" name="files_name[]" type="text">
                </div>
                <a class="btn-floating btn-large waves-effect waves-light red delete-node"><i class="material-icons">delete</i></a>
              </div>
              @endfor
            @endif
            
          </div>

          <a class="waves-effect waves-dark btn-flat underlined mx-1 key-files__add-more">Add More</a>

          <div class="spacer"></div>

          <h6 class="title">Client Certification Document (required)</h6>

          <div class="spacer"></div>
          
          <div>

            <div class="file-field input-field">
              <div class="btn pink">
                <span>Click to Attach File</span>
                <input type="file" name="client_cert" class="validate" accept=".doc,.docx,application/msword,application/vnd.openxmlformats-officedocument.wordprocessingml.document,.jpg,.jpeg,.png,.pdf"  @if($edit == true && isset($entry)) @if($entry->client_cert != null) '' @else required @endif @else required @endif>
              </div>
              <div class="file-path-wrapper">
                <input class="file-path validate" name="client_cert_name" @if($edit == true && isset($entry)) @if($entry->client_cert != null) value="Attach file to update." @endif @endif type="text">
              </div>
            </div>

          </div>

          <div class="spacer"></div>

          <h6 class="title">Media Certification of Performance (optional)</h6>

          <div class="spacer"></div>
          
          <div>

            <div class="file-field input-field">
              <div class="btn pink">
                <span>Click to Attach File</span>
                <input type="file" name="media" accept=".doc,.docx,application/msword,application/vnd.openxmlformats-officedocument.wordprocessingml.document,.jpg,.jpeg,.png,.pdf">
              </div>
              <div class="file-path-wrapper">
                <input class="file-path validate" name="media_name" @if($edit == true && isset($entry)) @if($entry->media != null) value="Attach file to update." @endif @endif type="text">
              </div>
            </div>

          </div>

          <div class="spacer"></div>

          <h6 class="title">ASC Clearance (optional)</h6>

          <div class="spacer"></div>
          
          <div>

            <div class="file-field input-field">
              <div class="btn pink">
                <span>Click to Attach File</span>
                <input type="file" name="asc_cert" accept=".doc,.docx,application/msword,application/vnd.openxmlformats-officedocument.wordprocessingml.document,.jpg,.jpeg,.png,.pdf">
              </div>
              <div class="file-path-wrapper">
                <input class="file-path validate" name="asc_name" @if($edit == true && isset($entry)) @if($entry->asc_cert != null) value="Attach file to update." @endif @endif type="text">
              </div>
            </div>

          </div>
          

          <div class="spacer"></div>

          <h6 class="title">SUPPORTING DOCUMENTS</h6>
          
          <div class="spacer"></div>
          
          <div class="supporting-files-container">
            @if(isset($entry) && $edit == true)
              @if($entry->files()->count() < 1)
              <div class="file-field input-field">
                <div class="btn pink">
                  <span>Click to Attach File</span>
                  <input type="hidden" name="pre_file[]" value="no">
                  <input type="hidden" name="f_id[]" value="">
                  <input type="hidden" name="file_type[]" value="supporting">
                  <input type="file" name="files[]" accept=".doc,.docx,application/msword,application/vnd.openxmlformats-officedocument.wordprocessingml.document,.jpg,.jpeg,.png,.pdf">
                </div>
                <div class="file-path-wrapper">
                  <input class="file-path validate" name="files_name[]" type="text">
                </div>
                <a class="btn-floating btn-large waves-effect waves-light red delete-node"><i class="material-icons">delete</i></a>
              </div>
              @else
              @foreach($entry->files() as $f)
              @if($f->type == 'supporting')
              <div class="file-field input-field">
                <div class="btn pink">
                  <span>Click to Attach File</span>
                  <input type="hidden" name="pre_file[]" value="yes">
                  <input type="hidden" name="f_id[]" value="{{$f->id}}">
                  <input type="hidden" name="file_type[]" value="supporting">
                  <input type="file" name="files[]" accept=".doc,.docx,application/msword,application/vnd.openxmlformats-officedocument.wordprocessingml.document,.jpg,.jpeg,.png,.pdf">
                </div>
                <div class="file-path-wrapper">
                  <input class="file-path validate" name="files_name[]" value="{{$f->name}}" type="text">
                </div>
                <a class="btn-floating btn-large waves-effect waves-light red delete-node"><i class="material-icons">delete</i></a>
              </div>
              @endif
              @endforeach
              @endif
            @else
            <div class="file-field input-field">
              <div class="btn pink">
                <span>Click to Attach File</span>
                <input type="hidden" name="pre_file[]" value="no">
                <input type="hidden" name="f_id[]" value="">
                <input type="hidden" name="file_type[]" value="supporting">
                <input type="file" name="files[]" accept=".doc,.docx,application/msword,application/vnd.openxmlformats-officedocument.wordprocessingml.document,.jpg,.jpeg,.png,.pdf">
              </div>
              <div class="file-path-wrapper">
                <input class="file-path validate" name="files_name[]" type="text">
              </div>
              <a class="btn-floating btn-large waves-effect waves-light red delete-node"><i class="material-icons">delete</i></a>
            </div>
            @endif

          </div>

          <a class="waves-effect waves-dark btn-flat underlined mx-1 supporting-files__add-more">Add More</a>

          <div class="spacer"></div>

          <h6 class="title">ADDITIONAL LINKS / REFERENCE</h6>

          <div class="reference-container">

            @if(isset($entry) && $edit == true)
              @if($entry->reference()->count() < 1)
              <div class="file-field input-field">
                <div class="input-field">
                  <input type="text" name="reference[]">
                </div>
                <a class="btn-floating btn-large waves-effect waves-light red delete-node"><i class="material-icons">delete</i></a>
              </div>
              @else
              @foreach($entry->reference() as $r)
              <div class="file-field input-field">
                <div class="input-field">
                  <input type="text" name="reference[]" value="{{$r->link}}">
                </div>
                <a class="btn-floating btn-large waves-effect waves-light red delete-node"><i class="material-icons">delete</i></a>
              </div>
              @endforeach
              @endif
            @else
            <div class="file-field input-field">
              <div class="input-field">
                <input type="text" name="reference[]">
              </div>
              <a class="btn-floating btn-large waves-effect waves-light red delete-node"><i class="material-icons">delete</i></a>
            </div>
            @endif

          </div>

          <a class="waves-effect waves-dark btn-flat underlined mx-1 reference__add-more">Add More</a>

        </div>
      </div>
      <div class="step-actions">
        <button class="waves-effect waves-dark btn pink next-step">Proceed to Review Entry <i class="material-icons">arrow_forward_ios</i></button>
        <button class="waves-effect waves-dark btn pink inverted previous-step"><i class="material-icons">arrow_back_ios</i> BACK</button>
      </div>
    </div>
  </li>