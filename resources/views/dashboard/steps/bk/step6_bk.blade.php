<li class="step">
    <div class="step-title waves-effect waves-dark">Review Entry</div>
    <div class="step-content">

      <div class="row">
        <div class="col s12 m8 offset-m2">

          <div class="flex justify-between align-center">
            <h5 class="title">Review Entry</h5>
            <a href="#rules" class="waves-effect waves-dark btn-flat underlined mx-1 modal-trigger">Review Entry Requirements</a>           
          </div>

          <div class="spacer"></div>

          <div class="flex justify-between">
            <h6 class="title"><span class="number-indicator">1</span>Campaign Details</h6>
            <a class="waves-effect waves-dark btn-flat underlined edit-step" data-step="0">Edit</a>
          </div>

          <div class="row">
            <div class="col s6">
              <label>ENTRY NAME</label>
            </div>
            <div class="col">
              <p class="label__entry_name">{{isset($entry) ? $entry->entry_name : ''}}</p>
            </div>
          </div>

          <div class="row">
            <div class="col s6">
              <label>AD DETAIL/S</label>
            </div>
            <div class="col">
              <p class="label__ad_details">{{isset($entry) ? $entry->ad_details : ''}}</p>
            </div>
          </div>

          <div class="row">
            <div class="col s6">
              <label>ADVERTISER</label>
            </div>
            <div class="col">
              <p class="label__advertiser">{{isset($entry) ? $entry->advertiser : ''}}</p>
            </div>
          </div>

          <div class="row">
            <div class="col s6">
              <label>BRAND</label>
            </div>
            <div class="col">
              <p class="label__brand">{{isset($entry) ? $entry->brand : ''}}</p>
            </div>
          </div>

          <div class="row">
            <div class="col s6">
              <label>AGENCY</label>
            </div>
            <div class="col">
              <p class="label__agency">{{isset($entry) ? $entry->agency : ''}}</p>
            </div>
          </div>

          <div class="row">
            <div class="col s6">
              <label>DATE OF FIRST PUBLICATION </label>
            </div>
            <div class="col">
              <p class="label__date_publication">{{isset($entry) ? date('F j, Y', strtotime($entry->date_publication)) : ''}}</p>
            </div>
          </div>

          <div class="row">
            <div class="col s6">
              <label>ENTRY TYPE</label>
            </div>
            <div class="col">
              <p class="label__type">{{isset($entry) ? $entry->type : ''}}</p>
            </div>
          </div>

          <hr>

          <div class="spacer"></div>

          <div class="flex justify-between">
            <h6 class="title"><span class="number-indicator">2</span>Category</h6>
            <a class="waves-effect waves-dark btn-flat underlined edit-step" data-step="1">Edit</a>
          </div>

          <div class="row">
            <div class="col s6">
              <label>CATEGORY</label>
            </div>
            <div class="col">
              <p class="label__category">{{isset($entry) ? $entry->category()->name : ''}}</p>
            </div>
          </div>

          <div class="row">
            <div class="col s6">
              <label>SUBCATEGORY</label>
            </div>
            <div class="col">
              <p class="label__sub_category">{{isset($entry) ? $entry->sub_category()->name : ''}}</p>
            </div>
          </div>

          <hr>
          <div class="spacer"></div>

          <div class="flex justify-between">
            <h6 class="title"><span class="number-indicator">3</span>Credits</h6>
            <a class="waves-effect waves-dark btn-flat underlined edit-step" data-step="2">Edit</a>
          </div>

          <div class="row">
            <div class="col field s6">
              <label>
                <span>NAME</span>
              </label>
            </div>
            <div class="col field">
              <label>
                ROLE
              </label>
            </div>
          </div>

          <div class="label-credits-container">

          </div>

          <hr>

          <div class="spacer"></div>

          <div class="flex justify-between">
            <h6 class="title"><span class="number-indicator">4</span>Contact Details</h6>
            <a class="waves-effect waves-dark btn-flat underlined edit-step" data-step="3">Edit</a>
          </div>

          <div class="row">
            <div class="col s6">
              <label>AGENCY CONTACT</label>
            </div>
            <div class="col">
              <p class="label__agency_contact">{{isset($entry) ? $entry->agency_contact : ''}}</p>
            </div>
          </div>

          <div class="row">
            <div class="col s6">
              <label>POSITION</label>
            </div>
            <div class="col">
              <p class="label__position">{{isset($entry) ? $entry->position : ''}}</p>
            </div>
          </div>

          <div class="row">
            <div class="col s6">
              <label>PHONE NUMBER</label>
            </div>
            <div class="col">
              <p class="label__phone_number">{{isset($entry) ? $entry->phone_number : ''}}</p>
            </div>
          </div>

          <div class="row">
            <div class="col s6">
              <label>EMAIL ADDRESS</label>
            </div>
            <div class="col">
              <p class="label__email">{{Auth::user()->email}}</p>
            </div>
          </div>

          <hr>

          <div class="spacer"></div>

          <div class="flex justify-between">
            <h6 class="title"><span class="number-indicator">5</span>Uploaded Files</h6>
            <a class="waves-effect waves-dark btn-flat underlined edit-step" data-step="4">Edit</a>
          </div>
          <h6 class="title">Key Requirements</h6>
          <div class="label-key-requirements">

          </div>

          
          <h6 class="title">Client Certification Document</h6>
          <div>
            <label class="label__client_cert_name">N/A</label>
          </div>

          <h6 class="title">Media Certification of Performance</h6>
          <div>
            <label class="label__media_name">N/A</label>
          </div>

          <h6 class="title">ASC Clearance</h6>
          <div>
            <label class="label__asc_name">N/A</label>
          </div>

          <h6 class="title">Supporting Documents</h6>
          <div class="label-supporting-requirements">

          </div>

          <h6 class="title">Additional links / Reference</h6>
          <div class="label-reference">

          </div>
        </div>
      </div>
      <div class="step-actions">
        <button class="waves-effect waves-dark btn pink">SUBMIT</button>
        <button class="waves-effect waves-dark btn pink inverted previous-step"><i class="material-icons">arrow_back_ios</i> BACK</button>
      </div>
    </div>
  </li>