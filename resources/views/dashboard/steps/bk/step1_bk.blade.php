<li class="step active">
    <div class="step-title waves-effect waves-dark">Entry Details <i class="material-icons">arrow_forward_ios</i></div> 
    <div class="step-content">
      <div class="row">
        <div class="col s12 m8 offset-m2">

          <div class="flex justify-between align-center">
            <h5 class="title">Entry Details</h5>
            <a href="#rules" class="waves-effect waves-dark btn-flat underlined mx-1 modal-trigger">Review Entry Requirements</a>          
          </div>
          

          <div class="field inline">
            <label>ENTRY NAME</label>
            <div class="input-field">
              <input type="text" name="entry_name" value="{{isset($entry) ? $entry->entry_name : ''}}" class="validate" required>
            </div>
          </div>

          <div class="field inline">
            <label>AD TITLE/S</label>
            <div class="input-field">
              <input type="text" name="ad_details" class="validate" value="{{isset($entry) ? $entry->ad_details : ''}}" required>
            </div>
          </div>

          <div class="field inline">
            <label>ADVERTISER</label>
            <div class="input-field">
              <input type="text" name="advertiser" class="validate" value="{{isset($entry) ? $entry->advertiser : ''}}" required>
            </div>
          </div>

          <div class="field inline">
            <label>BRAND</label>
            <div class="input-field">
              <input type="text" name="brand" class="validate" value="{{isset($entry) ? $entry->brand : ''}}" required>
            </div>
          </div>

          <div class="field inline">
            <label>AGENCY</label>
            <div class="input-field">
              <input type="text" name="agency" class="validate" value="{{isset($entry) ? $entry->agency : ''}}" required>
            </div>
          </div>

          <div class="field inline">
            <label>DATE OF FIRST PUBLICATION</label>
            <div class="input-field">
              <input type="date" name="date_publication" class="validate flatpickr date-field" value="{{isset($entry) ? $entry->date_publication : ''}}" required>
            </div>
          </div>

          <div class="row">
            <div class="col s6">
              <label>
                <input name="type" type="radio" value="SINGLE" class="with-gap" checked="checked" />
                <span>SINGLE</span>
              </label>
            </div>
            <div class="col s6">
              <label>
                <input name="type" type="radio" value="CAMPAIGN" class="with-gap" @if(isset($entry)) @if($entry->type== 'CAMPAIGN') checked="checked" @endif @endif/>
                <span>CAMPAIGN</span>
              </label>
            </div>
          </div>

        </div>
      </div>
      <div class="step-actions">
        <button class="waves-effect waves-dark btn pink next-step" data-feedback="validateField">Proceed to Category <i class="material-icons">arrow_forward_ios</i></button>
      </div>
    </div>
  </li>