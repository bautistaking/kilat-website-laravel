<li class="step">
    <div class="step-title waves-effect waves-dark">Credits <i class="material-icons">arrow_forward_ios</i></div>
    <div class="step-content">
      <div class="row">
        <div class="input-field col m8 offset-m2">
          
          <div class="flex justify-between align-center">
            <h5 class="title">Credits</h5>
            <a href="#rules" class="waves-effect waves-dark btn-flat underlined mx-1 modal-trigger">Review Entry Requirements</a>           
          </div>

          <div class="row">
            <div class="col field s6">
              <label>
                <span>NAME</span>
              </label>
            </div>
            <div class="col field s6">
              <label>
                ROLE
              </label>
            </div>
          </div>

          <div class="credits-container">
            @if(isset($entry))
              @if($entry->credits()->count() < 1)
              @for ($i = 0; $i < 5; $i++)
              <div class="row">
                <div class="col field s6 inline">
                  <div class="input-field w-100">
                    <input type="text" name="credit_name[]" class="validate">
                  </div>
                </div>
                <div class="col field s6 inline">
                  <div class="input-field w-100">
                    <input type="text" name="credit_role[]" class="validate">
                  </div>
                </div>
                <a class="btn-floating btn-large waves-effect waves-light red delete-node"><i class="material-icons">delete</i></a>
              </div>
              @endfor
              @else
              @foreach($entry->credits() as $c)
              <div class="row">
                <div class="col field s6 inline">
                  <div class="input-field w-100">
                    <input type="text" name="credit_name[]" value="{{$c->name}}" class="validate">
                  </div>
                </div>
                <div class="col field s6 inline">
                  <div class="input-field w-100">
                    <input type="text" name="credit_role[]" value="{{$c->role}}" class="validate">
                  </div>
                </div>
                <a class="btn-floating btn-large waves-effect waves-light red delete-node"><i class="material-icons">delete</i></a>
              </div>
              @endforeach
              @endif
            @else
            @for ($i = 0; $i < 5; $i++)
            <div class="row">
              <div class="col field s6 inline">
                <div class="input-field w-100">
                  <input type="text" name="credit_name[]" class="validate">
                </div>
              </div>
              <div class="col field s6 inline">
                <div class="input-field w-100">
                  <input type="text" name="credit_role[]" class="validate">
                </div>
              </div>
              <a class="btn-floating btn-large waves-effect waves-light red delete-node"><i class="material-icons">delete</i></a>
            </div>
            @endfor
            @endif
          </div>

          <a class="waves-effect waves-dark btn-flat underlined mx-1 credits__add-more">Add More</a>
          

        </div>
      </div>
      <div class="step-actions">
        <button class="waves-effect waves-dark btn pink next-step">Proceed to Contact Info <i class="material-icons">arrow_forward_ios</i></button>
        <button class="waves-effect waves-dark btn pink inverted previous-step"><i class="material-icons">arrow_back_ios</i> BACK</button>
      </div>
    </div>
  </li>