<li class="step">
    <div class="step-title waves-effect waves-dark">Contact Info <i class="material-icons">arrow_forward_ios</i></div>
    <div class="step-content">
      <div class="row">
        <div class="col s12 m8 offset-m2">

          <div class="flex justify-between align-center">
            <h5 class="title">Contact Info</h5>
            <a href="#rules" class="waves-effect waves-dark btn-flat underlined mx-1 modal-trigger">Review Entry Requirements</a>           
          </div>

          <div class="field inline">
            <label>AGENCY CONTACT</label>
            <div class="input-field">
              <input type="text" name="agency_contact" class="validate" value="{{isset($entry) ? $entry->agency_contact : ''}}" required>
            </div>
          </div>

          <div class="field inline">
            <label>POSITION</label>
            <div class="input-field">
              <input type="text" name="position" class="validate" value="{{isset($entry) ? $entry->position : ''}}" required>
            </div>
          </div>

          <div class="field inline">
            <label>PHONE NUMBER</label>
            <div class="input-field">
              <input type="text" name="phone_number" class="validate" value="{{isset($entry) ? $entry->phone_number : ''}}" required>
            </div>
          </div>

          <div class="field inline">
            <label>EMAIL ADDRESS</label>
            <div class="input-field">
              <input type="text" name="email" class="disabled" value="{{Auth::user()->email}}"  readonly>
            </div>
          </div>

          <div class="field inline">
            <label>Are you a 4As member?</label>
            <label class="indent-left">
              <input type="checkbox" class="filled-in" name="member" value="Yes" @if(isset($entry)) @if($entry->member == 'Yes') checked="checked" @endif @endif/>
              <span>Yes</span>
            </label>
          </div>
        </div>
      </div>
      <div class="step-actions">
        <button class="waves-effect waves-dark btn pink next-step">Proceed to File Upload <i class="material-icons">arrow_forward_ios</i></button>
        <button class="waves-effect waves-dark btn pink inverted previous-step"><i class="material-icons">arrow_back_ios</i> BACK</button>
      </div>
    </div>
  </li>