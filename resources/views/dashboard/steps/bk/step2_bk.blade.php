<li class="step">
    <div class="step-title waves-effect waves-dark">Category <i class="material-icons">arrow_forward_ios</i></div>
    <div class="step-content">
      <div class="row">
        <div class="col s12 m8 offset-m2">

          <div class="flex justify-between align-center">
            <h5 class="title">Category</h5>
            <a href="#rules" class="waves-effect waves-dark btn-flat underlined mx-1 modal-trigger">Review Entry Requirements</a>
          </div>

          <div class="row">
            <div class="field col s12 m6 offset-m3">
              <div class="input-field">
                <i class="material-icons prefix">search</i>
                <input type="text" class="search-category">
                <label for="icon_prefix" class="placeholder search-category">Search</label>
              </div>
            </div>
          </div>

          <div class="row">

            <div class="col s6">

              <input type="hidden" name="category" value="{{isset($entry) ? $entry->category : ''}}">

              @for($i=0;$i < 3 ;$i++)
                <h6 class="title">{{$categories[$i]->uid}}. {{$categories[$i]->name}}</h6>
                @foreach($categories[$i]->sub_categories() as $subcat)
                  <label class="input-radio">
                    <input name="sub_category" type="radio" value="{{$subcat->id}}" class="with-gap" required data-category-name="{{$categories[$i]->name}}" data-sub-name="{{$subcat->name}}" @if(isset($entry)) @if($entry->sub_category == $subcat->id) checked="checked" @endif @endif  data-category-id="{{$categories[$i]->id}}"/>
                    <span>{{$subcat->uid}}. {{$subcat->name}}</span>
                  </label>
                @endforeach
              @endfor

            </div>
            <div class="col s6">
              @for($i=3;$i < 5 ;$i++)
                <h6 class="title">{{$categories[$i]->uid}}. {{$categories[$i]->name}}</h6>
                @foreach($categories[$i]->sub_categories() as $subcat)
                  <label class="input-radio">
                    <input name="sub_category" type="radio" value="{{$subcat->id}}" class="with-gap" required data-category-name="{{$categories[$i]->name}}" data-sub-name="{{$subcat->name}}" @if(isset($entry)) @if($entry->sub_category == $subcat->id) checked="checked" @endif @endif data-category-id="{{$categories[$i]->id}}"/>
                    <span>{{$subcat->uid}}. {{$subcat->name}}</span>
                  </label>
                @endforeach
              @endfor
            </div>

          </div>

          
        </div>
      </div>
      <div class="step-actions">
        <button class="waves-effect waves-dark btn pink next-step">Proceed to Credits <i class="material-icons">arrow_forward_ios</i></button>
        <button class="waves-effect waves-dark btn pink inverted previous-step"><i class="material-icons">arrow_back_ios</i> BACK</button>
      </div>
    </div>
  </li>