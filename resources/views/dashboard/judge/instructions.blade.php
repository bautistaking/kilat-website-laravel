@extends('base')

@section('content')
<div class="dashboard">
  @include('dashboard.components.header')
  @include('dashboard.components.j-sub-nav',['crumb' => ''])

  <div class="row">
    <div class="col s12 m8 offset-m2">
      <div class="card">
        <div class="bg-color -purple d-flex align-center">
          <img src="{{asset('/images/lightning_flat.png')}}" height="150">
          <div class="text-color -white p-1">
            <h4 class="m-0">Kidlat Awards Judging</h4>
            <p> <small>Welcome to the judging site for Kidlat Awards. </small></p>
          </div>
        </div>
        <div class="p-1">
          <h5><span class="font-regular">Dear</span> {{Auth::user()->name}},</h5>
          <div class="text-color -gray font -small">
            <div class="spacer"></div>
            <p>We are incredibly fortunate to have you judging for the Kidlat Awards this 2019. The amount of talent and experience on the jury is incredibly impressive.</p>
            <div class="spacer"></div>
            <p>We’ve assigned the categories and we wanted to let you know that you will be judging the <strong>{{Auth::user()->instance()->category()->name}}</strong>  entries.</p>
            <div class="spacer"></div>
            <p>You’ll find more detailed instructions on the process and criteria for judging in the link below. More information on your fellow judges will be available soon too!</p>
            <p>Looking forward to having your expertise on the evaluation process!</p>
            <div class="spacer"></div>
            <p class="m-0">Please use the following guidelines when reviewing the entries:</p>
            <ul class="m-0">
              <li>5 - Not a good idea</li>
              <li>4 - It’s okay</li>
              <li>3 - I’m not sure if it should win or not. Let’s discuss further</li>
              <li>2 - Great idea. It should get something</li>
              <li>1 - It’s a sure a winner</li>
            </ul>
            <div class="spacer"></div>
            <p class="m-0">Things judges should consider when viewing the work:</p>
            @if(Auth::user()->instance()->category()->id == 1)
            <ul class="m-0 circle">
              <li>Is the work distinctive, fresh, and exceptional in its category?</li>
              <li>Did it make significant contribution to the brand’s growth or business performance?</li>
              <li>Is it brave and made your heart beat a little faster?</li>
              <li>Is it so great, that you find yourself nodding and totally convinced when you see the strong results?</li>
              <li>Does it showcase brilliant creativity that leads to effectiveness?</li>
            </ul>
            @endif

            @if(Auth::user()->instance()->category()->id == 2)
            <ul class="m-0 circle">
              <li>Is the work showcasing an innovative solution for the platform/ medium?</li>
              <li>Is it brave and made your heart beat a little faster?</li>
              <li>Does the work make you wish you’ve thought of the digital idea first?</li>
              <li>Is it a perfect campaign of creativity and technology?</li>
              <li>Is it something you’ve never thought technology can actually do for a brand?</li>
            </ul>
            @endif

            @if(Auth::user()->instance()->category()->id == 3)
            <ul class="m-0 circle">
              <li>Does the work make you wish you’ve thought of the idea first?</li>
              <li>Is it so awesome that it deserves some serious “slow clapping” ?</li>
              <li>Is it something you’ve never seen before?</li>
              <li>Is it brave and made your heart beat a little faster?</li>
            </ul>
            @endif

            @if(Auth::user()->instance()->category()->id == 4)
            <ul class="m-0 circle">
              <li>Is it brave and made your heart beat a little faster?</li>
              <li>Is it so great, that you find yourself touched and totally convinced when you see the effect to humanity?</li>
              <li>Is it genuine creativity that seeks to help humanity and the world?</li>
              <li>Is it a perfect campaign of creativity and technology?</li>
              <li>Does it make you love a brand because of the purpose it represents?</li>
            </ul>
            @endif

            @if(Auth::user()->instance()->category()->id == 4)
            <ul class="m-0 circle">
              <li>Is it brave and made your heart beat a little faster?</li>
              <li>Is it so great, that you find yourself touched and totally convinced when you see the effect to humanity?</li>
              <li>Is it genuine creativity that seeks to help humanity and the world?</li>
              <li>Does it make you love a brand because of the purpose it represents?</li>
            </ul>
            @endif

            @if(Auth::user()->instance()->category()->id == 5)
            <ul class="m-0 circle">
              <li>Is the work distinctive, fresh, and exceptional in its category?</li>
              <li>Is  it so awesome that it deserves some serious “slow clapping”?</li>
              <li>Is it something you’ve never seen before?</li>
              <li>Is it brave and made your heart beat a little faster?</li>
              <li>Does it deserve to be rewarded for the painstaking attention to detail and dedication to the craft?</li>
            </ul>
            @endif


            <div class="spacer"></div>
            <div class="text-center">
              <a href="{{url('/judge/dashboard/list')}}" class="waves-effect waves-dark btn pink">GET STARTED</a>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

</div>
@endsection