@extends('base')

@section('content')
<div class="dashboard">
  @include('dashboard.components.header')
  @include('dashboard.components.j-sub-nav',['crumb' => ''])

  <div class="row">
    <div class="col s12 m10 offset-m1">
      <div class="card text-center p-1">
        <h4>Hello, <strong>{{Auth::user()->name}}</strong>!</h4>
        <p class="text-color -gray">Here are the entries assigned to you for <strong>{{Auth::user()->instance()->category()->name}}</strong> category</p>
        <div class="spacer"></div>
        <table class="striped centered">
          <thead>
            <tr>
                <th>Entry Name</th>
                <th>Ad Title</th>
                <th>Score</th>
                <th></th>
            </tr>
          </thead>

          <tbody>
            @foreach (Auth::user()->instance()->votes() as $v)
            <tr>
              <td>{{$v->entry()->entry_name}}</td>
              <td>{{$v->entry()->ad_details}}</td>
              <td>
                @if($v->score == null)
                Pending
                @else 
                  @if($v->score == 0)
                  A
                  @else
                  {{$v->score}}
                  @endif
                @endif
              </td>
              <td>
               @if(Auth::user()->instance()->status == "Started")
                 <a href="{{url('judge/vote')}}/{{$v->id}}" class="waves-effect waves-dark btn pink">Edit Score</a>
               @endif
              </td>
            </tr>
            @endforeach
            
          </tbody>
        </table>
        <div class="spacer"></div>
        @foreach (Auth::user()->instance()->votes() as $index => $v)
        @if($index == 0)
          @if(Auth::user()->instance()->status == "Pending")
          <div class="text-center">
            <a href="{{url('judge/vote')}}/{{$v->id}}" class="waves-effect waves-dark btn pink">Start Judging</a>
          </div>
          @elseif(Auth::user()->instance()->status == "Started")
          <div class="text-center">
            <button class="waves-effect waves-dark btn pink submit-score">Complete Judging</button>
            <form action="{{url('/judge/vote/completeJudging')}}" method="post">
              @csrf
              <input type="hidden" name="id" value="{{Auth::user()->instance()->id}}">
            </form>
            <div class="spacer"></div>
            <p class="font -xs-small"><i>Additional entries will be assigned to you soon. <br> Kindly wait for email confirmation from 4AS Philippines once the additional entries are ready for judging.</i></p>
          </div>
          @endif
        @endif
        @endforeach
  
      </div>
    </div>
  </div>

</div>
@endsection