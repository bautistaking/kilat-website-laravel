@extends('base')

@section('content')
<div class="dashboard">
  @include('dashboard.components.header')
  @include('dashboard.components.j-sub-nav',['crumb' => ''])

  @if(!empty($next))
  <a href="{{url('judge/vote')}}/{{$next->id}}" class="btn white flex align-center next-entry"><i class="material-icons text-color -pink">arrow_forward_ios</i></a>
  @endif
  @if(!empty($prev))
  <a href="{{url('judge/vote')}}/{{$prev->id}}" class="btn white flex align-center prev-entry"><i class="material-icons text-color -pink">arrow_back_ios</i></a>
  @endif
  <div class="row">
    <div class="col s12 m10 offset-m1">
      <div class="card p-1">
        <p class="font -small"> <span class="text-color -pink"><b>{{Auth::user()->instance()->finished()->count()}}/{{Auth::user()->instance()->votes()->count()}}</b></span> Entries completed  <a href="mailto:4asp@pldtdsl.net,kidlatawards2019@gmail.com?subject=[Kidlat Awards 2019] - Report Entry" class="float -right text-color -pink">Report this Entry</a></p>
        <h4>{{$vote->entry()->entry_name}}</h4>
        <div class="spacer"></div>
        <?php $disabled = false; ?>
        @if($vote->entry()->key_files()->count() > 0)
          @foreach($vote->entry()->key_files() as $index => $f)
          @if($f->path == "0")
          <?php $disabled = true; ?>
          @endif
          @if($index == 0)
          <div class="text-center main-file">
            @if($f->file_type() == "Image")
              <img src="{{url('/judge/file')}}/{{$f->id}}" class="mw-100 mh-800px">
            @elseif($f->file_type() == "Video")
              <video controls class="mw-100 mh-800px">
                <source src="{{url('/judge/file')}}/{{$f->id}}"  type="video/mp4">
              </video>
            @elseif($f->file_type() == "Audio")
              <audio controls class="mw-100 mh-800px">
                <source src="{{url('/judge/file')}}/{{$f->id}}">
              </audio>
            @else 
              <div class="no-preview">
                <div>
                  <i class="material-icons">broken_image</i> <br>
                  No Preview Available
                </div>
              </div>
            @endif
            <div class="font -xs-small">
              <a href="{{url('/judge/file')}}/{{$f->id}}" class="preview-link" target="_blank"><i>*Click here to view / download the file if the preview is not working properly</i></a>
            </div>
          </div>
          @endif
          @endforeach
        @else 
        <?php $disabled = true; ?>
        @endif
        <div class="spacer"></div>
        @if($vote->entry()->key_files()->count() > 1)
        <p>ALSO FROM THIS ENTRY</p>
        <div class="files-list">
          @foreach($vote->entry()->key_files() as $index => $f)
            <div class="files" data-type="{{$f->file_type()}}" data-url="{{url('/judge/file')}}/{{$f->id}}">
              <div>
                @if($f->file_type() == "Image")
                <i class="material-icons">image</i>
                @elseif($f->file_type() == "Video")
                <i class="material-icons">videocam</i>
                @elseif($f->file_type() == "Audio")
                <i class="material-icons">headset</i>
                @else
                <i class="material-icons">broken_image</i>
                @endif
                <p class="font -xs-small">{{$f->name}}</p>
              </div>
            </div>
          @endforeach
        </div>
        @endif
        <div class="spacer"></div>
        <p>ENTRY DETAILS</p>
        <div class="font -small">
          <p>ENTRY NAME: <span class="mx-1 text-color -pink">{{$vote->entry()->entry_name}}</span></p>
          <p>CATEGORY: <span class="mx-1 text-color -pink">{{$vote->entry()->category()->name}}</span></p>
          <p>SUB CATEGORY: <span class="mx-1 text-color -pink">{{$vote->entry()->sub_category()->name}}</span></p>
          <p>AD TITLE: <span class="mx-1 text-color -pink">{{$vote->entry()->ad_details}}</span></p>
          <p>BRAND: <span class="mx-1 text-color -pink">{{$vote->entry()->brand}}</span></p>
          <p>DATE OF FIRST PUBLICATION: <span class="mx-1 text-color -pink">{{date('F j, Y', strtotime($vote->entry()->date_publication))}}</span></p>
          <p>ENTRY TYPE: <span class="mx-1 text-color -pink">{{$vote->entry()->type}}</span></p>
        </div>
        <div class="spacer"></div>
        @if($vote->entry()->reference()->count() > 0)
        <hr>
        <div class="spacer"></div>
        <p>ADDITIONAL LINKS / REFERENCE</p>
        @foreach($vote->entry()->reference() as $index => $r)
        <div><a href="{{$r->link}}" target="_blank"><label>{{$r->link}}</label></a></div>
        @endforeach
        <div class="spacer"></div>
        @endif
        <hr>
        <div class="spacer"></div>
        <p>VOTE</p>
        <form action="{{url('/judge/vote/saveScore')}}" method="post">
          @csrf
          <input type="hidden" value="{{$vote->id}}" name="id">
          <div>
            <label class="input-radio">
              <input name="score" type="radio" value="0" class="with-gap" @if($vote->score == 0) checked @endif/>
              <span>Abstain from judging this entry</span>
            </label>
          </div>
          <div>
            <label class="input-radio">
              <input name="score" type="radio" value="5" class="with-gap" @if($vote->score == 5) checked @endif/>
              <span>5 - Not a good idea</span>
            </label>
          </div>
          <div>
            <label class="input-radio">
              <input name="score" type="radio" value="4" class="with-gap" @if($vote->score == 4) checked @endif/>
              <span>4 - It’s okay</span>
            </label>
          </div>
          <div>
            <label class="input-radio">
              <input name="score" type="radio" value="3" class="with-gap" @if($vote->score == 3) checked @endif/>
              <span>3 - I’m not sure if it should win or not. Let’s discuss further</span>
            </label>
          </div>
          <div>
            <label class="input-radio">
              <input name="score" type="radio" value="2" class="with-gap" @if($vote->score == 2) checked @endif/>
              <span>2 - Great idea. It should get something</span>
            </label>
          </div>
          <div>
            <label class="input-radio">
              <input name="score" type="radio" value="1" class="with-gap" @if($vote->score == 1) checked @endif/>
              <span>1 - It’s a sure a winner</span>
            </label>
          </div>
          <div class="spacer"></div>
          <div class="text-center">
            <div>
              <a href="{{url('/judge/dashboard/list')}}" class="waves-effect waves-dark btn-flat underlined mx-1">View Score Summary</a>
              <button class="waves-effect waves-dark btn  pink mx-1">Submit Score</button>
            </div>
            <br>
            @if($disabled == true)
            <p class="font -xs-small"> <i>This entry will have to be re-submitted again. <br> Kindly wait for email confirmation from 4AS Philippines once this entry is ready for judging.</i> </p>
            @endif
          </div>
        </form>
      </div>
    </div>
  </div>

</div>
@endsection