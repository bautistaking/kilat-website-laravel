@extends('base')  
@push('styles')
    <style>
        .spinner-grow,
        .loading-label {
            display: none;
        }
    </style>
@endpush
@section('content')
    @include('dashboard.components.header')
        <div class="bg">
            <div class="wrapper-default pt72">
                <div class="container-fluid py72">
                    <div class="state d-flex flex-column justify-content-center align-items-center">
                        <img class="mb-4" src="{{url('/images/like.png')}}" alt="">
                        <h1 class="title mb-4 text-center">
                            Entry successfully submitted for<br/>
                            <span class="fw-bold">{{$category}}</span>
                        </h1>
                        <!-- <h1 class="title mb-4 text-center">
                            Entry successfully submitted for<br/>
                            <span class="fw-bold">Craft - Film</span>
                        </h1> -->
                        <!-- <h1 class="title mb-4 text-center">
                            Entry successfully submitted for<br/>
                            <span class="fw-bold">Craft - Design</span>
                        </h1> -->
                        <p class="desc mb-0 text-center">
                            We have generated an invoice for your entry.<br/> 
                            Click on the button below to view your invoice!
                        </p>
                        <a href="{{url('/my-invoice')}}">
                            <button type="button" class="btn btn-primary primary lg mt-5 text-uppercase" style="min-width:223px">View my invoice</button>
                        </a>
                    </div>            
                </div>
            </div>
        </div>
    @include('dashboard.components.footer')
@push('custom-js')
<script>
    $(document).ready(activateQuery())

    function activateQuery() {

    }
</script>
@endpush
@endsection