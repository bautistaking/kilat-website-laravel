@extends('base')  
@push('styles')
    <style>
        .spinner-grow,
        .loading-label {
            display: none;
        }
    </style>
@endpush
@section('content')
    @include('dashboard.components.header')
    <div class="wrapper-default pt72">
        <div class="container-fluid minHeight py72">
            <div class="state dark d-flex flex-column justify-content-center align-items-center">
                <img class="mb-4" src="{{url('/images/invoice.png')}}" alt="">
                <h1 class="title mb-2 text-center fw-bold">
                    Check your mailbox for the invoice
                </h1>
                <p class="desc mb-0 text-center">
                    We sent an email to <span class="fw-bold email-label">[email.com]</span>. You’ll find the invoice <br/>
                    and payment instructions. Once payment is settled, <br/>
                    kindly email <a style="color:#384753" href="mailto:4asp@pldtsl.net"><span class="fw-bold">4asp@pldtsl.net</span></a> to confirm payment.

                </p>
                <a href="{{url('/dashboard')}}">
                    <button type="button" class="btn btn-primary primary lg mt-5 text-uppercase" style="min-width:223px">Back to home</button>
                </a>
            </div>            
        </div>
    </div>
    @include('dashboard.components.footer')
@push('custom-js')
    <script>
        let email = localStorage.getItem('email')
        $('.email-label').text(email)

        if (!localStorage.hasOwnProperty('email')) {
            location.href = "{{url('/')}}"
        }

        $(document).on('click', '.btn_back', function() {
            localStorage.removeItem('email')
            setTimeout(function() {
                location.href = "{{url('/')}}"
            },1000)
        })
    </script>
@endpush
@endsection