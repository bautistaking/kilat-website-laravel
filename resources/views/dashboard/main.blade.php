@extends('base')  
@push('styles')
    <style>
        .spinner-grow,
        .loading-label {
            display: none;
        }
    </style>
@endpush
@section('content')
    @include('dashboard.components.header')
    <div class="wrapper-default pt72">
        <div class="container-fluid minHeight py72">
            <div class="card jumbutron">
                <div class="card-body p-0 d-flex align-items-center justify-content-between">
                    <div class="heading">
                        <h1 class="card-title">Welcome to Kidlat Awards!</h1>
                        <p class="card-text">
                            It's been 2 years! Are you ready to unlock the </br>
                            best of the best in Filipino creativity?
                        </p>
                    </div>
                    <div class="d-flex" style="column-gap: 25px;">
                        <a href="{{url('/documents/kidlat_rules.pdf')}}" target="_blank">
                            <button type="button" class="btn btn-outline-primary primary brown lg text-uppercase" style="min-width:218px">Download rules</button>
                        </a>
                        <!-- <a href="{{url('/submit-entry')}}">
                            <button type="button" class="btn btn-primary primary lg ml-3 text-uppercase" style="min-width:221px">Submit an entry</button>
                        </a> -->
                    </div>
                </div>
            </div>
            @if (count($entries) > 0)
                <div style="margin-top: 70px">
                    <div class="entries flex-column d-flex">
                        <div class="d-flex align-items-center justify-content-between">
                            <h1 class="page-title mb-0">
                            Your Entries
                            </h1>
                        </div>
                        @foreach ($entries as $entry)
                            <div class="entries-cards">
                                <div class="custom-card">
                                    <div class="custom-card-col">
                                        <div class="custom-card-col-info">
                                            <h2>{{$entry->category()->name}} | {{$entry->status}} | {{$entry->payment_status}}</h2>
                                            <h2>{{$entry->entry_name}}</h2>
                                            <p>{{$entry->ad_details}}</p>
                                        </div>
                                    </div>
                                    <a href="/view/entry/{{$entry->uid}}">
                                        <button type="button" class="btn btn-primary primary small me-3 text-uppercase" style="min-width:156px; height: 48px;">View entry</button>
                                    </a>
                                </div>
                            </div>
                        @endforeach
                    </div>
                    <div class="divider" style="margin: 80px auto; max-width: 980px;"></div>
                </div>
            @else
                <div class="wrapper-default">
                    <div class="container-fluid py72">
                        <div class="entries flex-column d-flex mb-5">
                            <h1 class="page-title mb-0">
                            Your Entries
                            </h1>
                        </div>
                        <div class="state dark d-flex flex-column justify-content-center align-items-center">
                            <img class="mb-4" src="/images/no-entries.png" alt="">
                            <h1 class="title mb-2 text-center fw-bold">
                            No entries yet
                            </h1>
                            <p class="desc mb-0 text-center">
                                Would you like to submit now?
                            </p>
                            <a href="{{url('/submit-entry')}}">
                                <button type="button" class="btn btn-primary primary lg mt-5 text-uppercase" style="min-width:223px">Submit an entry</button>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="divider" style="margin: 0px auto; max-width: 980px;"></div>
            @endif
            <!-- END -->
            @include('dashboard.components.homepage_tab')
        </div>
    </div>
    @include('dashboard.components.footer')
@push('custom-js')
<script>
    $(document).ready(activateQuery())

    function activateQuery() {

    }
</script>
@endpush
@endsection