<div class="header fixed-top">
  <div class="wrapper-default">
    <nav class="navbar navbar-expand navbar-dark">
      <div class="container-fluid">
        <a class="navbar-brand pt-0 pb-0" href="/dashboard">
          <img src="assets/img/kidlat_logo.png" alt="" width="145" height="56">
        </a>
        <div class="d-flex align-items-center">
          <div class="navbar-nav mr-3">
            <a class="nav-link active" aria-current="page" href="/dashboard">Home</a>
            <a class="nav-link" href="entries.php">Your Entries</a>
            <a class="nav-link" href="invoice.php">Your Invoice</a>
          </div>
          <a href="{{url('/documents/kidlat_rules.pdf')}}" target="_blank">
            <button type="button" class="btn btn-outline-primary primary small ms-3 text-uppercase" style="min-width:182px">Download rules</button>
          </a>
          <a href="submit-an-entry.php">
            <button type="button" class="btn btn-primary primary small ms-3 text-uppercase" style="min-width:185px">Submit an entry</button>
          </a>
        </div>    
      </div>
    </nav>
  </div>
</div>