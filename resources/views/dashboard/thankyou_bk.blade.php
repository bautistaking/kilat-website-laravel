@extends('base')

@section('content')
<div class="dashboard">
  @include('dashboard.components.header')
  @include('dashboard.components.sub-nav',['crumb' => 'Kidlat Entry Form 2019'])
  <div class="card text-center padded">
    <img src="{{asset('/images/check.png')}}" width="100" alt="">
    <div class="spacer"></div>
    <h5 class="title">Good job! Your entry is now pending payment.</h5>
    <p class="light">Successfully added {{$entry->entry_name}} for {{$entry->sub_category()->name}}.</p>
    <div class="spacer"></div>
    <a href="{{ url('/dashboard/form') }}/{{$entry->uid}}" class="waves-effect waves-dark btn pink inverted mx-1">Duplicate Entry</a><a href="{{ url('/dashboard/form') }}" class="waves-effect waves-dark btn pink">Submit Another Entry</a>
    <p class="light">or</p>
    <a href="{{ url('/dashboard/') }}" class="waves-effect waves-dark btn-flat underlined">Complete and Request Invoice Summary</a>
  </div>
</div>
@include('dashboard.components.footer')
@endsection