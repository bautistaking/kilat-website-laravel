@extends('base')
@push('styles')
  <link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
  <link rel="stylesheet" href="{{ mix('admin/css/admin.css') }}">
@endpush
@section('content')
@include('admin.components.nav')
<div class="content overflow-y-visible">
  @include('admin.components.sub-nav')
  <div class="card padded overflow-y-visible">
    <div class="overflow-auto overflow-y-visible">
      <div class="row m-0">
        <div class="input-field col m3 s12 m-0">
          <select class="entries-filter">
            <option @if($cat == 1) selected @endif data-url="{{url('/webmaster-kidlat/votes')}}">CREATIVE EFFECTIVENESS</option>
            <option @if($cat == 2) selected @endif data-url="{{url('/webmaster-kidlat/votes/2')}}">CREATIVE TECHNOLOGY & MEDIA</option>
            <option @if($cat == 3) selected @endif data-url="{{url('/webmaster-kidlat/votes/3')}}">CREATIVE STORYTELLING</option>
            <option @if($cat == 4) selected @endif data-url="{{url('/webmaster-kidlat/votes/4')}}">CREATIVE PURPOSE</option>
            <option @if($cat == 5) selected @endif data-url="{{url('/webmaster-kidlat/votes/5')}}">CRAFT</option>
          </select>
          <label>Filter</label>
        </div>
        <div class="col m9 text-right">
          <a href="{{url('/webmaster-kidlat/voteResults')}}/{{$cat}}" class="waves-effect waves-dark btn pink" target="_blank">Download CSV Report</a>
        </div>
      </div>
      <div class="text-right">
        <a href="#!" class="waves-effect waves-dark btn pink submit">Update {{$category->name}} Entries</a>
        <form action="{{url('/webmaster-kidlat/votes/updateEntries')}}" method="post">
          @csrf
          <input type="hidden" name="category_id" value="{{$cat}}">
        </form>
      </div>
      <div class="spacer"></div>
      <table class="striped centered dt entries-table">
        <thead>
          <tr>
              <th>Entry Name</th>
              <th>Entry UID</th>
              @foreach ($instances as $i)
              <th>{{$i->judge()->name}}</th>
              @endforeach
          </tr>
        </thead>

        <tbody>
          @foreach ($entries as $e)
          <tr>
            <td>{{$e->entry()->entry_name}}</td>
            <td><a href="{{url('webmaster-kidlat/view/entry')}}/{{$e->entry()->uid}}" target="_blank">{{$e->entry()->uid}}</a></td>
            @foreach ($instances as $i)
            <td>
              @if($i->status == 'Done')
              @foreach ($i->votes() as $v)
                @if($e->entry_uid == $v->entry_uid)
                  @if($v->score == 0)
                  A
                  @else
                  {{$v->score}}
                  @endif
                @endif
              @endforeach
              @else
              Pending
              @endif
            </td>
            @endforeach
          </tr>
          @endforeach  
        </tbody>
      </table>
    </div>

  </div>
</div>
@push('custom-js')
  <script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
  <script src="{{ mix('admin/js/admin.js') }}"></script>
  <script>
    @if (session('success'))
      $( document ).ready(function() {
        Swal.fire(
          "{{ session('success')['header'] }}",
          "{{ session('success')['message'] }}",
          'success'
        );
      });
    @endif
  </script>
@endpush
@endsection