@extends('base')  
@push('styles')
    <link rel="stylesheet" href="https://cdn.datatables.net/v/bs5/jq-3.6.0/dt-1.11.3/datatables.min.css">
    <style>
        .spinner-grow,
        .loading-label {
            display: none;
        }

        .entries {
            max-width: 100%;
        }

        #search {
            padding-left: 35px;
        }

        .search-icon {
            position: absolute;
            margin-left: 9px;
            margin-top: -34px;
            font-size: 20px;
            color: #d7a333;
        }

        table td {
            word-wrap: break-word;
            font-size: 14px;
            text-align: center;
        }

        .color-link {
            color: #1a2735;
        }

        .dropdown-content li {
            display: none;
        }

        .custom-thead {
            background: #3b4a56;
        }

        .custom-thead > tr > th {
            padding: 20px;
            color: #fff;
            text-align: center;
        }

        .form-check span,
        .form .form-check .form-check-input {
            margin-top: 20px;
            margin-left: 5px;
        }

        .dataTables_filter label {
            display: none;
        }

        .btn-custom-status {
            width: 130px;
        }

        .wrapper-default {
            max-width: 80% !important;
        }

        .total span {
            font-weight: 700;
            font-size: 22px;
            color: #1a2735;
            margin-left: 22px;
        }

        .progress-button {
            line-height: 2 !important;
        }
    </style>

    <link rel="stylesheet" href="https://cdn.datatables.net/1.11.2/css/dataTables.bootstrap5.min.css">
@endpush
@section('content')
    @include('admin.components.header')
    <div class="wrapper-default">
        <div class="container-fluid minHeight py72">
            @if (count($entries) > 0)
                <div style="margin-top: 70px">
                    <div class="entries flex-column d-flex">
                        <div class="d-flex align-items-center">
                            <div class="mt-5 mb-4 me-3">
                                <h1 class="page-title mb-0">Agency: {{ $agency_name }}</h1>
                            </div>
                        </div>
                        <table class="table table-striped centered dt entries-table w-100">
                            <thead class="custom-thead">
                              <tr>
                                  <th>ID</th>
                                  <th>Agency</th>
                                  <th>Entry Name</th>
                                  <th>Entry</th>
                                  <th>Category</th>
                                  <th>Fee</th>
                                  <th>Date Submitted</th>
                                  @if(Auth::user()->admin_type == 1 || Auth::user()->admin_type == null)
                                  <th>Payment Status</th>
                                  @endif
                                  @if(Auth::user()->admin_type == 1 || Auth::user()->admin_type == 2 || Auth::user()->admin_type == null)
                                  <th>Entry Status</th>
                                  @endif
                              </tr>
                            </thead>

                            <tbody>
                              @foreach ($entries as $e)
                              <tr>
                                <td class="uid flex align-center form">
                                    <span class="entry_id" data-uid="{{$e->uid}}">{{$e->uid}}</span>
                                </td>
                                <td>{{$e->agency}}</td>
                                <td>
                                    <a class="color-link" href="{{url('admin/view/entry')}}/{{$e->uid}}">
                                        {{$e->entry_name}}
                                    </a>
                                </td>
                                <td>{{$e->type}}</td>
                                <td>{{$e->category()->name}}</td>
                                <td>{{number_format($e->fee, 2, ".", ",")}}</td>
                                <td>{{ date('M j, Y h:i a', strtotime($e->created_at)) }}</td>
                                @if(Auth::user()->admin_type == 1 || Auth::user()->admin_type == null)
                                <td>
                                    <div class="dropdown">
                                        <button class='btn-custom-status mt-2 mb-1 btn dropdown-toggle @if($e->payment_status == "Paid") btn-success @elseif($e->payment_status == "Unpaid") btn-danger @elseif ($e->payment_status == "Billing Sent") btn-warning @elseif($e->payment_status == "Requested") btn-info @endif' type="button" id="paymentStatusDropdown" data-bs-toggle="dropdown">
                                        {{$e->payment_status}}
                                        </button>
                                        <ul class="dropdown-menu" aria-labelledby="paymentStatusDropdown">
                                            <li><a data-bs-toggle="modal" data-bs-target="#confirmModal" class="dropdown-item payment-status" data-identifier="payment_status" data-id="{{$e->id}}" data-paymentstatus="Unpaid" href="javascript:void(0)">Unpaid</a></li>
                                            <li><a data-bs-toggle="modal" data-bs-target="#confirmModal" class="dropdown-item payment-status" data-identifier="payment_status" data-id="{{$e->id}}" data-paymentstatus="Billing Sent" href="javascript:void(0)">Billing Sent</a></li>
                                            <li><a data-bs-toggle="modal" data-bs-target="#confirmModal" class="dropdown-item payment-status" data-identifier="payment_status" data-id="{{$e->id}}" data-paymentstatus="Paid" href="javascript:void(0)">Paid</a></li>
                                        </ul>
                                    </div>
                                </td>
                                @endif
                                @if(Auth::user()->admin_type == 1 || Auth::user()->admin_type == 2 || Auth::user()->admin_type == null)
                                    <td>
                                        <div class="dropdown">
                                            <button class='btn-custom-status mt-2 mb-1 btn dropdown-toggle @if($e->status == "Approve") btn-success @elseif($e->status == "Reject") btn-danger @else btn-warning @endif' type="button" id="statusDropdown" data-bs-toggle="dropdown">
                                                {{$e->status}}
                                            </button>
                                            <ul class="dropdown-menu" aria-labelledby="statusDropdown">
                                                <li><a data-bs-toggle="modal" data-bs-target="#confirmModal" class="dropdown-item status" data-identifier="status" data-id="{{$e->id}}" data-status="Pending" href="javascript:void(0)">Pending</a></li>
                                                <li><a data-bs-toggle="modal" data-bs-target="#confirmModal" class="dropdown-item status" data-identifier="status" data-id="{{$e->id}}" data-status="Approve" href="javascript:void(0)">Approve</a></li>
                                                <li><a data-bs-toggle="modal" data-bs-target="#confirmModal" class="dropdown-item status" data-identifier="status" data-id="{{$e->id}}" data-status="Reject" href="javascript:void(0)">Reject</a></li>
                                            </ul>
                                        </div>
                                    </td>
                                @endif
                              </tr>
                              @endforeach


                            </tbody>
                          </table>
                          <div class="d-flex flex-column justify-content-end">
                            <div class="d-flex align-items-center justify-content-end total">
                                Total Amount (<div class="total_entry" style="font-size:16px;font-weight:normal">{{ $entry_count }} </div>&nbsp;entry)
                                <span class="total_amount">PHP {{ $total_fee }}</span>
                            </div>
                            <div class="d-flex align-items-center justify-content-end vat-label" style="margin-right:122px">
                                <span>Inclusive of 12% VAT</span>
                            </div>
                            @if($is_first_timer > 0)
                            <div class="d-flex align-items-center justify-content-end total">
                                Total Amount with 10% Discount
                                <span class="total_amount">PHP {{ $total_fee_discounted }}</span>
                            </div>
                            <div class="d-flex justify-content-end mt-5">
                                <a href="javascript:void(0)" id="btn_confirm">
                                    <button type="button" class="progress-button text-uppercase" data-style="slide-down" data-horizontal>
                                        <span class="confirm-invoice-label link-color">Confirm invoice request <br/>with 10% discount</span>
                                    </button>
                                </a>
                            </div>
                            @endif
                        </div>
                    </div>
                </div>
            @endif
        </div>
    </div>

    <!-- modal -->
    <div class="voting-modal modal fade" id="confirmModal" tabindex="-1" aria-labelledby="flag" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    <h5 class="mx-auto" style="margin-top:-20px">Do you want to proceed?</h5>
                </div>
                <div class="modal-body p-0 pt-4 mx-auto">
                    <button type="button" class="btn btn-primary primary small me-3 text-uppercase mx-auto" data-bs-dismiss="modal" aria-label="Cancel">Cancel</button>
                    <button type="button" class="btn btn-primary primary small me-3 text-uppercase mx-auto btn-confirm">
                        <span class="spinner-grow spinner-grow-sm" role="status"></span>
                        <span class="spinner-grow spinner-grow-sm" role="status"></span>
                        <span class="spinner-grow spinner-grow-sm" role="status"></span>
                        <span class="confirm-label">Confirm</span>
                    </button>
                </div>
            </div>
        </div>
    </div>
    @include('admin.components.footer')
@push('custom-js')
<script type="text/javascript" src="https://cdn.datatables.net/v/bs5/jq-3.6.0/dt-1.11.3/datatables.min.js"></script>
<script>
    $(document).ready(activateQuery())

    function activateQuery() {
        var id             = 0;
        var status         = "";
        var payment_status = "";
        var identifier     = "";

        $(document).on('click', '.payment-status', function() {
            id             = $(this).attr('data-id')
            payment_status = $(this).attr('data-paymentstatus')
            identifier     = $(this).attr('data-identifier')
            $('#confirmModal').toggle('modal')
        })

        $(document).on('click', '.status', function() {
            id         = $(this).attr('data-id')
            status     = $(this).attr('data-status')
            identifier = $(this).attr('data-identifier')
            $('#confirmModal').toggle('modal')
        })

        $(document).on('click', '.btn-confirm', function() {
            $('.confirm-label').hide()
            $('.spinner-grow').addClass('d-inline-block')

            $(this).attr('disabled', true)

            if (identifier == "payment_status") {
                updatePayment()                
            } else if (identifier == "status") {
                updateStatus()
            }
        })

        $(document).on('click', '#btn_confirm', function() {
            let ids = [];
            $('.entry_id').each(function () {
                ids.push($(this).data('uid'));
            });

            let params = {
                '_token': '{{ csrf_token() }}',
                'ids'   : ids,
                'discount': true,
            }

            fetch("/request-invoice", {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify(params)
            })
            .then(response => response.json())
            .then((e) => {
                // if (e.status_code == 200) {
                //     alert('New invoice has been sent to agency registered email address.');
                // }
            })
            .catch((error) => {
            })
        })

        function updatePayment() {
            let params = {
                '_token'         : '{{ csrf_token() }}',
                'id'             : id,
                'payment_status' : payment_status
            }

            fetch("/admin/update/payment", {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify(params)
            })
            .then(response => response.json())
            .then((e) => {
                let html = ""

                if (e.status_code == 200) {
                    $('#confirmModal').toggle('modal')

                    Swal.fire({
                        icon: 'success',
                        title: e.header,
                        html: e.message,
                        confirmButtonText: 'Close',
                        confirmButonColor: 'btn-primary'
                    })

                    setTimeout(function() {
                        location.href = "/admin/entries"
                    }, 2000)
                }
            })
        }

        function updateStatus() {
             let params = {
                '_token' : '{{ csrf_token() }}',
                'id'     : id,
                'status' : status
            }

            fetch("/admin/update/status", {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify(params)
            })
            .then(response => response.json())
            .then((e) => {
                let html = ""

                if (e.status_code == 200) {
                    $('#confirmModal').toggle('modal')

                    Swal.fire({
                        icon: 'success',
                        title: e.header,
                        html: e.message,
                        confirmButtonText: 'Close',
                        confirmButonColor: 'btn-primary'
                    })

                    setTimeout(function() {
                        location.href = "/admin/entries"
                    }, 2000)
                }
            })
        }
    }
</script>
@endpush
@endsection