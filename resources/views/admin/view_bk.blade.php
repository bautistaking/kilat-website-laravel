@extends('base')

@section('content')
<div class="dashboard">
  @include('dashboard.components.header')
  @include('dashboard.components.sub-nav',['crumb' => 'View Entry'])

  <div class="card padded">
  
    <h5 class="title">Entry Details</h5>
    <div class="spacer"></div>

    <div class="row">
      <div class="col s6">
        <label>ENTRY NAME</label>
      </div>
      <div class="col">
        <p>{{$entry->entry_name}}</p>
      </div>
    </div>

    <div class="row">
      <div class="col s6">
        <label>CATEGORY</label>
      </div>
      <div class="col">
        <p>{{$entry->category()->name}}</p>
      </div>
    </div>

    <div class="row">
      <div class="col s6">
        <label>SUB CATEGORY</label>
      </div>
      <div class="col">
        <p>{{$entry->sub_category()->name}}</p>
      </div>
    </div>

    <div class="row">
      <div class="col s6">
        <label>AD TITLE/S</label>
      </div>
      <div class="col">
        <p>{{$entry->ad_details}}</p>
      </div>
    </div>

    <div class="row">
      <div class="col s6">
        <label>BRAND</label>
      </div>
      <div class="col">
        <p>{{$entry->brand}}</p>
      </div>
    </div>

    <div class="row">
      <div class="col s6">
        <label>AGENCY</label>
      </div>
      <div class="col">
        <p>{{$entry->agency}}</p>
      </div>
    </div>

    <div class="row">
      <div class="col s6">
        <label>DATE OF FIRST PUBLICATION </label>
      </div>
      <div class="col">
        <p>{{date('F j, Y', strtotime($entry->date_publication))}}</p>
      </div>
    </div>

    <div class="row">
      <div class="col s6">
        <label>ENTRY TYPE</label>
      </div>
      <div class="col">
        <p>{{$entry->type}}</p>
      </div>
    </div>

    <hr>

    @if(!empty($entry->credits()))
    <h5 class="title">Credits</h5>
    <div class="spacer"></div>
    <div class="row">
      <div class="col field s6">
        <label>
          <span>NAME</span>
        </label>
      </div>
      <div class="col field">
        <label>
          ROLE
        </label>
      </div>
    </div>
    @foreach($entry->credits() as $c)
    <div class="row">
      <div class="col s6">
        <p>{{$c->name}}</p>
      </div>
      <div class="col">
        <p>{{$c->role}}</p>
      </div>
    </div>
    @endforeach
    <hr>
    @endif

    <h5 class="title">Contact Details</h5>
    
    <div class="spacer"></div>

    <div class="row">
      <div class="col s6">
        <label>AGENCY CONTACT</label>
      </div>
      <div class="col">
        <p>{{$entry->agency_contact}}</p>
      </div>
    </div>

    <div class="row">
      <div class="col s6">
        <label>POSITION</label>
      </div>
      <div class="col">
        <p>{{$entry->position}}</p>
      </div>
    </div>

    <div class="row">
      <div class="col s6">
        <label>PHONE NUMBER</label>
      </div>
      <div class="col">
        <p>{{$entry->phone_number}}</p>
      </div>
    </div>

    <div class="row">
      <div class="col s6">
        <label>EMAIL ADDRESS</label>
      </div>
      <div class="col">
        <p>{{$entry->email}}</p>
      </div>
    </div>

    <div class="row">
      <div class="col s6">
        <label>DATE SUBMITTED</label>
      </div>
      <div class="col">
        <p>{{date('F j, Y', strtotime($entry->created_at))}}</p>
      </div>
    </div>

    <hr>

    <h5 class="title">Uploaded Files</h5>
    <div class="spacer"></div>
    <h6 class="title">Key Requirements</h6>
    @foreach($entry->files() as $f)
    @if($f->type == 'key')
    <div><label><a href="{{url('admin/download/file')}}/{{$f->id}}" target="_blank">{{$f->name}}</a></label></div>
    @endif
    @endforeach
    @if($entry->client_cert != null)
    <div class="spacer"></div>
    <h6 class="title"><a href="{{url('admin/certificate')}}/{{$entry->uid}}/client_cert" target="_blank">Client Certification Document</a></h6>
    @endif
    @if($entry->media != null)
    <div class="spacer"></div>
    <h6 class="title"><a href="{{url('admin/certificate')}}/{{$entry->uid}}/media" target="_blank">Media Certification of Performance</h6>
    @endif
    @if($entry->asc_cert != null)
    <div class="spacer"></div>
    <h6 class="title"><a href="{{url('admin/certificate')}}/{{$entry->uid}}/asc_cert" target="_blank">ASC Clearance</a></h6>
    @endif
    <div class="spacer"></div>
    <h6 class="title">Supporting Documents</h6>
    @foreach($entry->files() as $f)
    @if($f->type == 'supporting')
    <div><label><a href="{{url('admin/download/file')}}/{{$f->id}}" target="_blank">{{$f->name}}</a></label></div>
    @endif
    @endforeach
    <div class="spacer"></div>
    <h6 class="title">Additinal links / Reference</h6>
    @foreach($entry->reference() as $r)
    <div><a href="{{$r->link}}" target="_blank"><label>{{$r->link}}</label></a></div>
    @endforeach

  </div>


</div>
@include('dashboard.components.footer')
@endsection