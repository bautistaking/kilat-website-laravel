<header>
    <div class="header fixed-top">
        <div class="wrapper-default">
            <nav class="navbar navbar-expand navbar-dark">
                <div class="container-fluid">
                    <a class="navbar-brand pt-0 pb-0" href="{{url('/admin/entries')}}">
                        <img src="{{url('/images/kidlat-logo-horizontal.png')}}" alt="" width="145" height="56">
                    </a>
                    <div class="d-flex align-items-center">
                        <div class="navbar-nav mr-3">
                            <a class="nav-link active" href="{{url('/admin/entries')}}">Entries</a>
                            <!-- <a class="nav-link" href="{{url('/admin/round2-entries')}}">Round 2 Entries</a> -->

                            @if(Auth::user()->admin_type == 1)
                                <a class="nav-link" href="{{url('/admin/admin-users')}}">Admin Users</a>
                                <a class="nav-link" href="{{url('/admin/jurors')}}">Jurors</a>
                            @endif
                            <a class="nav-link" href="{{url('/admin/scores')}}">Votes</a>
                            @if(Auth::user()->admin_type == 1)
                            <a class="nav-link" href="javascript:void(0)" data-bs-toggle="modal" data-bs-target="#setExpiryModal">Expiry Status</a>
                            @endif
                            <a class="nav-link" href="/logout">
                               <!--  @if (auth()->user()->name != "")
                                    {{auth()->user()->name}}
                                @else
                                    Admin
                                @endif -->
                                Logout
                                <i class="fas fa-external-link-alt"></i>
                            </a>
                        </div>
                    </div>
                </div>
            </nav>
        </div>
    </div>
</header>

<div class="voting-modal modal fade" id="setExpiryModal" tabindex="-1" aria-labelledby="flag" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header d-flex justify-content-center">
                <h3 style="margin-top:-20px">Set Time Expiry</h3>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body p-0 pt-4">
                <div class="valid-feedback" id="expiry-feedback">Expiry status updated successfully.</div>
                <div class="mb-4">
                    <label for="juror_name" class="form-label fw-500 mb-2">Status</label>
                    <div class="form-check form-switch">
                      <input style="width:3em;" class="form-check-input" type="checkbox" name="status" id="status" @if($show_status->active == "1") checked @endif>
                      <label class="form-check-label" for="status">The competition status is active.</label>
                    </div>
                </div>
                <div class="mb-4">
                    <label for="juror_agency" class="form-label fw-500 mb-2">Expiry Date</label>
                    <input type="date" class="form-control form-control-lg lg fs-18" id="expiry_date" value="{{date('Y-m-d', strtotime($show_status->expired_at))}}">
                    <div id="invalid-date" class="invalid-feedback">Expiry date is required.</div>
                </div>
                <div class="d-flex justify-content-center">
                    <button type="button" class="btn btn-primary primary small me-3 text-uppercase" style="min-width:156px; height: 48px;" id="btn_set">Set</button>
                </div>
            </div>
        </div>
    </div>
</div>