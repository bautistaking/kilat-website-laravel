<div class="sub-nav z-depth-1">
  <div class="container">
    <div>
        @if ( session()->has('success') )
            <div class="text-color -green">
               <p> {!! session('success')['message'] !!}</p>
            </div>
        @endif
    </div>

    <a class='dropdown-trigger flex align-center text-color -black' data-target='logout'>
      Hello,&nbsp;<strong>Admin</strong><i class="material-icons right text-color -pink">expand_more</i>
    </a>

    <!-- Dropdown Structure -->
    <ul id='logout' class='dropdown-content'>
      <li><a href="{{ url('/logout') }}">Logout</a></li>
    </ul>

  </div>
</div>