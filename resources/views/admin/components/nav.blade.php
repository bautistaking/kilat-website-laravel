<nav>
  <div class="mx-1">
    <div class="nav-wrapper">

      <a href="#" class="brand-logo left"><img src="{{ asset('/images/logo.png') }}" alt=""></a>
      <a href="#" data-target="mobile-nav" class="sidenav-trigger"><i class="material-icons">menu</i></a>

      <ul class="mx-1 hide-on-med-and-down">
        <li><a href="{{url('/webmaster-kidlat/entries')}}">Entries</a></li>
        @if(Auth::user()->admin_type != 1)
        <li><a href="{{url('/webmaster-kidlat/judges')}}">Judges</a></li>
        <li><a href="{{url('/webmaster-kidlat/votes')}}">Votes</a></li>
        @endif
      </ul>

      <ul class="sidenav" id="mobile-nav">
        <li><a href="{{url('/webmaster-kidlat/entries')}}">Entries</a></li>
        @if(Auth::user()->admin_type != 1)
        <li><a href="{{url('/webmaster-kidlat/judges')}}">Judges</a></li>
        <li><a href="{{url('/webmaster-kidlat/votes')}}">Votes</a></li>
        @endif
      </ul>

    </div>
  </div>
</nav>