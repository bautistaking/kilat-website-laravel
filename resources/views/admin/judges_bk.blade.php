@extends('base')
@push('styles')
  <link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
  <link rel="stylesheet" href="{{ mix('admin/css/admin.css') }}">
@endpush
@section('content')
@include('admin.components.nav')
<div class="content overflow-y-visible">
  @include('admin.components.sub-nav')
  <div class="card padded overflow-y-visible">
    <div class="overflow-auto overflow-y-visible">
      <div class="text-right">
        <a href="{{url('/admin/judge/add')}}" class="btn-floating btn-large waves-effect waves-light pink"><i class="material-icons">add</i></a>
      </div>      
      <div class="spacer"></div>
      <table class="striped centered dt entries-table">
        <thead>
          <tr>
              <th>Name</th>
              <th>Email</th>
              <th>Date Added</th>
              <th>Assigned Category</th>
          </tr>
        </thead>

        <tbody>
          @foreach ($judges as $j)
          <tr>
            <td>{{$j->name}}</td>
            <td>{{$j->email}}</td>
            <td>{{ date('F j, Y. h:i a', strtotime($j->created_at)) }}</td>
            <td>@if(!empty($j->instance())) {{$j->instance()->category()->name}} @endif</td>
          </tr>
          @endforeach
        </tbody>
      </table>
    </div>

  </div>
</div>
@push('custom-js')
  <script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
  <script src="{{ mix('admin/js/admin.js') }}"></script>
@endpush
@endsection