@extends('base')  
@push('styles')
    <style>
        .spinner-grow,
        .loading-label {
            display: none;
        }
    </style>
@endpush
@section('content')
<div class="d-flex flex-grow-1 w-100 h-100">
    <div class="signin wrapper">
        <div class="d-flex justify-content-center flex-grow-1">
            <div class="img-banner d-flex">
                <img class="banner img-fluid" src="{{url('/images/banner.png')}}" alt="...">
            </div>
            <div class="signin-form d-flex flex-column">
                <div class="d-flex flex-column flex-grow-1 justify-content-center mb-5">
                    <div class="text-center header-wrapper">
                       <h2>Administrator Login</h2>
                    </div>
                    <div class="form">
                        <div class="form-group mb-3">
                            <label for="email">Email</label>
                            <input type="email" class="form-control lg" id="email" name="email" placeholder="name@email.com">
                        </div>
                        <div id="invalid-email" class="invalid-feedback mb-3"></div>
                        <div class="form-group">
                            <label for="password">Password</label>
                            <input type="password" class="form-control lg" id="password" name="password" placeholder="password">
                        </div>
                        <div id="invalid-password" class="invalid-feedback"></div>
                        <div class="text-center mt-46">
                            <button class="btn btn-primary primary lg mb-4" id="btn_signin">
                                <span class="spinner-grow spinner-grow-sm" role="status"></span>
                                <span class="spinner-grow spinner-grow-sm" role="status"></span>
                                <span class="spinner-grow spinner-grow-sm" role="status"></span>
                                <span class="signin-label text-uppercase">Login</span>
                            </button>
                            <br>
                        </div>
                    </div>
                </div>
                <div class="signin-form-footer justify-content-end">
                    <h3 class="title">Need help? Contact 4As</h3>
                    <ul class="info">
                        <li>
                            <img src="{{url('/images/email.png')}}" alt="" />
                            <a href="mailto:4asp@pldtdsl.net">4asp@pldtdsl.net</a>
                        </li>
                        <li>
                            <img src="{{url('/images/call.png')}}" alt="" />
                            <a href="#">8893-1205</a>
                        </li>
                        <li>
                            <img src="{{url('/images/mobile-phone.png')}}" alt="" />
                            <a href="#">0917 5222 427</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
@push('custom-js')
<script>
    $(document).ready(activateQuery())

    function activateQuery() {
        $(document).on('click', '#btn_signin', function() {
            login()
        })

        $(document).on('keyup', '#password', function(e) {
            if (e.keyCode === 13) {
                login()
            }
        })

        function login() {
            $.LoadingOverlay("show", {
                image: '{{url("/images/loading1.png")}}'
            },100);

            $('.signin-label').hide()
            $('.spinner-grow').addClass('d-inline-block')

            var checker = 0;
            if ($('#email').val() == "") {
                $.LoadingOverlay("hide");
                $('.signin-label').show()
                $('.spinner-grow').removeClass('d-inline-block')
                $('#invalid-email').text('Email address is required.').show()
                $('#email').addClass('is-invalid')
                checker++;
            } else {
                var email_filter = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
                if (!email_filter.test($('#email').val())) {
                    $.LoadingOverlay("hide");
                    $('.signin-label').show()
                    $('.spinner-grow').removeClass('d-inline-block')
                    $('#email').addClass('is-invalid')
                    $('#invalid-email').text('Email address is invalid.').show()
                    checker++;
                } else {
                    $('#email').removeClass('is-invalid')
                    $('#invalid-email').hide
                }
            }

            if ($('#password').val() == "") {
                $.LoadingOverlay("hide");
                $('.signin-label').show()
                $('.spinner-grow').removeClass('d-inline-block')
                $('#invalid-password').text('Password is required.').show()
                $('#password').addClass('is-invalid')
                checker++;
            } else {
                $('#password').removeClass('is-invalid')
                $('#invalid-password').hide()
            }

            if (checker == 0) {
                let params = {
                    '_token'  : '{{ csrf_token() }}',
                    'email'   : $('#email').val(),
                    'password': $('#password').val()
                }

                fetch("/admin/login/attempt", {
                    method: 'POST',
                    headers: {
                        'Content-Type': 'application/json'
                    },
                    body: JSON.stringify(params)
                })
                .then(response => response.json())
                .then((e) => {
                    let html = ""

                    $('#email').removeClass('is-invalid')
                    $('#password').removeClass('is-invalid')
                    $('.invalid-feedback').hide()

                    var checker = 0
                    if (e.status_code == 400) {
                        $.LoadingOverlay("hide");
                        $('.signin-label').show()
                        $('.spinner-grow').removeClass('d-inline-block')

                        $('#email').addClass('is-invalid')
                        $('#password').addClass('is-invalid')
                        $('#invalid-password').text(e.message.content).show()
                    }

                    if (e.status_code == 200) {
                        $.LoadingOverlay("hide");
                        setTimeout(function() {
                            location.href = "/admin/entries"
                        }, 100)
                    }
                })

            }


        }
    }
</script>
@endpush
@endsection