@extends('base')  
@push('styles')
    <link rel="stylesheet" href="https://cdn.datatables.net/v/bs5/jq-3.6.0/dt-1.11.3/datatables.min.css">
    <style>
        .spinner-grow,
        .loading-label {
            display: none;
        }

        .entries {
            max-width: 100%;
        }

        #search {
            padding-left: 35px;
        }

        .search-icon {
            position: absolute;
            margin-left: 9px;
            margin-top: -34px;
            font-size: 20px;
            color: #d7a333;
        }

        table td {
            word-wrap: break-word;
            font-size: 14px;
            text-align: center;
        }

        .color-link {
            color: #1a2735;
        }

        .dropdown-content li {
            display: none;
        }

        .custom-thead {
            background: #3b4a56;
        }

        .custom-thead > tr > th {
            padding: 20px;
            color: #fff;
            text-align: left;
        }

        table tr > td {
            text-align: left;
        }

        .form-check span,
        .form .form-check .form-check-input {
            margin-top: 20px;
            margin-left: 5px;
        }

        .dataTables_filter label {
            display: none;
        }

        .btn-custom-status {
            width: 130px;
            border-radius: 100px;
        }

        .spinner-border {
            display: none;
        }
    </style>

    <link rel="stylesheet" href="https://cdn.datatables.net/1.11.2/css/dataTables.bootstrap5.min.css">
@endpush
@section('content')
    @include('admin.components.header')
    <div class="wrapper-default">
        <div class="container-fluid minHeight py72">
                <div style="margin-top: 70px">
                    <div class="entries flex-column d-flex">
                        <div class="d-flex align-items-center justify-content-between mb-5">
                            <h1 class="page-title mb-0">Round 2 Entries</h1>
                            <button class="btn btn-primary primary small me-3 text-uppercase btn_import" style="min-width:156px; height: 48px;position:absolute;margin-left:750px">Import Entries CSV</button>
                            <a target="_blank" href="{{url('/webmaster-kidlat/export-round2')}}">
                            <button class="btn btn-outline-primary primary small me-3 text-uppercase link-color btn_export" style="min-width:156px; height: 48px;margin-left:350px">Export Entries CSV</button>
                            </a>
                        </div>

                        <table class="table table-striped centered dt entries-table w-100">
                            <thead class="custom-thead">
                              <tr>
                                  <th>ID</th>
                                  <th>Agency</th>
                                  <th>Entry Name</th>
                                  <th>Entry</th>
                                  <th>Category</th>
                                  <th>Sub Category</th>
                              </tr>
                            </thead>

                            <tbody>
                                @foreach ($entries as $e)
                                    <tr>
                                        <td class="uid flex align-center form">
                                            <span>{{$e->uid}}</span>
                                        </td>
                                        <td>{{$e->agency}}</td>
                                        <td>
                                            <a class="color-link" href="{{url('webmaster-kidlat/view/entry')}}/{{$e->uid}}">
                                            {{$e->entry_name}}
                                            </a>
                                        </td>
                                        <td>{{$e->type}}</td>
                                        <td>{{$e->cat_name}}</td>
                                        <td>{{$e->sub_name}}</td>
                                    </tr>
                                @endforeach
                            </tbody>
                          </table>
                    </div>
                </div>
        </div>
    </div>

    <!-- modal -->
    <div class="voting-modal modal fade" id="importEntriesModal" tabindex="-1" aria-labelledby="flag" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    <h5 class="mx-auto" style="margin-top:-20px">Import Round 2 Entries</h5>
                </div>
                <div>
                    <label for="formFileLg" class="form-label">CSV File</label>
                    <input class="form-control form-control-lg" id="formFileLg" type="file" accept=".csv">
                </div>
                <div class="modal-body p-0 pt-4 mx-auto">
                    <button type="button" class="btn btn-primary primary small me-3 text-uppercase mx-auto btn-submit">
                        <span class="spinner-grow spinner-grow-sm" role="status"></span>
                        <span class="spinner-grow spinner-grow-sm" role="status"></span>
                        <span class="spinner-grow spinner-grow-sm" role="status"></span>
                        <span class="submit-label">Submit</span>
                    </button>
                </div>
            </div>
        </div>
    </div>

    @include('admin.components.footer')
@push('custom-js')
<script type="text/javascript" src="https://cdn.datatables.net/v/bs5/jq-3.6.0/dt-1.11.3/datatables.min.js"></script>
<script>
    $(document).ready(activateQuery())

    function activateQuery() {
        var dtable = $('.dt').DataTable({
            "aaSorting": [],
            "autoWidth": false,
            "bFilter": true,
            "bInfo": false,
            "lengthChange": false,
            "paging": true,
            "pagingType": "simple_numbers",
            "sorting": true,
            "order": [[ 2, "desc" ]]
            // "searching": false
        });

        $('#search').keyup(function(){
            dtable.search($(this).val()).draw();
        });
    }

    $(document).on('click', '.btn_import', function() {
        $('#importEntriesModal').modal('show')
    })

    $(document).on('click', '.btn-submit', function() {

        let form_data = new FormData()
        form_data.append('entries', $('input[type=file]')[0].files[0])

        let params = {
            '_token'     : '{{ csrf_token() }}',
            'entries'    : form_data
        }

        fetch("/webmaster-kidlat/import-entries", {
            method: 'POST',
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            body: form_data
        })
        .then(response => response.json())
        .then((e) => {
            if (e.status_code == 200) {
                location.reload();
            }
        })

    })

</script>
@endpush
@endsection