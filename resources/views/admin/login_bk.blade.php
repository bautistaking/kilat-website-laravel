@extends('base')
@push('styles')
  <link rel="stylesheet" href="{{ mix('admin/css/admin.css') }}">
@endpush
@section('content')
<div class="login">
  @include('error.components.header')
  <div class="container">
    <div class="card padded">


        <div class="row">
          <div class="col m8 offset-m2">
            @if ($errors->any())
                <div class="text-color -red">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <form method="POST" action="{{ url('admin/login/attempt') }}">
              @csrf
              <div class="input-field col s12">
                <input placeholder="Username" type="text" name="email" class="validate">
                <label for="username">Username</label>
              </div>

              <div class="input-field col s12">
                <input placeholder="Password" type="password" name="password" class="validate">
                <label for="password">Password</label>
              </div>
              <div class="text-center">
                <button class="waves-effect waves-dark btn pink ">Login</button>
              </div>
            </form>
          </div>
        </div>

    </div>
  </div>
</div>
@endsection