@extends('base')  
@push('styles')
    <link rel="stylesheet" href="https://cdn.datatables.net/v/bs5/jq-3.6.0/dt-1.11.3/datatables.min.css">
    <style>
        .spinner-grow,
        .loading-label {
            display: none;
        }

        .entries {
            max-width: 100%;
        }

        #search {
            padding-left: 35px;
        }

        .search-icon {
            position: absolute;
            margin-left: 9px;
            margin-top: -34px;
            font-size: 20px;
            color: #d7a333;
        }

        table td {
            word-wrap: break-word;
            font-size: 14px;
            text-align: center;
        }

        .color-link {
            color: #1a2735;
        }

        .dropdown-content li {
            display: none;
        }

        .custom-thead {
            background: #3b4a56;
        }

        .custom-thead > tr > th {
            padding: 20px;
            color: #fff;
            text-align: left;
        }

        table tr > td {
            text-align: left;
        }

        .form-check span,
        .form .form-check .form-check-input {
            margin-top: 20px;
            margin-left: 5px;
        }

        .dataTables_filter label {
            display: none;
        }

        .btn-custom-status {
            width: 130px;
            border-radius: 100px;
        }

        .spinner-border {
            display: none;
        }
    </style>

    <link rel="stylesheet" href="https://cdn.datatables.net/1.11.2/css/dataTables.bootstrap5.min.css">
@endpush
@section('content')
    @include('admin.components.header')
    <div class="wrapper-default">
        <div class="container-fluid minHeight py72">
            @if (count($entries) > 0)
                <div style="margin-top: 70px">
                    <div class="entries flex-column d-flex">
                        <div class="d-flex align-items-center justify-content-between">
                            <h1 class="page-title mb-0">All Entries</h1>
                            <a href="{{url('/admin/download-entries')}}">
                                <button type="button" class="btn btn-primary primary small me-3 text-uppercase" style="min-width:156px; height: 48px;">Download CSV Report</button>
                            </a>
                        </div>
                        <div class="d-flex align-items-center">
                            <div class="mt-5 mb-4 me-3">
                                <label for="filter" class="form-label fw-500 mb-2">Filter</label>
                            </div>
                            <div class="mt-5 mb-4 me-5">
                                <select id="dropdown-payment-status" class="form-select form-select-lg custom-select lg" style="width: 210px" id="filter" aria-label=".form-select-lg">
                                    <option value="All">All</option>
                                    <option value="Requested">Requested</option>
                                    <option value="Unpaid">Unpaid</option>
                                    <option value="Paid">Paid</option>
                                </select>
                            </div>
                            <div class="mt-5 mb-4 me-3">
                                <label for="status" class="form-label fw-500 mb-2">Entry Status</label>
                            </div>
                            <div class="mt-5 mb-4 me-5">
                                <select id="dropdown-status" class="form-select form-select-lg custom-select lg" style="width: 210px" id="status" aria-label=".form-select-lg">
                                        <option value="All">All</option>
                                        <option value="Approved">Draft</option>
                                        <option value="Approved">Approved</option>
                                        <option value="Pending">Pending</option>
                                        <option value="Rejected">Rejected</option>
                                </select>
                            </div>
                            <div class="mt-5 mb-4" style="margin-left: 170px">
                                <div style="width: 100%">
                                    <input type="text" class="form-control form-control-lg lg fs-18" id="search" placeholder="Search">
                                    <i class="fas fa-search search-icon"></i>
                                </div>
                            </div>
                        </div>

                        <table class="table table-striped centered dt entries-table w-100">
                            <thead class="custom-thead">
                              <tr>
                                  <th style="display:none">payment status hidden</th>
                                  <th style="display:none">status hidden</th>
                                  <th>ID</th>
                                  <th>Entrant</th>
                                  <th>Entry Name</th>
                                  <th>Entry</th>
                                  <th>Category</th>
                                  <th>Sub Category</th>
                                  <th>Fee</th>
                                  <th>Date Submitted</th>
                                  @if(Auth::user()->admin_type == 1 || Auth::user()->admin_type == null)
                                  <th>Payment Status</th>
                                  @endif
                                  @if(Auth::user()->admin_type == 1 || Auth::user()->admin_type == 2 || Auth::user()->admin_type == null)
                                  <th>Entry Status</th>
                                  @endif
                                  <th>Invoice</th>
                              </tr>
                            </thead>

                            <tbody>
                              @foreach ($entries as $e)
                              <tr>
                                <td style="display:none">
                                    {{$e->payment_status}}
                                </td>
                                <td style="display:none">
                                    {{$e->status}}
                                </td>
                                <td class="uid flex align-center form">
                                    <span>{{$e->uid}}</span>
                                </td>
                                <td>{{$e->agency}}</td>
                                <td>
                                    <a class="color-link" href="{{url('admin/view/entry')}}/{{$e->uid}}">
                                        {{$e->entry_name}}
                                    </a>
                                </td>
                                <td>{{$e->type}}</td>
                                <td>{{$e->category()->name}}</td>
                                <td>{{$e->sub_category()->name}}</td>
                                <td>{{number_format($e->fee, 2, ".", ",")}}</td>
                                <td data-sort="{{ date('Ymd', strtotime($e->created_at)) }}">{{ date('m-j h:i a', strtotime($e->created_at)) }}</td>
                                @if(Auth::user()->admin_type == 1 || Auth::user()->admin_type == null)
                                <td>
                                    <div class="dropdown">
                                        <button class='btn-custom-status mt-2 mb-1 btn dropdown-toggle @if($e->payment_status == "Paid") btn-success @elseif($e->payment_status == "Unpaid") btn-danger @elseif($e->payment_status == "Requested") btn-info @endif' type="button" id="paymentStatusDropdown" data-bs-toggle="dropdown" data-id="{{$e->id}}">
                                        {{$e->payment_status}}
                                        </button>
                                        <ul class="dropdown-menu" aria-labelledby="paymentStatusDropdown">
                                            <li><a class="dropdown-item payment-status" data-identifier="payment_status" data-id="{{$e->id}}" data-paymentstatus="Unpaid" href="javascript:void(0)">Unpaid</a></li>
                                            <li><a class="dropdown-item payment-status" data-identifier="payment_status" data-id="{{$e->id}}" data-paymentstatus="Paid" href="javascript:void(0)">Paid</a></li>
                                        </ul>
                                    </div>
                                </td>
                                @endif
                                @if(Auth::user()->admin_type == 1 || Auth::user()->admin_type == 2 || Auth::user()->admin_type == null)
                                    <td>
                                        <div class="dropdown">
                                            <button class='btn-custom-status mt-2 mb-1 btn dropdown-toggle @if($e->status == "Approved") btn-success @elseif($e->status == "Rejected") btn-danger @else btn-warning @endif' type="button" id="statusDropdown" data-bs-toggle="dropdown" data-id="{{$e->id}}">
                                                {{$e->status}}
                                            </button>
                                            <ul class="dropdown-menu" aria-labelledby="statusDropdown">
                                                <li><a class="dropdown-item status" data-identifier="status" data-id="{{$e->id}}" data-status="Pending" href="javascript:void(0)">Pending</a></li>
                                                <li><a class="dropdown-item status" data-identifier="status" data-id="{{$e->id}}" data-status="Approved" href="javascript:void(0)">Approved</a></li>
                                                <li><a class="dropdown-item status" data-identifier="status" data-id="{{$e->id}}" data-status="Rejected" href="javascript:void(0)">Rejected</a></li>
                                            </ul>
                                        </div>
                                    </td>
                                @endif
                                <td>
                                    <button class="btn btn-primary btn_email" id="send_email" data-id="{{$e->id}}" data-uid="{{$e->uid}}" data-email="{{$e->email}}">
                                        <div class="spinner-border text-warning spinner-border-sm entry-loader" style="" role="status">
                                            <span class="visually-hidden">Loading...</span>
                                        </div>
                                        <i class="email-icon far fa-envelope"></i>
                                    </button>
                                </td>
                              </tr>
                              @endforeach
                            </tbody>
                          </table>
                    </div>
                </div>
            @endif
        </div>
    </div>

    <!-- modal -->
    <div class="voting-modal modal fade" id="confirmModal" tabindex="-1" aria-labelledby="flag" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    <h5 class="mx-auto" style="margin-top:-20px">Do you want to proceed?</h5>
                </div>
                <div class="modal-body p-0 pt-4 mx-auto">
                    <button type="button" class="btn btn-primary primary small me-3 text-uppercase mx-auto" data-bs-dismiss="modal" aria-label="Cancel">Cancel</button>
                    <button type="button" class="btn btn-primary primary small me-3 text-uppercase mx-auto btn-confirm">
                        <span class="spinner-grow spinner-grow-sm" role="status"></span>
                        <span class="spinner-grow spinner-grow-sm" role="status"></span>
                        <span class="spinner-grow spinner-grow-sm" role="status"></span>
                        <span class="confirm-label">Confirm</span>
                    </button>
                </div>
            </div>
        </div>
    </div>
    @include('admin.components.footer')
@push('custom-js')
<script type="text/javascript" src="https://cdn.datatables.net/v/bs5/jq-3.6.0/dt-1.11.3/datatables.min.js"></script>
<script>
    $(document).ready(activateQuery())

    function activateQuery() {
        var dtable = $('.dt').DataTable({
            "aaSorting": [],
            "autoWidth": false,
            "bFilter": true,
            "bInfo": false,
            "lengthChange": false,
            "paging": true,
            "pagingType": "simple_numbers",
            "sorting": true,
            "order": [[ 6, "desc" ]]
            // "searching": false
        });

        $('#search').keyup(function(){
            dtable.search($(this).val()).draw();
        });

        $("#dropdown-payment-status").change(function (e) {
            if ($(this).val() == "All") {
                location.reload()
            } else {
                dtable.column(0).search($(this).val()).draw();
            }
        });

        $("#dropdown-status").change(function (e) {
            if ($(this).val() == "All") {
                location.reload()
            } else {
                dtable.column(1).search($(this).val()).draw();
            }
        });

        var id             = 0;
        var status         = "";
        var payment_status = "";
        var identifier     = "";

        $(document).on('click', '.payment-status', function() {
            id             = $(this).attr('data-id')
            payment_status = $(this).attr('data-paymentstatus')
            identifier     = $(this).attr('data-identifier')
            
            updatePayment()
        })

        $(document).on('click', '.status', function() {
            id         = $(this).attr('data-id')
            status     = $(this).attr('data-status')
            identifier = $(this).attr('data-identifier')
            // $('#confirmModal').toggle('modal')
            updateStatus()
        })

        // $(document).on('click', '.btn-confirm', function() {
        //     $('.confirm-label').hide()
        //     $('.spinner-grow').addClass('d-inline-block')

        //     // $(this).attr('disabled', true)

        //     if (identifier == "payment_status") {
        //         updatePayment()                
        //     } else if (identifier == "status") {
        //         updateStatus()
        //     }
        // })

        function updatePayment() {
            let params = {
                '_token'         : '{{ csrf_token() }}',
                'id'             : id,
                'payment_status' : payment_status
            }

            fetch("/admin/update/payment", {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify(params)
            })
            .then(response => response.json())
            .then((e) => {
                let html = ""

                if (e.status_code == 200) {
                    // $('#confirmModal').modal('hide')
                    $('.confirm-label').show()
                    $('.spinner-grow').removeClass('d-inline-block')

                    $('#paymentStatusDropdown[data-id="'+id+'"]').removeClass('btn-info btn-success btn-danger').prop('disabled', false).attr('disabled', false)

                    if (payment_status == "Paid") {
                        $('#paymentStatusDropdown[data-id="'+id+'"]').addClass('btn-success').text('Paid')
                    } else if (payment_status == "Unpaid") {
                        $('#paymentStatusDropdown[data-id="'+id+'"]').addClass('btn-danger').text('Unpaid')
                    }

                    setTimeout(function() {
                        // Swal.fire({
                        //     icon: 'success',
                        //     title: e.header,
                        //     html: e.message,
                        //     confirmButtonText: 'Close',
                        //     confirmButonColor: 'btn-primary'
                        // })
                        // location.href = "/admin/entries"
                    }, 2000)
                }
            })
        }

        function updateStatus() {
            let params = {
                '_token' : '{{ csrf_token() }}',
                'id'     : id,
                'status' : status
            }

            fetch("/admin/update/status", {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify(params)
            })
            .then(response => response.json())
            .then((e) => {
                let html = ""

                if (e.status_code == 200) {
                    // $('#confirmModal').toggle('modal')
                    // $('#confirmModal').modal('hide')
                    $('.confirm-label').show()
                    $('.spinner-grow').removeClass('d-inline-block')

                    $('#statusDropdown[data-id="'+id+'"]').removeClass('btn-info btn-success btn-danger btn-warning').prop('disabled', false).attr('disabled', false)

                    if (status == "Pending") {
                        $('#statusDropdown[data-id="'+id+'"]').addClass('btn-warning').text('Pending')
                    } else if (status == "Approved") {
                        $('#statusDropdown[data-id="'+id+'"]').addClass('btn-success').text('Approved')
                    } else if (status == "Rejected") {
                        $('#statusDropdown[data-id="'+id+'"]').addClass('btn-danger').text('Rejected')
                    }

                    // Swal.fire({
                    //     icon: 'success',
                    //     title: e.header,
                    //     html: e.message,
                    //     confirmButtonText: 'Close',
                    //     confirmButonColor: 'btn-primary'
                    // })

                    setTimeout(function() {

                        // location.href = "/admin/entries"
                    }, 2000)
                }
            })
        }
    }

    $(document).on('click', '#send_email', function() {
        $(this).find('.spinner-border').show()
        $(this).find('.email-icon').hide()

        let params = {
            '_token' : '{{ csrf_token() }}',
            'email'  : $(this).attr('data-email'),
            'uid'    : $(this).attr('data-uid')
        }

        fetch("/admin/request-invoice-solo", {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(params)
        })
        .then(response => response.json())
        .then((e) => {
            // location.href = "/admin/entries"
            if (e.status_code == 200) {

                setTimeout(function() {
                    Swal.fire({
                        icon: 'success',
                        title: e.header,
                        html: e.message.email,
                        confirmButtonText: 'Close',
                        confirmButonColor: 'btn-primary'
                    })
                }, 1000)

                $('.spinner-border').hide()
                $('.email-icon').show()

                $('#paymentStatusDropdown[data-id="'+$(this).attr('data-id')+'"]').removeClass('btn-info btn-success btn-danger').prop('disabled', false).attr('disabled', false)

                $('#paymentStatusDropdown[data-id="'+$(this).attr('data-id')+'"]').addClass('btn-info').text('Requested')
            }
        })
        .catch((error) => {
        })
    });
</script>
@endpush
@endsection