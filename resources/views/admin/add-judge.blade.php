@extends('base')
@push('styles')
  <link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
  <link rel="stylesheet" href="{{ mix('admin/css/admin.css') }}">
@endpush
@section('content')
@include('admin.components.nav')
<div class="content overflow-y-visible">
  @include('admin.components.sub-nav')
  <div class="card padded overflow-y-visible">
    
    <div class="overflow-auto overflow-y-visible">
      <a href="{{url('/webmaster-kidlat/judges')}}" class="flex align-center"><i class="material-icons right">keyboard_arrow_left</i>Back</a>
      <div class="row">
         <div class="col s12 m6 offset-m3">
          <div class="text-center">
            <h5>ADD JUDGE</h5>
          </div>
          <div class="spacer"></div>
          <form action="{{url('/webmaster-kidlat/judge/add/post')}}" method="post">
            @csrf
            <div class="field inline">
              <label>NAME</label>
              <div class="input-field">
                <input type="text" name="name" class="validate" autocomplete="off" required>
              </div>
            </div>

            <div class="field inline">
              <label>Email</label>
              <div class="input-field">
                <input type="text" name="email" class="validate" autocomplete="off" required>
              </div>
            </div>

            <div class="field inline">
              <label>Category</label>
              <div class="input-field">
                <select name="category">
                  @foreach($categories as $cat)
                    <option value="{{$cat->id}}">{{$cat->name}}</option>
                  @endforeach
                </select>
              </div>
            </div>

            <div class="text-center">
              <button  class="waves-effect waves-dark btn pink">ADD JUDGE</button>
            </div>
          </form>
          <div class="spacer"></div>
          <ul class="text-color -red">
            @foreach ($errors->all() as $error)
              <li>{{$error}}</li>
            @endforeach
          </ul>
        </div>
      </div>
    </div>

  </div>
</div>
@push('custom-js')
  <script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
  <script src="{{ mix('admin/js/admin.js') }}"></script>
  <script>
    @if (session('success'))
      $( document ).ready(function() {
        Swal.fire(
          "{{ session('success')['header'] }}",
          "{{ session('success')['message'] }}",
          'success'
        );
      });
    @endif
  </script>
@endpush
@endsection