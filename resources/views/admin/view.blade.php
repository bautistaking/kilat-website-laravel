@extends('base')  
@push('styles')
    <style>
        .spinner-grow,
        .loading-label {
            display: none;
        }

        .status {
            color: #fff;
            margin-top: 15px;
            font-size: 14px;
        }

        .edit-link {
            color: #fff;
            text-decoration: none;
        }

        .edit-link > i {
            color: #d7a333;
            font-size: 20px;
        }

        .edit-link:hover {
            color: #d7a333;
        }

        video {
            max-width: 272px;
            height: auto;
        }

        .btn-custom-status {
            width: 130px;
            border-radius: 100px;
        }

        .dropdown a {
            color: #333 !important;
        }
    </style>
@endpush
@section('content')
    @include('admin.components.header')
        <div class="bg">
            <div class="wrapper-default pt72">
                <div class="container-fluid py72">
                    <div class="d-flex align-items-center justify-content-between mb-3">
                        <a href="{{url('/admin/entries')}}" class="top-action">
                            <i class="bi bi-chevron-left me-2"></i> <span>Go Back</span>
                        </a>
                        <p class="status">
                            {{$entry->payment_status}} |

                            @if(Auth::user()->admin_type == 1 || Auth::user()->admin_type == 2 || Auth::user()->admin_type == null) 
                            <div class="dropdown" style="margin-left:-900px">
                                <button class='btn-custom-status mt-2 mb-1 btn dropdown-toggle @if($entry->status == "Approved") btn-success @elseif($entry->status == "Rejected") btn-danger @else btn-warning @endif' type="button" id="statusDropdown" data-bs-toggle="dropdown">
                                {{$entry->status}}
                                </button>
                                <ul class="dropdown-menu" aria-labelledby="statusDropdown">
                                    <li><a data-bs-toggle="modal" data-bs-target="#confirmModal" class="dropdown-item entry-status" data-identifier="status" data-uid="{{$entry->uid}}" data-id="{{$entry->id}}" data-status="Pending" href="javascript:void(0)">Pending</a></li>
                                    <li><a data-bs-toggle="modal" data-bs-target="#confirmModal" class="dropdown-item entry-status" data-identifier="status" data-uid="{{$entry->uid}}" data-id="{{$entry->id}}" data-status="Approved" href="javascript:void(0)">Approved</a></li>
                                    <li><a data-bs-toggle="modal" data-bs-target="#confirmModal" class="dropdown-item entry-status" data-identifier="status" data-uid="{{$entry->uid}}" data-id="{{$entry->id}}" data-status="Rejected" href="javascript:void(0)">Rejected</a></li>
                                </ul>
                            </div>
                            @else
                                {{$entry->status}}
                            @endif
                    </div>
                    <h1 class="page-title text-center mb-0">{{$entry->entry_name}}</h1>
                    <div class="tabs entry">
                        <ul class="nav nav-pills" id="myTab" role="tablist">
                            <li class="nav-item" role="presentation">
                                <button class="nav-link active" id="pills-category-tab" data-bs-toggle="pill" data-bs-target="#pills-category" type="button" role="tab" aria-controls="pills-category" aria-selected="true">Category details</button>
                            </li>
                            <li class="nav-item" role="presentation">
                                <button class="nav-link" id="pills-campaign-tab" data-bs-toggle="pill" data-bs-target="#pills-campaign" type="button" role="tab" aria-controls="pills-campaign" aria-selected="false">Campaign details</button>
                            </li>
                            <li class="nav-item" role="presentation">
                                <button class="nav-link" id="pills-entry -tab" data-bs-toggle="pill" data-bs-target="#pills-entry " type="button" role="tab" aria-controls="pills-entry " aria-selected="false">Entry upload</button>
                            </li>
                            @if ($entry->category != 2)
                                <li class="nav-item" role="presentation">
                                    <button class="nav-link" id="pills-written-tab" data-bs-toggle="pill" data-bs-target="#pills-written" type="button" role="tab" aria-controls="pills-written" aria-selected="false">Written submission</button>
                                </li>
                            @endif
                            <li class="nav-item" role="presentation">
                                <button class="nav-link" id="pills-entry -tab" data-bs-toggle="pill" data-bs-target="#pills-agency " type="button" role="tab" aria-controls="pills-entry " aria-selected="false">Entrant details</button>
                            </li>
                        </ul>
                        <div class="tab-content" id="myTabContent">
                            <div class="tab-pane fade show active" id="pills-category" role="tabpanel" aria-labelledby="pills-category-tab">
                                <div class="review-card">
                                    <div class="d-flex align-items-start justify-content-between my-3">
                                        <h3>Category details</h3>
                                    </div>
                                    <div class="review-line">
                                        <div class="label">Category</div>
                                        <div class="value">{{$entry->category()->name}}</div>
                                    </div>
                                    <div class="review-line">
                                        <div class="label">Subcategory</div>
                                        <div class="value text-uppercase">{{$entry->sub_category()->name}}</div>
                                    </div>
                                    <div class="review-line">
                                        <div class="label">Entry type</div>
                                        <div class="value">{{$entry->type}}</div>
                                    </div>
                                    <div class="review-line">
                                        <div class="label">Entry type list</div>
                                        <div class="value">{{$entry->entry_type_list}}</div>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="pills-campaign" role="tabpanel" aria-labelledby="pills-campaign-tab">
                                <div class="review-card">
                                    <div class="d-flex align-items-start justify-content-between my-3">
                                        <h3>Campaign details</h3>
                                    </div>
                                    <div class="review-line">
                                        <div class="label">Entry name</div>
                                        <div class="value">{{$entry->entry_name}}</div>
                                    </div>
                                    <div class="review-line">
                                        <div class="label">Ad title/s</div>
                                        <div class="value">{{$entry->ad_details}}</div>
                                    </div>
                                    <div class="review-line">
                                        <div class="label">Advertiser</div>
                                        <div class="value">{{$entry->advertiser}}</div>
                                    </div>
                                    <div class="review-line">
                                        <div class="label">Brand</div>
                                        <div class="value">{{$entry->brand}}</div>
                                    </div>
                                    <div class="review-line">
                                        <div class="label">Entrant Name</div>
                                        <div class="value">{{$entry->agency}}</div>
                                    </div>
                                    <div class="review-line">
                                        <div class="label">Date of first publication</div>
                                        <div class="value">{{date('F j, Y', strtotime($entry->date_publication))}}</div>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="pills-entry" role="tabpanel" aria-labelledby="pills-entry-tab">
                                <div class="review-card">
                                    <div class="d-flex align-items-start justify-content-between mb-3 my-3">
                                        <div>
                                            <h3>Entry upload</h3>
                                        </div>
                                    </div>
                                    <div class="file-preview-entry">
                                        <div class="review-line">
                                            <div class="label">Concept board</div>
                                            <div class="value">
                                                <div class="img mt-2" id="preview_concept_board_dropzone">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="review-line">
                                            <div class="label">Case video</div>
                                            <div class="value">
                                                <div class="img mt-2" id="preview_case_study_video_dropzone">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="review-line">
                                            <div class="label">Written Case Study</div>
                                            <div class="value">
                                                <div class="img mt-2" id="preview_written_case_dropzone">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="review-line">
                                            <div class="label">ASC Clearance</div>
                                            <div class="value">
                                                <div class="img mt-2" id="preview_asc_clearance_dropzone">                                                    
                                                </div>
                                            </div>
                                        </div>
                                        <div class="review-line">
                                            <div class="label">Media Certification of Performance</div>
                                            <div class="value">
                                                <div class="img mt-2" id="preview_media_certification_dropzone">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="review-line">
                                            <div class="label">Client Certification</div>
                                            <div class="value">
                                                <div class="img mt-2" id="preview_client_certification_dropzone">
                                                </div>
                                            </div>
                                        </div>

                                        <div class="review-line">
                                            <div class="label">Film</div>
                                            <div class="value">
                                                <div class="img mt-2" id="preview_film_dropzone">
                                                    None
                                                </div>
                                            </div>
                                        </div>
                                        <div class="review-line">
                                            <div class="label">Posters</div>
                                            <div class="value">
                                                <div class="img mt-2" id="preview_posters_dropzone">
                                                    None
                                                </div>
                                            </div>
                                        </div>
                                        <div class="review-line">
                                            <div class="label">Audio</div>
                                            <div class="value" id="preview_audio_dropzone">
                                                None
                                            </div>
                                        </div>
                                        <div class="review-line">
                                            <div class="label">Demo Film (if video)</div>
                                            <div class="value">
                                                <div class="img mt-2" id="preview_demo_film_dropzone">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="review-line">
                                            <div class="label">English Translation if needed</div>
                                            <div class="value">
                                                <div class="img mt-2" id="english_translation_dropzone_pdf">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="review-line">
                                            <div class="label">JPEG of the Piece</div>
                                            <div class="value">
                                                <div class="img mt-2" id="preview_photo_piece_dropzone">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="review-line">
                                            <div class="label">JPEG of Entry</div>
                                            <div class="value">
                                                <div class="img mt-2" id="preview_entry_dropzone">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="review-line">
                                            <div class="label">Campaign components, Youtube Links & Working URLs</div>
                                            <div class="value">
                                                <div class="preview_online_link_container" style="overflow-wrap: break-word;"></div>
                                            </div>
                                        </div>
                                    </div>

                                    <div style="border-bottom:1px solid rgba(201,210,218,0.5)"></div>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="pills-written" role="tabpanel" aria-labelledby="pills-written-tab">
                                <div class="review-card">
                                    <div class="d-flex align-items-start justify-content-between my-3">
                                        <h3>Written submission</h3>
                                    </div>
                                    <div class="review-line flex-column">
                                        <h3>Creative effectiveness</h3>
                                        <p>
                                            To help the jury appreciate your entry, please fill up the written submission as succinctly and clearly as possible.
                                        </p>
                                    </div>
                                    @if($entry->background != 'N/A' && $entry->background != null && $entry->background != 'undefined')
                                    <div class="review-line flex-column">
                                        <h3>Background ({{str_word_count($entry->background)}})</h3>
                                        <p>
                                            {{$entry->background}}
                                        </p>
                                    </div>
                                    @endif

                                    @if($entry->objectives != 'N/A' && $entry->objectives != null && $entry->objectives != 'undefined')
                                    <div class="review-line flex-column">
                                        <h3>Objectives ({{str_word_count($entry->objectives)}})</h3>
                                        <p>
                                            {{$entry->objectives}}
                                        </p>
                                    </div>
                                    @endif

                                    @if($entry->strategy != 'N/A' && $entry->strategy != null && $entry->strategy != 'undefined')
                                    <div class="review-line flex-column">
                                        <h3>Strategy ({{str_word_count($entry->strategy)}})</h3>
                                        <p>
                                            {{$entry->strategy}}
                                        </p>
                                    </div>
                                    @endif

                                    @if($entry->execution != 'N/A' && $entry->execution != null && $entry->execution != 'undefined')
                                    <div class="review-line flex-column">
                                        <h3>Execution ({{str_word_count($entry->execution)}})</h3>
                                        <p>
                                            {{$entry->execution}}
                                        </p>
                                    </div>
                                    @endif

                                    @if($entry->describe_strategy != 'N/A' && $entry->describe_strategy != null && $entry->describe_strategy != 'undefined')
                                    <div class="review-line flex-column">
                                        <h3>Describe the strategy ({{str_word_count($entry->describe_strategy)}})</h3>
                                        <p>
                                            {{$entry->describe_strategy}}
                                        </p>
                                    </div>
                                    @endif

                                    @if($entry->solution != 'N/A' && $entry->solution != null && $entry->solution != 'undefined')
                                    <div class="review-line flex-column">
                                        <h3>Describe the idea / solution ({{str_word_count($entry->solution)}})</h3>
                                        <p>
                                            {{$entry->solution}}
                                        </p>
                                    </div>
                                    @endif

                                    @if($entry->creative_idea != 'N/A' && $entry->creative_idea != null && $entry->creative_idea != 'undefined')
                                    <div class="review-line flex-column">
                                        <h3>Describe the creative idea ({{str_word_count($entry->creative_idea)}})</h3>
                                        <p>
                                            {{$entry->creative_idea}}
                                        </p>
                                    </div>
                                    @endif

                                    @if($entry->describe_execution != 'N/A' && $entry->describe_execution != null && $entry->describe_execution != 'undefined')
                                    <div class="review-line flex-column">
                                        <h3>Describe the execution (implementation, timeline, placement, scale) ({{str_word_count($entry->describe_execution)}})</h3>
                                        <p>
                                            {{$entry->describe_execution}}
                                        </p>
                                    </div>
                                    @endif

                                    @if($entry->results != 'N/A' && $entry->results != null && $entry->results != 'undefined')
                                    <div class="review-line flex-column">
                                        <h3>Results ({{str_word_count($entry->results)}})</h3>
                                        <p>
                                            {{$entry->results}}
                                        </p>
                                    </div>
                                    @endif
                                </div>
                            </div>
                            <div class="tab-pane fade" id="pills-agency" role="tabpanel" aria-labelledby="pills-campaign-tab">
                                <div class="review-card">
                                    <div class="d-flex align-items-start justify-content-between my-3">
                                        <h3>Entrant details</h3>
                                    </div>
                                    <div class="review-line">
                                        <div class="label">Member</div>
                                        <div class="value">{{$entry->member}}</div>
                                    </div>
                                    <div class="review-line">
                                        <div class="label">Name of agency contact person</div>
                                        <div class="value">{{$entry->agency_contact}}</div>
                                    </div>
                                    <div class="review-line">
                                        <div class="label">Position</div>
                                        <div class="value">{{$entry->position}}</div>
                                    </div>
                                    <div class="review-line">
                                        <div class="label">Phone number</div>
                                        <div class="value">{{$entry->phone_number}}</div>
                                    </div>
                                    <div class="review-line">
                                        <div class="label">Email address</div>
                                        <div class="value">{{$entry->email}}</div>
                                    </div>
                                    <div class="review-line">
                                        @if(!empty($entry->credits()))
                                        <div class="label">Credits</div>
                                        <div class="value">
                                            @foreach($entry->credits() as $credit)
                                                <div class="mb-4">
                                                    <span class="fs-18 fw-500">{{$credit->name}}</span> <br/>
                                                    <span class="fw-400">{{$credit->role}}</span>
                                                </div>
                                            @endforeach
                                        </div>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- modal -->
        <div class="voting-modal modal fade" id="confirmModal" tabindex="-1" aria-labelledby="flag" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                        <h5 class="mx-auto" style="margin-top:-20px">Do you want to proceed?</h5>
                    </div>
                    <div class="modal-body p-0 pt-4 mx-auto">
                        <button type="button" class="btn btn-primary primary small me-3 text-uppercase mx-auto" data-bs-dismiss="modal" aria-label="Cancel">Cancel</button>
                        <button type="button" class="btn btn-primary primary small me-3 text-uppercase mx-auto btn-confirm">
                        <span class="spinner-grow spinner-grow-sm" role="status"></span>
                        <span class="spinner-grow spinner-grow-sm" role="status"></span>
                        <span class="spinner-grow spinner-grow-sm" role="status"></span>
                        <span class="confirm-label">Confirm</span>
                        </button>
                    </div>
                </div>
            </div>
        </div>
    @include('admin.components.footer')
@push('custom-js')
<script>
    var entry_uid = "{{request()->uid}}";

    $(document).ready(activateQuery())

    function activateQuery() {
        setTimeout(function() {
            //categoryDetailsChecker();
            previewFiles("#preview_concept_board_dropzone", 'concept_board_dropzone')
            previewFiles('#preview_film_dropzone', 'film_dropzone')
            previewFiles('#preview_posters_dropzone', 'posters_dropzone')
            previewFiles('#preview_audio_dropzone', 'audio_dropzone')
            previewFiles('#preview_case_study_video_dropzone', 'case_study_video_dropzone')
            previewFiles('#preview_written_case_dropzone', 'written_case_dropzone')
            previewFiles('#preview_asc_clearance_dropzone', 'asc_clearance_dropzone')
            previewFiles('#preview_media_certification_dropzone', 'media_certification_dropzone')
            previewFiles('#preview_client_certification_dropzone', 'client_certification_dropzone')
            previewFiles('#english_translation_dropzone_pdf', 'english_translation_dropzone_pdf')
            previewFiles('#preview_demo_film_dropzone', 'preview_demo_film_dropzone')
            previewFiles('#preview_photo_piece_dropzone', 'photo_piece_dropzone')
            previewFiles('#preview_entry_dropzone', 'entry_dropzone')
            getLinks()
        }, 500)
    }

    function previewFiles(element_id, type) {
        alert('test');

        let html = ""
        let params = {
            '_token' : '{{ csrf_token() }}',
            'uid'    : entry_uid,
            'type'   : type
        }

        fetch("{{url('/preview-files')}}", {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(params)
        })
        .then(response => response.json())
        .then((e) => {
            $.each(e.files, function(index, file) {
                let ext = '';

                if(file.path) {
                    ext = file.path.split('.').pop();
                    if(type == "film_dropzone" || type == 'case_study_video_dropzone' || type == 'demo_film_dropzone') {
                        html = html + '<video controls><source src="{{asset("/storage/")}}/'+file.path+'" type="video/mp4">Your browser does not support the video tag.</video>';
                    } else if (type == "audio_dropzone") {
                        html = html + '<audio controls><source src="{{asset("/storage/")}}/'+file.path+'" type="audio/mpeg"></audio>';
                    } else if (type == "concept_board_dropzone" || type == "posters_dropzone") {
                        if(ext == 'jpg') {
                            html = html + '<img src="{{asset("/storage/")}}/'+file.path+'" alt=""><br/>';
                        }
                        else {
                            html = html + '<a href="{{asset("/storage/")}}/'+file.path+'" target="_blank"><embed type="application/pdf" src="{{asset("/storage/")}}/'+file.path+'" frameBorder="0" height="100%" width="100%"></embed></a><br/>';
                        }
                    } else if (type == "media_certification_dropzone" || type == "client_certification_dropzone" || type == "english_translation_dropzone_pdf" || type == "photo_piece_dropzone" || type == "entry_dropzone" || type == "written_case_dropzone" || type == "asc_clearance_dropzone") {
                        if(ext == 'jpg') {
                            html = html + '<img src="{{asset("/storage/")}}/'+file.path+'" alt=""><br/>';
                        }
                        else {
                            html = html + '<a href="{{asset("/storage/")}}/'+file.path+'" target="_blank"><embed type="application/pdf" src="{{asset("/storage/")}}/'+file.path+'" frameBorder="0" height="100%" width="100%"></embed></a><br/>';
                        }
                    }
                }
                else {
                    html = html + 'None';
                }
            })

            $(element_id).html(html);
        })

    }

    var id             = 0;
    var uid            = 0;
    var status         = "";

    $(document).on('click', '.entry-status', function() {
        uid        = $(this).attr('data-uid')
        id         = $(this).attr('data-id')
        status     = $(this).attr('data-status')
        $('#confirmModal').toggle('modal')
    })

    $(document).on('click', '.btn-confirm', function() {
        $('.confirm-label').hide()
        $('.spinner-grow').addClass('d-inline-block')

        $(this).attr('disabled', true)
        updateStatus()
    })

    function updateStatus() {
        let params = {
            '_token' : '{{ csrf_token() }}',
            'id'     : id,
            'status' : status
        }

        fetch("/admin/update/status", {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(params)
        })
        .then(response => response.json())
        .then((e) => {
            let html = ""

            if (e.status_code == 200) {
                $('#confirmModal').toggle('modal')

                Swal.fire({
                    icon: 'success',
                    title: e.header,
                    html: e.message,
                    confirmButtonText: 'Close',
                    confirmButonColor: 'btn-primary'
                })

                setTimeout(function() {
                    location.href = "/admin/view/entry/"+uid
                }, 2000)
            }
        })
    }

    function getLinks() {
        $('.next-loading-label').show()
        $('.entry-loader').show()

        let param = {
            'uid': entry_uid
        }

        fetch("/preview-entry-links", {
            method: 'POST',
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(param)
        })
        .then(response => response.json())
        .then((data) => {
            var html = "";
            $.each(data.references, function(index, ref) {
                    html += '<div class="mb-1 border-bottom">'
                        html += '<a class="link-color" href="'+ref.link+'" target="_blank">'
                            html += '<span class="fs-18 fw-500">'+ref.link+'</span><br/>'
                        html += '</a>'
                    html += '</div>'
            })

            $('.preview_online_link_container').html(html)
            // $('.preview_working_url_container').append(html)
        })
    }

    function previewFiles(element_id, type) {
        let html = ""
        let params = {
            '_token' : '{{ csrf_token() }}',
            'uid'    : entry_uid,
            'type'   : type
        }

        fetch("/preview-files", {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(params)
        })
        .then(response => response.json())
        .then((e) => {
            $.each(e.files, function(index, file) {
                let ext = '';

                if(file.path) {
                    ext = file.path.split('.').pop();
                    if(type == "film_dropzone" || type == 'case_study_video_dropzone' || type == 'demo_film_dropzone') {
                        html = html + '<video controls><source src="{{asset("/storage/")}}/'+file.path+'" type="video/mp4">Your browser does not support the video tag.</video>';
                    } else if (type == "audio_dropzone") {
                        html = html + '<audio controls><source src="{{asset("/storage/")}}/'+file.path+'" type="audio/mpeg"></audio>';
                    } else if (type == "concept_board_dropzone" || type == "posters_dropzone") {
                        if(ext == 'jpg') {
                            html = html + '<img src="{{asset("/storage/")}}/'+file.path+'" alt=""><br/>';
                        }
                        else {
                            html = html + '<a href="{{asset("/storage/")}}/'+file.path+'" target="_blank"><embed type="application/pdf" src="{{asset("/storage/")}}/'+file.path+'" frameBorder="0" height="100%" width="100%"></embed></a><br/>';
                        }
                    } else if (type == "media_certification_dropzone" || type == "client_certification_dropzone" || type == "english_translation_dropzone_pdf" || type == "photo_piece_dropzone" || type == "entry_dropzone" || type == "written_case_dropzone" || type == "asc_clearance_dropzone") {
                        if(ext == 'jpg') {
                            html = html + '<img src="{{asset("/storage/")}}/'+file.path+'" alt=""><br/>';
                        }
                        else {
                            html = html + '<a href="{{asset("/storage/")}}/'+file.path+'" target="_blank"><embed type="application/pdf" src="{{asset("/storage/")}}/'+file.path+'" frameBorder="0" height="100%" width="100%"></embed></a><br/>';
                        }
                    }
                }
                else {
                    html = html + 'None';
                }
            })

            $(element_id).html(html);   
        })

    }

    // function categoryDetailsChecker() {
    //     if ("{{$entry->category()->name}}" == "CREATIVE EFFECTIVENESS") {
    //         $('.file-preview-entry > .review-line:nth-child(1)').show() // preview_concept_board_dropzone
    //         $('.file-preview-entry > .review-line:nth-child(2)').show() // preview_case_study_video_dropzone
    //         $('.file-preview-entry > .review-line:nth-child(3)').show() // preview_written_case_dropzone
    //         $('.file-preview-entry > .review-line:nth-child(4)').show() // preview_asc_clearance_dropzone
    //         $('.file-preview-entry > .review-line:nth-child(5)').show() // preview_media_certification_dropzone
    //         $('.file-preview-entry > .review-line:nth-child(6)').show() // preview_client_certification_dropzone
    //         $('.file-preview-entry > .review-line:nth-child(7)').show() // preview_film_dropzone
    //         $('.file-preview-entry > .review-line:nth-child(8)').show() // preview_posters_dropzone
    //         $('.file-preview-entry > .review-line:nth-child(9)').show() // preview_audio_dropzone
    //         $('.file-preview-entry > .review-line:nth-child(10)').hide() // preview_demo_film_dropzone
    //         $('.file-preview-entry > .review-line:nth-child(11)').hide() // english_translation_dropzone_pdf
    //         $('.file-preview-entry > .review-line:nth-child(12)').hide() // preview_photo_piece_dropzone
    //         $('.file-preview-entry > .review-line:nth-child(13)').hide() // preview_entry_dropzone

    //     } else if ("{{$entry->category()->name}}" == "CREATIVE TECHNOLOGY &amp; MEDIA") {  
    //         $('.file-preview-entry > .review-line:nth-child(1)').show() // preview_concept_board_dropzone
    //         $('.file-preview-entry > .review-line:nth-child(2)').show() // preview_case_study_video_dropzone
    //         $('.file-preview-entry > .review-line:nth-child(3)').hide() // preview_written_case_dropzone
    //         $('.file-preview-entry > .review-line:nth-child(4)').show() // preview_asc_clearance_dropzone
    //         $('.file-preview-entry > .review-line:nth-child(5)').show() // preview_media_certification_dropzone
    //         $('.file-preview-entry > .review-line:nth-child(6)').show() // preview_client_certification_dropzone
    //         $('.file-preview-entry > .review-line:nth-child(7)').show() // preview_film_dropzone
    //         $('.file-preview-entry > .review-line:nth-child(8)').show() // preview_posters_dropzone
    //         $('.file-preview-entry > .review-line:nth-child(9)').show() // preview_audio_dropzone
    //         $('.file-preview-entry > .review-line:nth-child(10)').hide() // preview_demo_film_dropzone
    //         $('.file-preview-entry > .review-line:nth-child(11)').hide() // english_translation_dropzone_pdf
    //         $('.file-preview-entry > .review-line:nth-child(12)').show() // preview_photo_piece_dropzone
    //         $('.file-preview-entry > .review-line:nth-child(13)').hide() // preview_entry_dropzone
    //     } else if ("{{$entry->category()->name}}" == "CREATIVE STORYTELLING") {
    //         $('.file-preview-entry > .review-line:nth-child(1)').show() // preview_concept_board_dropzone
    //         $('.file-preview-entry > .review-line:nth-child(2)').show() // preview_case_study_video_dropzone
    //         $('.file-preview-entry > .review-line:nth-child(3)').hide() // preview_written_case_dropzone
    //         $('.file-preview-entry > .review-line:nth-child(4)').show() // preview_asc_clearance_dropzone
    //         $('.file-preview-entry > .review-line:nth-child(5)').show() // preview_media_certification_dropzone
    //         $('.file-preview-entry > .review-line:nth-child(6)').show() // preview_client_certification_dropzone
    //         $('.file-preview-entry > .review-line:nth-child(7)').show() // preview_film_dropzone
    //         $('.file-preview-entry > .review-line:nth-child(8)').show() // preview_posters_dropzone
    //         $('.file-preview-entry > .review-line:nth-child(9)').show() // preview_audio_dropzone
    //         $('.file-preview-entry > .review-line:nth-child(10)').show() // preview_demo_film_dropzone
    //         $('.file-preview-entry > .review-line:nth-child(11)').hide() // english_translation_dropzone_pdf
    //         $('.file-preview-entry > .review-line:nth-child(12)').hide() // preview_photo_piece_dropzone
    //         $('.file-preview-entry > .review-line:nth-child(13)').hide() // preview_entry_dropzone
    //     } else if ("{{$entry->category()->name}}" == "CREATIVE PURPOSE") {
    //         $('.file-preview-entry > .review-line:nth-child(1)').show() // preview_concept_board_dropzone
    //         $('.file-preview-entry > .review-line:nth-child(2)').show() // preview_case_study_video_dropzone
    //         $('.file-preview-entry > .review-line:nth-child(3)').hide() // preview_written_case_dropzone
    //         $('.file-preview-entry > .review-line:nth-child(4)').show() // preview_asc_clearance_dropzone
    //         $('.file-preview-entry > .review-line:nth-child(5)').show() // preview_media_certification_dropzone
    //         $('.file-preview-entry > .review-line:nth-child(6)').show() // preview_client_certification_dropzone
    //         $('.file-preview-entry > .review-line:nth-child(7)').show() // preview_film_dropzone
    //         $('.file-preview-entry > .review-line:nth-child(8)').show() // preview_posters_dropzone
    //         $('.file-preview-entry > .review-line:nth-child(9)').show() // preview_audio_dropzone
    //         $('.file-preview-entry > .review-line:nth-child(10)').show() // preview_demo_film_dropzone
    //         $('.file-preview-entry > .review-line:nth-child(11)').hide() // english_translation_dropzone_pdf
    //         $('.file-preview-entry > .review-line:nth-child(12)').hide() // preview_photo_piece_dropzone
    //         $('.file-preview-entry > .review-line:nth-child(13)').hide() // preview_entry_dropzone
    //     } else if ("{{$entry->category()->name}}" == "CRAFT") {
    //         $('.file-preview-entry > .review-line:nth-child(1)').show() // preview_concept_board_dropzone
    //         $('.file-preview-entry > .review-line:nth-child(2)').show() // preview_case_study_video_dropzone
    //         $('.file-preview-entry > .review-line:nth-child(3)').hide() // preview_written_case_dropzone
    //         $('.file-preview-entry > .review-line:nth-child(4)').show() // preview_asc_clearance_dropzone
    //         $('.file-preview-entry > .review-line:nth-child(5)').show() // preview_media_certification_dropzone
    //         $('.file-preview-entry > .review-line:nth-child(6)').show() // preview_client_certification_dropzone
    //         $('.file-preview-entry > .review-line:nth-child(7)').show() // preview_film_dropzone
    //         $('.file-preview-entry > .review-line:nth-child(8)').show() // preview_posters_dropzone
    //         $('.file-preview-entry > .review-line:nth-child(9)').show() // preview_audio_dropzone
    //         $('.file-preview-entry > .review-line:nth-child(10)').show() // preview_demo_film_dropzone
    //         $('.file-preview-entry > .review-line:nth-child(11)').hide() // english_translation_dropzone_pdf
    //         $('.file-preview-entry > .review-line:nth-child(12)').hide() // preview_photo_piece_dropzone
    //         $('.file-preview-entry > .review-line:nth-child(13)').hide() // preview_entry_dropzone
    //     }
    // }
</script>
@endpush
@endsection