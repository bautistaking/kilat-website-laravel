@extends('base')  
@push('styles')
    <style>
        .spinner-grow,
        .loading-label {
            display: none;
        }

        .users {
            max-width: 100%;
        }

        .tabs-divider {
            margin-left: -50px;
        }

        .tab-content {
            max-width: 1100px !important; 
        }

        .announcement {
            margin-left: 60px;
        }

        #search {
            padding-left: 35px;
        }

        .search-icon {
            position: absolute;
            margin-left: 9px;
            margin-top: -34px;
            font-size: 20px;
            color: #d7a333;
        }

        table td {
            word-wrap: break-word;
            font-size: 14px;
            text-align: center;
        }

        .color-link {
            color: #1a2735;
        }

        .dropdown-content li {
            display: none;
        }

        .custom-thead {
            background: #3b4a56;
        }

        .custom-thead > tr > th {
            padding: 20px;
            color: #fff;
            text-align: left;
        }

        table tr > td {
            text-align: left;
        }

        .form-check span,
        .form .form-check .form-check-input {
            margin-top: 20px;
            margin-left: 5px;
        }

        .dataTables_filter label {
            display: none;
        }

        .btn-custom-status {
            width: 130px;
        }

        .page-title {
            font-size: 30px;
            font-weight: 600;
            color: #1a2735;
        }

        .btn-custom-status {
            border-radius: 100px;
            width: 250px;
        }

        .dropdown {
            margin-top: -6px;
            border-color: #d7a333;
            border-radius: 100px;
            padding: 5px;
        }

        .dropdown option {
            background: #fff !important;
        }

        .dropdown option:hover {
            color: #1e2125 !important;
            background-color: #e9ecef !important;
        }

        .custom-card {
            min-height: 0px !important;
            padding: 0px !important;
            padding-left: 20px !important;
        }
    </style>

    <link rel="stylesheet" href="https://cdn.datatables.net/1.11.2/css/dataTables.bootstrap5.min.css">
@endpush
@section('content')
    @include('admin.components.header')
    <div class="wrapper-default pt72 fullWidth">
        <div class="container-fluid minHeight py72">
            <div class="dashboard">
                <div class="flex-row d-flex maxWidth">
                    <h1 class="page-title mb-0 me-4">All Entries - Votes</h1>
                    <select id="dropdown-round1" class="dropdown">
                        <option value="ALL CATEGORIES">ALL CATEGORIES</option>
                        <option value="CREATIVE EFFECTIVENESS">CREATIVE EFFECTIVENESS</option>
                        <option value="CREATIVE TECHNOLOGY & MEDIA">CREATIVE TECHNOLOGY & MEDIA</option>
                        <option value="CREATIVE STORYTELLING">CREATIVE STORYTELLING</option>
                        <option value="CREATIVE PURPOSE">CREATIVE PURPOSE</option>
                        <option value="CRAFT">CRAFT</option>
                    </select>
                    <!-- <div class="dropdown mt-n2">
                        <button class='btn-custom-status mt-2 mb-1 btn dropdown-toggle btn-warning' type="button" id="paymentStatusDropdown" data-bs-toggle="dropdown">
                        ALL CATEGORIES
                        </button>
                        <ul class="dropdown-menu" aria-labelledby="paymentStatusDropdown">
                            <li class="dropdown-item">ALL CATEGORIES</li>
                            <li class="dropdown-item">CREATIVE EFFECTIVENESS</li>
                            <li class="dropdown-item">CREATIVE TECHNOLOGY &amp; MEDIA</li>
                            <li class="dropdown-item">CREATIVE STORYTELLING</li>
                            <li class="dropdown-item">CREATIVE PURPOSE</li>
                            <li class="dropdown-item">CRAFT</li>
                        </ul> -->
                    </div>
                </div>
                <div class="tabs">
                    <div class="tabs-divider">
                        <ul class="nav nav-pills justify-content-start maxWidth" id="myTab" role="tablist">
                            <li class="nav-item" role="presentation">
                                <button class="nav-link active" id="pills-round-1-tab" data-bs-toggle="pill" data-bs-target="#pills-round-1" type="button" role="tab" aria-controls="pills-round-1" aria-selected="true">Round 1</button>
                            </li>
                            <li class="nav-item" role="presentation">
                                <button class="nav-link" id="pills-round-2-tab" data-bs-toggle="pill" data-bs-target="#pills-round-2" type="button" role="tab" aria-controls="pills-round-2" aria-selected="false">Round 2</button>
                            </li>
                            <li class="nav-item" role="presentation">
                                <button class="nav-link" id="pills-round-3-tab" data-bs-toggle="pill" data-bs-target="#pills-round-3" type="button" role="tab" aria-controls="pills-round-3" aria-selected="false">Round 3</button>
                            </li>
                        </ul>
                    </div>
                    <div class="tab-content" id="myTabContent">
                        <div class="tab-pane fade show active" id="pills-round-1" role="tabpanel" aria-labelledby="pills-round-1-tab">
                            <div class="announcement">
                                <p>All Categories</p>
                            </div>
                            <div class="dashboard-cards">
                                @if (count($entries_round1) > 0)
                                    <div>
                                        <div class="entries flex-column d-flex">
                                            <div class="d-flex align-items-center justify-content-between">
                                                <h1 class="page-title mb-0">
                                                Shortlisted
                                                </h1>
                                                <button class="btn btn-outline-primary primary small me-3 text-uppercase link-color btn_export1" style="min-width:156px; height: 48px;margin-left:350px">Export Results in CSV</button>
                                              <!--   <button class="btn btn-primary primary small me-3 text-uppercase" style="min-width:156px; height: 48px;">Open Judging</button> -->
                                            </div>

                                            <table class="table table-striped centered dt users-table w-100">
                                                <thead class="custom-thead">
                                                  <tr>
                                                      <th>ID</th>
                                                      <th>Entry Name</th>
                                                      <th>Category</th>
                                                      <th>Subcategory</th>
                                                      <th>Status</th>
                                                      <th>Reason</th>
                                                      <th>Vote</th>
                                                      <th>Action</th>
                                                  </tr>
                                                </thead>
                                                <tbody>
                                                @foreach ($entries_round1 as $entry)
                                                  <tr>
                                                        <td>{{$entry->uid}}</td>
                                                        <td>{{$entry->entry_name}}</td>
                                                        <td>{{$entry->cat_name}}</td>
                                                        <td>{{strtoupper($entry->sub_name)}}</td>
                                                        <td>
                                                            @if($entry->vote_status == "in")
                                                            <span>In</span>
                                                            @elseif($entry->vote_status == "out")
                                                            <span>Out</span>
                                                            @elseif($entry->vote_status == "flag")
                                                            <span>Flag</span>
                                                            @elseif($entry->vote_status == "abstain")
                                                            <span>Abstain</span>
                                                            @endif
                                                        </td>
                                                        <td>
                                                            @if($entry->reason != null)
                                                            <span>{{ucfirst($entry->reason)}}</span>
                                                            @else
                                                            <span>-</span>
                                                            @endif
                                                        </td>
                                                        <td class="text-center">{{$entry->vote_count}}</td>
                                                        <td>
                                                            <a class="link-color" href="{{url('admin/view/score/')}}/{{$entry->uid}}">
                                                                View entry
                                                            </a>
                                                        </td>
                                                  </tr>
                                                  @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                        <div class="divider" style="margin: 80px auto; max-width: 980px;"></div>
                                    </div>
                                @else
                                    <div class="wrapper-default">
                                        <div class="container-fluid py72">
                                            <div class="entries flex-column d-flex mb-5">
                                                <h1 class="page-title mb-0">
                                                Your Entries
                                                </h1>
                                            </div>
                                            <div class="state dark d-flex flex-column justify-content-center align-items-center">
                                                <img class="mb-4" src="/images/no-entries.png" alt="">
                                                <h1 class="title mb-2 text-center fw-bold">
                                                No entries yet
                                                </h1>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="divider" style="margin: 0px auto; max-width: 980px;"></div>
                                @endif
                            </div>
                        </div>

                        <div class="tab-pane fade" id="pills-round-2" role="tabpanel" aria-labelledby="pills-round-2-tab">
                            <div class="announcement">
                                <p>All Categories</p>
                            </div>
                            <div class="dashboard-cards">
                                @if (count($entries_round2) > 0)
                                    <div>
                                        <div class="entries flex-column d-flex">
                                            <div class="d-flex align-items-center justify-content-between">
                                                <h1 class="page-title mb-0">
                                                Shortlisted
                                                </h1>
                                                <button class="btn btn-outline-primary primary small me-3 text-uppercase link-color btn_export2" style="min-width:156px; height: 48px;margin-left:350px">Export Results in CSV</button>
                                              <!--   <button class="btn btn-primary primary small me-3 text-uppercase" style="min-width:156px; height: 48px;">Open Judging</button> -->
                                            </div>

                                            <table class="table table-striped centered dt users-table w-100">
                                                <thead class="custom-thead">
                                                  <tr>
                                                      <th>ID</th>
                                                      <th>Entry Name</th>
                                                      <th>Category</th>
                                                      <th>Subcategory</th>
                                                      <th>Status</th>
                                                      <th>Reason</th>
                                                      <th>Vote</th>
                                                      <th>Action</th>
                                                  </tr>
                                                </thead>
                                                <tbody>
                                                @foreach ($entries_round2 as $entry)
                                                  <tr>
                                                        <td>{{$entry->uid}}</td>
                                                        <td>{{$entry->entry_name}}</td>
                                                        <td>{{$entry->cat_name}}</td>
                                                        <td>{{strtoupper($entry->sub_name)}}</td>
                                                        <td>
                                                            @if($entry->vote_status == "shortlist")
                                                            <span>Shortlist</span>
                                                            @elseif($entry->vote_status == "outofshow")
                                                            <span>Out of show</span>
                                                            @elseif($entry->vote_status == "flag")
                                                            <span>Flag</span>
                                                            @elseif($entry->vote_status == "abstain")
                                                            <span>Abstain</span>
                                                            @endif
                                                        </td>
                                                        <td>
                                                            @if($entry->reason != null)
                                                            <span>{{ucfirst($entry->reason)}}</span>
                                                            @else
                                                            <span>-</span>
                                                            @endif
                                                        </td>
                                                        <td class="text-center">{{$entry->vote_count}}</td>
                                                        <td>
                                                            <a class="link-color" href="{{url('admin/view/score/')}}/{{$entry->uid}}">
                                                                View entry
                                                            </a>
                                                        </td>
                                                  </tr>
                                                  @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                        <div class="divider" style="margin: 80px auto; max-width: 980px;"></div>
                                    </div>
                                @else
                                    <div class="wrapper-default">
                                        <div class="container-fluid py72">
                                            <div class="entries flex-column d-flex mb-5">
                                                <h1 class="page-title mb-0">
                                                Your Entries
                                                </h1>
                                            </div>
                                            <div class="state dark d-flex flex-column justify-content-center align-items-center">
                                                <img class="mb-4" src="/images/no-entries.png" alt="">
                                                <h1 class="title mb-2 text-center fw-bold">
                                                No entries yet
                                                </h1>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="divider" style="margin: 0px auto; max-width: 980px;"></div>
                                @endif
                            </div>
                        </div>
                        <div class="tab-pane fade" id="pills-round-3" role="tabpanel" aria-labelledby="pills-round-3-tab">
                            <div class="announcement">
                                <p>To be announced</p>
                            </div>
                            <div class="dashboard-cards">
                                
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="voting-modal modal fade" id="downloadVotesummaryRound1Modal" tabindex="-1" aria-labelledby="flag" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header d-flex justify-content-center">
                    <h3 style="margin-top:-20px">Export Results - Round 1</h3>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body p-0 pt-4">
                    <div class="d-flex justify-content-center">
                        <a href="{{url('/admin/download-score-summary-round1')}}">
                            <button type="button" class="btn btn-outline-primary primary small me-3 text-uppercase link-color" style="min-width:156px; height: 48px;">Score Summary</button>
                        </a>
                        <a href="{{url('/admin/download-score-all-round1')}}">
                            <button type="button" class="btn btn-primary primary small me-3 text-uppercase" style="min-width:156px; height: 48px;">Score All</button>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="voting-modal modal fade" id="downloadVotesummaryRound2Modal" tabindex="-1" aria-labelledby="flag" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header d-flex justify-content-center">
                    <h3 style="margin-top:-20px">Export Results - Round 2</h3>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body p-0 pt-4">
                    <div class="d-flex justify-content-center">
                        <a href="{{url('/admin/download-score-summary-round2')}}">
                            <button type="button" class="btn btn-outline-primary primary small me-3 text-uppercase link-color" style="min-width:156px; height: 48px;">Score Summary</button>
                        </a>
                        <a href="{{url('/admin/download-score-all-round2')}}">
                            <button type="button" class="btn btn-primary primary small me-3 text-uppercase" style="min-width:156px; height: 48px;">Score All</button>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @include('admin.components.footer')
@push('custom-js')
<script type="text/javascript" src="https://cdn.datatables.net/v/bs5/jq-3.6.0/dt-1.11.3/datatables.min.js"></script>
<script>
    $(document).ready(activateQuery())

    function activateQuery() {
        var dtable = $('.dt').DataTable({
            // "aaSorting": [],
            "autoWidth": false,
            "bFilter": true,
            "bInfo": false,
            "lengthChange": false,
            "paging": true,
            "pagingType": "simple_numbers",
            "sorting": true,
            "order": [[ 3, "desc" ]],
            // "searching": true
        });


        $("#dropdown-round1").change(function (e) {
            if ($(this).val() == "ALL CATEGORIES") {
                location.reload()
            } else {
                dtable.search($(this).val()).draw();
            }
        });

    }

    $(document).on('click', '.btn_export1', function() {
        $('#downloadVotesummaryRound1Modal').modal('show')
    })

    $(document).on('click', '.btn_export2', function() {
        $('#downloadVotesummaryRound2Modal').modal('show')
    })
</script>
@endpush
@endsection