@extends('base')  
@push('styles')
    <link rel="stylesheet" href="https://cdn.datatables.net/v/bs5/jq-3.6.0/dt-1.11.3/datatables.min.css">
    <style>
        .spinner-grow,
        .loading-label {
            display: none;
        }

        .jurors {
            max-width: 100%;
        }

        #search {
            padding-left: 35px;
        }

        .search-icon {
            position: absolute;
            margin-left: 9px;
            margin-top: -34px;
            font-size: 20px;
            color: #d7a333;
        }

        table td {
            word-wrap: break-word;
            font-size: 14px;
            text-align: center;
        }

        .color-link {
            color: #1a2735;
        }

        .dropdown-content li {
            display: none;
        }

        .custom-thead {
            background: #3b4a56;
        }

        .custom-thead > tr > th {
            padding: 20px;
            color: #fff;
            text-align: center;
        }

        .form-check span,
        .form .form-check .form-check-input {
            margin-top: 20px;
            margin-left: 5px;
        }

        .dataTables_filter label {
            display: none;
        }

        .btn-edit {
            background: #223448;
            color: #fff;
        }

        .delete-dropdown {
            position: absolute;
            margin-left: 311px;
            margin-top: -46px;
            font-size: 30px;
            color: #d7a333;
        }

        .delete-dropdown:hover {
            color: #223448;
        }
    </style>
@endpush
@section('content')
    @include('admin.components.header')
    <div class="wrapper-default">
        <div class="container-fluid minHeight py72">
                <div style="margin-top: 70px">
                    <div class="jurors flex-column d-flex">
                        <div class="d-flex align-items-center justify-content-between">
                            <h1 class="page-title mb-0" style="color: #1a2735;font-size:30px">All Jurors</h1>
                            <button type="button" class="btn btn-primary primary small me-3 text-uppercase" style="min-width:156px; height: 48px;" data-bs-toggle="modal" data-bs-target="#addJurorModal">Add a Juror</button>
                        </div>
                        <div class="d-flex align-items-center">
                           <!--  <div class="mt-5 mb-4 me-3">
                                <label for="filter" class="form-label fw-500 mb-2">Filter</label>
                            </div>
                            <div class="mt-5 mb-4 me-5">
                                <select class="form-select form-select-lg custom-select lg" style="width: 400px" id="filter" aria-label=".form-select-lg">
                                    @foreach($categories as $c)
                                        <option value="{{$c->name}}">{{$c->name}}</option>
                                    @endforeach
                                </select>
                            </div> -->
                             <button class="btn btn-primary primary small me-3 text-uppercase" style="min-width:156px; height: 48px;" id="send_email_all">Send to All</button>
                            <div class="w-25"></div>
                            <div class="mt-5 mb-4" style="margin-left: 350px">
                                <div style="width: 100%">
                                    <input type="text" class="form-control form-control-lg lg fs-18" id="search" placeholder="Search">
                                    <i class="fas fa-search search-icon"></i>
                                </div>
                            </div>
                        </div>

                        @if (count($multi_categories) > 0)
                        <table class="table-striped centered dt entries-table w-100">
                            <thead class="custom-thead">
                              <tr>
                                  <th>Juror</th>
                                  <th>Email</th>
                                  <th>Agency</th>
                                  <th>Date Added</th>
                                  <th>Category</th>
                                  <!-- <th>Sub Category</th> -->
                                  <th>Action</th>
                              </tr>
                            </thead>

                            <tbody>
                                @foreach ($judges as $j)
                                <tr>
                                    <td>{{$j->name}}</td>
                                    <td>{{$j->email}}</td>
                                    <td>{{$j->name}}</td>
                                    <td>{{$j->created_at}}</td>
                                    <td>
                                        @foreach ($multi_categories as $mc)
                                            <p class="mb-0">
                                                {{$mc->cat_name}} - {{$mc->sub_name}}{{ $loop->last ? '' : ','}}
                                            </p>
                                        @endforeach
                                       <!--  <div class="dropdown" style="margin-top:10px">
                                            <button class='btn btn-warning' id="categoryDropdown" data-bs-toggle="dropdown">
                                                {{$categories[0]->name}}
                                            </button>
                                            <ul class="dropdown-menu text-uppercase" aria-labelledby="categoryDropdown">
                                                @foreach($categories as $c)
                                                    <li>
                                                        <a class="dropdown-item" href="#">{{$c->name}}</a>
                                                    </li>
                                                @endforeach
                                            </ul>
                                        </div> -->
                                    </td>
                        <!--             <td>
                                        <span class="text-uppercase">
                                           
                                        </span> -->
                                        <!-- <div class="dropdown" style="margin-top:10px">
                                            <button class='btn btn-warning' id="subCategoryDropdown" data-bs-toggle="dropdown">
                                                {{$sub_categories[0]->name}}
                                            </button>
                                            <ul class="dropdown-menu text-uppercase" aria-labelledby="subCategoryDropdown">
                                                @foreach($sub_categories as $sub)
                                                    <li>
                                                        <a class="dropdown-item" href="#">{{$sub->name}}</a>
                                                    </li>
                                                @endforeach
                                            </ul>
                                        </div> -->
                                    <!-- </td> -->
                                    <td>
                                        <div class="btn-group" role="group" style="margin-top:10px">
                                            <button class="btn btn-primary btn_email" id="send_email" data-email="{{$j->email}}">
                                                <i class="far fa-envelope"></i>
                                            </button>
                                            <button class="btn btn-secondary btn_edit" data-id="{{$j->id}}" data-bs-toggle="modal" data-bs-target="#editJurorModal">
                                                <i class="far fa-edit"></i>
                                            </button>
                                            <button class="btn btn-danger btn_delete" data-id="{{$j->id}}" data-bs-toggle="modal" data-bs-target="#deleteJurorModal">
                                                <i class="far fa-trash-alt"></i>
                                            </button>
                                        </div>
                                    </td>
                                </tr>
                                @endforeach
                              
                            </tbody>
                          </table>
                          @else
                            <table class="table-striped centered entries-table w-100">
                            <thead class="custom-thead">
                              <tr>
                                  <th>Juror</th>
                                  <th>Email</th>
                                  <th>Agency</th>
                                  <th>Date Added</th>
                                  <th>Category</th>
                                  <th>Sub Category</th>
                                  <th>Action</th>
                              </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td class="mt-5" colspan="8">No records found.</td>
                                </tr>
                            </tbody>
                          </table>
                          @endif
                    </div>
                </div>
        </div>
    </div>

    <!-- modal -->
    <div class="voting-modal modal fade" id="addJurorModal" tabindex="-1" aria-labelledby="flag" aria-hidden="true">
      <div class="modal-dialog modal-lg">
        <div class="modal-content">
          <div class="modal-header d-flex justify-content-center">
            <h3 style="margin-top:-20px">Add A Juror</h3>
            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
          </div>
          <div class="modal-body p-0 pt-4">
                <div class="mb-4">
                    <label for="juror_name" class="form-label fw-500 mb-2">Juror's name</label>
                    <input type="text" class="form-control form-control-lg lg fs-18" id="juror_name" placeholder="Enter name">
                    <div id="invalid-name" class="invalid-feedback"></div>
                </div>
                <div class="mb-4">
                    <label for="juror_agency" class="form-label fw-500 mb-2">Juror's agency/company</label>
                    <input type="text" class="form-control form-control-lg lg fs-18" id="juror_agency" placeholder="Enter agency/company">
                    <div id="invalid-agency" class="invalid-feedback"></div>
                </div>
                <div class="mb-4">
                    <label for="juror_email" class="form-label fw-500 mb-2">Juror's email address</label>
                    <input type="text" class="form-control form-control-lg lg fs-18" id="juror_email" placeholder="Enter email address">
                    <div id="invalid-email" class="invalid-feedback"></div>
                </div>
                <div class="mb-4">
                    <div class="row">
                        <div class="col col-md-6 mb-4">
                            <label for="category_name" class="form-label fw-500 mb-2">Choose a category</label>
                            <select class="form-select form-select-lg custom-select lg" id="category_name">
                                <option selected value="NONE" data-catid="0" disabled>SELECT</option>
                                @foreach($categories as $category)
                                <option value="{{$category->name}}" data-uid="{{$category->uid}}" data-catid="{{$category->id}}">{{$category->name}}</option>
                                @endforeach
                            </select>
                            <div id="invalid-category" class="invalid-feedback"></div>
                        </div>
                        <div class="col col-md-6 mb-4">
                            <label for="subcategory" class="form-label fw-500 mb-2">Choose a subcategory</label>
                            <select class="form-select form-select-lg lg text-uppercase" id="subcategory">
                                <option selected value="NONE" disabled="">SELECT</option>
                                @foreach ($sub_categories as $sub)
                                <option class="d-none" value="{{$sub->name}}" data-uid="{{$sub->uid}}" data-pid="{{$sub->category_id}}" data-subid="{{$sub->id}}">{{$sub->name}}</option>
                                @endforeach
                            </select>
                            <div id="invalid-subcategory" class="invalid-feedback"></div>
                        </div>
                    </div>
                    <div class="dropdown-container"></div>
                </div>
                <div class="d-flex justify-content-center">
                    <button id="btn_add" class="btn btn-primary primary small me-3 text-uppercase" style="min-width:156px; height: 48px;">
                        <span class="spinner-grow spinner-grow-sm" role="status"></span>
                        <span class="spinner-grow spinner-grow-sm" role="status"></span>
                        <span class="spinner-grow spinner-grow-sm" role="status"></span>
                        <span class="add-label">Add a Juror</span>
                    </button>
                </div>
          </div>
        </div>
      </div>
    </div>

    <div class="voting-modal modal fade" id="editJurorModal" tabindex="-1" aria-labelledby="flag" aria-hidden="true">
      <div class="modal-dialog modal-lg">
        <div class="modal-content">
          <div class="modal-header d-flex justify-content-center">
            <h3 style="margin-top:-20px">Edit A Juror</h3>
            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
          </div>
          <div class="modal-body p-0 pt-4">
                <div class="mb-4">
                    <label for="edit_juror_name" class="form-label fw-500 mb-2">Juror's name</label>
                    <input type="text" class="form-control form-control-lg lg fs-18" id="edit_juror_name" placeholder="Enter name">
                    <div id="invalid-edit-name" class="invalid-feedback"></div>
                </div>
                <div class="mb-4">
                    <label for="edit_juror_agency" class="form-label fw-500 mb-2">Juror's agency/company</label>
                    <input type="text" class="form-control form-control-lg lg fs-18" id="edit_juror_agency" placeholder="Enter agency/company">
                    <div id="invalid-edit-agency" class="invalid-feedback"></div>
                </div>
                <div class="mb-4">
                    <label for="edit_juror_email" class="form-label fw-500 mb-2">Juror's email address</label>
                    <input type="text" class="form-control form-control-lg lg fs-18" id="edit_juror_email" placeholder="Enter email address">
                    <div id="invalid-edit-email" class="invalid-feedback"></div>
                </div>
                <div class="mb-4">
                    <div class="row">
                        <div class="col col-md-6 mb-4">
                            <label for="edit_category_name" class="form-label fw-500 mb-2">Choose a category</label>
                            <select class="form-select form-select-lg custom-select lg" id="edit_category_name">
                                <option selected value="NONE" data-catid="0" disabled="">SELECT</option>
                                @foreach($categories as $category)
                                <option value="{{$category->name}}" data-uid="{{$category->uid}}" data-catid="{{$category->id}}">{{$category->name}}</option>
                                @endforeach
                            </select>
                            <div id="invalid-edit-category" class="invalid-feedback"></div>
                        </div>
                        <div class="col col-md-6 mb-4">
                            <label for="edit_subcategory" class="form-label fw-500 mb-2">Choose a subcategory</label>
                            <select class="form-select form-select-lg lg text-uppercase" id="edit_subcategory">
                                <option selected value="NONE" disabled>SELECT</option>
                                @foreach ($sub_categories as $sub)
                                <option class="d-none"  value="{{$sub->name}}" data-uid="{{$sub->uid}}" data-pid="{{$sub->category_id}}" data-subid="{{$sub->id}}">{{$sub->name}}</option>
                                @endforeach
                            </select>
                            <div id="invalid-edit-subcategory" class="invalid-feedback"></div>
                        </div>
                    </div>
                    <div class="edit-dropdown-container"></div>
                </div>
                <div class="d-flex justify-content-center">
                    <button id="btn_update" class="btn btn-primary primary small me-3 text-uppercase" style="min-width:156px; height: 48px;">
                        <span class="spinner-grow spinner-grow-sm" role="status"></span>
                        <span class="spinner-grow spinner-grow-sm" role="status"></span>
                        <span class="spinner-grow spinner-grow-sm" role="status"></span>
                        <span class="edit-label">Update a Juror</span>
                    </button>
                </div>
          </div>
        </div>
      </div>
    </div>

    <div class="voting-modal modal fade" id="deleteJurorModal" tabindex="-1" aria-labelledby="flag" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header d-flex">
            <h4 style="margin-top:-20px">Are you sure you want to delete?</h4>
            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
          </div>
          <div class="modal-body p-0 pt-4">
               <div class="d-flex justify-content-center">
                    <button class="btn btn-outline-primary brown primary small me-3 text-uppercase brown" style="min-width:156px; height: 48px;"  data-bs-dismiss="modal" aria-label="Close">
                        <span class="delete-label">Cancel</span>
                    </button>
                    <button id="btn_remove" class="btn btn-primary primary small me-3 text-uppercase" style="min-width:156px; height: 48px;">
                        <span class="spinner-grow spinner-grow-sm" role="status"></span>
                        <span class="spinner-grow spinner-grow-sm" role="status"></span>
                        <span class="spinner-grow spinner-grow-sm" role="status"></span>
                        <span class="delete-label">Confirm</span>
                    </button>
                </div>
          </div>
        </div>
      </div>
    </div>
    @include('admin.components.footer')
@push('custom-js')
<script type="text/javascript" src="https://cdn.datatables.net/v/bs5/jq-3.6.0/dt-1.11.3/datatables.min.js"></script>
<script>
    $(document).ready(activateQuery())

    function activateQuery() {
        var dtable = $('.dt').DataTable({
            "aaSorting": [],
            "autoWidth": false,
            "bFilter": true,
            "bInfo": false,
            "lengthChange": false,
            "paging": true,
            "pagingType": "simple_numbers",
            "sorting": true,
            "order": [[ 3, "desc" ]]
        });

        $('#search').keyup(function(){
            dtable.search($(this).val()).draw() ;
        })
    }

    $(document).on('click', '#send_email_all', function() {
        let params = {
            '_token' : '{{ csrf_token() }}',
        }

        fetch("{{url('admin/juror/send-email-all')}}", {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(params)
        })
        .then(response => response.json())
        .then((e) => {

            if (e.status_code == 200) {
                setTimeout(function() {
                    // location.href = "{{url('admin/jurors')}}"
                }, 100)
            }
        })

    });

    $(document).on('click', '#send_email', function() {
        let params = {
            '_token' : '{{ csrf_token() }}',
            'email'  : $(this).attr('data-email')
        }

        fetch("{{url('admin/juror/send-email')}}", {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(params)
        })
        .then(response => response.json())
        .then((e) => {

            if (e.status_code == 200) {
                setTimeout(function() {
                    location.href = "{{url('admin/jurors')}}"
                }, 100)
            }
        })
    });

    $(document).on('change', '#category_name', function() {
        categoryDetailsChecker()
    })

    $(document).on('change', '#subcategory', function() {
        categoryDetailsChecker()

        setTimeout(function() {
            var category = document.querySelector('#category_name');
            category.selectedIndex = 0; 
            $('#category_name option[value="NONE"]').attr('selected', 'selected')

            var subcategory = document.querySelector('#subcategory');
            subcategory.selectedIndex = 0; 
            $('#subcategory option[value="NONE"]').attr('selected', 'selected')
        },300)
    })

    $(document).on('change', '#edit_category_name', function() {
        categoryDetailsEditChecker()
    })

    $(document).on('change', '#edit_subcategory', function() {
        categoryDetailsEditChecker()


        setTimeout(function() {
            var edit_category = document.querySelector('#edit_category_name');
            edit_category.selectedIndex = 0; 
            $('#edit_category_name option[value="NONE"]').attr('selected', 'selected')

            var edit_subcategory = document.querySelector('#edit_subcategory');
            edit_subcategory.selectedIndex = 0; 
            $('#edit_subcategory option[value="NONE"]').attr('selected', 'selected')
        },300)
    })

    function categoryDetailsChecker() {
        if ($('#category_name option:selected').attr('data-uid') == "A") {
            $('#subcategory option[data-pid="1"]').removeClass('d-none')
            $('#subcategory option[data-pid="2"]').addClass('d-none')
            $('#subcategory option[data-pid="3"]').addClass('d-none')
            $('#subcategory option[data-pid="4"]').addClass('d-none')
            $('#subcategory option[data-pid="5"]').addClass('d-none')
            $('#subcategory option[data-pid="6"]').addClass('d-none')
            $('#subcategory option[data-pid="7"]').addClass('d-none')
        } else if ($('#category_name option:selected').attr('data-uid') == "B") {   
            $('#subcategory option[data-pid="1"]').addClass('d-none')
            $('#subcategory option[data-pid="2"]').removeClass('d-none')
            $('#subcategory option[data-pid="3"]').addClass('d-none')
            $('#subcategory option[data-pid="4"]').addClass('d-none')
            $('#subcategory option[data-pid="5"]').addClass('d-none')
            $('#subcategory option[data-pid="6"]').addClass('d-none')
            $('#subcategory option[data-pid="7"]').addClass('d-none')
        } else if ($('#category_name option:selected').attr('data-uid') == "C") {
            $('#subcategory option[data-pid="1"]').addClass('d-none')
            $('#subcategory option[data-pid="2"]').addClass('d-none')
            $('#subcategory option[data-pid="3"]').removeClass('d-none')
            $('#subcategory option[data-pid="4"]').addClass('d-none')
            $('#subcategory option[data-pid="5"]').addClass('d-none')
            $('#subcategory option[data-pid="6"]').addClass('d-none')
            $('#subcategory option[data-pid="7"]').addClass('d-none')
        } else if ($('#category_name option:selected').attr('data-uid') == "D") {
            $('#subcategory option[data-pid="1"]').addClass('d-none')
            $('#subcategory option[data-pid="2"]').addClass('d-none')
            $('#subcategory option[data-pid="3"]').addClass('d-none')
            $('#subcategory option[data-pid="4"]').removeClass('d-none')
            $('#subcategory option[data-pid="5"]').addClass('d-none')
            $('#subcategory option[data-pid="6"]').addClass('d-none')
            $('#subcategory option[data-pid="7"]').addClass('d-none')
        } else if ($('#category_name option:selected').attr('data-uid') == "E") {
            $('#subcategory option[data-pid="1"]').addClass('d-none')
            $('#subcategory option[data-pid="2"]').addClass('d-none')
            $('#subcategory option[data-pid="3"]').addClass('d-none')
            $('#subcategory option[data-pid="4"]').addClass('d-none')
            $('#subcategory option[data-pid="5"]').removeClass('d-none')
            $('#subcategory option[data-pid="6"]').addClass('d-none')
            $('#subcategory option[data-pid="7"]').addClass('d-none')
        }else if ($('#category_name option:selected').attr('data-uid') == "F") {
            $('#subcategory option[data-pid="1"]').addClass('d-none')
            $('#subcategory option[data-pid="2"]').addClass('d-none')
            $('#subcategory option[data-pid="3"]').addClass('d-none')
            $('#subcategory option[data-pid="4"]').addClass('d-none')
            $('#subcategory option[data-pid="5"]').addClass('d-none')
            $('#subcategory option[data-pid="6"]').removeClass('d-none')
            $('#subcategory option[data-pid="7"]').addClass('d-none')
        }else if ($('#category_name option:selected').attr('data-uid') == "G") {
            $('#subcategory option[data-pid="1"]').addClass('d-none')
            $('#subcategory option[data-pid="2"]').addClass('d-none')
            $('#subcategory option[data-pid="3"]').addClass('d-none')
            $('#subcategory option[data-pid="4"]').addClass('d-none')
            $('#subcategory option[data-pid="5"]').addClass('d-none')
            $('#subcategory option[data-pid="6"]').addClass('d-none')
            $('#subcategory option[data-pid="7"]').removeClass('d-none')
        }
    }

    function categoryDetailsEditChecker() {
        if ($('#category_name option:selected').attr('data-uid') == "A") {
            $('#subcategory option[data-pid="1"]').removeClass('d-none')
            $('#subcategory option[data-pid="2"]').addClass('d-none')
            $('#subcategory option[data-pid="3"]').addClass('d-none')
            $('#subcategory option[data-pid="4"]').addClass('d-none')
            $('#subcategory option[data-pid="5"]').addClass('d-none')
            $('#subcategory option[data-pid="6"]').addClass('d-none')
            $('#subcategory option[data-pid="7"]').addClass('d-none')
        } else if ($('#category_name option:selected').attr('data-uid') == "B") {   
            $('#subcategory option[data-pid="1"]').addClass('d-none')
            $('#subcategory option[data-pid="2"]').removeClass('d-none')
            $('#subcategory option[data-pid="3"]').addClass('d-none')
            $('#subcategory option[data-pid="4"]').addClass('d-none')
            $('#subcategory option[data-pid="5"]').addClass('d-none')
            $('#subcategory option[data-pid="6"]').addClass('d-none')
            $('#subcategory option[data-pid="7"]').addClass('d-none')
        } else if ($('#category_name option:selected').attr('data-uid') == "C") {
            $('#subcategory option[data-pid="1"]').addClass('d-none')
            $('#subcategory option[data-pid="2"]').addClass('d-none')
            $('#subcategory option[data-pid="3"]').removeClass('d-none')
            $('#subcategory option[data-pid="4"]').addClass('d-none')
            $('#subcategory option[data-pid="5"]').addClass('d-none')
            $('#subcategory option[data-pid="6"]').addClass('d-none')
            $('#subcategory option[data-pid="7"]').addClass('d-none')
        } else if ($('#category_name option:selected').attr('data-uid') == "D") {
            $('#subcategory option[data-pid="1"]').addClass('d-none')
            $('#subcategory option[data-pid="2"]').addClass('d-none')
            $('#subcategory option[data-pid="3"]').addClass('d-none')
            $('#subcategory option[data-pid="4"]').removeClass('d-none')
            $('#subcategory option[data-pid="5"]').addClass('d-none')
            $('#subcategory option[data-pid="6"]').addClass('d-none')
            $('#subcategory option[data-pid="7"]').addClass('d-none')
        } else if ($('#category_name option:selected').attr('data-uid') == "E") {
            $('#subcategory option[data-pid="1"]').addClass('d-none')
            $('#subcategory option[data-pid="2"]').addClass('d-none')
            $('#subcategory option[data-pid="3"]').addClass('d-none')
            $('#subcategory option[data-pid="4"]').addClass('d-none')
            $('#subcategory option[data-pid="5"]').removeClass('d-none')
            $('#subcategory option[data-pid="6"]').addClass('d-none')
            $('#subcategory option[data-pid="7"]').addClass('d-none')
        }else if ($('#category_name option:selected').attr('data-uid') == "F") {
            $('#subcategory option[data-pid="1"]').addClass('d-none')
            $('#subcategory option[data-pid="2"]').addClass('d-none')
            $('#subcategory option[data-pid="3"]').addClass('d-none')
            $('#subcategory option[data-pid="4"]').addClass('d-none')
            $('#subcategory option[data-pid="5"]').addClass('d-none')
            $('#subcategory option[data-pid="6"]').removeClass('d-none')
            $('#subcategory option[data-pid="7"]').addClass('d-none')
        }else if ($('#category_name option:selected').attr('data-uid') == "G") {
            $('#subcategory option[data-pid="1"]').addClass('d-none')
            $('#subcategory option[data-pid="2"]').addClass('d-none')
            $('#subcategory option[data-pid="3"]').addClass('d-none')
            $('#subcategory option[data-pid="4"]').addClass('d-none')
            $('#subcategory option[data-pid="5"]').addClass('d-none')
            $('#subcategory option[data-pid="6"]').addClass('d-none')
            $('#subcategory option[data-pid="7"]').removeClass('d-none')
        }
    }

    var juror_id = 0;
    $(document).on('click', '.btn_edit', function() {
        juror_id = $(this).attr('data-id')

        $.get('{{url("admin/juror/")}}/' + juror_id, function (data) {
            $('#edit_juror_name').val(data.juror.name);
            $('#edit_juror_email').val(data.juror.email);
            $('#edit_juror_agency').val(data.user_details.juror_agency);

            let dropdown_name_id = 1
            let dropdown_id = 1
            var html = ""
            $.each(data.multi_categories, function(index, mc) {
                html += '<div class="juror-category row">'
                    html += '<div class="col-md-6" style="column-gap: 25px">'
                        html += '<div class="mb-2">'
                            html += '<input data-catid="'+mc['catid']+'" disabled type="text" class="text-uppercase form-control form-control-lg lg fs-18" id="category_id'+ dropdown_name_id++ +'" name="category_id[]" placeholder="Category" value="'+mc['cat_name']+'">'
                        html += '</div>'
                    html += '</div>'

                    html += '<div class="col-md-6" style="column-gap: 25px">'
                        html += '<div class="mb-2">'
                            html += '<input data-subid="'+mc['subid']+'" disabled type="text" class="text-uppercase form-control form-control-lg lg fs-18" id="subcategory_id'+ dropdown_id++ +'" name="subcategory_id[]" placeholder="Subcategory" value="'+mc['sub_name']+'">'
                            html += '<a href="javascript:void(0)" class="delete-dropdown"><i class="fas fa-times"></i></a>'
                        html += '</div>'
                    html += '</div>'
                html += '</div>'
            });

            $('.edit-dropdown-container').html(html)

            // $('#edit_category_name option[data-catid="'+data.user_categories.category_id+'"]').attr('selected', 'selected')
            // $('#edit_subcategory option[value="'+data.user_categories.subcategory_id+'"]').attr('selected', 'selected')
        }) 
    })

    $(document).on('click', '.btn_delete', function() {
        juror_id = $(this).attr('data-id')
    })

    $(document).on('click', '#btn_add', function() {
        // $.LoadingOverlay("show", {
        //     image: '{{url("/images/loading1.png")}}'
        // },100);

        $('.add-label').hide()
        $('.spinner-grow').addClass('d-inline-block')

        $(this).attr('disabled', true)

        var checker = 0
        if ($('#juror_name').val() == "") {
            checker++;
            $('#juror_name').addClass('is-invalid')
            $('#invalid-name').text('Juror\'s name is required.').show()
        } else {
            $('#juror_name').removeClass('is-invalid')
            $('#invalid-name').hide()
        }

        if ($('#juror_agency').val() == "") {
            checker++;
            $('#juror_agency').addClass('is-invalid')
            $('#invalid-agency').text('Juror\'s agency is required.').show()
        } else {
            $('#juror_agency').removeClass('is-invalid')
            $('#invalid-agency').hide()
        }

        if ($('#juror_email').val() == "") {
            checker++;
            $('#juror_email').addClass('is-invalid')
            $('#invalid-email').text('Email address is required.').show()
        } else {
            var email_filter = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
            if (!email_filter.test($('#juror_email').val())) {
                checker++;
                $('#juror_email').addClass('is-invalid')
                $('#invalid-email').text('Email address is invalid.').show()
            } else {
                $('#juror_email').removeClass('is-invalid')
                $('#invalid-email').hide()
            }
        }

        let categories    = []
        let category_id = []
        let subcategory_id = []

        $('input[name="category_id[]"').each(function() {
            category_id.push($(this).attr('data-catid'))
        })

        $('input[name="subcategory_id[]"').each(function() {
            subcategory_id.push($(this).attr('data-subid'))
        })

        for (let i = 0; i < category_id.length; i++) {
            categories.push({
                category_id: category_id[i],
                subcategory_id: subcategory_id[i]
            })
        }

        // if ($('#category_name option:selected').val() == "NONE") {
        //     checker++;
        //     $('#category_name').addClass('is-invalid')
        //     $('#invalid-category').text('Category is required.').show()
        // } else {
        //     $('#category_name').removeClass('is-invalid')
        //     $('#invalid-category').hide()
        // }

        // if ($('#subcategory option:selected').val() == "NONE") {
        //     checker++;
        //     $('#subcategory').addClass('is-invalid')
        //     $('#invalid-subcategory').text('Subcategory is required.').show()
        // } else {
        //     $('#subcategory').removeClass('is-invalid')
        //     $('#invalid-subcategory').hide()
        // }

        if (checker == 0) {

            let params = {
                '_token'         : '{{ csrf_token() }}',
                'juror_name'     : $('#juror_name').val(),
                'juror_agency'   : $('#juror_agency').val(),
                'email'          : $('#juror_email').val(),
                'categories'          : JSON.stringify(categories)
                // 'category_id'    : $('#category_name option:selected').attr('data-catid'),
                // 'subcategory_id' : $('#subcategory option:selected').attr('data-subid')
            }

            fetch("{{url('admin/juror/add')}}", {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify(params)
            })
            .then(response => response.json())
            .then((e) => {

                if (e.status_code == 200) {
                    // $.LoadingOverlay("hide");

                    setTimeout(function() {
                        location.href = "{{url('admin/jurors')}}"
                    }, 100)
                }
            })

        } else {
            // $.LoadingOverlay("hide");
            $('.add-label').show()
            $('.spinner-grow').removeClass('d-inline-block')
            $(this).attr('disabled', false)
        }

    })

    $(document).on('click', '#btn_update', function() {
        // $.LoadingOverlay("show", {
        //     image: '{{url("/images/loading1.png")}}'
        // },100);

        $('.edit-label').hide()
        $('.spinner-grow').addClass('d-inline-block')

        $(this).attr('disabled', true)

        var checker = 0
        if ($('#edit_juror_name').val() == "") {
            checker++;
            $('#edit_juror_name').addClass('is-invalid').show()
            $('#invalid-edit-name').text('Juror\'s name is required.').show()
        } else {
            $('#edit_juror_name').removeClass('is-invalid')
            $('#invalid-edit-name').hide()
        }

        if ($('#edit_juror_agency').val() == "") {
            checker++;
            $('#edit_juror_agency').addClass('is-invalid').show()
            $('#invalid-edit-agency').text('Juror\'s agency is required.').show()
        } else {
            $('#edit_juror_agency').removeClass('is-invalid')
            $('#invalid-edit-agency').hide()
        }

        if ($('#edit_juror_email').val() == "") {
            checker++;
            $('#edit_juror_email').addClass('is-invalid').show()
            $('#invalid-edit-email').text('Email address is required.').show()
        } else {
            var email_filter = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
            if (!email_filter.test($('#edit_juror_email').val())) {
                checker++;
                $('#edit_juror_email').addClass('is-invalid').show()
                $('#invalid-edit-email').text('Email address is invalid.').show()
            } else {
                $('#edit_juror_email').removeClass('is-invalid')
                $('#invalid-edit-email').hide()
            }
        }

        let categories    = []
        let category_id = []
        let subcategory_id = []

        $('input[name="category_id[]"').each(function() {
            category_id.push($(this).attr('data-catid'))
        })

        $('input[name="subcategory_id[]"').each(function() {
            subcategory_id.push($(this).attr('data-subid'))
        })

        for (let i = 0; i < category_id.length; i++) {
            categories.push({
                category_id: category_id[i],
                subcategory_id: subcategory_id[i]
            })
        }

        // if ($('#edit_category_name option:selected').val() == "NONE") {
        //     checker++;
        //     $('#edit_category_name').addClass('is-invalid').show()
        //     $('#invalid-edit-category').text('Category is required.').show()
        // } else {
        //     $('#edit_category_name').removeClass('is-invalid')
        //     $('#invalid-edit-category').hide()
        // }

        // if ($('#edit_subcategory option:selected').val() == "NONE") {
        //     checker++;
        //     $('#edit_subcategory').addClass('is-invalid').show()
        //     $('#invalid-edit-subcategory').text('Subcategory is required.').show()
        // } else {
        //     $('#edit_subcategory').removeClass('is-invalid')
        //     $('#invalid-edit-subcategory').hide()
        // }

        if (checker == 0) {

            let params = {
                '_token'       : '{{ csrf_token() }}',
                'id'           : juror_id,
                'juror_name'   : $('#edit_juror_name').val(),
                'juror_agency' : $('#edit_juror_agency').val(),
                'email'        : $('#edit_juror_email').val(),
                'categories'          : JSON.stringify(categories)
                // 'category_id'    : $('#edit_category_name option:selected').attr('data-catid'),
                // 'subcategory_id' : $('#edit_subcategory option:selected').attr('data-subid')
            }

            fetch("{{url('admin/juror/edit')}}", {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify(params)
            })
            .then(response => response.json())
            .then((e) => {

                if (e.status_code == 400 || e.status_code == 409) {
                    // $.LoadingOverlay("hide");

                    $('.edit-label').show()
                    $('.spinner-grow').removeClass('d-inline-block')

                    let html = "";

                    $.each(e.message, function(a,b) {
                        if (e.header == "Duplicate") {
                            html += "Email address already registered."
                        }
                    })

                    $('#invalid-edit-email').html(html).show()
                    $('#edit_juror_email').addClass('is-invalid')

                    $(this).attr('disabled', false)
                }

                if (e.status_code == 200) {
                    // $.LoadingOverlay("hide");

                    setTimeout(function() {
                        location.href = "{{url('admin/jurors')}}"
                    }, 100)
                }
            })

        } else {
            // $.LoadingOverlay("hide");
            $('.edit-label').show()
            $('.spinner-grow').removeClass('d-inline-block')
            $(this).attr('disabled', false)
        }

    })

    $(document).on('click', '#btn_remove', function() {
        // $.LoadingOverlay("show", {
        //     image: '{{url("/images/loading1.png")}}'
        // },100);

        $('.delete-label').hide()
        $('.spinner-grow').addClass('d-inline-block')

        $(this).attr('disabled', true)

        let params = {
            '_token'     : '{{ csrf_token() }}',
            'id'         : juror_id,
        }

        fetch("{{url('/admin/juror/delete')}}", {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(params)
        })
        .then(response => response.json())
        .then((e) => {

            if (e.status_code == 409) {
                // $.LoadingOverlay("hide");

                $('.delete-label').show()
                $('.spinner-grow').removeClass('d-inline-block')

                $(this).attr('disabled', false)
            }

            if (e.status_code == 200) {
                // $.LoadingOverlay("hide");
                $('#deleteJurorModal').toggle('modal')
                setTimeout(function() {
                    location.href = "{{url('/admin/jurors')}}"
                }, 100)
            }
        })

    })

    let dropdown_name_id = 1
    let dropdown_id = 1

    $(document).on('change','#subcategory', function() {

        $('.dropdown-container').append(
            '<div class="juror-category row">'+
                '<div class="col-md-6" style="column-gap: 25px">'+
                    '<div class="mb-2">'+
                        '<input data-catid="'+$('#category_name option:selected').attr('data-catid')+'" disabled type="text" class="text-uppercase form-control form-control-lg lg fs-18" id="category_id'+ dropdown_name_id++ +'" name="category_id[]" placeholder="Category" value="'+$('#category_name').val()+'">'+
                    '</div>' +
                '</div>' +

                '<div class="col-md-6" style="column-gap: 25px">'+
                    '<div class="mb-2">'+
                        '<input data-subid="'+$('#subcategory option:selected').attr('data-subid')+'" disabled type="text" class="text-uppercase form-control form-control-lg lg fs-18" id="subcategory_id'+ dropdown_id++ +'" name="subcategory_id[]" placeholder="Subcategory" value="'+$(this).val()+'">'+
                        '<a href="javascript:void(0)" class="delete-dropdown"><i class="fas fa-times"></i></a>'+
                    '</div>'+
                '</div>'+
            '</div>'
        )
    })

    $(document).on('change','#edit_subcategory', function() {

        $('.edit-dropdown-container').append(
            '<div class="juror-category row">'+
                '<div class="col-md-6" style="column-gap: 25px">'+
                    '<div class="mb-2">'+
                        '<input data-catid="'+$('#edit_category_name option:selected').attr('data-catid')+'" disabled type="text" class="text-uppercase form-control form-control-lg lg fs-18" id="category_id'+ dropdown_name_id++ +'" name="category_id[]" placeholder="Category" value="'+$('#edit_category_name').val()+'">'+
                    '</div>' +
                '</div>' +

                '<div class="col-md-6" style="column-gap: 25px">'+
                    '<div class="mb-2">'+
                        '<input data-subid="'+$('#edit_subcategory option:selected').attr('data-subid')+'" disabled type="text" class="text-uppercase form-control form-control-lg lg fs-18" id="subcategory_id'+ dropdown_id++ +'" name="subcategory_id[]" placeholder="Subcategory" value="'+$(this).val()+'">'+
                        '<a href="javascript:void(0)" class="delete-dropdown"><i class="fas fa-times"></i></a>'+
                    '</div>'+
                '</div>'+
            '</div>'
        )
    })

    $(document).on('click', '.delete-dropdown', function() {
        $(this).parent().parent().parent().remove()
    })
</script>
@endpush
@endsection