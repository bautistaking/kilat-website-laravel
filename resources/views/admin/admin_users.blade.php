@extends('base')  
@push('styles')
    <link rel="stylesheet" href="https://cdn.datatables.net/v/bs5/jq-3.6.0/dt-1.11.3/datatables.min.css">
    <style>
        .spinner-grow,
        .loading-label {
            display: none;
        }

        .users {
            max-width: 100%;
        }

        #search {
            padding-left: 35px;
        }

        .search-icon {
            position: absolute;
            margin-left: 9px;
            margin-top: -34px;
            font-size: 20px;
            color: #d7a333;
        }

        table td {
            word-wrap: break-word;
            font-size: 14px;
            text-align: center;
        }

        .color-link {
            color: #1a2735;
        }

        .dropdown-content li {
            display: none;
        }

        .custom-thead {
            background: #3b4a56;
        }

        .custom-thead > tr > th {
            padding: 20px;
            color: #fff;
            text-align: left;
        }

        table tr > td {
            text-align: left;
        }

        .form-check span,
        .form .form-check .form-check-input {
            margin-top: 20px;
            margin-left: 5px;
        }

        .dataTables_filter label {
            display: none;
        }

        .btn-custom-status {
            width: 130px;
        }

        .page-title {
            font-size: 30px;
            font-weight: 600;
            color: #1a2735;
        }
    </style>

    <link rel="stylesheet" href="https://cdn.datatables.net/1.11.2/css/dataTables.bootstrap5.min.css">
@endpush
@section('content')
    @include('admin.components.header')
    <div class="wrapper-default">
        <div class="container-fluid minHeight py72">
            @if (count($admin_users) > 0)
                <div style="margin-top: 70px">
                    <div class="users flex-column d-flex">
                        <div class="d-flex align-items-center justify-content-between">
                            <h1 class="page-title mb-0">All Admin</h1>
                            <button type="button" class="btn btn-primary primary small me-3 text-uppercase" style="min-width:156px; height: 48px;" data-bs-toggle="modal" data-bs-target="#adduserModal">Add User</button>
                        </div>
                        <div class="d-flex align-items-center">
                           <!--  <div class="mt-5 mb-4 me-3">
                                <label for="filter" class="form-label fw-500 mb-2">Filter</label>
                            </div> -->
                           <!--  <div class="mt-5 mb-4 me-5">
                                <select class="form-select form-select-lg custom-select lg" style="width: 210px" id="filter_admin" aria-label=".form-select-lg">
                                    <option value="All">All</option>
                                    <option value="Admin">Admin</option>
                                    <option value="Auditor">Auditor</option>
                                </select>
                            </div> -->
                            <div class="w-75"></div>
                            <div class="mt-5 mb-4" style="margin-left: 170px">
                                <div style="width: 100%">
                                    <input type="text" class="form-control form-control-lg lg fs-18" id="search" placeholder="Search">
                                    <i class="fas fa-search search-icon"></i>
                                </div>
                            </div>
                        </div>

                        <table class="table table-striped centered dt users-table w-100">
                            <thead class="custom-thead">
                              <tr>
                                  <th>Name</th>
                                  <th>Email</th>
                                  <th>Date Created</th>
                                  <th>Date Updated</th>
                                  <th>Type</th>
                                  <th>Action</th>
                              </tr>
                            </thead>
                            <tbody>
                              @foreach ($admin_users as $user)
                              <tr>
                                <td>{{$user->name}}</td>
                                <td>{{$user->email}}</td>
                                <td>{{ date('M j, Y h:i a', strtotime($user->created_at)) }}</td>
                                <td>{{ date('M j, Y h:i a', strtotime($user->updated_at)) }}</td>
                                <td>
                                    @if ($user->admin_type == 1)
                                        <span>Admin</span>
                                    @else 
                                        <span>Auditor</span>
                                    @endif
                                </td>
                                <td>
                                    <div class="btn-group" role="group">
                                        <button class="btn btn-secondary btn_edit" data-id="{{$user->id}}" data-bs-toggle="modal" data-bs-target="#edituserModal">
                                            <i class="far fa-edit"></i>
                                        </button>
                                        <button class="btn btn-danger btn_delete" data-id="{{$user->id}}" data-bs-toggle="modal" data-bs-target="#deleteuserModal">
                                            <i class="far fa-trash-alt"></i>
                                        </button>
                                    </div>
                                </td>
                              </tr>
                              @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            @endif
        </div>
    </div>

    <!-- modal -->
    <div class="voting-modal modal fade" id="adduserModal" tabindex="-1" aria-labelledby="flag" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header d-flex justify-content-center">
            <h3 style="margin-top:-20px">Add User</h3>
            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
          </div>
          <div class="modal-body p-0 pt-4">
                <div class="mb-4">
                    <label for="username" class="form-label fw-500 mb-2">Name</label>
                    <input type="text" class="form-control form-control-lg lg fs-18" id="username" placeholder="Enter name here">
                    <div id="invalid-username" class="invalid-feedback"></div>
                </div>
                <div class="mb-4">
                    <label for="email" class="form-label fw-500 mb-2">Email address</label>
                    <input type="email" class="form-control form-control-lg lg fs-18" id="email" placeholder="Email">
                    <div id="invalid-email" class="invalid-feedback"></div>
                </div>
                <div class="mb-4">
                    <label for="admin_type" class="form-label fw-500 mb-2">Role</label>
                    <select class="form-select form-select-lg custom-select lg" id="admin_type">
                        <option value="None" disabled selected>Select Role</option>
                        <option value="Admin">Admin</option>
                        <option value="Auditor">Auditor</option>
                    </select>
                    <div id="invalid-admin-type" class="invalid-feedback"></div>
                </div>
                <div class="mb-4">
                    <label for="password" class="form-label fw-500 mb-2">Password</label>
                    <input type="password" class="form-control form-control-lg lg fs-18" id="password" placeholder="Password">
                    <div id="invalid-password" class="invalid-feedback"></div>
                </div>
                <div class="d-flex justify-content-center">
                    <button id="btn_add" class="btn btn-primary primary small me-3 text-uppercase" style="min-width:156px; height: 48px;">
                        <span class="spinner-grow spinner-grow-sm" role="status"></span>
                        <span class="spinner-grow spinner-grow-sm" role="status"></span>
                        <span class="spinner-grow spinner-grow-sm" role="status"></span>
                        <span class="add-label">Add Admin</span>
                    </button>
                </div>
          </div>
        </div>
      </div>
    </div>

    <div class="voting-modal modal fade" id="edituserModal" tabindex="-1" aria-labelledby="flag" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header d-flex justify-content-center">
            <h3 style="margin-top:-20px">Edit User</h3>
            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
          </div>
          <div class="modal-body p-0 pt-4">
                <div class="mb-4">
                    <label for="edit_username" class="form-label fw-500 mb-2">Name</label>
                    <input type="text" class="form-control form-control-lg lg fs-18" id="edit_username" placeholder="Enter name here">
                    <div id="invalid-edit-username" class="invalid-feedback"></div>
                </div>
                <div class="mb-4">
                    <label for="email" class="form-label fw-500 mb-2">Email address</label>
                    <input type="edit_email" class="form-control form-control-lg lg fs-18" id="edit_email" placeholder="Email">
                    <div id="invalid-edit-email" class="invalid-feedback"></div>
                </div>
                <div class="mb-4">
                    <label for="edit_admin_type" class="form-label fw-500 mb-2">Role</label>
                    <select class="form-select form-select-lg custom-select lg" id="edit_admin_type">
                        <option value="None" disabled selected>Select Role</option>
                        <option value="Admin">Admin</option>
                        <option value="Auditor">Auditor</option>
                    </select>
                    <div id="invalid-edit-admin-type" class="invalid-feedback"></div>
                </div>
                <div class="mb-4">
                    <label for="edit_password" class="form-label fw-500 mb-2">Password</label>
                    <input type="password" class="form-control form-control-lg lg fs-18" id="edit_password" placeholder="Password">
                    <div id="invalid-edit-password" class="invalid-feedback"></div>
                </div>
                <div class="d-flex justify-content-center">
                    <button id="btn_update" class="btn btn-primary primary small me-3 text-uppercase" style="min-width:156px; height: 48px;">
                        <span class="spinner-grow spinner-grow-sm" role="status"></span>
                        <span class="spinner-grow spinner-grow-sm" role="status"></span>
                        <span class="spinner-grow spinner-grow-sm" role="status"></span>
                        <span class="edit-label">Update Admin</span>
                    </button>
                </div>
          </div>
        </div>
      </div>
    </div>

    <div class="voting-modal modal fade" id="deleteuserModal" tabindex="-1" aria-labelledby="flag" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header d-flex">
            <h4 style="margin-top:-20px">Are you sure you want to delete?</h4>
            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
          </div>
          <div class="modal-body p-0 pt-4">
               <div class="d-flex justify-content-center">
                    <button class="btn btn-outline-primary brown primary small me-3 text-uppercase brown" style="min-width:156px; height: 48px;"  data-bs-dismiss="modal" aria-label="Close">
                        <span class="delete-label">Cancel</span>
                    </button>
                    <button id="btn_remove" class="btn btn-primary primary small me-3 text-uppercase" style="min-width:156px; height: 48px;">
                        <span class="spinner-grow spinner-grow-sm" role="status"></span>
                        <span class="spinner-grow spinner-grow-sm" role="status"></span>
                        <span class="spinner-grow spinner-grow-sm" role="status"></span>
                        <span class="delete-label">Confirm</span>
                    </button>
                </div>
          </div>
        </div>
      </div>
    </div>
    @include('admin.components.footer')
@push('custom-js')
<script type="text/javascript" src="https://cdn.datatables.net/v/bs5/jq-3.6.0/dt-1.11.3/datatables.min.js"></script>
<script>
    $(document).ready(activateQuery())

    function activateQuery() {
        var dtable = $('.dt').DataTable({
            // "aaSorting": [],
            "autoWidth": false,
            "bFilter": true,
            "bInfo": false,
            "lengthChange": false,
            "paging": true,
            "pagingType": "simple_numbers",
            "sorting": true,
            "order": [[ 3, "desc" ]]
            // "searching": false
        });

        $('#search').keyup(function(){
            dtable.search($(this).val()).draw();
        });

        var admin_id = 0;
        $(document).on('click', '.btn_edit', function() {
            admin_id = $(this).attr('data-id')

            $.get('{{url("/admin/admin-id/")}}/' + admin_id, function (data) {
                $('#edit_username').val(data.name);
                $('#edit_email').val(data.email);

                var admin_type = "Admin"
                if (data.admin_type == "2") {
                    admin_type = "Auditor"
                }

                $('#edit_admin_type').val(admin_type)
            }) 
        })

        $(document).on('click', '.btn_delete', function() {
            admin_id = $(this).attr('data-id')
        })

        $(document).on('click', '#btn_add', function() {
            // $.LoadingOverlay("show", {
            //     image: '{{url("/images/loading1.png")}}'
            // },100);

            $('.add-label').hide()
            $('.spinner-grow').addClass('d-inline-block')

            $(this).attr('disabled', true)

            var checker = 0
            if ($('#username').val() == "") {
                checker++;
                $('#username').addClass('is-invalid')
                $('#invalid-username').text('Name is required.').show()
            } else {
                $('#username').removeClass('is-invalid')
                $('#invalid-username').hide()
            }

            if ($('#email').val() == "") {
                checker++;
                $('#email').addClass('is-invalid')
                $('#invalid-email').text('Email address is required.').show()
            } else {
                var email_filter = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
                if (!email_filter.test($('#email').val())) {
                    checker++;
                    $('#email').addClass('is-invalid')
                    $('#invalid-email').text('Email address is invalid.').show()
                } else {
                    $('#email').removeClass('is-invalid')
                    $('#invalid-email').hide()
                }
            }

            if ($('#admin_type option:selected').val() == "None") {
                checker++;
                $('#admin_type').addClass('is-invalid')
                $('#invalid-admin-type').text('Role is required.').show()
            } else {
                $('#admin_type').removeClass('is-invalid')
                $('#invalid-admin-type').hide()
            }

            if ($('#password').val() == "") {
                checker++;
                $('#password').addClass('is-invalid')
                $('#invalid-password').text('Password is required.').show()
            } else {

                var password = /^(?=.*\d)(?=.*[!@#$%^&*])(?=.*[a-z])(?=.*[A-Z]).{8,}$/;
                if (!password.test($('#password').val())) {
                    checker++;
                    $('#password').addClass('is-invalid')
                    $('#invalid-password').text('Password must be atleast 8 characters with 1 symbol,  1uppercase,  1lowercase and a 1 number.').show()
                } else {
                    $('#password').removeClass('is-invalid')
                    $('#invalid-password').hide()
                }
            }

            if (checker == 0) {

                let params = {
                    '_token'     : '{{ csrf_token() }}',
                    'name'       : $('#username').val(),
                    'email'      : $('#email').val(),
                    'role'       : 'Admin',
                    'admin_type' : $('#admin_type').val(),
                    'password'   : $('#password').val()
                }

                fetch("{{url('/admin/admin-user/add')}}", {
                    method: 'POST',
                    headers: {
                        'Content-Type': 'application/json'
                    },
                    body: JSON.stringify(params)
                })
                .then(response => response.json())
                .then((e) => {

                    if (e.status_code == 400 || e.status_code == 409) {
                        // $.LoadingOverlay("hide");

                        $('.add-label').show()
                        $('.spinner-grow').removeClass('d-inline-block')

                        let html = "";

                        $.each(e.message, function(a,b) {
                            if (e.header == "Duplicate") {
                                html += "Email address already registered."
                            }
                        })

                        $('#invalid-email').html(html).show()
                        $('#email').addClass('is-invalid')

                        $(this).attr('disabled', false)
                    }

                    if (e.status_code == 200) {
                        // $.LoadingOverlay("hide");

                        setTimeout(function() {
                            location.href = "{{url('/admin/admin-users')}}"
                        }, 100)
                    }
                })

            } else {
                // $.LoadingOverlay("hide");
                $('.add-label').show()
                $('.spinner-grow').removeClass('d-inline-block')
                $(this).attr('disabled', false)
            }

        })

        $(document).on('click', '#btn_update', function() {
            // $.LoadingOverlay("show", {
            //     image: '{{url("/images/loading1.png")}}'
            // },100);

            $('.edit-label').hide()
            $('.spinner-grow').addClass('d-inline-block')

            $(this).attr('disabled', true)

            var checker = 0
            if ($('#edit_username').val() == "") {
                checker++;
                $('#edit_username').addClass('is-invalid').show()
                $('#invalid-edit-username').text('Name is required.').show()
            }

            if ($('#edit_email').val() == "") {
                checker++;
                $('#edit_email').addClass('is-invalid').show()
                $('#invalid-edit-email').text('Email address is required.').show()
            } else {
                var email_filter = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
                if (!email_filter.test($('#edit_email').val())) {
                    checker++;
                    $('#edit_email').addClass('is-invalid').show()
                    $('#invalid-edit-email').text('Email address is invalid.').show()
                } else {
                    $('#edit_email').removeClass('is-invalid')
                    $('#invalid-edit-email').hide()
                }
            }

            if ($('#edit_admin_type option:selected').val() == "None") {
                checker++;
                $('#edit_admin_type').addClass('is-invalid').show()
                $('#invalid-edit--admin-type').text('Role is required.').show()
            } else {
                $('#edit_admin_type').removeClass('is-invalid')
                $('#invalid-edit-admin-type').hide()
            }

            if ($('#edit_password').val() == "") {
            } else {
                var password = /^(?=.*\d)(?=.*[!@#$%^&*])(?=.*[a-z])(?=.*[A-Z]).{8,}$/;
                if (!password.test($('#edit_password').val())) {
                    checker++;
                    $('#edit_password').addClass('is-invalid').show()
                    $('#invalid-edit-password').text('Password must be atleast 8 characters with 1 symbol,  1uppercase,  1lowercase and a 1 number.').show()
                } else {
                    $('#edit_password').removeClass('is-invalid')
                    $('#invalid-edit-password').hide()
                }
            }

            if (checker == 0) {

                let params = {
                    '_token'     : '{{ csrf_token() }}',
                    'id'         : admin_id,
                    'name'       : $('#edit_username').val(),
                    'email'      : $('#edit_email').val(),
                    'role'       : 'Admin',
                    'admin_type' : $('#edit_admin_type').val(),
                    'password'   : $('#edit_password').val()
                }

                fetch("{{url('/admin/admin-user/edit')}}", {
                    method: 'POST',
                    headers: {
                        'Content-Type': 'application/json'
                    },
                    body: JSON.stringify(params)
                })
                .then(response => response.json())
                .then((e) => {

                    if (e.status_code == 409) {
                        // $.LoadingOverlay("hide");

                        $('.edit-label').show()
                        $('.spinner-grow').removeClass('d-inline-block')

                        let html = "";

                        $.each(e.message, function(a,b) {
                            if (e.header == "Duplicate") {
                                html += "Email address already registered."
                            }
                        })

                        $('#invalid-email').html(html).show()
                        $('#edit_email').addClass('is-invalid')

                        $(this).attr('disabled', false)
                    }

                    if (e.status_code == 200) {
                        // $.LoadingOverlay("hide");

                        $('#edituserModal').toggle('modal')

                        setTimeout(function() {
                            location.href = "{{url('/admin/admin-users')}}"
                        }, 100)
                    }
                })

            } else {
                // $.LoadingOverlay("hide");
                $('.edit-label').show()
                $('.spinner-grow').removeClass('d-inline-block')
                $(this).attr('disabled', false)
            }

        })

        $(document).on('click', '#btn_remove', function() {
            // $.LoadingOverlay("show", {
                // image: '{{url("/images/loading1.png")}}'
            // },100);

            $('.delete-label').hide()
            $('.spinner-grow').addClass('d-inline-block')

            $(this).attr('disabled', true)

            let params = {
                '_token'     : '{{ csrf_token() }}',
                'id'         : admin_id,
            }

            fetch("{{url('/admin/admin-user/delete')}}", {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify(params)
            })
            .then(response => response.json())
            .then((e) => {

                if (e.status_code == 409) {
                    // $.LoadingOverlay("hide");

                    $('.delete-label').show()
                    $('.spinner-grow').removeClass('d-inline-block')

                    $(this).attr('disabled', false)
                }

                if (e.status_code == 200) {
                    // $.LoadingOverlay("hide");
                    $('#deleteuserModal').toggle('modal')
                    setTimeout(function() {
                        location.href = "{{url('/admin/admin-users')}}"
                    }, 100)
                }
            })

        })
    }
</script>
@endpush
@endsection