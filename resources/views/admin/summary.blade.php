@extends('base')
@push('styles')
  <link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
  <link rel="stylesheet" href="{{ mix('admin/css/admin.css') }}">
@endpush
@section('content')
@include('admin.components.nav')
<div class="content overflow-y-visible">
  @include('admin.components.sub-nav')
  <div class="card text-center padded overflow-y-visible">
    <div class="row m-0">
      <div class="input-field col m3 s12 m-0">
          <select class="entries-filter">
            <option @if($cat_id == 1) selected @endif data-url="{{url('/webmaster-kidlat/summary')}}">CREATIVE EFFECTIVENESS</option>
            <option @if($cat_id == 2) selected @endif data-url="{{url('/webmaster-kidlat/summary/creative-technology-media')}}">CREATIVE TECHNOLOGY & MEDIA</option>
            <option @if($cat_id == 3) selected @endif data-url="{{url('/webmaster-kidlat/summary/creative-storytelling')}}">CREATIVE STORYTELLING</option>
            <option @if($cat_id == 4) selected @endif data-url="{{url('/webmaster-kidlat/summary/creative-purpose')}}">CREATIVE PURPOSE</option>
            <option @if($cat_id == 5) selected @endif data-url="{{url('/webmaster-kidlat/summary/craft')}}">CRAFT</option>
          </select>
        <label>Category</label>
      </div>
    </div>
    <div class="mw-100 overflow-auto overflow-y-visible">
      <table class="striped centered dt entries-table">
        <thead>
          <tr>
              <th>ID</th>
              <th>Entry Name</th>
              <th>Ad Title</th>
              <th>Brand</th>
              <th>Entry</th>
              <th>Category</th>
              <th>Sub Category</th>
          </tr>
        </thead>

        <tbody>
          @foreach ($entries as $e)
          <tr>
            <td class="uid flex align-center"><a href="{{url('admin/preview')}}/{{$category}}/{{$e->id}}" target="_blank">{{$e->uid}}</a></td>
            <td>{{$e->entry_name}}</td>
            <td>{{$e->ad_details}}</td>
            <td>{{$e->brand}}</td>
            <td>{{$e->type}}</td>
            <td>{{$e->category()->name}}</td>
            <td>{{$e->sub_category()->name}}</td>
          </tr>
          @endforeach
        </tbody>
      </table>
    </div>

  </div>
</div>
@push('custom-js')
  <script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
  <script src="{{ mix('admin/js/admin.js') }}"></script>
@endpush
@endsection