@extends('base')

@section('content')
<div class="dashboard">
  @include('dashboard.components.header')

  @if(!empty($next))
  <a href="{{url('webmaster-kidlat/preview')}}/{{$category}}/{{$next->id}}" class="btn white flex align-center next-entry"><i class="material-icons text-color -pink">arrow_forward_ios</i></a>
  @endif
  @if(!empty($prev))
  <a href="{{url('webmaster-kidlat/preview')}}/{{$category}}/{{$prev->id}}" class="btn white flex align-center prev-entry"><i class="material-icons text-color -pink">arrow_back_ios</i></a>
  @endif
  <div class="row">
    <div class="col s12 m10 offset-m1">
      <div class="card p-1">
        <h4>{{$entry->entry_name}}</h4>
        <div class="spacer"></div>
        <?php $disabled = false; ?>
        @if($entry->key_files()->count() > 0)
          @foreach($entry->key_files() as $index => $f)
          @if($f->path == "0")
          <?php $disabled = true; ?>
          @endif
          @if($index == 0)
          <div class="text-center main-file">
            @if($f->file_type() == "Image")
              <img src="{{url('/webmaster-kidlat/download/file')}}/{{$f->id}}" class="mw-100 mh-800px">
            @elseif($f->file_type() == "Video")
              <video controls class="mw-100 mh-800px">
                <source src="{{url('/webmaster-kidlat/download/file')}}/{{$f->id}}"  type="video/mp4">
              </video>
            @elseif($f->file_type() == "Audio")
              <audio controls class="mw-100 mh-800px">
                <source src="{{url('/webmaster-kidlat/download/file')}}/{{$f->id}}">
              </audio>
            @else 
              <div class="no-preview">
                <div>
                  <i class="material-icons">broken_image</i> <br>
                  No Preview Available
                </div>
              </div>
            @endif
            <div class="font -xs-small">
              <a href="{{url('/webmaster-kidlat/download/file')}}/{{$f->id}}" class="preview-link" target="_blank"><i>*Click here to view / download the file if the preview is not working properly</i></a>
            </div>
          </div>
          @endif
          @endforeach
        @else 
        <?php $disabled = true; ?>
        @endif
        <div class="spacer"></div>
        @if($entry->key_files()->count() > 1)
        <p>ALSO FROM THIS ENTRY</p>
        <div class="files-list">
          @foreach($entry->key_files() as $index => $f)
            <div class="files" data-type="{{$f->file_type()}}" data-url="{{url('/webmaster-kidlat/download/file')}}/{{$f->id}}">
              <div>
                @if($f->file_type() == "Image")
                <i class="material-icons">image</i>
                @elseif($f->file_type() == "Video")
                <i class="material-icons">videocam</i>
                @elseif($f->file_type() == "Audio")
                <i class="material-icons">headset</i>
                @else
                <i class="material-icons">broken_image</i>
                @endif
                <p class="font -xs-small">{{$f->name}}</p>
              </div>
            </div>
          @endforeach
        </div>
        @endif
        <div class="spacer"></div>
        <p>ENTRY DETAILS</p>
        <div class="font -small">
          <p>ENTRY NAME: <span class="mx-1 text-color -pink">{{$entry->entry_name}}</span></p>
          <p>CATEGORY: <span class="mx-1 text-color -pink">{{$entry->category()->name}}</span></p>
          <p>SUB CATEGORY: <span class="mx-1 text-color -pink">{{$entry->sub_category()->name}}</span></p>
          <p>AD TITLE: <span class="mx-1 text-color -pink">{{$entry->ad_details}}</span></p>
          <p>BRAND: <span class="mx-1 text-color -pink">{{$entry->brand}}</span></p>
          <p>DATE OF FIRST PUBLICATION: <span class="mx-1 text-color -pink">{{date('F j, Y', strtotime($entry->date_publication))}}</span></p>
          <p>ENTRY TYPE: <span class="mx-1 text-color -pink">{{$entry->type}}</span></p>
        </div>
        <div class="spacer"></div>
        @if($entry->reference()->count() > 0)
        <hr>
        <div class="spacer"></div>
        <p>ADDITIONAL LINKS / REFERENCE</p>
        @foreach($entry->reference() as $index => $r)
        <div><a href="{{$r->link}}" target="_blank"><label>{{$r->link}}</label></a></div>
        @endforeach
        <div class="spacer"></div>
        @endif
        <div class="spacer"></div>
      </div>
    </div>
  </div>

</div>
@endsection