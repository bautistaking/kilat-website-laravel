@extends('base')
@push('styles')
  <link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
  <link rel="stylesheet" href="{{ mix('admin/css/admin.css') }}">
@endpush
@section('content')
@include('admin.components.nav')
<div class="content overflow-y-visible">
  @include('admin.components.sub-nav')
  <div class="card text-center padded overflow-y-visible">
    <div class="row m-0">
      <div class="input-field col m3 s12 m-0">
        <select class="entries-filter">
          <option @if($filter == null) selected @endif data-url="{{url('/admin/entries')}}">All</option>
          <option @if($filter == 'draft') selected @endif data-url="{{url('/admin/entries/draft')}}">Draft</option>
          <option @if($filter == 'pending_payment') selected @endif data-url="{{url('/admin/entries/pending_payment')}}">Pending Payment</option>
          <option @if($filter == 'pending_approval') selected @endif data-url="{{url('/admin/entries/pending_approval')}}">Paid and Pending Entry Review</option>
          <option @if($filter == 'approved') selected @endif data-url="{{url('/admin/entries/approved')}}">Paid and Confirmed for Judging</option>
          <option @if($filter == 'rejected') selected @endif data-url="{{url('/admin/entries/rejected')}}">With issues</option>
        </select>
        <label>Filter</label>
      </div>
      <div class="col m9 text-right">
        <form action="{{url('/admin/interim-export')}}" method="get" target="_blank" class="prevent-loading">
          <div class="input-field inline">
            <select name="filter" class="entries-filter">
              <option value="All">All</option>
              <option value="Approved">Approved</option>
              <option value="Pending">Pending</option>
            </select>
            <label>Entry Status</label>
          </div>
          <button class="waves-effect waves-dark btn pink" target="_blank">Download CSV Report</button>
        </form>
        <div>
          @if($filter == 'approved')
          <form action="{{url('/admin/credits-export')}}" method="get"  target="_blank" class="prevent-loading">
            <div class="input-field inline" style="min-width:250px;">
              <select name="category">
                <option value="0">All</option>
                <option value="1">CREATIVE EFFECTIVENESS</option>
                <option value="2">CREATIVE TECHNOLOGY & MEDIA</option>
                <option value="3">CREATIVE STORYTELLING</option>
                <option value="4">CREATIVE PURPOSE</option>
                <option value="5">CRAFT</option>
              </select>
              <label>Category</label>
            </div>
            <button target="_blank" rel="noopener noreferrer" class="waves-effect waves-dark btn pink" >Export Credits</button>
          </form>
          @endif
        </div>
        
      </div>
    </div>
    <div class="mw-100 overflow-auto overflow-y-visible">
      <table class="striped centered dt entries-table">
        <thead>
          <tr>
              <th>ID</th>
              <th>Agency Name</th>
              <th>Entry Name</th>
              <th>Brand</th>
              <th>Entry</th>
              <th>Category</th>
              <th>Sub Category</th>
              <th>Fee</th>
              <th>Date Submitted</th>
              @if(Auth::user()->admin_type == 2 || Auth::user()->admin_type == null)
              <th>Payment Status</th>
              @endif
              @if(Auth::user()->admin_type == 1 || Auth::user()->admin_type == null)
              <th>Entry Status</th>
              @endif
          </tr>
        </thead>

        <tbody>
          @foreach ($entries as $e)
          <tr>
            <td class="uid flex align-center"><a href="{{url('admin/view/entry')}}/{{$e->uid}}" target="_blank">{{$e->uid}}</a></td>
            <td>{{$e->agency_name}}</td>
            <td>{{$e->entry_name}}</td>
            <td>{{$e->brand}}</td>
            <td>{{$e->type}}</td>
            <td>{{$e->category()->name}}</td>
            <td>{{!empty($e->sub_category) ? $e->sub_category()->name : '-'}}</td>
            <td>{{number_format($e->fee)}}</td>
            <td>{{ date('F j, Y. h:i a', strtotime($e->created_at)) }}</td>
            @if(Auth::user()->admin_type == 2 || Auth::user()->admin_type == null)
            <td>
              <a class='dropdown2-trigger flex align-center btn inverted @if($e->payment_status == "Paid") green @elseif($e->payment_status == "Unpaid") red @else orange @endif' href='#' data-target='dropdown-fee-{{$e->uid}}' style="line-height: 1em;">{{$e->payment_status}} <i class="material-icons right">expand_more</i></a>
              <ul id='dropdown-fee-{{$e->uid}}' class='dropdown-content'>
                <li>
                  <a href="#!" class="text-color -red submit"><i class="material-icons">close</i>Unpaid</a>
                  <form method="POST" action="{{ url('admin/update/payment') }}">
                    @csrf
                    <input type="hidden" name="id" value="{{$e->id}}">
                    <input type="hidden" name="payment_status" value="Unpaid">
                  </form>
                </li>
                <li>
                  <a href="#!" class="text-color -orange submit"><i class="material-icons">check</i>Billing Sent</a>
                  <form method="POST" action="{{ url('admin/update/payment') }}">
                    @csrf
                    <input type="hidden" name="id" value="{{$e->id}}">
                    <input type="hidden" name="payment_status" value="Billing Sent">
                  </form>
                </li>
                <li>
                  <a href="#!" class="submit"><i class="material-icons">check</i>Paid</a>
                  <form method="POST" action="{{ url('admin/update/payment') }}">
                    @csrf
                    <input type="hidden" name="id" value="{{$e->id}}">
                    <input type="hidden" name="payment_status" value="Paid">
                  </form>
                </li>
              </ul>
            </td>
            @endif
            @if(Auth::user()->admin_type == 1 || Auth::user()->admin_type == null)
            <td>
              <a class='dropdown2-trigger flex align-center btn inverted @if($e->status == "Approved") green @elseif($e->status == "Rejected") red @else orange @endif' href='#' data-target='dropdown-status-{{$e->uid}}'>{{$e->status}} <i class="material-icons right">expand_more</i></a>
              <ul id='dropdown-status-{{$e->uid}}' class='dropdown-content'>
                <li>
                  <a href="#!" class="submit"><i class="material-icons">check</i>Approve</a>
                  <form method="POST" action="{{ url('admin/update/status') }}">
                    @csrf
                    <input type="hidden" name="id" value="{{$e->id}}">
                    <input type="hidden" name="status" value="Approved">
                  </form>
                </li>
                <li>
                  <a href="#!" class="text-color -red submit"><i class="material-icons">close</i>Reject</a>
                  <form method="POST" action="{{ url('admin/update/status') }}">
                    @csrf
                    <input type="hidden" name="id" value="{{$e->id}}">
                    <input type="hidden" name="status" value="Rejected">
                  </form>
                </li>
              </ul>
            </td>
            @endif
          </tr>
          @endforeach
          
        </tbody>
      </table>
    </div>

  </div>
</div>
@push('custom-js')
  <script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
  <script src="{{ mix('admin/js/admin.js') }}"></script>
@endpush
@endsection