@extends('base')  
@push('styles')
    <style>
        .spinner-grow,
        .loading-label {
            display: none;
        }

        .nav-tabs {
            border-bottom: none;
        }

        .tab-content {
            margin-top: 0px !important;
        }
    </style>
@endpush
@section('content')
    @include('juror.components.header')
    <div class="wrapper-default pt72 fullWidth">
        <div class="dashboard judging">
            <div class="container-fluid ps-0">
                <div class="row justify-content-center minHeight">
                    @include('juror.components.sidebar')
                    <div class="col right-panel px-0">
                        @include('juror.components.actions')
                        <div class="border-divider"></div>
                        
                        <div class="tab-content" id="v-pills-tabContent">
                            @foreach($entries as $entry)
                            <?php
                                $custom_entry = preg_replace('/\s+/', '', strtolower($entry->entry_name))
                            ?>
                            <div class="tab-pane fade @if($loop->index == 0) ? show active :  @endif " id="v-pills-{{$custom_entry}}" role="tabpanel" aria-labelledby="v-pills-{{$custom_entry}}-tab">
                                <div class="px-65 py-40">
                                    <div class="label d-flex justify-content-between mb-3">
                                        <span class="label-title">Round 1: {{$category->name}}</span>
                                        <p class="position-absolute" style="color:#384753;margin-top: 25px;font-weight:bold;">{{$subcategory->name}}</p>
                                        <!-- <div class="top-action">
                                            <i class="bi bi-check2 me-2"></i> <span>All changes saved</span>
                                        </div> -->
                                    </div>
                                    <h2 class="page-title mb-0 mt-4">{{$entry->entry_name}}</h2>
                                    <div class="video-wrapper">
                                        <iframe src="https://www.youtube.com/embed/9bnOVV3EBC8" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                                    </div>
                                    <h4 class="tab-title mb-3">Also from this campaign</h4>
                                    <div class="d-flex justify-content-start images flex-wrap">
                                        <?php for ($i = 1; $i <= 3; $i++) : ?>
                                            <a href="#">
                                                <img src="https://picsum.photos/272/153" alt="">
                                            </a>
                                        <?php endfor; ?>
                                    </div>
                                </div>

                                <div class="border-divider"></div>

                                <div class="px-65 py-40"> 
                                    <h3 class="block-title mb-20">Campaign details</h3>
                                    <div class="sub-label">Entry name: <span>{{$entry->entry_name}}</span></div>
                                    <div class="sub-label">Ad title/s: <span>{{$entry->ad_details}}</span></div>
                                    <div class="sub-label">Advertiser: <span>{{$entry->advertiser}}</span></div>
                                    <div class="sub-label mb-0">Brand: <span>{{$entry->brand}}</span></div>
                                </div>

                                <div class="border-divider"></div>

                                <div class="px-65 py-40">
                                    <h3 class="block-title mb-0">Written submission</h3>
                                    <div class="tabs">
                                        <!-- Nav tabs -->
                                        <ul class="nav nav-pills justify-content-start" id="myTab" role="tablist">
                                            <li class="nav-item" role="presentation">
                                                <button class="nav-link active" id="pills-campaign-summary-tab{{$loop->index}}" data-bs-toggle="pill" data-bs-target="#pills-campaign-summary{{$loop->index}}" type="button" role="tab" aria-controls="pills-campaign-summary" aria-selected="true">Campaign Summary</button>
                                            </li>
                                            <li class="nav-item" role="presentation">
                                                <button class="nav-link" id="pills-objectives-tab{{$loop->index}}" data-bs-toggle="pill" data-bs-target="#pills-objectives{{$loop->index}}" type="button" role="tab" aria-controls="pills-objectives" aria-selected="false">Objectives</button>
                                            </li>
                                            <li class="nav-item" role="presentation">
                                                <button class="nav-link" id="pills-strategy-tab{{$loop->index}}" data-bs-toggle="pill" data-bs-target="#pills-strategy{{$loop->index}}" type="button" role="tab" aria-controls="pills-strategy" aria-selected="false">Strategy</button>
                                            </li>
                                            <li class="nav-item" role="presentation">
                                                <button class="nav-link" id="pills-execution-tab{{$loop->index}}" data-bs-toggle="pill" data-bs-target="#pills-execution{{$loop->index}}" type="button" role="tab" aria-controls="pills-execution" aria-selected="false">Execution</button>
                                            </li>
                                            <li class="nav-item" role="presentation">
                                                <button class="nav-link" id="pills-results-tab{{$loop->index}}" data-bs-toggle="pill" data-bs-target="#pills-results{{$loop->index}}" type="button" role="tab" aria-controls="pills-results" aria-selected="false">Results</button>
                                            </li>
                                        </ul>
                                        <div class="tab-content" id="myTabContent">
                                            <div class="tab-pane fade show active" id="pills-campaign-summary{{$loop->index}}" role="tabpanel" aria-labelledby="pills-campaign-summary-tab">
                                                <h4 class="tab-title">
                                                    Campaign Summary
                                                </h4>
                                                <p>{{$entry->campaign_summary}}</p>
                                            </div>
                                            <div class="tab-pane fade" id="pills-objectives{{$loop->index}}" role="tabpanel" aria-labelledby="pills-objectives-tab">
                                                <h4 class="tab-title">
                                                    Objectives
                                                </h4>
                                                <p>{{$entry->objectives}}</p>
                                            </div>
                                            <div class="tab-pane fade" id="pills-strategy{{$loop->index}}" role="tabpanel" aria-labelledby="pills-strategy-tab">
                                                <h4 class="tab-title">
                                                    Strategy
                                                </h4>
                                                <p>{{$entry->strategy}}</p>
                                            </div>
                                            <div class="tab-pane fade" id="pills-execution{{$loop->index}}" role="tabpanel" aria-labelledby="pills-execution-tab">
                                                <h4 class="tab-title">
                                                    Execution
                                                </h4>
                                                <p>{{$entry->execution}}</p>
                                            </div>
                                            <div class="tab-pane fade" id="pills-results{{$loop->index}}" role="tabpanel" aria-labelledby="pills-results-tab">
                                                <h4 class="tab-title">
                                                    Results
                                                </h4>
                                                <p>{{$entry->results}}</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>    
                        
                                <div class="border-divider"></div>
                            </div>
                            @endforeach
                        </div>

                        @include('juror.components.actions')

                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="form">
        <div class="container-fluid px-0 foating-wrapper">
            <div class="row justify-content-center">
                <div class="col left-panel px-0" style="max-width:322px; background: transparent;"></div>
                <div class="col right-panel px-0">
                    <div class="d-flex justify-content-center">
                        <div class="floating mb-5">
                            <div class="form-check d-flex align-items-center">
                                <input class="form-check-input custom-checkbox" type="radio" name="status" id="flag" data-bs-toggle="modal" data-bs-target="#flagModal" value="flag">
                                <label class="form-check-label fs-18" style="margin-left:14px;" for="flag">
                                    <img src="/images/flags.png" alt="" class="vote-icon">
                                    Flag
                                </label>
                            </div>
                            <div class="form-check d-flex align-items-center">
                                <input class="form-check-input custom-checkbox" type="radio" name="status" id="abstain"  data-bs-toggle="modal" data-bs-target="#abstainModal" value="abstain">
                                <label class="form-check-label fs-18" style="margin-left:14px;" for="abstain">
                                    <img src="/images/no-stopping.png" alt="" class="vote-icon">
                                    Abstain
                                </label>
                            </div>
                            <div class="form-check d-flex align-items-center">
                                <input class="form-check-input custom-checkbox" type="radio" name="status" id="outofshow" value="outofshow">
                                <label class="form-check-label fs-18" style="margin-left:14px;" for="outofshow">
                                    Out of show
                                </label>
                            </div>
                            <div class="form-check d-flex align-items-center">
                                <input class="form-check-input custom-checkbox" type="radio" name="status" id="shorlist" value="shortlist">
                                <label class="form-check-label fs-18" style="margin-left:14px;" for="shorlist">
                                    Shortlist
                                </label>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Modals -->
    <div class="form">
        <div class="voting-modal modal fade" id="flagModal" tabindex="-1" aria-labelledby="flag" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body">
                        <div class="selection">
                            <p class="question">Select reason for flagging this entry</p>
                            <div id="invalid-flag-reason" class="invalid-feedback mb-3">Flag reason is required.</div>
                            <div class="form-check form-check-inline">
                                <!-- <input class="form-check-input custom-checkbox" name="flag" type="radio" value="wrong_category" id="flag1"> -->
                                <input class="form-check-input custom-checkbox" type="radio" name="flag" value="wrong_category" id="flag1">
                                <label class="form-check-label" for="flag1">Wrong category</label>
                            </div>
                            <div class="form-check form-check-inline">
                                <!-- <input class="form-check-input custom-checkbox" name="flag" type="radio" value="wrong_eligibility" id="flag2"> -->
                                <input class="form-check-input custom-checkbox" type="radio" name="flag" value="wrong_eligibility" id="flag2">
                                <label class="form-check-label" for="flag2">Wrong eligibility date</label>
                            </div>
                            <div class="form-check form-check-inline">
                                <!-- <input class="form-check-input custom-checkbox" name="flag" type="radio" value="others" id="flag3"> -->
                                <input class="form-check-input custom-checkbox" type="radio" name="flag" value="other_flag" id="flag3">
                                <label class="form-check-label" for="flag3">Others (Kindly specify):</label>
                            </div>
                            <textarea class="form-control" id="flag_reason" rows="3" style="resize: none; width: 100%; text-align: left;"></textarea>
                        </div>
                        <div class="text-center mt-40">
                            <button class="btn btn-primary primary small" id="btn_flag" style="min-width:262px; height: 48px;">Flag this entry</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="voting-modal modal fade" id="abstainModal" tabindex="-1" aria-labelledby="abstain" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body">
                        <div class="selection">
                            <p class="question">Select reason for abstaining from this entry</p>
                            <div id="invalid-abstain-reason" class="invalid-feedback mb-3">Abstain reason is required.</div>
                            <div class="form-check form-check-inline">
                                <input class="form-check-input custom-checkbox" name="abstain" type="radio" value="same_agency" id="abstain1">
                                <label class="form-check-label" for="abstain1">I am from the same agency / network</label>
                            </div>
                            <div class="form-check form-check-inline">
                                <input class="form-check-input custom-checkbox" name="abstain" type="radio" value="involved" id="abstain2">
                                <label class="form-check-label" for="abstain2">I am involved in this project</label>
                            </div>
                            <div class="form-check form-check-inline">
                                <input class="form-check-input custom-checkbox" name="abstain" type="radio" value="other_abstain" id="abstain3">
                                <label class="form-check-label" for="abstain3">Others (Kindly specify):</label>
                            </div>
                            <textarea class="form-control" id="abstain_reason" rows="3" style="resize: none; width: 100%; text-align: left;"></textarea>
                        </div>
                        <div class="text-center mt-40">
                            <button class="btn btn-primary primary small" id="btn_abstain" style="min-width:262px; height: 48px;">Abstain from voting</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="voting-modal modal fade" id="errorVoteModal" tabindex="-1" aria-labelledby="abstain" aria-hidden="true">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body">
                        <div class="selection">
                            <p class="question">Instructions for juror voting</p>
                            <ul>
                               <li><strong>Step 1.</strong> Select entry on the sidebar.</li> 
                               <li><strong>Step 2.</strong> Choose vote status on the middle pop-up.</li>
                               <li><strong>Step 3.</strong> If the vote status is either flag or abstain, reason is required.</li> 
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="voting-modal modal fade" id="successVoteModal" tabindex="-1" aria-labelledby="abstain" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body">
                        <div class="selection">
                            <p class="question text-center">Entry voted successfully!</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @include('juror.components.footer')
@push('custom-js')
<script>
    $(document).ready(activateQuery())

    var entry_id = 0;
    var reason = "";

    function activateQuery() {
        $(document).on('click', '#v-pill-tab > li.nav-link', function() {
            entry_id = $(this).attr('data-id')
            $('#v-pill-tab > .nav-link').not(this).removeClass('active')
            $(this).addClass('active')
        })
    }

    $(document).on('click', '#btn_flag', function() {
        if (typeof $('input[name="flag"]:checked').val() == "undefined") {
            $('input[name="flag"]').addClass('is-invalid')
            $('#invalid-flag-reason').show()
        } else {
            $('input[name="flag"]').removeClass('is-invalid')
            $('#invalid-flag-reason').hide()

            if ($('input[name="flag"]:checked').val() == "other_flag") {
                reason = $('#flag_reason').val()
            } else {
                reason = $('input[name="flag"]:checked').val()
            }

            $('#flagModal').modal('hide')
        }
    })

    $(document).on('click', '#btn_abstain', function() {
        if (typeof $('input[name="abstain"]:checked').val() == "undefined") {
            $('input[name="abstain"]').addClass('is-invalid')
            $('#invalid-abstain-reason').show()
        } else {
            $('input[name="abstain"]').removeClass('is-invalid')
            $('#invalid-abstain-reason').hide()

            if ($('input[name="abstain"]:checked').val() == "other_abstain") {
                reason = $('#abstain_reason').val()
            } else {
                reason = $('input[name="abstain"]:checked').val()
            }

            $('#abstainModal').modal('hide')
        }
    })

    $(document).on('click', '.btn_next', function() {

        var checker = 0

        if (entry_id == 0) {
            checker++;
            $('#errorVoteModal').modal('show')
        }

        if (typeof $('input[name="status"]:checked').val() == "undefined") {
            checker++;
            $('#errorVoteModal').modal('show')
        }

        if ($('input[name="status"]:checked').val() == "flag") {
            if (typeof $('input[name="flag"]:checked').val() == "undefined") {
                checker++;
                $('#errorVoteModal').modal('show')
            }
        }

        if ($('input[name="status"]:checked').val() == "abstain") {
            if (typeof $('input[name="abstain"]:checked').val() == "undefined") {
                checker++;
                $('#errorVoteModal').modal('show')
            }
        }

        if (checker == 0) {
            let params = {
                '_token'        : '{{ csrf_token() }}',
                'entry_id'      : entry_id,
                'status' : $('input[name="status"]:checked').val(),
                'reason'        : reason,
            }

            fetch("{{url('juror/submit-vote-round1')}}", {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify(params)
            })
            .then(response => response.json())
            .then((e) => {
                if (e.status_code == 200) {
                    entry_id = 0
                    $('#flag_reason').val("")
                    $('#abstain_reason').val("")
                    $('#successVoteModal').modal('show')
                }
            })
        }

    })
</script>
@endpush
@endsection