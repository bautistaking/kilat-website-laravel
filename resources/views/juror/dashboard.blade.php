@extends('base')  
@push('styles')
    <style>
        .spinner-grow,
        .loading-label {
            display: none;
        }

        .round1-close-label {
            float: right;
            margin-top: -38px;
            margin-right: 185px;
            display: none;
        }
    </style>
@endpush
@section('content')
    @include('juror.components.header')
    <div class="wrapper-default pt72 fullWidth">
        <div class="container-fluid minHeight py72">
            <div class="dashboard">
                <div class="flex-column d-flex maxWidth">
                    <h1 class="page-title mb-0">
                    Entries
                    </h1>
                </div>
                <div class="tabs">
                    <div class="tabs-divider">
                        <ul class="nav nav-pills justify-content-start maxWidth" id="myTab" role="tablist">

                            @if($round2_entries == 0) 
                                <!-- active  -->
                            @endif
                            @if($round2_entries > 0) 
                                <!-- disabled  -->
                            @endif

                            <li class="nav-item" role="presentation">
                                <button class="nav-link @if($round2_entries == 0) active @endif" id="pills-round-1-tab" data-bs-toggle="pill" data-bs-target="#pills-round-1" type="button" role="tab" aria-controls="pills-round-1" aria-selected="true">Round 1</button>
                            </li>

                            @if($round2_entries > 0) 
                                <!-- active -->
                            @endif 
                            @if($round2_entries == 0) 
                                <!-- disabled  -->
                            @endif

                            <li class="nav-item" role="presentation">
                                <button class="nav-link @if($round2_entries > 0) active @endif" id="pills-round-2-tab" data-bs-toggle="pill" data-bs-target="#pills-round-2" type="button" role="tab" aria-controls="pills-round-2" aria-selected="false">Round 2</button>
                            </li>
                            <li class="nav-item" role="presentation">
                                <button class="nav-link disabled" id="pills-round-3-tab" data-bs-toggle="pill" data-bs-target="#pills-round-3" type="button" role="tab" aria-controls="pills-round-3" aria-selected="false">Round 3</button>
                            </li>
                        </ul>
                        <!-- <p class="link-color round1-close-label">Judging for Round 1 is now closed</p> -->
                    </div>
                    <div class="tab-content" id="myTabContent">
                        @if($round2_entries == 0) <!--show active--> @endif
                        <div class="tab-pane fade @if($round2_entries == 0) show active @endif" id="pills-round-1" role="tabpanel" aria-labelledby="pills-round-1-tab">

                            <!-- @if(!$next)
                                @include('juror.components.success1')
                            @endif -->

                            <div class="dashboard-cards mb-4">

                                @foreach($user_categories1 as $ucat)
                                    <?php $vote_status = ''; ?>

                                    @if($ucat->total_entries1 > 0)
                                    <div class="custom-card">
                                        <div class="custom-card-info">
                                            <h2>{{$ucat->cat_name}}</h2>
                                            <p class="mb-2 text-uppercase">
                                                <strong>{{$ucat->sub_name}}</strong>
                                            </p>
                                            @if($ucat->left_entries1 == $ucat->total_entries1)
                                                <?php $vote_status = 'completed'; ?>
                                            @elseif($ucat->left_entries1 != $ucat->total_entries1 && $ucat->left_entries1 > 0)
                                                <?php $vote_status = 'ongoing'; ?>
                                            @endif

                                            <div class="info-status {{$vote_status}}">
                                                <p><span class="fw-bold">{{$ucat->left_entries1}}</span> out of <span class="fw-bold">{{$ucat->total_entries1}}</span> entries completed</p>

                                                @if($ucat->left_entries1 < $ucat->total_entries1)
                                                <p>Complete the judging by <span class="fw-bold text-decoration-underline">February 12, 2021</span></p>
                                                @endif
                                            </div>
                                        </div>
                                        <a href="/judge/view-round-1/category/{{$ucat->category_id}}/subcategory/{{$ucat->subcategory_id}}">

                                            <?php $btn_style = ""; ?>
                                            @if($ucat->is_submitted1 == 1)
                                                <?php $btn_style = "btn-outline-primary link-color"; ?>
                                            @else
                                                <?php $btn_style = "btn-primary"; ?>
                                            @endif

                                            <button class="btn {{$btn_style}} primary small text-uppercase" style="min-width:175px; height: 48px;">
                                                @if($ucat->left_entries1 > 0 && $ucat->left_entries1 < $ucat->total_entries1)
                                                    <span>Continue Judging</span>
                                                @elseif($ucat->left_entries1 == 0)
                                                    <span>Start Judging</span>
                                                @elseif($ucat->left_entries1 == $ucat->total_entries1 && $ucat->is_submitted1 != 1)
                                                     <span>Review Votes</span>
                                                @elseif($ucat->left_entries1 == $ucat->total_entries1 && $ucat->is_submitted1 == 1)
                                                    <span>Review Votes</span>
                                                @endif
                                            </button>
                                        </a>
                                    </div>
                                    @endif
                                @endforeach
                            </div>

                            <!-- @if(!$next)
                                @include('juror.components.success1')
                            @endif -->

                        </div>
                        @if($round2_entries > 0) <!--show active--> @endif

                        <div class="tab-pane fade @if($round2_entries > 0) show active @endif" id="pills-round-2" role="tabpanel" aria-labelledby="pills-round-2-tab">
                           <!--  <div class="announcement">
                                <p>Judging for Round 2 will be on <span class="fw-bold">November 9, 2021</span>.</p>
                                <p>The toughest decisions are about to be made. As the adage goes,
                                    <br/>
                                    "when lightning strikes, it deserves a metal!"
                                </p>
                                <p>Yeah, we just made that up.</p>
                            </div> -->

                            <!-- @if(!$next)
                                @include('juror.components.success2')
                            @endif -->

                            <div class="dashboard-cards">
                                @if($round2_entries > 0)
                                    @foreach($user_categories2 as $ucat)
                                        <div class="custom-card">
                                            <div class="custom-card-info">
                                                <h2>{{$ucat->cat_name}}</h2>
                                                <p class="mb-2 text-uppercase">
                                                    <strong>{{$ucat->sub_name}}</strong>
                                                </p>

                                                <!-- <?php $vote_status2 = ""; ?>
                                                @if($ucat->left_entries2 == $ucat->total_entries2)
                                                    <?php $vote_status2 = 'completed'; ?>
                                                @elseif($ucat->left_entries2 != $ucat->total_entries2 && $ucat->left_entries2 > 0)
                                                    <?php $vote_status2 = 'ongoing'; ?>
                                                @endif

                                                <div class="info-status {{$vote_status2}}">
                                                    <p><span class="fw-bold">{{$ucat->left_entries2}}</span> out of <span class="fw-bold">{{$ucat->total_entries2}}</span> entries completed</p>
                                                    <p>Complete the judging by <span class="fw-bold text-decoration-underline">February 12, 2023</span></p>
                                                </div> -->
                                            </div>
                                            <a href="/judge/view-round-2/category/{{$ucat->category_id}}/subcategory/{{$ucat->subcategory_id}}">

                                                <?php $btn_style = ""; ?>
                                                @if($ucat->is_submitted2 == 1)
                                                    <?php $btn_style = "btn-outline-primary link-color"; ?>
                                                @else
                                                    <?php $btn_style = "btn-primary"; ?>
                                                @endif

                                                <button class="btn {{$btn_style}} primary small text-uppercase" style="min-width:175px; height: 48px;">
                                                    @if($ucat->left_entries2 > 0 && $ucat->left_entries2 < $ucat->total_entries2)
                                                        <span>Continue Judging</span>
                                                    @elseif($ucat->left_entries2 == 0)
                                                        <span>Start Judging</span>
                                                    @elseif($ucat->left_entries2 == $ucat->total_entries2 && $ucat->is_submitted1 != 1)
                                                         <span>Review Votes</span>
                                                    @elseif($ucat->left_entries2 == $ucat->total_entries2 && $ucat->is_submitted1 == 1)
                                                        <span>Review Votes</span>
                                                    @endif
                                                </button>
                                            </a>
                                        </div>
                                    @endforeach
                                @endif
                            </div>

                            <!-- @if(!$next)
                                @include('juror.components.success2')
                            @endif -->

                        </div>
                        <div class="tab-pane fade" id="pills-round-3" role="tabpanel" aria-labelledby="pills-round-3-tab">
                            <div class="announcement">
                                <p>Judging for Round 3 via Zoom will be on <span class="fw-bold">February 12, 2023, 3:00PM - 7:00PM</span>.</p><br>
                                <p>The toughest decisions are about to be made. As the adage goes,
                                    <br/>
                                    "when lightning strikes, it deserves a metal!"
                                </p>
                                <p>Yeah, we just made that up.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @include('juror.components.footer')
@push('custom-js')
<script>
    $(document).ready(activateQuery())

    function activateQuery() {
        $(document).on('click', '#pills-round-1-tab', function() {
            $('.round1-close-label').css('display', 'block')
        })

        $(document).on('click', '#pills-round-2-tab', function() {
            $('.round1-close-label').css('display', 'none')
        })

    }
</script>
@endpush
@endsection