<div class="col left-panel px-0" style="max-width:322px;">
    <div class="sticky" style="z-index:20 !important">
        <a href="{{url('/juror/dashboard')}}" class="top-action px-30 py-4 minHeight80">
            <i class="bi bi-chevron-left me-2 ms-n1"></i> <span>Back to dashboard</span>
        </a>
        <div class="border-divider"></div>
        <h1 class="category-title mb-0 py-4 px-30">{{ $category->name }}</h1>
        <div class="border-divider"></div>
        <ul class="py-4 nav nav-pills" id="v-pill-tab" role="tablist" aria-orientation="vertical">
            <?php $last_index = 0; ?>
            @foreach($entries as $entry)
                <?php
                    $custom_entry = preg_replace('/\s+/', '', strtolower($entry->entry_name));
                    $custom_entry = str_replace('+','',str_replace('*','',str_replace(',','',str_replace(')','',str_replace('(','',str_replace('#','',str_replace('&','',str_replace('|','',$custom_entry))))))));
                    $custom_entry = str_replace('.','',str_replace('-','',str_replace('!','',str_replace(':','',str_replace('"','',str_replace('\'','',str_replace('/','',$custom_entry))))))).$entry->uid;

                    $last_index = count(end($entries));

                ?>
                
                <li class="@if($loop->index == 0) ? active : @endif nav-link @if($entry->completed1 != null) ? done : @endif" id="v-pills-{{$custom_entry}}-tab" data-bs-toggle="pill" data-bs-target="#v-pills-{{$custom_entry}}" type="button" role="tab" aria-controls="v-pills-{{$custom_entry}}" aria-selected="@if($loop->index == 0) ? true : false @endif" data-uid="{{$entry->uid}}" data-id="{{$entry->id}}" data-counter="{{$loop->index}}">
                    
                    <span>{{$entry->entry_name}}</span>
                </li>
            @endforeach
            <li class="nav-link" id="v-pills-review-tab" data-bs-toggle="pill" data-bs-target="#v-pills-review" type="button" role="tab" aria-controls="v-pills-review" data-counter="<?=$last_index?>">
                <span>Review Votes</span>
            </li>
        </ul>
    </div>
</div>