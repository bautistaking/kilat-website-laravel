<header>
    <div class="header fixed-top">
        <div class="wrapper-default">
            <nav class="navbar navbar-expand navbar-dark">
                <div class="container-fluid">
                    <a class="navbar-brand pt-0 pb-0" href="{{url('/juror/dashboard')}}">
                        <img src="{{url('/images/kidlat-logo-horizontal.png')}}" alt="" width="145" height="56">
                    </a>
                    <div class="d-flex align-items-center">
                        <div class="navbar-nav mr-3">
                            <a class="nav-link active" href="{{url('/juror/dashboard')}}">Dashboard</a>
                            <a class="nav-link" href="{{url('/')}}">Log Out</a>
                        </div>
                    </div>
                </div>
            </nav>
        </div>
    </div>
</header>