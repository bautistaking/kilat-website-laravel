<div class="tab-actions d-flex align-items-center justify-content-between px-65 py-0 minHeight80">
    <a href="#" class="top-action btn_previous">
        <i class="bi bi-chevron-left ms-n1"></i><span class="ms-2">Previous entry</span>
    </a>

    <a href="#" class="top-action btn_next">
        <span class="me-2">Next entry</span> <i class="bi bi-chevron-right me-n1"></i>
    </a>

    <a href="javascript:void(0)" class="btn_submit">
        <button class="btn btn-primary primary small text-uppercase" style="min-width:175px; height: 48px;">SUBMIT SCORES</button>
    </a>

    <div class="ms-auto btn_back">
        <a href="{{url('/judge/dashboard')}}">
            <button type="button" class="btn btn-primary primary small text-uppercase" style="min-width:225px; height: 48px;">BACK TO DASHBOARD</button>
        </a>
    </div>
</div>