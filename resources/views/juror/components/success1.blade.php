<div class="card-body d-flex mb-4 mt-5">
	<img src="{{url('/images/bang_success.png')}}" alt="success" style="margin-top:-15px">
	<div class="ms-3">
		<span><strong>Bang! The lightning has struck and your <br> first round of judging is complete</strong></span>
	</div>
</div>