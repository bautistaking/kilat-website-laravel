<footer class="footer">
    <div class="wrapper-default">
        <div class="container-fluid">
            <div class="d-flex justify-content-between">
                <div class="footer-logo">
                    <img src="{{url('/images/logos.png')}}" alt="" width="299" height="153" style="display:block;" />
                </div>
                <div class="footer-links">
                    <div class="footer-links--first">
                        <p class="label">
                            4As Secretariat: <span class="value">Vanne Tomada</span>
                        </p>
                        <p class="label">
                            Address: <span class="value">Unit 442 Cityland Pasong Tamo Tower, 2210 Chino Roces, Makati City</span>
                        </p>
                        <p class="label">
                            Email address: <a href="mailto:4asp@pldtdsl.net" class="value" target="_blank">4asp@pldtdsl.net</a>
                        </p>
                    </div>
                    <div class="footer-links--second">
                        <p class="label">
                            Telephone number: <a href="#" class="value" target="_blank">8893-1205</a>
                        </p>
                        <p class="label">
                            Mobile number: <a href="#" class="value" target="_blank">0917 5222 427</a>
                        </p>
                       <!--  <p class="label">
                            For technical concerns: <a href="mailto:4asp@pldtdsl.net?subject=Email Subject&body=Contents of email" style="z-index:10" class="value" target="_blank">4asp@pldtdsl.net</a>
                        </p> -->
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>