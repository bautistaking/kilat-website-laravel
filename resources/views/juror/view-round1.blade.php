@extends('base')  
@push('styles')
    <style>
        .spinner-grow,
        .loading-label {
            display: none;
        }

        .nav-tabs {
            border-bottom: none;
        }

        .tab-content {
            margin-top: 0px !important;
        }

        .btn_submit,
        .btn_back {
            display: none;
        }

        .slick-track {
            width: 100% !important;
        }

        .video-wrapper {
            margin-bottom: -325px;
        }

        .main-file {
            width: 800px !important;
            margin: auto;
            display: block;
        }

        .file-entry {
            width: 206px;
            border-radius: 5px;
        }

        #image-slider .splide__list {
            background: #eee;
            padding: 10px !important;
        }

        #image-slider li {
            width: 500px !important;
            margin: auto;
            background: none !important;
            height: auto !important;
        }

        #image-slider img,
        #image-slider video,
        #image-slider iframe {
            width: 500px !important;
            display: block !important;
            border-radius: 5px !important;
        }

        audio {
            width: 250px;
        }

        .splide__arrow {
            z-index: 5;
        }

        .foating-wrapper {
            bottom: 0px !important;
        }

        .view-file {
            text-decoration: none;
        }

        .file-preview-entry video {
            max-width: 272px;
            height: auto;
        }

/*        .mfp-arrow-left  {
            margin-left: 250px;
        }

        .mfp-arrow-right  {
            margin-right: 250px;
        }*/

        #v-pill-tab {
            overflow: hidden;
        }

        .left-panel ul li::before {
            padding: 7px;
        }

        table td:nth-child(1) {
            border: 1px solid #333;
            padding: 5px;
        }

        .loadingoverlay_text {
            color: #fff !important;
            position: absolute;
            margin-top: 253px;
            width: 630px;
            text-align: center;
        }

    </style>
@endpush
@section('content')
    @include('juror.components.header')
    <div class="wrapper-default pt72 fullWidth">
        <div class="dashboard judging">
            <div class="container-fluid ps-0">
                <div class="row justify-content-center minHeight">
                    @include('juror.components.sidebar1')
                    <div class="col right-panel px-0">
                        <div class="border-divider"></div>
                        
                        <div class="tab-content" id="v-pills-tabContent">
                            @foreach($entries as $entry)
                            <?php
                                $custom_entry = preg_replace('/\s+/', '', strtolower($entry->entry_name));
                                $custom_entry = str_replace('+','',str_replace('*','',str_replace(',','',str_replace(')','',str_replace('(','',str_replace('#','',str_replace('&','',str_replace('|','',$custom_entry))))))));
                                $custom_entry = str_replace('.','',str_replace('-','',str_replace('!','',str_replace(':','',str_replace('"','',str_replace('\'','',str_replace('/','',$custom_entry))))))).$entry->uid;
                            ?>
                            <div class="tab-pane fade @if($loop->index == 0) ? show active :  @endif " id="v-pills-{{$custom_entry}}" role="tabpanel" aria-labelledby="v-pills-{{$custom_entry}}-tab" data-tab="{{$loop->index}}">
                                <div class="px-65 py-40">
                                    <div class="label d-flex justify-content-between mb-3">
                                        <span class="label-title">Round 1: {{$category->name}}</span>
                                        <p class="position-absolute" style="color:#384753;margin-top: 25px;font-weight:bold;">{{$subcategory->name}}</p>
                                    </div>
                                    <h2 class="page-title mb-0 mt-4">{{$entry->entry_name}}</h2>

                                    <div class="magnific-container-{{$entry->id}}">
                                        @if(count($entry->files()['concept_board']) > 0)
                                            @foreach($entry->files()['concept_board'] as $cb)
                                                @if($cb->type == "concept_board_dropzone")
                                                    <div class="d-flex mt-3 mb-2">
                                                        <div class="thumbnail-container me-4">
                                                             @if(strstr($cb->path, '.') == '.pdf')
                                                                <a href="{{asset('storage/'.$cb->path)}}" target="_blank">
                                                                    <img class="file-entry"src="{{url('/images/download_pdf.png')}}" alt="">
                                                                </a>
                                                            @else
                                                                <img class="file-entry" src="{{asset('storage/'.$cb->path)}}" alt="">
                                                            @endif
                                                        </div>
                                                        <div>
                                                            <p>
                                                                <strong>Concept Board:</strong>
                                                                <span>{{--$cb->name--}}</span>
                                                            </p>

                                                            <?php $cb_view = ""; ?>
                                                            @if(strstr($cb->path, '.') != '.pdf')
                                                                <?php $cb_view = "view-file"; ?>
                                                            @endif
                                                            <a class="{{$cb_view}} link-color" href="{{asset('storage/'.$cb->path)}}" data-lightbox="concept-board" data-title="{{$cb->name}}" target="_blank">
                                                                 <span>View File <i class="fas fa-external-link-alt"></i></span>
                                                            </a>
                                                            <br/>
                                                            <a class="link-color" href="{{asset('storage/'.$cb->path)}}" target="_blank">
                                                                 <span>Link Out <i class="fas fa-external-link-alt"></i></span>
                                                            </a>
                                                        </div>
                                                    </div>
                                                    <hr>
                                                @endif
                                            @endforeach
                                        @elseif(count($entry->files()['concept_board']) == 0 && $entry->concept_board_dropzone != null)
                                            <div class="d-flex mt-3 mb-2">
                                                <div class="thumbnail-container me-4">
                                                    @if(strstr($entry->concept_board_dropzone, '.') == '.pdf')
                                                    <a href="{{asset('storage/'.$entry->concept_board_dropzone)}}">
                                                        <img class="file-entry"src="{{url('/images/download_pdf.png')}}" alt="">
                                                    </a>
                                                    @else
                                                    <img class="file-entry" src="{{asset('storage/'.$entry->concept_board_dropzone)}}" alt="">
                                                    @endif
                                                </div>
                                                <div>
                                                    <p>
                                                        <strong>Concept Board:</strong>
                                                        <span>{{--$entry->concept_board_dropzone--}}</span>
                                                    </p>

                                                    <?php $cb_view = ""; ?>
                                                    @if(strstr($entry->concept_board_dropzone, '.') != '.pdf')
                                                        <?php $cb_view = "view-file"; ?>
                                                    @endif
                                                    <a class="{{$cb_view}} link-color" href="{{asset('storage/'.$entry->concept_board_dropzone)}}" data-lightbox="concept-board" data-title="{{$entry->name}}" target="_blank">
                                                        <span>View File <i class="fas fa-external-link-alt"></i></span>
                                                    </a>
                                                    <br/>
                                                    <a class="link-color" href="{{asset('storage/'.$entry->concept_board_dropzone)}}" target="_blank">
                                                        <span>Link Out <i class="fas fa-external-link-alt"></i></span>
                                                    </a>
                                                </div>
                                            </div>
                                        @endif

                                        @if(count($entry->files()['case_video']) > 0)
                                            @foreach($entry->files()['case_video'] as $cb)
                                                @if($cb->type == "case_study_video_dropzone")   
                                                    <div class="d-flex mt-3 mb-2">
                                                        <div class="thumbnail-container me-4">
                                                            <video class="file-entry" id="preview_case_study_video_dropzone" controls>
                                                                <source src="{{asset('storage/'.$cb->path)}}">
                                                                Your browser does not support the video tag.
                                                            </video>
                                                        </div>
                                                        <div>
                                                            <p>
                                                                <strong>Case Video:</strong>
                                                                <span>{{--$cb->name--}}</span>
                                                            </p>

                                                            <a class="view-file link-color video-link" href="{{asset('storage/'.$cb->path)}}" data-lightbox="case-video">
                                                                 <span>View File <i class="fas fa-external-link-alt"></i></span>
                                                            </a>
                                                            <br/>
                                                            <a class="link-color video-link" href="{{asset('storage/'.$cb->path)}}" target="_blank">
                                                                 <span>Link Out <i class="fas fa-external-link-alt"></i></span>
                                                            </a>
                                                        </div>
                                                    </div>
                                                    <hr>
                                                @endif
                                            @endforeach
                                        @elseif(count($entry->files()['case_video']) == 0 && $entry->case_study_video_dropzone != null)
                                            <div class="d-flex mt-3 mb-2">
                                                <div class="thumbnail-container me-4">
                                                    <video class="file-entry" id="preview_case_study_video_dropzone" controls>
                                                        <source src="{{asset('storage/'.$entry->case_study_video_dropzone)}}">
                                                        Your browser does not support the video tag.
                                                    </video>
                                                </div>
                                                <div>
                                                    <p>
                                                        <strong>Case Video:</strong>
                                                        <span>{{--$entry->case_study_video_dropzone--}}</span>
                                                    </p>

                                                    <a class="view-file link-color video-link" href="{{asset('storage/'.$entry->case_study_video_dropzone)}}" data-lightbox="case-video">
                                                         <span>View File <i class="fas fa-external-link-alt"></i></span>
                                                    </a>
                                                    <br/>
                                                    <a class="link-color video-link" href="{{asset('storage/'.$entry->case_study_video_dropzone)}}" target="_blank">
                                                         <span>Link Out <i class="fas fa-external-link-alt"></i></span>
                                                    </a>
                                                </div>
                                            </div>
                                            <hr>
                                        @endif

                                        @if(count($entry->files()['written_case']) > 0)
                                            @foreach($entry->files()['written_case'] as $wc)
                                                @if($wc->type == "written_case_dropzone")
                                                    <div class="d-flex mt-3 mb-2">
                                                        <div class="thumbnail-container me-4">
                                                             @if(strstr($wc->path, '.') == '.pdf')
                                                                <a href="{{asset('storage/'.$wc->path)}}">
                                                                    <img class="file-entry"src="{{url('/images/download_pdf.png')}}" alt="">
                                                                </a>
                                                            @else
                                                                <img class="file-entry" src="{{asset('storage/'.$wc->path)}}" alt="">
                                                            @endif
                                                        </div>
                                                        <div>
                                                            <p>
                                                                <strong>Written Case Study:</strong>
                                                                <span>{{--$wc->name--}}</span>
                                                            </p>

                                                            <?php $wc_view = ""; ?>
                                                            @if(strstr($wc->path, '.') != '.pdf')
                                                                <?php $wc_view = "view-file"; ?>
                                                            @endif
                                                             <a class="{{$wc_view}} link-color" href="{{asset('storage/'.$wc->path)}}" data-lightbox="written-case" data-title="{{$wc->name}}" target="_blank">
                                                                 <span>View File <i class="fas fa-external-link-alt"></i></span>
                                                             </a>
                                                             <br/>
                                                             <a class="link-color" href="{{asset('storage/'.$wc->path)}}" target="_blank">
                                                                 <span>Link Out <i class="fas fa-external-link-alt"></i></span>
                                                             </a>
                                                        </div>
                                                    </div>
                                                    <hr>
                                                @endif
                                            @endforeach
                                        @elseif(count($entry->files()['written_case']) == 0 && ($entry->written_case_dropzone != null && $entry->written_case_dropzone != 'undefined'))
                                            <div class="d-flex mt-3 mb-2">
                                                <div class="thumbnail-container me-4">
                                                    @if(strstr($entry->written_case_dropzone, '.') == '.pdf')
                                                    <a href="{{asset('storage/'.$entry->written_case_dropzone)}}">
                                                        <img class="file-entry"src="{{url('/images/download_pdf.png')}}" alt="">
                                                    </a>
                                                    @else
                                                    <img class="file-entry" src="{{asset('storage/'.$entry->written_case_dropzone)}}" alt="">
                                                    @endif
                                                </div>
                                                <div>
                                                    <p>
                                                        <strong>Written Case Study:</strong>
                                                        <span>{{--$entry->written_case_dropzone--}}</span>
                                                    </p>

                                                    <?php $wc_view = ""; ?>
                                                    @if(strstr($entry->written_case_dropzone, '.') != '.pdf')
                                                        <?php $wc_view = "view-file"; ?>
                                                    @endif
                                                    <a class="{{$wc_view}} link-color" href="{{asset('storage/'.$entry->written_case_dropzone)}}" data-lightbox="concept-board" data-title="{{$entry->written_case_dropzone}}" target="_blank">
                                                        <span>View File <i class="fas fa-external-link-alt"></i></span>
                                                    </a>
                                                    <br/>
                                                    <a class="link-color" href="{{asset('storage/'.$entry->written_case_dropzone)}}" target="_blank">
                                                        <span>Link Out <i class="fas fa-external-link-alt"></i></span>
                                                    </a>
                                                </div>
                                            </div>
                                            <hr>
                                        @endif

                                        @if(count($entry->files()['film']) > 0)
                                            @foreach($entry->files()['film'] as $f)
                                                @if($f->type == "film_dropzone")   
                                                    <div class="d-flex mt-3 mb-2">
                                                        <div class="thumbnail-container me-4">
                                                            <video class="file-entry" id="preview_case_study_video_dropzone" controls>
                                                                <source src="{{asset('storage/'.$f->path)}}">
                                                                Your browser does not support the video tag.
                                                            </video>
                                                        </div>
                                                        <div>
                                                            <p>
                                                                <strong>Film:</strong>
                                                                <span>{{--$f->name--}}</span>
                                                            </p>
                                                            <a class="view-file link-color video-link" href="{{asset('storage/'.$f->path)}}" data-lightbox="film">
                                                                 <span>View File <i class="fas fa-external-link-alt"></i></span>
                                                            </a>
                                                            <br/>
                                                            <a class="link-color video-link" href="{{asset('storage/'.$f->path)}}" target="_blank">
                                                                 <span>Link Out <i class="fas fa-external-link-alt"></i></span>
                                                            </a>
                                                        </div>
                                                    </div>
                                                    <hr>
                                                @endif
                                            @endforeach
                                        @elseif(count($entry->files()['film']) == 0 && ($entry->film_dropzone != null && $entry->film_dropzone != 'undefined'))
                                            <div class="d-flex mt-3 mb-2">
                                                <div class="thumbnail-container me-4">
                                                    <video class="file-entry" id="preview_case_study_video_dropzone" controls>
                                                        <source src="{{asset('storage/'.$entry->film_dropzone)}}">
                                                        Your browser does not support the video tag.
                                                    </video>
                                                </div>
                                                <div>
                                                    <p>
                                                        <strong>Film:</strong>
                                                        <span>{{--$entry->film_dropzone--}}</span>
                                                    </p>
                                                    <a class="view-file link-color video-link" href="{{asset('storage/'.$entry->film_dropzone)}}" data-lightbox="film">
                                                         <span>View File <i class="fas fa-external-link-alt"></i></span>
                                                    </a>
                                                    <br/>
                                                    <a class="link-color video-link" href="{{asset('storage/'.$entry->film_dropzone)}}" target="_blank">
                                                         <span>Link Out <i class="fas fa-external-link-alt"></i></span>
                                                    </a>
                                                </div>
                                            </div>
                                            <hr>
                                        @endif

                                        @if(count($entry->files()['posters']) > 0)
                                            @foreach($entry->files()['posters'] as $p)
                                                @if($p->type == "posters_dropzone")
                                                    <div class="d-flex mt-3 mb-2">
                                                        <div class="thumbnail-container me-4">
                                                             @if(strstr($p->path, '.') == '.pdf')
                                                                <a href="{{asset('storage/'.$p->path)}}">
                                                                    <img class="file-entry"src="{{url('/images/download_pdf.png')}}" alt="">
                                                                </a>
                                                            @else
                                                                <img class="file-entry" src="{{asset('storage/'.$p->path)}}" alt="">
                                                            @endif
                                                        </div>
                                                        <div>
                                                            <p>
                                                                <strong>Posters:</strong>
                                                                <span>{{--$p->name--}}</span>
                                                            </p>

                                                            <?php $p_view = ""; ?>
                                                            @if(strstr($p->path, '.') != '.pdf')
                                                                <?php $p_view = "view-file"; ?>
                                                            @endif
                                                             <a class="{{$p_view}} link-color" href="{{asset('storage/'.$p->path)}}" data-lightbox="posters" data-title="{{$p->name}}" target="_blank">
                                                                 <span>View File <i class="fas fa-external-link-alt"></i></span>
                                                             </a>
                                                             <br/>
                                                             <a class="link-color" href="{{asset('storage/'.$p->path)}}" target="_blank">
                                                                 <span>Link Out <i class="fas fa-external-link-alt"></i></span>
                                                             </a>
                                                        </div>
                                                    </div>
                                                    <hr>
                                                @endif
                                            @endforeach
                                        @elseif(count($entry->files()['posters']) == 0 && ($entry->posters_dropzone != null && $entry->posters_dropzone != 'undefined'))
                                            <div class="d-flex mt-3 mb-2">
                                                <div class="thumbnail-container me-4">
                                                    @if(strstr($entry->posters_dropzone, '.') == '.pdf')
                                                    <a href="{{asset('storage/'.$entry->posters_dropzone)}}">
                                                        <img class="file-entry"src="{{url('/images/download_pdf.png')}}" alt="">
                                                    </a>
                                                    @else
                                                    <img class="file-entry" src="{{asset('storage/'.$entry->posters_dropzone)}}" alt="">
                                                    @endif
                                                </div>
                                                <div>
                                                    <p>
                                                        <strong>Posters:</strong>
                                                        <span>{{--$entry->posters_dropzone--}}</span>
                                                    </p>

                                                    <?php $p_view = ""; ?>
                                                    @if(strstr($entry->posters_dropzone, '.') != '.pdf')
                                                        <?php $p_view = "view-file"; ?>
                                                    @endif
                                                    <a class="{{$p_view}} link-color" href="{{asset('storage/'.$entry->posters_dropzone)}}" data-lightbox="concept-board" data-title="{{$entry->posters_dropzone}}" target="_blank">
                                                        <span>View File <i class="fas fa-external-link-alt"></i></span>
                                                    </a>
                                                    <br/>
                                                    <a class="link-color" href="{{asset('storage/'.$entry->posters_dropzone)}}" target="_blank">
                                                        <span>Link Out <i class="fas fa-external-link-alt"></i></span>
                                                    </a>
                                                </div>
                                            </div>
                                            <hr>
                                        @endif

                                        @if(count($entry->files()['audio']) > 0)
                                            @foreach($entry->files()['audio'] as $a)
                                                @if($a->type == "audio_dropzone")
                                                    <div class="d-flex mt-3 mb-2">
                                                        <div class="thumbnail-container me-4">
                                                            <audio id="audio-{{$entry->id}}" controls>
                                                                <source src="{{asset('storage/'.$a->path)}}" type="audio/mpeg">
                                                                Your browser does not support the audio element.
                                                            </audio>
                                                        </div>
                                                        <div>
                                                            <p>
                                                                <strong>Audio:</strong>
                                                                <span>{{--$a->name--}}</span>
                                                            </p>
                                                        </div>
                                                    </div>
                                                    <hr>
                                                @endif
                                            @endforeach
                                        @elseif(count($entry->files()['audio']) == 0 && ($entry->audio_dropzone != null && $entry->audio_dropzone != 'undefined'))
                                            <div class="d-flex mt-3 mb-2">
                                                <div class="thumbnail-container me-4">
                                                    <audio id="audio-{{$entry->id}}" controls>
                                                        <source src="{{asset('storage/'.$entry->audio_dropzone)}}" type="audio/mpeg">
                                                        Your browser does not support the audio element.
                                                    </audio>
                                                </div>
                                                <div>
                                                    <p>
                                                        <strong>Audio:</strong>
                                                        <span>{{--$entry->audio_dropzone--}}</span>
                                                    </p>
                                                </div>
                                            </div>
                                            <hr>
                                        @endif

                                        @if(count($entry->files()['english_translation']) > 0)
                                            @foreach($entry->files()['english_translation'] as $et)
                                                @if($et->type == "english_translation_dropzone")
                                                    <div class="d-flex mt-3 mb-2">
                                                        <div class="thumbnail-container me-4">
                                                             @if(strstr($et->path, '.') == '.pdf')
                                                                <a href="{{asset('storage/'.$et->path)}}" target="_blank">
                                                                    <img class="file-entry"src="{{url('/images/download_pdf.png')}}" alt="">
                                                                </a>
                                                            @else
                                                                <img class="file-entry" src="{{asset('storage/'.$et->path)}}" alt="">
                                                            @endif
                                                        </div>
                                                        <div>
                                                            <p>
                                                                <strong>English Translation:</strong>
                                                                <span>{{--$et->name--}}</span>
                                                            </p>

                                                            <?php $et_view = ""; ?>
                                                            @if(strstr($et->path, '.') != '.pdf')
                                                                <?php $et_view = "view-file"; ?>
                                                            @endif
                                                            <a class="{{$et_view}} link-color" href="{{asset('storage/'.$et->path)}}" data-lightbox="english-translation" data-title="{{$et->name}}" target="_blank">
                                                                 <span>View File <i class="fas fa-external-link-alt"></i></span>
                                                             </a>
                                                             <br/>
                                                             <a class="link-color" href="{{asset('storage/'.$et->path)}}" target="_blank">
                                                                 <span>Link Out <i class="fas fa-external-link-alt"></i></span>
                                                             </a>
                                                        </div>
                                                    </div>
                                                    <hr>
                                                @endif
                                            @endforeach
                                        @elseif(count($entry->files()['english_translation']) == 0 && ($entry->english_translation_dropzone != null && $entry->english_translation_dropzone != 'undefined'))
                                            <div class="d-flex mt-3 mb-2">
                                                <div class="thumbnail-container me-4">
                                                    @if(strstr($entry->english_translation_dropzone, '.') == '.pdf')
                                                    <a href="{{asset('storage/'.$entry->english_translation_dropzone)}}">
                                                        <img class="file-entry"src="{{url('/images/download_pdf.png')}}" alt="">
                                                    </a>
                                                    @else
                                                    <img class="file-entry" src="{{asset('storage/'.$entry->english_translation_dropzone)}}" alt="">
                                                    @endif
                                                </div>
                                                <div>
                                                    <p>
                                                        <strong>English Translation:</strong>
                                                        <span>{{--$entry->english_translation_dropzone--}}</span>
                                                    </p>

                                                    <?php $et_view = ""; ?>
                                                    @if(strstr($entry->english_translation_dropzone, '.') != '.pdf')
                                                        <?php $et_view = "view-file"; ?>
                                                    @endif

                                                    <a class="{{$et_view}} link-color" href="{{asset('storage/'.$entry->english_translation_dropzone)}}" data-lightbox="concept-board" data-title="{{$entry->english_translation_dropzone}}" target="_blank">
                                                        <span>View File <i class="fas fa-external-link-alt"></i></span>
                                                    </a>
                                                    <br/>
                                                    <a class="link-color" href="{{asset('storage/'.$entry->english_translation_dropzone)}}" target="_blank">
                                                        <span>Link Out <i class="fas fa-external-link-alt"></i></span>
                                                    </a>
                                                </div>
                                            </div>
                                            <hr>
                                        @endif

                                        @if(count($entry->files()['demo_film']) > 0)
                                            @foreach($entry->files()['demo_film'] as $df)
                                                @if($df->type == "demo_film_dropzone")   
                                                    <div class="d-flex mt-3 mb-2">
                                                        <div class="thumbnail-container me-4">
                                                            <video class="file-entry" id="preview_case_study_video_dropzone" controls>
                                                                <source src="{{asset('storage/'.$df->path)}}">
                                                                Your browser does not support the video tag.
                                                            </video>
                                                        </div>
                                                        <div>
                                                            <p>
                                                                <strong>Demo Film:</strong>
                                                                <span>{{--$df->name--}}</span>
                                                            </p>
                                                            <a class="view-file link-color video-link" href="{{asset('storage/'.$df->path)}}" data-lightbox="demo-film">
                                                                 <span>View File <i class="fas fa-external-link-alt"></i></span>
                                                            </a>
                                                            <a class="link-color video-link" href="{{asset('storage/'.$df->path)}}" target="_blank">
                                                                 <span>Link Out <i class="fas fa-external-link-alt"></i></span>
                                                            </a>
                                                        </div>
                                                    </div>
                                                    <hr>
                                                @endif
                                            @endforeach
                                        @elseif(count($entry->files()['demo_film']) == 0 && ($entry->demo_film_dropzone != null && $entry->demo_film_dropzone != 'undefined'))
                                            <div class="d-flex mt-3 mb-2">
                                                <div class="thumbnail-container me-4">
                                                    <video class="file-entry" id="preview_case_study_video_dropzone" controls>
                                                        <source src="{{asset('storage/'.$entry->demo_film_dropzone)}}">
                                                        Your browser does not support the video tag.
                                                    </video>
                                                </div>
                                                <div>
                                                    <p>
                                                        <strong>Demo Film:</strong>
                                                        <span>{{--$entry->demo_film_dropzone--}}</span>
                                                    </p>
                                                    <a class="view-file link-color video-link" href="{{asset('storage/'.$entry->demo_film_dropzone)}}" data-lightbox="demo-film">
                                                         <span>View File <i class="fas fa-external-link-alt"></i></span>
                                                    </a>
                                                    <br/>
                                                    <a class="link-color video-link" href="{{asset('storage/'.$entry->demo_film_dropzone)}}" target="_blank">
                                                         <span>Link Out <i class="fas fa-external-link-alt"></i></span>
                                                    </a>
                                                </div>
                                            </div>
                                            <hr>
                                        @endif

                                        @if(count($entry->files()['jpeg_piece']) > 0)
                                            @foreach($entry->files()['jpeg_piece'] as $jp)
                                                @if($jp->type == "photo_piece_dropzone")
                                                    <div class="d-flex mt-3 mb-2">
                                                        <div class="thumbnail-container me-4">
                                                             @if(strstr($jp->path, '.') == '.pdf')
                                                                <a href="{{asset('storage/'.$jp->path)}}">
                                                                    <img class="jp-entry"src="{{url('/images/download_pdf.png')}}" alt="">
                                                                </a>
                                                            @else
                                                                <img class="file-entry" src="{{asset('storage/'.$jp->path)}}" alt="">
                                                            @endif
                                                        </div>
                                                        <div>
                                                            <p>
                                                                <strong>JPEG of the Piece:</strong>
                                                                <span>{{--$jp->name--}}</span>
                                                            </p>

                                                            <?php $jp_view = ""; ?>
                                                            @if(strstr($jp->path, '.') != '.pdf')
                                                                <?php $jp_view = "view-file"; ?>
                                                            @endif
                                                             <a class="{{$jp_view}} link-color" href="{{asset('storage/'.$jp->path)}}" data-lightbox="jpeg-piece" data-title="{{$jp->name}}" target="_blank">
                                                                 <span>View File <i class="fas fa-external-link-alt"></i></span>
                                                             </a>
                                                             <br/>
                                                             <a class="link-color" href="{{asset('storage/'.$jp->path)}}" target="_blank">
                                                                 <span>Link Out <i class="fas fa-external-link-alt"></i></span>
                                                             </a>
                                                        </div>
                                                    </div>
                                                    <hr>
                                                @endif
                                            @endforeach
                                        @elseif(count($entry->files()['jpeg_piece']) == 0 && ($entry->photo_piece_dropzone != null && $entry->photo_piece_dropzone != 'undefined'))
                                            <div class="d-flex mt-3 mb-2">
                                                <div class="thumbnail-container me-4">
                                                    @if(strstr($entry->photo_piece_dropzone, '.') == '.pdf')
                                                    <a href="{{asset('storage/'.$entry->photo_piece_dropzone)}}">
                                                        <img class="file-entry"src="{{url('/images/download_pdf.png')}}" alt="">
                                                    </a>
                                                    @else
                                                    <img class="file-entry" src="{{asset('storage/'.$entry->photo_piece_dropzone)}}" alt="">
                                                    @endif
                                                </div>
                                                <div>
                                                    <p>
                                                        <strong>JPEG of the Piece:</strong>
                                                        <span>{{--$entry->photo_piece_dropzone--}}</span>
                                                    </p>

                                                    <?php $jp_view = ""; ?>
                                                    @if(strstr($entry->photo_piece_dropzone, '.') != '.pdf')
                                                        <?php $jp_view = "view-file"; ?>
                                                    @endif
                                                    <a class="{{$jp_view}} link-color" href="{{asset('storage/'.$entry->photo_piece_dropzone)}}" data-lightbox="concept-board" data-title="{{$entry->photo_piece_dropzone}}" target="_blank">
                                                        <span>View File <i class="fas fa-external-link-alt"></i></span>
                                                    </a>
                                                    <br/>
                                                    <a class="link-color" href="{{asset('storage/'.$entry->photo_piece_dropzone)}}" target="_blank">
                                                        <span>Link Out <i class="fas fa-external-link-alt"></i></span>
                                                    </a>
                                                </div>
                                            </div>
                                            <hr>
                                        @endif

                                        @if(count($entry->files()['entry_piece']) > 0)
                                            @foreach($entry->files()['entry_piece'] as $ep)
                                                @if($ep->type == "entry_dropzone")
                                                    <div class="d-flex mt-3 mb-2">
                                                        <div class="thumbnail-container me-4">
                                                             @if(strstr($ep->path, '.') == '.pdf')
                                                                <a href="{{asset('storage/'.$ep->path)}}">
                                                                    <img class="file-entry"src="{{url('/images/download_pdf.png')}}" alt="">
                                                                </a>
                                                            @else
                                                                <img class="file-entry" src="{{asset('storage/'.$ep->path)}}" alt="">
                                                            @endif
                                                        </div>
                                                        <div>
                                                            <p>
                                                                <strong>Entry Piece:</strong>
                                                                <span>{{--$ep->name--}}</span>
                                                            </p>

                                                            <?php $ep_view = ""; ?>
                                                            @if(strstr($ep->path, '.') != '.pdf')
                                                                <?php $ep_view = "view-file"; ?>
                                                            @endif
                                                             <a class="{{$ep_view}} link-color" href="{{asset('storage/'.$ep->path)}}" data-lightbox="entry-piece" data-title="{{$ep->name}}" target="_blank">
                                                                 <span>View File <i class="fas fa-external-link-alt"></i></span>
                                                             </a>
                                                             <br/>
                                                             <a class="link-color" href="{{asset('storage/'.$ep->path)}}" target="_blank">
                                                                 <span>Link Out <i class="fas fa-external-link-alt"></i></span>
                                                             </a>
                                                        </div>
                                                    </div>
                                                    <hr>
                                                @endif
                                            @endforeach
                                        @elseif(count($entry->files()['entry_piece']) == 0 && ($entry->entry_dropzone != null && $entry->entry_dropzone != 'undefined'))
                                            <div class="d-flex mt-3 mb-2">
                                                <div class="thumbnail-container me-4">
                                                    @if(strstr($entry->entry_dropzone, '.') == '.pdf')
                                                    <a href="{{asset('storage/'.$entry->entry_dropzone)}}">
                                                        <img class="file-entry"src="{{url('/images/download_pdf.png')}}" alt="">
                                                    </a>
                                                    @else
                                                    <img class="file-entry" src="{{asset('storage/'.$entry->entry_dropzone)}}" alt="">
                                                    @endif
                                                </div>
                                                <div>
                                                    <p>
                                                        <strong>Entry Piece:</strong>
                                                        <span>{{--$entry->entry_dropzone--}}</span>
                                                    </p>

                                                    <?php $ep_view = ""; ?>
                                                    @if(strstr($entry->entry_dropzone, '.') != '.pdf')
                                                        <?php $ep_view = "view-file"; ?>
                                                    @endif
                                                    <a class="{{$ep_view}} link-color" href="{{asset('storage/'.$entry->entry_dropzone)}}" data-lightbox="concept-board" data-title="{{$entry->entry_dropzone}}" target="_blank">
                                                        <span>View File <i class="fas fa-external-link-alt"></i></span>
                                                    </a>
                                                    <br/>
                                                    <a class="link-color" href="{{asset('storage/'.$entry->entry_dropzone)}}" target="_blank">
                                                        <span>Link Out <i class="fas fa-external-link-alt"></i></span>
                                                    </a>
                                                </div>
                                            </div>
                                            <hr>
                                        @endif

                                    </div>

                                    <div class="link-container" style="overflow-wrap: break-word;"></div>
                                </div>

                                <div class="border-divider"></div>

                                <div class="px-65 py-40"> 
                                    <h3 class="block-title mb-20">Campaign details</h3>
                                    <div class="sub-label">Entry name: <span>{{$entry->entry_name}}</span></div>
                                    <div class="sub-label">Ad title/s: <span>{{$entry->ad_details}}</span></div>
                                    <div class="sub-label">Advertiser: <span>{{$entry->advertiser}}</span></div>
                                    <div class="sub-label mb-0">Brand: <span>{{$entry->brand}}</span></div>
                                </div>

                                <div class="border-divider"></div>

                                <div class="px-65 py-40">
                                    <h3 class="block-title mb-0">Written submission</h3>
                                    <div class="tabs">
                                        <!-- Nav tabs -->
                                        <ul class="nav nav-pills justify-content-start" id="myTab" role="tablist">
                                            @if($entry->objectives != 'N/A' && $entry->objectives != null && $entry->objectives != 'undefined')
                                            <li class="nav-item" role="presentation">
                                                <button class="nav-link" id="pills-objectives-tab{{$loop->index}}" data-bs-toggle="pill" data-bs-target="#pills-objectives{{$loop->index}}" type="button" role="tab" aria-controls="pills-objectives" aria-selected="false">Objectives</button>
                                            </li>
                                            @endif
                                            @if($entry->background != 'N/A' && $entry->background != null && $entry->background != 'undefined')
                                            <li class="nav-item" role="presentation">
                                                <button class="nav-link" id="pills-background-tab{{$loop->index}}" data-bs-toggle="pill" data-bs-target="#pills-background{{$loop->index}}" type="button" role="tab" aria-controls="pills-background" aria-selected="false">Background</button>
                                            </li>
                                            @endif
                                            @if($entry->strategy != 'N/A' && $entry->strategy != null && $entry->strategy != 'undefined')
                                            <li class="nav-item" role="presentation">
                                                <button class="nav-link" id="pills-strategy-tab{{$loop->index}}" data-bs-toggle="pill" data-bs-target="#pills-strategy{{$loop->index}}" type="button" role="tab" aria-controls="pills-strategy" aria-selected="false">Strategy</button>
                                            </li>
                                            @endif
                                            @if($entry->execution != 'N/A' && $entry->execution != null && $entry->execution != 'undefined')
                                            <li class="nav-item" role="presentation">
                                                <button class="nav-link" id="pills-execution-tab{{$loop->index}}" data-bs-toggle="pill" data-bs-target="#pills-execution{{$loop->index}}" type="button" role="tab" aria-controls="pills-execution" aria-selected="false">Execution</button>
                                            </li>
                                            @endif
                                            @if($entry->describe_strategy != 'N/A' || $entry->describe_strategy != null || $entry->describe_strategy != 'undefined') 
                                            <li class="nav-item" role="presentation">
                                                <button class="nav-link" id="pills-describe_strategy-tab{{$loop->index}}" data-bs-toggle="pill" data-bs-target="#pills-describe_strategy{{$loop->index}}" type="button" role="tab" aria-controls="pills-describe_strategy" aria-selected="false">Describe the strategy</button>
                                            </li>
                                            @endif
                                            @if($entry->solution != 'N/A' && $entry->solution != null && $entry->solution != 'undefined')
                                            <li class="nav-item" role="presentation">
                                                <button class="nav-link" id="pills-solution-tab{{$loop->index}}" data-bs-toggle="pill" data-bs-target="#pills-solution{{$loop->index}}" type="button" role="tab" aria-controls="pills-solution" aria-selected="false">Describe the idea / solution</button>
                                            </li>
                                            @endif
                                            @if($entry->creative_idea != 'N/A' && $entry->creative_idea != null && $entry->creative_idea != 'undefined')
                                            <li class="nav-item" role="presentation">
                                                <button class="nav-link" id="pills-creative_idea-tab{{$loop->index}}" data-bs-toggle="pill" data-bs-target="#pills-creative_idea{{$loop->index}}" type="button" role="tab" aria-controls="pills-creative_idea" aria-selected="false">Describe the creative idea</button>
                                            </li>
                                            @endif
                                            @if($entry->describe_execution != 'N/A' && $entry->describe_execution != null && $entry->describe_execution != 'undefined')
                                            <li class="nav-item" role="presentation">
                                                <button class="nav-link" id="pills-describe_execution-tab{{$loop->index}}" data-bs-toggle="pill" data-bs-target="#pills-describe_execution{{$loop->index}}" type="button" role="tab" aria-controls="pills-describe_execution" aria-selected="false">Describe the execution (implementation, timeline, placement, scale)</button>
                                            </li>
                                            @endif
                                            @if($entry->results != 'N/A' && $entry->results != null && $entry->results != 'undefined')
                                            <li class="nav-item" role="presentation">
                                                <button class="nav-link" id="pills-results-tab{{$loop->index}}" data-bs-toggle="pill" data-bs-target="#pills-results{{$loop->index}}" type="button" role="tab" aria-controls="pills-results" aria-selected="false">Results</button>
                                            </li>
                                            @endif
                                        </ul>
                                        <div class="tab-content" id="myTabContent">
                                            @if($entry->objectives != 'N/A' || $entry->objectives != null)
                                            <div class="tab-pane fade show active" id="pills-objectives{{$loop->index}}" role="tabpanel" aria-labelledby="pills-objectives-tab">
                                                <h4 class="tab-title">
                                                    Objectives
                                                </h4>
                                                <p><textarea class="form-control fs-18 textarea-ws" id="results" rows="9" style="resize: none; width: 100%; text-align: left; background-color: white;" data-kb="results" readonly>{!! $entry->objectives !!}</textarea></p>
                                            </div>
                                            @endif
                                            @if($entry->background != 'N/A' || $entry->background != null)
                                            <div class="tab-pane fade" id="pills-background{{$loop->index}}" role="tabpanel" aria-labelledby="pills-background-tab">
                                                <h4 class="tab-title">
                                                    Objectives
                                                </h4>
                                                <p><textarea class="form-control fs-18 textarea-ws" id="results" rows="9" style="resize: none; width: 100%; text-align: left; background-color: white;" data-kb="results" readonly>{!! $entry->background !!}</textarea></p>
                                            </div>
                                            @endif
                                            @if($entry->strategy != 'N/A' || $entry->strategy != null)
                                            <div class="tab-pane fade" id="pills-strategy{{$loop->index}}" role="tabpanel" aria-labelledby="pills-strategy-tab">
                                                <h4 class="tab-title">
                                                    Strategy
                                                </h4>
                                                <p><textarea class="form-control fs-18 textarea-ws" id="results" rows="9" style="resize: none; width: 100%; text-align: left; background-color: white;" data-kb="results" readonly>{!! $entry->strategy !!}</textarea></p>
                                            </div>
                                            @endif
                                            @if($entry->execution != 'N/A' || $entry->execution != null)
                                            <div class="tab-pane fade" id="pills-execution{{$loop->index}}" role="tabpanel" aria-labelledby="pills-execution-tab">
                                                <h4 class="tab-title">
                                                    Execution
                                                </h4>
                                                <p><textarea class="form-control fs-18 textarea-ws" id="results" rows="9" style="resize: none; width: 100%; text-align: left; background-color: white;" data-kb="results" readonly>{!! $entry->execution !!}</textarea></p>
                                            </div>
                                            @endif
                                            @if($entry->describe_strategy != 'N/A' || $entry->describe_strategy != null)
                                            <div class="tab-pane fade" id="pills-describe_strategy{{$loop->index}}" role="tabpanel" aria-labelledby="pills-describe_strategy-tab">
                                                <h4 class="tab-title">
                                                    Execution
                                                </h4>
                                                <p><textarea class="form-control fs-18 textarea-ws" id="results" rows="9" style="resize: none; width: 100%; text-align: left; background-color: white;" data-kb="results" readonly>{!! $entry->describe_strategy !!}</textarea></p>
                                            </div>                                            
                                            @endif
                                            @if($entry->solution != 'N/A' || $entry->solution != null)
                                            <div class="tab-pane fade" id="pills-solution{{$loop->index}}" role="tabpanel" aria-labelledby="pills-solution-tab">
                                                <h4 class="tab-title">
                                                    Describe the idea / solution
                                                </h4>
                                                <p><textarea class="form-control fs-18 textarea-ws" id="results" rows="9" style="resize: none; width: 100%; text-align: left; background-color: white;" data-kb="results" readonly>{!! $entry->solution !!}</textarea></p>
                                            </div>                                            
                                            @endif
                                            @if($entry->creative_idea != 'N/A' || $entry->creative_idea != null)
                                            <div class="tab-pane fade" id="pills-creative_idea{{$loop->index}}" role="tabpanel" aria-labelledby="pills-creative_idea-tab">
                                                <h4 class="tab-title">
                                                    Describe the creative idea
                                                </h4>
                                                <p><textarea class="form-control fs-18 textarea-ws" id="results" rows="9" style="resize: none; width: 100%; text-align: left; background-color: white;" data-kb="results" readonly>{!! $entry->creative_idea !!}</textarea></p>
                                            </div>
                                            @endif
                                            @if($entry->describe_execution != 'N/A' || $entry->describe_execution != null)
                                            <div class="tab-pane fade" id="pills-describe_execution{{$loop->index}}" role="tabpanel" aria-labelledby="pills-describe_execution-tab">
                                                <h4 class="tab-title">
                                                Describe the execution (implementation, timeline, placement, scale)
                                                </h4>
                                                <p><textarea class="form-control fs-18 textarea-ws" id="results" rows="9" style="resize: none; width: 100%; text-align: left; background-color: white;" data-kb="results" readonly>{!! $entry->describe_execution !!}</textarea></p>
                                            </div>
                                            @endif
                                            @if($entry->results != 'N/A' || $entry->results != null)
                                            <div class="tab-pane fade" id="pills-results{{$loop->index}}" role="tabpanel" aria-labelledby="pills-results-tab">
                                                <h4 class="tab-title">
                                                    Results
                                                </h4>
                                                <p><textarea class="form-control fs-18 textarea-ws" id="results" rows="9" style="resize: none; width: 100%; text-align: left; background-color: white;" data-kb="results" readonly>{!! $entry->results !!}</textarea></p>
                                            </div>
                                            @endif
                                        </div>
                                    </div>
                                </div>

                                <div class="px-65 py-40">
                                    <div class="row">
                                        <div class="col col-md-10">
                                            <div class="card pt-1">
                                                <div class="card-body d-flex">
                                                    <img src="{{url('/images/service.svg')}}" alt="service">
                                                    <div class="ms-3">
                                                        <span>In case you encounter a problem, please reach out to <a class="link-color" href="mailto:4asp@pldtdsl.net">4asp@pldtdsl.net</a>.</span><br>
                                                        <span>Do take a screenshot so we can assess and assist you better.  Happy judging!</span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                        
                                <div class="border-divider"></div>
                            </div>
                            @endforeach

                            <div class="tab-pane" id="v-pills-review" role="tabpanel" aria-labelledby="v-pills-review-tab">
                    
                                <div class="px-65 py-40">
                                    <div class="label d-flex justify-content-between mb-3">
                                        <span class="label-title">Round 1: {{$category->name}}</span>
                                        <!-- <div class="top-action">
                                            <i class="bi bi-check2 me-2"></i> <span>All changes saved</span>
                                        </div> -->
                                    </div>
                                    <h2 class="page-title mb-30">
                                        Review Votes
                                    </h2>
                                    <div class="entries-cards">
                                        @foreach($entries as $entry)
                                            <?php
                                                $custom_entry = preg_replace('/\s+/', '', strtolower($entry->entry_name));
                                                $custom_entry = str_replace('+','',str_replace('*','',str_replace(',','',str_replace(')','',str_replace('(','',str_replace('#','',str_replace('&','',str_replace('|','',$custom_entry))))))));
                                                $custom_entry = str_replace('.','',str_replace('-','',str_replace('!','',str_replace(':','',str_replace('"','',str_replace('\'','',str_replace('/','',$custom_entry))))))).$entry->uid;
                                                ?>
                                            <div class="custom-card">
                                                <div class="custom-card-col">
                                                    <div class="custom-card-col-info">
                                                        <h2>{{$entry->entry_name}}</h2>
                                                    </div>
                                                </div>
                                                <div class="custom-card-col2">
                                                    @if($entry->status_round1 != null)
                                                    <span class="status">
                                                        {{$entry->status_round1}}
                                                        {{$entry->reason_round1}}
                                                    </span>
                                                    @else
                                                    <span class="status text-danger vote-empty">
                                                        You haven't scored on this entry yet
                                                    </span>
                                                    @endif
                                                    <a class="edit-score" href="javascript:void(0)" data-tab="{{$custom_entry}}">
                                                        <span>Edit score</span>
                                                    </a> 
                                                </div>
                                            </div>
                                        @endforeach
                                    </div>
                                </div>

                                <div class="border-divider"></div>
                            </div>

                        </div>

                        @include('juror.components.actions')

                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="form">
        <div class="container-fluid px-0 foating-wrapper">
            <div class="row justify-content-center">
                <div class="col left-panel px-0" style="max-width:322px; background: transparent;"></div>
                <div class="col right-panel px-0">
                    <div class="d-flex justify-content-center">
                        <div class="floating mb-5">
                            <div class="form-check d-flex align-items-center">
                                <input class="form-check-input custom-checkbox" type="radio" name="status" id="flag" data-bs-toggle="modal" data-bs-target="#flagModal" value="flag">
                                <label class="form-check-label fs-18" style="margin-left:14px;" for="flag">
                                    <img src="/images/flags.svg" alt="" class="vote-icon">
                                    Flag
                                </label>
                            </div>
                            <div class="form-check d-flex align-items-center">
                                <input class="form-check-input custom-checkbox" type="radio" name="status" id="abstain"  data-bs-toggle="modal" data-bs-target="#abstainModal" value="abstain">
                                <label class="form-check-label fs-18" style="margin-left:14px;" for="abstain">
                                    <img src="/images/no-stopping.svg" alt="" class="vote-icon">
                                    Abstain
                                </label>
                            </div>
                            <div class="form-check d-flex align-items-center">
                                <input class="form-check-input custom-checkbox" type="radio" name="status" id="shortlist" value="in" onclick="saveVote();">
                                <label class="form-check-label fs-18" style="margin-left:14px;" for="shortlist">
                                    In
                                </label>
                            </div>
                            <div class="form-check d-flex align-items-center">
                                <input class="form-check-input custom-checkbox" type="radio" name="status" id="outofshow" value="out" onclick="saveVote();">
                                <label class="form-check-label fs-18" style="margin-left:14px;" for="outofshow">
                                    Out
                                </label>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Modals -->
    <div class="form">
        <div class="voting-modal modal fade" id="flagModal" tabindex="-1" aria-labelledby="flag" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body">
                        <div class="selection">
                            <p class="question">Select reason for flagging this entry</p>
                            <div id="invalid-flag-reason" class="invalid-feedback mb-3">Flag reason is required.</div>
                            <div class="form-check form-check-inline">
                                <!-- <input class="form-check-input custom-checkbox" name="flag" type="radio" value="wrong_category" id="flag1"> -->
                                <input class="form-check-input custom-checkbox" type="radio" name="flag" value="wrong_category" id="flag1">
                                <label class="form-check-label" for="flag1">Wrong category</label>
                            </div>
                            <div class="form-check form-check-inline">
                                <!-- <input class="form-check-input custom-checkbox" name="flag" type="radio" value="wrong_eligibility" id="flag2"> -->
                                <input class="form-check-input custom-checkbox" type="radio" name="flag" value="wrong_eligibility" id="flag2">
                                <label class="form-check-label" for="flag2">Wrong eligibility date</label>
                            </div>
                            <div class="form-check form-check-inline">
                                <!-- <input class="form-check-input custom-checkbox" name="flag" type="radio" value="others" id="flag3"> -->
                                <input class="form-check-input custom-checkbox" type="radio" name="flag" value="other_flag" id="flag3">
                                <label class="form-check-label" for="flag3">Others (Kindly specify):</label>
                            </div>
                            <textarea class="form-control" id="flag_reason" rows="3" style="resize: none; width: 100%; text-align: left;"></textarea>
                        </div>
                        <div class="text-center mt-40">
                            <button class="btn btn-primary primary small" id="btn_flag" style="min-width:262px; height: 48px;">Flag this entry</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="voting-modal modal fade" id="abstainModal" tabindex="-1" aria-labelledby="abstain" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body">
                        <div class="selection">
                            <p class="question">Select reason for abstaining from this entry</p>
                            <div id="invalid-abstain-reason" class="invalid-feedback mb-3">Abstain reason is required.</div>
                            <div class="form-check form-check-inline">
                                <input class="form-check-input custom-checkbox" name="abstain" type="radio" value="same_agency" id="abstain1">
                                <label class="form-check-label" for="abstain1">I am from the same agency / network</label>
                            </div>
                            <div class="form-check form-check-inline">
                                <input class="form-check-input custom-checkbox" name="abstain" type="radio" value="involved" id="abstain2">
                                <label class="form-check-label" for="abstain2">I am involved in this project</label>
                            </div>
                            <div class="form-check form-check-inline">
                                <input class="form-check-input custom-checkbox" name="abstain" type="radio" value="other_abstain" id="abstain3">
                                <label class="form-check-label" for="abstain3">Others (Kindly specify):</label>
                            </div>
                            <textarea class="form-control" id="abstain_reason" rows="3" style="resize: none; width: 100%; text-align: left;"></textarea>
                        </div>
                        <div class="text-center mt-40">
                            <button class="btn btn-primary primary small" id="btn_abstain" style="min-width:262px; height: 48px;">Abstain from voting</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="voting-modal modal fade" id="errorVoteModal" tabindex="-1" aria-labelledby="abstain" aria-hidden="true">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body">
                        <div class="selection">
                            <p class="question">Instructions for juror voting</p>
                            <ul>
                               <li><strong>Step 1.</strong> Choose vote status on the middle pop-up.</li>
                               <li><strong>Step 2.</strong> If the vote status is either flag or abstain, reason is required.</li> 
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="voting-modal modal fade" id="successVoteModal" tabindex="-1" aria-labelledby="abstain" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body">
                        <div class="selection">
                            <p class="question text-center">Entry voted successfully!</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="voting-modal modal fade" id="notCompleteVoteModal" tabindex="-1" aria-labelledby="abstain" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <div class="selection">
                        <p class="question text-center">Complete the entry voting to continue.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @include('juror.components.footer')
@push('custom-js')
<script>
    $(document).ready(activateQuery())

    var entry_id = 0;
    var reason = "";
    var main;
    var thumbnails;

    function activateQuery() {
        getVoteStatus($('ul.nav-pills>li:first-child').attr('data-id'))

        activateMagnificPopup($('ul.nav-pills>li:first-child').attr('data-id'))

        $(document).on('click', '#v-pill-tab > li.nav-link', function() {
            entry_id = $(this).attr('data-id');
            $('#v-pill-tab > .nav-link').not(this).removeClass('active');
            $(this).addClass('active');
            setTimeout(function() {
                // activateCarousel(entry_id);
                activateMagnificPopup(entry_id)
                //$('.entries-cards').html("")
                getPreview();
            }, 1000)
            getVoteStatus(entry_id)
        })

        var first_entry_id = $( "li" ).first().attr('data-id');
        $('.btn_previous').children().hide()
        // activateCarousel(first_entry_id);

    }

    function activateMagnificPopup(id) {

        $('.magnific-container-'+id).magnificPopup({
            delegate: '.view-file',
            type: 'image',
            gallery: {
                enabled: true,
                navigateByImgClick: true,
            },
            callbacks: {
                elementParse: function(item) {
                    var ext = item.src.split('.').pop();
                    if (ext == "mp4" || ext == "MP4" || ext == "pdf") {
                        item.type = 'iframe'
                    } else if (ext == "jpeg" || ext == "jpeg" || ext == "JPG" || ext == "JPEG"){
                        item.type = 'image'
                    }
                }
            }
        });
    }

    function activateCarousel(id) {
        if (id != null) {
            main = new Splide( '.splide'+id, {
            type       : 'fade',
            cover      : true,
            rewind     : true,
            pagination : false,
            heightRatio: 0.5,
            arrows    : false
            } );

            thumbnails = new Splide( '#thumbnail-slider'+id, {
            fixedWidth  : 100,
            fixedHeight : 60,
            gap         : 10,
            rewind      : true,
            pagination  : false,
            cover       : true,
            isNavigation: true,
            breakpoints : {
              600: {
                fixedWidth : 60,
                fixedHeight: 44,
              },
            },
            } );

            main.sync( thumbnails );
            main.mount();
            thumbnails.mount();
        }
    }

    $(document).on('click', 'input[name="flag"]', function() {
        // if ($('input[name="flag"]:checked').val() == "other_flag") {
        //     $('#flag_reason').prop('disabled', false).removeClass('disabled')
        // } else {
        //     $('#flag_reason').prop('disabled', true).addClass('disabled')
        // }
    })

    $(document).on('click', 'input[name="abstain"]', function() {
        // if ($('input[name="abstain"]:checked').val() == "other_abstain") {
        //     $('#abstain_reason').prop('disabled', false).removeClass('disabled')
        // } else {
        //     $('#abstain_reason').prop('disabled', true).addClass('disabled')
        // }
    })

    $(document).on('click', '#btn_flag', function() {
        if (typeof $('input[name="flag"]:checked').val() == "undefined") {
           $('input[name="flag"]').addClass('is-invalid')
           $('#invalid-flag-reason').show()
        } else {
            if ($('input[name="flag"]:checked').val() == "other_flag") {
                if ($('#flag_reason').val() == "") {
                   $('input[name="flag"]').addClass('is-invalid')
                   $('#invalid-flag-reason').show()
                } else {
                   saveVote()
                   reason = $('#flag_reason').val()
                   $('input[name="flag"]').removeClass('is-invalid')
                   $('#invalid-flag-reason').hide()
                   $('#flagModal').modal('hide')
               }
            } else {
               saveVote()
               reason = ""
               $('input[name="flag"]').removeClass('is-invalid')
               $('#invalid-flag-reason').hide()
               $('#flagModal').modal('hide')
            }            
        }
    })

    $(document).on('click', '#btn_abstain', function() {
        if (typeof $('input[name="abstain"]:checked').val() == "undefined") {
           $('input[name="abstain"]').addClass('is-invalid')
           $('#invalid-abstain-reason').show()
        } else {
            if ($('input[name="abstain"]:checked').val() == "other_abstain") {
                if ($('#abstain_reason').val() == "") {
                   $('input[name="abstain"]').addClass('is-invalid')
                   $('#invalid-abstain-reason').show()
                } else {
                   saveVote()
                   reason = $('#abstain_reason').val()
                   $('input[name="abstain"]').removeClass('is-invalid')
                   $('#invalid-abstain-reason').hide()
                   $('#abstainModal').modal('hide')
               }
            } else {
               saveVote()
               reason = ""
               $('input[name="abstain"]').removeClass('is-invalid')
               $('#invalid-abstain-reason').hide()
               $('#abstainModal').modal('hide')
            }            
        }
    })

    $(document).on('click', '#v-pill-tab > .nav-link', function() {
        $('.link-container').html('');
        var counter = parseInt($('#v-pill-tab>li.nav-link[class="active"]').attr('data-counter'))

        if ($(this).attr('data-counter') == 0) {
            $('.btn_previous').children().hide()
        } else {
            $('.btn_previous').children().show()
        }

        setTimeout(function() {
            $('#pills-campaign-summary'+counter).addClass('show active')
            $('video').trigger('pause');
            $('audio').trigger('pause');
        }, 1000)

        if ($(this).attr('id') == "v-pills-review-tab") {

            $.LoadingOverlay("show", {
                image: '{{url("/images/loading1.png")}}',
                text: 'We are now loading the entry. Kindly wait for a couple of seconds!',
                textResizeFactor: '0.3',
            },100);

            $('.btn_next').hide();
            $('.btn_submit').show();
            $('.form').hide()

            setTimeout(function() {
                $.LoadingOverlay("hide");
            }, 3000)

        } else {
            $('.btn_next').show();
            $('.btn_submit').hide();
            $('.form').show()
        }

        $('#abstain_reason').val("")
        $('#flag_reason').val("")

        if ($(this).hasClass('done')) {
            entry_id = $(this).attr('data-id')
            $('input[type="radio"]').removeAttr('checked')

            getVoteStatus(entry_id)
        } else {
            $('input[type="radio"]').prop('checked', false)
        }

        activateMagnificPopup($(this).attr('data-id'))
    })

    $(document).on('click', '.btn_previous', function() {
        $('.link-container').html('');
        $('video').trigger('pause');
        $('audio').trigger('pause');

        $('#abstain_reason').val("")
        $('#flag_reason').val("")

        entry_id = $('li.nav-link.active').prev().attr('data-id')

        setTimeout(function() {
            // activateCarousel(entry_id)
            activateMagnificPopup(entry_id)
            $('#myTab>li>button').removeClass('active')
            $('#myTab>li>button[aria-controls="pills-campaign-summary"]').addClass('active')
        }, 1000)
        
        entry_id = $('li.nav-link.active').prev().attr('data-id')
        setTimeout(function() {
            getVoteStatus(entry_id)
        }, 1000)

        var counter = parseInt($('li.nav-link.active').attr('data-counter'))
        counter -= 1

        setTimeout(function() {
            $('#pills-campaign-summary'+counter).addClass('show active')
        }, 1000)

        if (counter >= 0) {
            $('li.nav-link.active').removeClass('active')
            $('.nav-link[data-counter="'+counter+'"]').addClass('active')
            $('#v-pills-tabContent > .tab-pane').removeClass('show active')
            $('.tab-pane[data-tab="'+counter+'"]').addClass('show active')
        }

        if (counter == 0) {
            $('.btn_previous').children().hide()
        }

        //$('.entries-cards').html("")
        getPreview();

        $('.btn_submit').hide()
        $('.btn_next').show()
        $('.form').show()
    })

    $(document).on('click', '.btn_next', function() {
        $('.link-container').html('');
        $.LoadingOverlay("show", {
            image: '{{url("/images/loading1.png")}}',
            text: 'We are now loading the entry. Kindly wait for a couple of seconds!',
            textResizeFactor: '0.3',
        },100);

        $('video').trigger('pause');
        $('audio').trigger('pause');

        entry_id = $('li.nav-link.active').attr('data-id')
        // var checker = 0
        // if (entry_id == 0) {
        //     checker++;
        //     $('#errorVoteModal').modal('show')
        //     $.LoadingOverlay("hide");
        // }

        // if (typeof $('input[name="status"]:checked').val() == "undefined") {
        //     checker++;
        //     $('#errorVoteModal').modal('show')
        //     $.LoadingOverlay("hide");
        // }

        // if ($('input[name="status"]:checked').val() == "flag") {
        //     if (typeof $('input[name="flag"]:checked').val() == "undefined") {
        //         checker++;
        //         $('#errorVoteModal').modal('show')
        //         $.LoadingOverlay("hide");
        //     }
        // }

        // if ($('input[name="status"]:checked').val() == "abstain") {
        //     if (typeof $('input[name="abstain"]:checked').val() == "undefined") {
        //         checker++;
        //         $('#errorVoteModal').modal('show')
        //         $.LoadingOverlay("hide");
        //     }
        // }

        // if ($('input[name="flag"]:checked').val() == "other_flag") {
        //     reason = $('#flag_reason').val()
        // } else {
        //     reason = $('input[name="flag"]:checked').val()
        // }

        // if ($('input[name="abstain"]:checked').val() == "other_abstain") {
        //     reason = $('#abstain_reason').val()
        // } else {
        //     reason = $('input[name="abstain"]:checked').val()
        // }

        // if (checker == 0) {
        //     let params = {
        //         '_token'        : '{{ csrf_token() }}',
        //         'entry_id'      : entry_id,
        //         'status'        : $('input[name="status"]:checked').val(),
        //         'reason'        : reason,
        //     }

        //     fetch("{{url('judge/submit-vote-round1')}}", {
        //         method: 'POST',
        //         headers: {
        //             'Content-Type': 'application/json'
        //         },
        //         body: JSON.stringify(params)
        //     })
        //     .then(response => response.json())
        //     .then((e) => {
        //         if (e.status_code == 200) {
                    $('input[type="radio"]').prop('checked', false); 
                    var counter = parseInt($('li.nav-link.active').attr('data-counter'))
                    entry_id = $('li.nav-link.active').attr('data-id')
                    counter += 1
                    var last_item = parseInt("{{count($entries)-1}}")

                    setTimeout(function() {
                        $('#pills-campaign-summary'+counter).addClass('show active')
                    }, 1000)

                    if (counter > last_item) {
                        $('li.nav-link').removeClass('active')
                        $('.tab-pane').removeClass('show active')

                        setTimeout(function() {
                            $('#v-pills-review-tab').addClass('active')
                            $('#v-pills-review').addClass('show active')
                            $('.form').hide()
                            $('.btn_submit').show()
                            $('.btn_next').hide()
                            //$('.entries-cards').html("")
                            getPreview();
                        }, 1000)
                    }

                    $('li.nav-link.active').removeClass('active')
                    $('.nav-link[data-counter="'+counter+'"]').addClass('active')
                    // $('.nav-link[data-id="'+entry_id+'"]').addClass('done')
                    entry_id = $('li.nav-link.active').attr('data-id')
                    // activateCarousel(entry_id)
                    activateMagnificPopup(entry_id)

                    $('.tab-pane').removeClass('show active')
                    $('.tab-pane[data-tab="'+counter+'"]').addClass('show active')

                    // entry_id = 0
                    $('#flag_reason').val("")
                    $('#abstain_reason').val("")
                    // $('#successVoteModal').modal('show')
                    setTimeout(function() {
                        $.LoadingOverlay("hide");
                        // $('#successVoteModal').modal('hide')
                    },1000)
        //         }
        //     })
        // }

        $('#abstain_reason').val("")
        $('#flag_reason').val("")
        
        if ($('li.nav-link.active').hasClass('done')) {
            entry_id = $('li.nav-link.active').next().attr('data-id')
            setTimeout(function() {
                $('input[type="radio"]').removeAttr('checked')
            }, 1000)

        }
        
        

        entry_id = $('li.nav-link.active').attr('data-id');

        /* NEXT BUTTON FOR DISPLAYING ENTRIES ONLY */
        /* AND GET VOTES STATUS*/
        getVoteStatus(entry_id);
        $('.btn_previous').children().show()

    })

    function saveVote() {
        $.LoadingOverlay("show", {
            image: '{{url("/images/loading1.png")}}',
            text: 'We are now loading the entry. Kindly wait for a couple of seconds!',
            textResizeFactor: '0.3',
        },100);

        entry_id = $('li.nav-link.active').attr('data-id');

        var checker = 0
        if (entry_id == 0) {
            checker++;
            // $('#errorVoteModal').modal('show')
            $.LoadingOverlay("hide");
        }

        if (typeof $('input[name="status"]:checked').val() == "undefined") {
            checker++;
            // $('#errorVoteModal').modal('show')
            $.LoadingOverlay("hide");
        }

        if ($('input[name="status"]:checked').val() == "flag") {
            if (typeof $('input[name="flag"]:checked').val() == "undefined") {
                checker++;
                // $('#errorVoteModal').modal('show')
                $.LoadingOverlay("hide");
            }
        }

        if ($('input[name="status"]:checked').val() == "abstain") {
            if (typeof $('input[name="abstain"]:checked').val() == "undefined") {
                checker++;
                // $('#errorVoteModal').modal('show')
                $.LoadingOverlay("hide");
            }
        }

        if ($('input[name="flag"]').is(':checked')) {
            if ($('input[name="flag"]:checked').val() == "other_flag") {
                reason = $('#flag_reason').val()
            } else {
                reason = $('input[name="flag"]:checked').val()
            }
        }

        if ($('input[name="abstain"]').is(':checked')) {
            if ($('input[name="abstain"]:checked').val() == "other_abstain") {
                reason = $('#abstain_reason').val()
            } else {
                reason = $('input[name="abstain"]:checked').val()
            }
        }


        if (checker == 0) {
            let params = {
                '_token'        : '{{ csrf_token() }}',
                'entry_id'      : entry_id,
                'status'        : $('input[name="status"]:checked').val(),
                'reason'        : reason,
            }

            fetch("{{url('judge/submit-vote-round1')}}", {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify(params)
            })
            .then(response => response.json())
            .then((e) => {
                if (e.status_code == 200) {
                    $('input[type="radio"]').prop('checked', false); 
                    var counter = parseInt($('li.nav-link.active').attr('data-counter'))
                    entry_id = $('li.nav-link.active').attr('data-id')
                    counter += 1
                    var last_item = parseInt("{{count($entries)-1}}")

                    setTimeout(function() {
                        $('#pills-campaign-summary'+counter).addClass('show active')
                    }, 1000)

                    if (counter > last_item) {
                        $('li.nav-link').removeClass('active')
                        $('.tab-pane').removeClass('show active')

                        setTimeout(function() {
                            $('#v-pills-review-tab').addClass('active')
                            $('#v-pills-review').addClass('show active')
                            $('.form').hide()
                            $('.btn_submit').show()
                            $('.btn_next').hide()
                            //$('.entries-cards').html("")
                            getPreview();

                            $('#flagModal').modal('hide')
                            $('#abstainModal').modal('hide')
                        }, 1000)
                    }

                    $('li.nav-link.active').removeClass('active')
                    $('.nav-link[data-counter="'+counter+'"]').addClass('active')
                    $('.nav-link[data-id="'+entry_id+'"]').addClass('done')
                    entry_id = $('li.nav-link.active').attr('data-id')
                    activateMagnificPopup(entry_id)

                    $('.tab-pane').removeClass('show active')
                    $('.tab-pane[data-tab="'+counter+'"]').addClass('show active')

                    // entry_id = 0
                    $('#flag_reason').val("")
                    $('#abstain_reason').val("")
                    setTimeout(function() {
                        $.LoadingOverlay("hide");
                    },5000)
                }
            })
        }

        $('#abstain_reason').val("")
        $('#flag_reason').val("")
        $('.link-container').html("");
        
        if ($('li.nav-link.active').hasClass('done')) {
            entry_id = $('li.nav-link.active').next().attr('data-id')
            setTimeout(function() {
                $('input[type="radio"]').removeAttr('checked')
                getVoteStatus(entry_id)
            }, 1000)

        }

        $('.btn_previous').children().show()
    
    }

    function getVoteStatus(entry_id) {
        let params = {
            '_token'   : '{{ csrf_token() }}',
            'entry_id' : entry_id
        }

        fetch("{{url('judge/vote-status-round1')}}", {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(params)
        })
        .then(response => response.json())
        .then((e) => {
            if (e.status_code == 200) {
                $('.link-container').html('');

                $.LoadingOverlay("show", {
                    image: '{{url("/images/loading1.png")}}',
                    text: 'We are now loading the entry. Kindly wait for a couple of seconds!',
                    textResizeFactor: '0.3',
                },100);

                setTimeout(function() {
                    console.log(e.data);
                    if(e.data.vote_status != null) {
                        var status = e.data.vote_status.status
                        var reason = e.data.vote_status.reason
                        var is_submitted = e.data.vote_status.submitted

                        if(is_submitted == 1) {
                            $('input[type="radio"]').prop('disabled', true).addClass('disabled')
                        }

                        $('input[value="'+status+'"]').prop('checked', true)

                        if (status == "flag") {
                            if (reason == "wrong_category" || reason == "wrong_eligibility") {
                                $('input[value="'+reason+'"]').prop('checked', true)
                            } else {
                                $('input[value="other_flag"]').prop('checked', true)
                                $('#flag_reason').val(reason)
                            }
                        }

                        if (status == "abstain") {
                            if (reason == "same_agency" || reason == "involved") {
                                $('input[value="'+reason+'"]').prop('checked', true)
                            } else {
                                $('input[value="other_abstain"]').prop('checked', true)
                                $('#abstain_reason').val(reason)
                            }
                        }
                    } else {
                        $('input[type="radio"]').prop('checked', false); 
                        $('input[type="radio"]').prop('disabled', false).removeClass('disabled')
                    }

                    var html = "";
                    if (e.data.references.length > 0) {
                        html += '<h4 class="tab-title mt-5 mb-3">Online links available</h4>'
                        html += '<table>'
                        $.each(e.data.references, function(index, ref) {
                            html += '<tr>'
                            html += '<td>'+ref.link.slice(0,50)+'</td>'
                            html += '<td><a class="view-file link-color" style="margin-left: 20px" href="'+ref.link+'" target="_blank"><span>View Link <i class="fas fa-external-link-alt"></i></span></a><td>'
                            html += '</tr>'
                        })
                        html += '</table>'

                        $('.link-container').html(html)
                    } else {
                        html += '<h4 class="tab-title mt-5 mb-3">Online links available</h4>'
                        html += '<p>None</p>'
                        $('.link-container').html(html)
                    }

                    setTimeout(function() {
                        $.LoadingOverlay("hide");
                    },5000)
                }, 1000)
            }
        })
    }

    function getPreview() {
        $('.entries-cards').html("")

        let params = {
            '_token'        : '{{ csrf_token() }}',
            'cat_id'        : "{{Request::segment(4)}}",
            'sub_id'        : "{{Request::segment(6)}}",
        }

        console.log(params);

        fetch("{{url('judge/vote-round1')}}", {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(params)
        })
        .then(response => response.json())
        .then((e) => {
            if (e.status_code == 200) {
                var html = ""
                $.each(e.data.entries, function(index, entry) {
                    html += '<div class="custom-card">';
                    html += '    <div class="custom-card-col">';
                    html += '        <div class="custom-card-col-info">';
                    html += '            <h2>'+entry.entry_name+'</h2>';
                    html += '        </div>';
                    html += '    </div>';
                    html += '    <div class="custom-card-col2">';

                    if (entry.status_round1 != null) {
                        html += '        <span class="status" style="color:#333;">';
                        html += entry.status_round1
                        if (entry.reason_round1 != null) {
                            html += ""+entry.reason_round1
                        }
                        html += '        </span>';
                    }

                    else {
                        html += '        <span class="status text-danger vote-empty">';
                        html += '            You haven\'t scored on this entry yet';
                        html += '        </span>';
                    }

                    var custom_entry = entry.entry_name+entry.uid
                    custom_entry = custom_entry.replace(/\s/g, '').toLowerCase();
                    custom_entry = custom_entry.replace("|", "").replace("&", "").replace("#", "").replace("(", "").replace(")", "").replace(",", "").replace("'", "").replace("/", "").replace('"', '').replace("!","").replace(":","");
                    
                    html += '        <a class="edit-score" href="javascript:void(0)" data-tab="'+custom_entry+'" data-counter="'+index+'">';
                    html += '            <span>Edit score</span>';
                    html += '        </a> ';
                    html += '    </div>';
                    html += '</div>';

                    if (entry.submitted_round1 == 1) {
                        $('.btn_submit>button').text('BACK TO DASHBOARD')
                    } else {
                        $('.btn_submit>button').text('SUBMIT Votes') 
                    }
                })

                $('.entries-cards').html(html)
            }
        })
    }

    $(document).on('click', '.edit-score', function() {
        var entry_tab = $(this).attr('data-tab')
        // entry_tab = entry_tab.replace("|", "").replace("&", "");

        $('.form').show()
        $('.btn_next').show()
        $('.btn_submit').hide()
        $('#v-pills-'+entry_tab+'-tab').addClass('active')
        $('.nav-link').not($('#v-pills-'+entry_tab+'-tab')).removeClass('active')

        $('#v-pills-'+entry_tab).addClass('show active')
        $('.tab-pane').not($('#v-pills-'+entry_tab)).removeClass('show active')
        entry_id = $('.nav-link.active').attr('data-id')
        setTimeout(function() {
            // activateCarousel(entry_id)
            activateMagnificPopup(entry_id)
            //$('.entries-cards').html("")
            getPreview();
            getVoteStatus(entry_id)
        }, 1000)

        if($(this).attr('data-counter') == 0) {
            $('.btn_previous').children().hide()
        } else {
            $('.btn_previous').children().show()
        }
    })

    $(document).on('click', '.btn_submit', function() {
        if ($(this).find('button').text() != "BACK TO DASHBOARD") {
             var empty_vote = $('.custom-card-col2 .vote-empty').length;

            if (empty_vote == 0) {
                var entry_ids = [];
                $.each($('.nav-link'), function() {
                    if ($(this).hasClass('done')) {
                        entry_ids.push($(this).attr('data-id'))
                    }
                })

                let params = {
                    '_token'        : '{{ csrf_token() }}',
                    'entry_ids'      : entry_ids,
                }

                fetch("{{url('/judge/submit-allvote-round1')}}", {
                    method: 'POST',
                    headers: {
                        'Content-Type': 'application/json'
                    },
                    body: JSON.stringify(params)
                })
                .then(response => response.json())
                .then((e) => {
                    if (e.status_code == 200) {
                        setTimeout(function() {
                            location.href = "{{url('/judge/success-round-1')}}"
                        },1000)
                    }
                })
            } else {
                setTimeout(function() {
                    $('#notCompleteVoteModal').modal('show')
                }, 1000)
            }
        } else {
            location.href = "{{url('/juror/dashboard')}}"
        }
    })
</script>
@endpush
@endsection