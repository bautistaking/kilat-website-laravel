@extends('base')  
@push('styles')
    <style>
        .spinner-grow,
        .loading-label {
            display: none;
        }        
    </style>
@endpush
@section('content')
    @include('juror.components.header')
    <div class="wrapper-default pt72 fullWidth">
        <div class="dashboard judging">
            <div class="container-fluid ps-0">
                <div class="row justify-content-center minHeight">
                    @include('juror.components.sidebar')
                    <div class="col right-panel px-0">
                        @include('juror.components.actions')
                        <div class="border-divider"></div>
                        <div class="px-65 py-40">
                            <div class="label d-flex justify-content-between mb-3">
                                <span class="label-title">Round 2:  {{$category->name}}</span>
                                <!-- <div class="top-action">
                                    <i class="bi bi-check2 me-2"></i> <span>All changes saved</span>
                                </div> -->
                            </div>
                            <h2 class="page-title mb-30">
                            Review Scores
                            </h2>
                            <div class="entries-cards">
                                <?php for ($i = 1; $i <= 8; $i++) : ?>
                                <div class="custom-card">
                                    <div class="custom-card-col">
                                        <img src="assets/img/small-img.png" alt="">
                                        <div class="custom-card-col-info">
                                            <h2>Entry Name here lorem ipsum dolor sit amet<?php echo $i ?></h2>
                                        </div>
                                    </div>
                                    <div class="custom-card-col2">
                                        <span class="status"><span class="pe-2 fw-bold">4</span> Gold</span>
                                        <a href="#">
                                            <span>Edit score</span>
                                        </a>
                                    </div>
                                </div>
                                <?php endfor; ?>
                            </div>
                        </div>
                        
                        <div class="border-divider"></div>
                        @include('juror.components.actions')
                    </div>
                </div>
            </div>
        </div>
    </div>

    @include('juror.components.footer')
@push('custom-js')
<script>
    $(document).ready(activateQuery())

    function activateQuery() {
        
    }
</script>
@endpush
@endsection