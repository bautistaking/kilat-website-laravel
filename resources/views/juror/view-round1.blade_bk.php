@extends('base')  
@push('styles')
    <style>
        .spinner-grow,
        .loading-label {
            display: none;
        }

        .nav-tabs {
            border-bottom: none;
        }

        .tab-content {
            margin-top: 0px !important;
        }

        .btn_submit,
        .btn_back {
            display: none;
        }

        .slick-track {
            width: 100% !important;
        }

        .video-wrapper {
            margin-bottom: -325px;
        }

        .main-file {
            width: 800px !important;
            margin: auto;
            display: block;
        }

        .file-entry {
            width: 206px;
            border-radius: 5px;
        }

        #image-slider .splide__list {
            background: #eee;
            padding: 10px !important;
        }

        #image-slider li {
            width: 500px !important;
            margin: auto;
            background: none !important;
            height: auto !important;
        }

        #image-slider img,
        #image-slider video,
        #image-slider audio,
        #image-slider iframe {
            width: 500px !important;
            display: block !important;
            border-radius: 10px !important;
        }

        .splide__arrow {
            z-index: 5;
        }

        .foating-wrapper {
            bottom: 0px !important;
        }
    </style>
@endpush
@section('content')
    @include('juror.components.header')
    <div class="wrapper-default pt72 fullWidth">
        <div class="dashboard judging">
            <div class="container-fluid ps-0">
                <div class="row justify-content-center minHeight">
                    @include('juror.components.sidebar1')
                    <div class="col right-panel px-0">
                        @include('juror.components.actions')
                        <div class="border-divider"></div>
                        
                        <div class="tab-content" id="v-pills-tabContent">
                            @foreach($entries as $entry)
                            <?php
                                $custom_entry = preg_replace('/\s+/', '', strtolower($entry->entry_name))
                            ?>
                            <div class="tab-pane fade @if($loop->index == 0) ? show active :  @endif " id="v-pills-{{$custom_entry}}" role="tabpanel" aria-labelledby="v-pills-{{$custom_entry}}-tab" data-tab="{{$loop->index}}">
                                <div class="px-65 py-40">
                                    <div class="label d-flex justify-content-between mb-3">
                                        <span class="label-title">Round 1: {{$category->name}}</span>
                                        <p class="position-absolute" style="color:#384753;margin-top: 25px;font-weight:bold;">{{$subcategory->name}}</p>
                                        <!-- <div class="top-action">
                                            <i class="bi bi-check2 me-2"></i> <span>All changes saved</span>
                                        </div> -->
                                    </div>
                                    <h2 class="page-title mb-0 mt-4">{{$entry->entry_name}}</h2>

                                    <div id="image-slider" class="splide splide{{$entry->id}} mt-3">
                                        <div class="splide__track">
                                            @if(count($entry->files()) > 0)
                                                <ul class="splide__list">
                                                    @foreach($entry->files() as $file)
                                                        @if($file->type == "Concept Board" || $file->type == "Posters")
                                                            <li class="splide__slide">
                                                                 @if(strstr($file->path, '.') == '.pdf')
                                                                    <a href="{{asset('storage/'.$file->path)}}">
                                                                    <img class="file-entry"src="{{url('/images/download_pdf.png')}}" alt="">
                                                                </a>
                                                                @else
                                                                    <img class="file-entry" src="{{asset('storage/'.$file->path)}}" alt="">
                                                                @endif
                                                            </li>
                                                        @elseif($file->type == "Film")   
                                                            <li class="splide__slide">
                                                                <video class="file-entry" id="preview_case_study_video_dropzone" controls>
                                                                    <source src="{{asset('storage/'.$file->path)}}">
                                                                    Your browser does not support the video tag.
                                                                </video>
                                                            </li>
                                                        @elseif($file->type == "Audio")
                                                            <li class="splide__slide">
                                                                <audio controls>
                                                                    <source src="{{asset('storage/'.$file->path)}}" type="audio/mpeg">
                                                                    Your browser does not support the audio element.
                                                                </audio>
                                                            </li>
                                                        @endif
                                                    @endforeach
                                                </ul>
                                            @else
                                                <ul class="splide__list">
                                                    @if($entry->concept_board_dropzone != null)
                                                    <li class="splide__slide">
                                                        <img class="file-entry" id="preview_concept_board_dropzone" src="{{asset('storage/'.$entry->concept_board_dropzone)}}" alt="">
                                                    </li>
                                                    @endif

                                                    @if($entry->case_study_video_dropzone != null)
                                                    <li class="splide__slide">
                                                        <video class="file-entry" id="preview_case_study_video_dropzone" controls>
                                                            <source src="{{asset('storage/'.$entry->case_study_video_dropzone)}}">
                                                            Your browser does not support the video tag.
                                                        </video>
                                                    </li>
                                                    @endif

                                                    @if($entry->written_case_dropzone != null)
                                                    <li class="splide__slide">
                                                        <img class="file-entry" id="written_case_dropzone" src="{{asset('storage/'.$entry->written_case_dropzone)}}" alt="">
                                                    </li>
                                                    @endif

                                                    @if($entry->asc_clearance_dropzone != null)
                                                    <li class="splide__slide">
                                                        @if(strstr($entry->asc_clearance_dropzone, '.') == '.pdf')
                                                        <a href="{{asset('storage/'.$entry->asc_clearance_dropzone)}}">
                                                            <img class="file-entry" id="preview_asc_clearance_dropzone" src="{{url('/images/download_pdf.png')}}" alt="">
                                                        </a>
                                                        @else
                                                        <img class="file-entry" id="preview_asc_clearance_dropzone" src="{{asset('storage/'.$entry->asc_clearance_dropzone)}}" alt="">
                                                        @endif
                                                    </li>
                                                    @endif

                                                    @if($entry->media_certification_dropzone != null)
                                                    <li class="splide__slide">
                                                        @if(strstr($entry->media_certification_dropzone, '.') == '.pdf')
                                                        <a href="{{asset('storage/'.$entry->media_certification_dropzone)}}">
                                                            <img class="file-entry" id="preview_media_certification_dropzone" src="{{url('/images/download_pdf.png')}}" alt="">
                                                        </a>
                                                        @else
                                                        <img class="file-entry" id="preview_media_certification_dropzone" src="{{asset('storage/'.$entry->media_certification_dropzone)}}" alt="">
                                                        @endif
                                                    </li>
                                                    @endif

                                                    @if($entry->client_certification_dropzone != null)
                                                    <li class="splide__slide">
                                                        @if(strstr($entry->asc_clearance_dropzone, '.') == '.pdf')
                                                        <a href="{{asset('storage/'.$entry->client_certification_dropzone)}}">
                                                            <img class="file-entry" id="preview_client_certification_dropzone" src="{{url('/images/download_pdf.png')}}" alt="">
                                                        </a>
                                                        @else
                                                        <img class="file-entry" id="preview_client_certification_dropzone" src="{{asset('storage/'.$entry->client_certification_dropzone)}}" alt="">
                                                        @endif
                                                    </li>
                                                    @endif

                                                    @if($entry->film_dropzone != null)
                                                    <li class="splide__slide">
                                                        <video class="file-entry" id="preview_film_dropzone" controls>
                                                            <source src="{{asset('storage/'.$entry->film_dropzone)}}">
                                                            Your browser does not support the video tag.
                                                        </video>
                                                    </li>
                                                    @endif

                                                    @if($entry->posters_dropzone != null)
                                                    <li class="splide__slide">
                                                        <img class="file-entry" id="preview_posters_dropzone" src="{{asset('storage/'.$entry->posters_dropzone)}}" alt="">
                                                    </li>
                                                    @endif

                                                    @if($entry->audio_dropzone != null)
                                                    <li class="splide__slide">
                                                        <audio controls>
                                                            <source src="{{asset('storage/'.$entry->audio_dropzone)}}" type="audio/mpeg">
                                                            Your browser does not support the audio element.
                                                        </audio>
                                                    </li>
                                                    @endif

                                                    @if($entry->demo_film_dropzone != null)
                                                    <li class="splide__slide">
                                                        <video class="file-entry" id="preview_demo_film_dropzone" controls>
                                                            <source src="{{asset('storage/'.$entry->demo_film_dropzone)}}">
                                                            Your browser does not support the video tag.
                                                        </video>
                                                    </li>
                                                    @endif

                                                    @if($entry->english_translation_dropzone != null)
                                                    <li class="splide__slide">
                                                        <a href="{{asset('storage/'.$entry->english_translation_dropzone)}}">
                                                            <img class="file-entry" id="preview_english_translation_dropzone" src="{{url('/images/download_pdf.png')}}" alt="">
                                                        </a>
                                                    </li>
                                                    @endif

                                                    @if($entry->photo_piece_dropzone != null)
                                                    <li class="splide__slide">
                                                        @if(strstr($entry->photo_piece_dropzone, '.') == '.pdf')
                                                        <a href="{{asset('storage/'.$entry->photo_piece_dropzone)}}">
                                                            <img class="file-entry" id="preview_photo_piece_dropzone" src="{{url('/images/download_pdf.png')}}" alt="">
                                                        </a>
                                                        @else
                                                        <img class="file-entry" id="preview_photo_piece_dropzone" src="{{asset('storage/'.$entry->photo_piece_dropzone)}}" alt="">
                                                        @endif
                                                    </li>
                                                    @endif

                                                    @if($entry->entry_dropzone != null)
                                                    <li class="splide__slide">
                                                        @if(strstr($entry->entry_dropzone, '.') == '.pdf')
                                                        <a href="{{asset('storage/'.$entry->entry_dropzone)}}">
                                                            <img class="file-entry" id="preview_entry_dropzone" src="{{url('/images/download_pdf.png')}}" alt="">
                                                        </a>
                                                        @else
                                                        <img class="file-entry" id="preview_entry_dropzone" src="{{asset('storage/'.$entry->entry_dropzone)}}" alt="">
                                                        @endif
                                                    </li>
                                                    @endif
                                                </ul>
                                            @endif
                                        </div>
                                    </div>

                                    <h4 class="tab-title mt-3 mb-3">Also from this campaign</h4>
                                    <div id="thumbnail-slider{{$entry->id}}" class="splide">
                                      <div class="splide__track">
                                            @if(count($entry->files()) > 0)
                                                <ul class="splide__list">
                                                    @foreach($entry->files() as $file)
                                                        @if($file->type == "Concept Board" || $file->type == "Posters")
                                                            <li class="splide__slide">
                                                                 @if(strstr($file->path, '.') == '.pdf')
                                                                    <a href="{{asset('storage/'.$file->path)}}">
                                                                    <img class="file-entry"src="{{url('/images/download_pdf.png')}}" alt="">
                                                                </a>
                                                                @else
                                                                    <img class="file-entry" src="{{asset('storage/'.$file->path)}}" alt="">
                                                                @endif
                                                            </li>
                                                        @elseif($file->type == "Film")   
                                                            <li class="splide__slide">
                                                                <img class="file-entry" src="{{url('/images/template_mp4.png')}}" alt="">
                                                            </li>
                                                        @elseif($file->type == "Audio")
                                                            <li class="splide__slide">
                                                                <img class="file-entry" src="{{url('/images/template_audio.png')}}" alt="">
                                                            </li>
                                                        @endif
                                                    @endforeach
                                                </ul>
                                            @else
                                                <ul class="splide__list">
                                                    @if($entry->concept_board_dropzone != null)
                                                    <li class="splide__slide">
                                                        <img class="file-entry" id="preview_concept_board_dropzone" src="{{asset('storage/'.$entry->concept_board_dropzone)}}" alt="">
                                                    </li>
                                                    @endif

                                                    @if($entry->case_study_video_dropzone != null)
                                                    <li class="splide__slide">
                                                        <img class="file-entry" id="case_study_video_dropzone" src="{{url('/images/template_mp4.png')}}" alt="">
                                                    </li>
                                                    @endif

                                                    @if($entry->written_case_dropzone != null)
                                                    <li class="splide__slide">
                                                        <img class="file-entry" id="written_case_dropzone" src="{{asset('storage/'.$entry->written_case_dropzone)}}" alt="">
                                                    </li>
                                                    @endif

                                                    @if($entry->asc_clearance_dropzone != null)
                                                    <li class="splide__slide">
                                                        @if(strstr($entry->asc_clearance_dropzone, '.') == '.pdf')
                                                        <a href="{{asset('storage/'.$entry->asc_clearance_dropzone)}}">
                                                            <img class="file-entry" id="preview_asc_clearance_dropzone" src="{{url('/images/download_pdf.png')}}" alt="">
                                                        </a>
                                                        @else
                                                        <img class="file-entry" id="preview_asc_clearance_dropzone" src="{{asset('storage/'.$entry->asc_clearance_dropzone)}}" alt="">
                                                        @endif
                                                    </li>
                                                    @endif

                                                    @if($entry->media_certification_dropzone != null)
                                                    <li class="splide__slide">
                                                        @if(strstr($entry->media_certification_dropzone, '.') == '.pdf')
                                                        <a href="{{asset('storage/'.$entry->media_certification_dropzone)}}">
                                                            <img class="file-entry" id="preview_media_certification_dropzone" src="{{url('/images/download_pdf.png')}}" alt="">
                                                        </a>
                                                        @else
                                                        <img class="file-entry" id="preview_media_certification_dropzone" src="{{asset('storage/'.$entry->media_certification_dropzone)}}" alt="">
                                                        @endif
                                                    </li>
                                                    @endif

                                                    @if($entry->client_certification_dropzone != null)
                                                    <li class="splide__slide">
                                                        @if(strstr($entry->asc_clearance_dropzone, '.') == '.pdf')
                                                        <a href="{{asset('storage/'.$entry->client_certification_dropzone)}}">
                                                            <img class="file-entry" id="preview_client_certification_dropzone" src="{{url('/images/download_pdf.png')}}" alt="">
                                                        </a>
                                                        @else
                                                        <img class="file-entry" id="preview_client_certification_dropzone" src="{{asset('storage/'.$entry->client_certification_dropzone)}}" alt="">
                                                        @endif
                                                    </li>
                                                    @endif

                                                    @if($entry->film_dropzone != null)
                                                    <li class="splide__slide">
                                                        <img class="file-entry" id="case_study_video_dropzone" src="{{url('/images/template_mp4.png')}}" alt="">
                                                    </li>
                                                    @endif

                                                    @if($entry->posters_dropzone != null)
                                                    <li class="splide__slide">
                                                        <img class="file-entry" id="preview_posters_dropzone" src="{{asset('storage/'.$entry->posters_dropzone)}}" alt="">
                                                    </li>
                                                    @endif

                                                    @if($entry->audio_dropzone != null)
                                                    <li class="splide__slide">
                                                        <img class="file-entry" src="{{url('/images/template_audio.png')}}" alt="">
                                                    </li>
                                                    @endif

                                                    @if($entry->demo_film_dropzone != null)
                                                    <li class="splide__slide">
                                                        <img class="file-entry" id="case_study_video_dropzone" src="{{url('/images/template_mp4.png')}}" alt="">
                                                    </li>
                                                    @endif

                                                    @if($entry->english_translation_dropzone != null)
                                                    <li class="splide__slide">
                                                        <img class="file-entry" src="{{url('/images/download_pdf.png')}}" alt="">
                                                    </li>
                                                    @endif

                                                    @if($entry->photo_piece_dropzone != null)
                                                    <li class="splide__slide">
                                                        @if(strstr($entry->photo_piece_dropzone, '.') == '.pdf')
                                                        <a href="{{asset('storage/'.$entry->photo_piece_dropzone)}}">
                                                            <img class="file-entry" id="preview_photo_piece_dropzone" src="{{url('/images/download_pdf.png')}}" alt="">
                                                        </a>
                                                        @else
                                                        <img class="file-entry" id="preview_photo_piece_dropzone" src="{{asset('storage/'.$entry->photo_piece_dropzone)}}" alt="">
                                                        @endif
                                                    </li>
                                                    @endif

                                                    @if($entry->entry_dropzone != null)
                                                    <li class="splide__slide">
                                                        @if(strstr($entry->entry_dropzone, '.') == '.pdf')
                                                        <a href="{{asset('storage/'.$entry->entry_dropzone)}}">
                                                            <img class="file-entry" id="preview_entry_dropzone" src="{{url('/images/download_pdf.png')}}" alt="">
                                                        </a>
                                                        @else
                                                        <img class="file-entry" id="preview_entry_dropzone" src="{{asset('storage/'.$entry->entry_dropzone)}}" alt="">
                                                        @endif
                                                    </li>
                                                    @endif
                                                </ul>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="link-container" style="overflow-wrap: break-word;"></div>
                                </div>

                                <div class="border-divider"></div>

                                <div class="px-65 py-40"> 
                                    <h3 class="block-title mb-20">Campaign details</h3>
                                    <div class="sub-label">Entry name: <span>{{$entry->entry_name}}</span></div>
                                    <div class="sub-label">Ad title/s: <span>{{$entry->ad_details}}</span></div>
                                    <div class="sub-label">Advertiser: <span>{{$entry->advertiser}}</span></div>
                                    <div class="sub-label mb-0">Brand: <span>{{$entry->brand}}</span></div>
                                </div>

                                <div class="border-divider"></div>

                                <div class="px-65 py-40">
                                    <h3 class="block-title mb-0">Written submission</h3>
                                    <div class="tabs">
                                        <!-- Nav tabs -->
                                        <ul class="nav nav-pills justify-content-start" id="myTab" role="tablist">
                                            <li class="nav-item" role="presentation">
                                                <button class="nav-link active" id="pills-campaign-summary-tab{{$loop->index}}" data-bs-toggle="pill" data-bs-target="#pills-campaign-summary{{$loop->index}}" type="button" role="tab" aria-controls="pills-campaign-summary" aria-selected="true">Campaign Summary</button>
                                            </li>
                                            <li class="nav-item" role="presentation">
                                                <button class="nav-link" id="pills-objectives-tab{{$loop->index}}" data-bs-toggle="pill" data-bs-target="#pills-objectives{{$loop->index}}" type="button" role="tab" aria-controls="pills-objectives" aria-selected="false">Objectives</button>
                                            </li>
                                            <li class="nav-item" role="presentation">
                                                <button class="nav-link" id="pills-strategy-tab{{$loop->index}}" data-bs-toggle="pill" data-bs-target="#pills-strategy{{$loop->index}}" type="button" role="tab" aria-controls="pills-strategy" aria-selected="false">Strategy</button>
                                            </li>
                                            <li class="nav-item" role="presentation">
                                                <button class="nav-link" id="pills-execution-tab{{$loop->index}}" data-bs-toggle="pill" data-bs-target="#pills-execution{{$loop->index}}" type="button" role="tab" aria-controls="pills-execution" aria-selected="false">Execution</button>
                                            </li>
                                            <li class="nav-item" role="presentation">
                                                <button class="nav-link" id="pills-results-tab{{$loop->index}}" data-bs-toggle="pill" data-bs-target="#pills-results{{$loop->index}}" type="button" role="tab" aria-controls="pills-results" aria-selected="false">Results</button>
                                            </li>
                                        </ul>
                                        <div class="tab-content" id="myTabContent">
                                            <div class="tab-pane fade show active" id="pills-campaign-summary{{$loop->index}}" role="tabpanel" aria-labelledby="pills-campaign-summary-tab">
                                                <h4 class="tab-title mt-3">
                                                    Campaign Summary
                                                </h4>
                                                <p>{{$entry->campaign_summary}}</p>
                                            </div>
                                            <div class="tab-pane fade" id="pills-objectives{{$loop->index}}" role="tabpanel" aria-labelledby="pills-objectives-tab">
                                                <h4 class="tab-title mt-3">
                                                    Objectives
                                                </h4>
                                                <p>{{$entry->objectives}}</p>
                                            </div>
                                            <div class="tab-pane fade" id="pills-strategy{{$loop->index}}" role="tabpanel" aria-labelledby="pills-strategy-tab">
                                                <h4 class="tab-title mt-3">
                                                    Strategy
                                                </h4>
                                                <p>{{$entry->strategy}}</p>
                                            </div>
                                            <div class="tab-pane fade" id="pills-execution{{$loop->index}}" role="tabpanel" aria-labelledby="pills-execution-tab">
                                                <h4 class="tab-title mt-3">
                                                    Execution
                                                </h4>
                                                <p>{{$entry->execution}}</p>
                                            </div>
                                            <div class="tab-pane fade" id="pills-results{{$loop->index}}" role="tabpanel" aria-labelledby="pills-results-tab">
                                                <h4 class="tab-title mt-3">
                                                    Results
                                                </h4>
                                                <p>{{$entry->results}}</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>    
                        
                                <div class="border-divider"></div>
                            </div>
                            @endforeach

                            <div class="tab-pane" id="v-pills-review" role="tabpanel" aria-labelledby="v-pills-review-tab">
                    
                                <div class="px-65 py-40">
                                    <div class="label d-flex justify-content-between mb-3">
                                        <span class="label-title">Round 1: Creative Effectiveness</span>
                                        <!-- <div class="top-action">
                                            <i class="bi bi-check2 me-2"></i> <span>All changes saved</span>
                                        </div> -->
                                    </div>
                                    <h2 class="page-title mb-30">
                                        Review Scores
                                    </h2>
                                    <div class="entries-cards">
                                        @foreach($entries as $entry)
                                            <?php
                                                $custom_entry = preg_replace('/\s+/', '', strtolower($entry->entry_name))
                                            ?>
                                            <div class="custom-card">
                                                <div class="custom-card-col">
                                                    <div class="custom-card-col-info">
                                                        <h2>{{$entry->entry_name}}</h2>
                                                    </div>
                                                </div>
                                                <div class="custom-card-col2">
                                                    @if($entry->status_round1 != null)
                                                    <span class="status">
                                                        {{$entry->status_round1}}
                                                        {{$entry->reason_round1}}
                                                    </span>
                                                    @else
                                                    <span class="status text-danger vote-empty">
                                                        You haven't scored on this entry yet
                                                    </span>
                                                    @endif
                                                    <a class="edit-score" href="javascript:void(0)" data-tab="{{$custom_entry}}">
                                                        <span>Edit score</span>
                                                    </a> 
                                                </div>
                                            </div>
                                        @endforeach
                                    </div>
                                </div>

                                <div class="border-divider"></div>
                            </div>

                        </div>

                        @include('juror.components.actions')

                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="form">
        <div class="container-fluid px-0 foating-wrapper">
            <div class="row justify-content-center">
                <div class="col left-panel px-0" style="max-width:322px; background: transparent;"></div>
                <div class="col right-panel px-0">
                    <div class="d-flex justify-content-center">
                        <div class="floating mb-5">
                            <div class="form-check d-flex align-items-center">
                                <input class="form-check-input custom-checkbox" type="radio" name="status" id="flag" data-bs-toggle="modal" data-bs-target="#flagModal" value="flag">
                                <label class="form-check-label fs-18" style="margin-left:14px;" for="flag">
                                    <img src="/images/flags.svg" alt="" class="vote-icon">
                                    Flag
                                </label>
                            </div>
                            <div class="form-check d-flex align-items-center">
                                <input class="form-check-input custom-checkbox" type="radio" name="status" id="abstain"  data-bs-toggle="modal" data-bs-target="#abstainModal" value="abstain">
                                <label class="form-check-label fs-18" style="margin-left:14px;" for="abstain">
                                    <img src="/images/no-stopping.svg" alt="" class="vote-icon">
                                    Abstain
                                </label>
                            </div>
                            <div class="form-check d-flex align-items-center">
                                <input class="form-check-input custom-checkbox" type="radio" name="status" id="outofshow" value="outofshow">
                                <label class="form-check-label fs-18" style="margin-left:14px;" for="outofshow">
                                    Out of show
                                </label>
                            </div>
                            <div class="form-check d-flex align-items-center">
                                <input class="form-check-input custom-checkbox" type="radio" name="status" id="shortlist" value="shortlist">
                                <label class="form-check-label fs-18" style="margin-left:14px;" for="shortlist">
                                    Shortlist
                                </label>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Modals -->
    <div class="form">
        <div class="voting-modal modal fade" id="flagModal" tabindex="-1" aria-labelledby="flag" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body">
                        <div class="selection">
                            <p class="question">Select reason for flagging this entry</p>
                            <div id="invalid-flag-reason" class="invalid-feedback mb-3">Flag reason is required.</div>
                            <div class="form-check form-check-inline">
                                <!-- <input class="form-check-input custom-checkbox" name="flag" type="radio" value="wrong_category" id="flag1"> -->
                                <input class="form-check-input custom-checkbox" type="radio" name="flag" value="wrong_category" id="flag1">
                                <label class="form-check-label" for="flag1">Wrong category</label>
                            </div>
                            <div class="form-check form-check-inline">
                                <!-- <input class="form-check-input custom-checkbox" name="flag" type="radio" value="wrong_eligibility" id="flag2"> -->
                                <input class="form-check-input custom-checkbox" type="radio" name="flag" value="wrong_eligibility" id="flag2">
                                <label class="form-check-label" for="flag2">Wrong eligibility date</label>
                            </div>
                            <div class="form-check form-check-inline">
                                <!-- <input class="form-check-input custom-checkbox" name="flag" type="radio" value="others" id="flag3"> -->
                                <input class="form-check-input custom-checkbox" type="radio" name="flag" value="other_flag" id="flag3">
                                <label class="form-check-label" for="flag3">Others (Kindly specify):</label>
                            </div>
                            <textarea class="form-control" id="flag_reason" rows="3" style="resize: none; width: 100%; text-align: left;"></textarea>
                        </div>
                        <div class="text-center mt-40">
                            <button class="btn btn-primary primary small" id="btn_flag" style="min-width:262px; height: 48px;">Flag this entry</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="voting-modal modal fade" id="abstainModal" tabindex="-1" aria-labelledby="abstain" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body">
                        <div class="selection">
                            <p class="question">Select reason for abstaining from this entry</p>
                            <div id="invalid-abstain-reason" class="invalid-feedback mb-3">Abstain reason is required.</div>
                            <div class="form-check form-check-inline">
                                <input class="form-check-input custom-checkbox" name="abstain" type="radio" value="same_agency" id="abstain1">
                                <label class="form-check-label" for="abstain1">I am from the same agency / network</label>
                            </div>
                            <div class="form-check form-check-inline">
                                <input class="form-check-input custom-checkbox" name="abstain" type="radio" value="involved" id="abstain2">
                                <label class="form-check-label" for="abstain2">I am involved in this project</label>
                            </div>
                            <div class="form-check form-check-inline">
                                <input class="form-check-input custom-checkbox" name="abstain" type="radio" value="other_abstain" id="abstain3">
                                <label class="form-check-label" for="abstain3">Others (Kindly specify):</label>
                            </div>
                            <textarea class="form-control" id="abstain_reason" rows="3" style="resize: none; width: 100%; text-align: left;"></textarea>
                        </div>
                        <div class="text-center mt-40">
                            <button class="btn btn-primary primary small" id="btn_abstain" style="min-width:262px; height: 48px;">Abstain from voting</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="voting-modal modal fade" id="errorVoteModal" tabindex="-1" aria-labelledby="abstain" aria-hidden="true">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body">
                        <div class="selection">
                            <p class="question">Instructions for juror voting</p>
                            <ul>
                               <li><strong>Step 1.</strong> Choose vote status on the middle pop-up.</li>
                               <li><strong>Step 2.</strong> If the vote status is either flag or abstain, reason is required.</li> 
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="voting-modal modal fade" id="successVoteModal" tabindex="-1" aria-labelledby="abstain" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body">
                        <div class="selection">
                            <p class="question text-center">Entry voted successfully!</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="voting-modal modal fade" id="notCompleteVoteModal" tabindex="-1" aria-labelledby="abstain" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <div class="selection">
                        <p class="question text-center">Complete the entry voting to continue.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @include('juror.components.footer')
@push('custom-js')
<script>
    $(document).ready(activateQuery())

    var entry_id = 0;
    var reason = "";
    var main;
    var thumbnails;

    function activateQuery() {
        getVoteStatus($('ul.nav-pills>li:first-child').attr('data-id'))

        $(document).on('click', '#v-pill-tab > li.nav-link', function() {
            entry_id = $(this).attr('data-id');
            $('#v-pill-tab > .nav-link').not(this).removeClass('active');
            $(this).addClass('active');
            setTimeout(function() {
                activateCarousel(entry_id);
                getPreview();
            }, 1000)
        })

        var first_entry_id = $( "li" ).first().attr('data-id');
        activateCarousel(first_entry_id);
    }

    function activateCarousel(id) {
        if (id != null) {
            main = new Splide( '.splide'+id, {
            type       : 'fade',
            cover      : true,
            rewind     : true,
            pagination : false,
            heightRatio: 0.5,
            arrows    : false
            } );

            thumbnails = new Splide( '#thumbnail-slider'+id, {
            fixedWidth  : 100,
            fixedHeight : 60,
            gap         : 10,
            rewind      : true,
            pagination  : false,
            cover       : true,
            isNavigation: true,
            breakpoints : {
              600: {
                fixedWidth : 60,
                fixedHeight: 44,
              },
            },
            } );

            main.sync( thumbnails );
            main.mount();
            thumbnails.mount();
        }
    }

    $(document).on('click', '#btn_flag', function() {
        if (typeof $('input[name="flag"]:checked').val() == "undefined") {
            $('input[name="flag"]').addClass('is-invalid')
            $('#invalid-flag-reason').show()
        } else {
        
            if ($('input[name="flag"]:checked').val() == "other_flag") {
                if ($('#flag_reason').val() == "") {
                    $('input[name="flag"]').addClass('is-invalid')
                    $('#invalid-flag-reason').show()
                } else {
                    reason = $('#flag_reason').val()
                    $('input[name="flag"]').removeClass('is-invalid')
                    $('#invalid-flag-reason').hide()
                    $('#flagModal').modal('hide')
                }
            } else {
                reason = ""
                $('input[name="flag"]').removeClass('is-invalid')
                $('#invalid-flag-reason').hide()
                $('#flagModal').modal('hide')
            }
        }
    })

    $(document).on('click', '#btn_abstain', function() {
        if (typeof $('input[name="abstain"]:checked').val() == "undefined") {
            $('input[name="abstain"]').addClass('is-invalid')
            $('#invalid-abstain-reason').show()
        } else {
            if ($('input[name="abstain"]:checked').val() == "other_abstain") {
                if ($('#abstain_reason').val() == "") {
                    $('input[name="abstain"]').addClass('is-invalid')
                    $('#invalid-abstain-reason').show()
                } else {
                    reason = $('#flag_reason').val()
                    $('input[name="abstain"]').removeClass('is-invalid')
                    $('#invalid-abstain-reason').hide()
                    $('#abstainModal').modal('hide')
                }
            } else {
                reason = $('input[name="abstain"]:checked').val()
                $('input[name="abstain"]').removeClass('is-invalid')
                $('#invalid-abstain-reason').hide()
                $('#abstainModal').modal('hide')
            }
        }
    })

    $(document).on('click', '#v-pill-tab > .nav-link', function() {
        var counter = parseInt($('#v-pill-tab>li.nav-link[class="active"]').attr('data-counter'))
        setTimeout(function() {
            $('#pills-campaign-summary'+counter).addClass('show active')
        }, 1000)

        if ($(this).attr('id') == "v-pills-review-tab") {
            $('.btn_next').hide();
            $('.btn_submit').show();
            $('.form').hide()
        } else {
            $('.btn_next').show();
            $('.btn_submit').hide();
            $('.form').show()
        }

        $('#abstain_reason').val("")
        $('#flag_reason').val("")

        if ($(this).hasClass('done')) {
            entry_id = $(this).attr('data-id')
            $('input[type="radio"]').removeAttr('checked')

            getVoteStatus(entry_id)
        } else {
            $('input[type="radio"]').prop('checked', false)
        }
    })

    $(document).on('click', '.btn_previous', function() {

        $('#abstain_reason').val("")
        $('#flag_reason').val("")

        entry_id = $('li.nav-link.active').prev().attr('data-id')
        setTimeout(function() {
            activateCarousel(entry_id)

            $('#myTab>li>button').removeClass('active')
            $('#myTab>li>button[aria-controls="pills-campaign-summary"]').addClass('active')
        }, 1000)
        
        entry_id = $('li.nav-link.active').prev().attr('data-id')
        setTimeout(function() {
            getVoteStatus(entry_id)
        }, 1000)

        var counter = parseInt($('li.nav-link.active').attr('data-counter'))
        counter -= 1

        setTimeout(function() {
            $('#pills-campaign-summary'+counter).addClass('show active')
        }, 1000)

        if (counter >= 0) {
            $('li.nav-link.active').removeClass('active')
            $('.nav-link[data-counter="'+counter+'"]').addClass('active')
            $('#v-pills-tabContent > .tab-pane').removeClass('show active')
            $('.tab-pane[data-tab="'+counter+'"]').addClass('show active')
        }

        getPreview();

        $('.btn_submit').hide()
        $('.btn_next').show()
        $('.form').show()
    })

    $(document).on('click', '.btn_next', function() {
        // $.LoadingOverlay("show", {
        //     image: '{{url("/images/loading1.png")}}'
        // },100);

        entry_id = $('li.nav-link.active').attr('data-id')
        var checker = 0
        if (entry_id == 0) {
            checker++;
            $('#errorVoteModal').modal('show')
            $.LoadingOverlay("hide");
        }

        if (typeof $('input[name="status"]:checked').val() == "undefined") {
            checker++;
            $('#errorVoteModal').modal('show')
            $.LoadingOverlay("hide");
        }

        if ($('input[name="status"]:checked').val() == "flag") {
            if (typeof $('input[name="flag"]:checked').val() == "undefined") {
                checker++;
                $('#errorVoteModal').modal('show')
                $.LoadingOverlay("hide");
            }
        }

        if ($('input[name="status"]:checked').val() == "abstain") {
            if (typeof $('input[name="abstain"]:checked').val() == "undefined") {
                checker++;
                $('#errorVoteModal').modal('show')
                $.LoadingOverlay("hide");
            }
        }

        if ($('input[name="flag"]:checked').val() == "other_flag") {
            reason = $('#flag_reason').val()
        } else {
            reason = $('input[name="flag"]:checked').val()
        }

        if ($('input[name="abstain"]:checked').val() == "other_abstain") {
            reason = $('#abstain_reason').val()
        } else {
            reason = $('input[name="abstain"]:checked').val()
        }

        if (checker == 0) {
            let params = {
                '_token'        : '{{ csrf_token() }}',
                'entry_id'      : entry_id,
                'status'        : $('input[name="status"]:checked').val(),
                'reason'        : reason,
            }

            fetch("{{url('judge/submit-vote-round1')}}", {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify(params)
            })
            .then(response => response.json())
            .then((e) => {
                if (e.status_code == 200) {
                    $('input[type="radio"]').prop('checked', false); 
                    var counter = parseInt($('li.nav-link.active').attr('data-counter'))
                    entry_id = $('li.nav-link.active').attr('data-id')
                    counter += 1
                    var last_item = parseInt("{{count($entries)-1}}")

                    setTimeout(function() {
                        $('#pills-campaign-summary'+counter).addClass('show active')
                    }, 1000)

                    if (counter > last_item) {
                        $('li.nav-link').removeClass('active')
                        $('.tab-pane').removeClass('show active')

                        setTimeout(function() {
                            $('#v-pills-review-tab').addClass('active')
                            $('#v-pills-review').addClass('show active')
                            $('.form').hide()
                            $('.btn_submit').show()
                            $('.btn_next').hide()
                            getPreview();
                        }, 1000)
                    }

                    $('li.nav-link.active').removeClass('active')
                    $('.nav-link[data-counter="'+counter+'"]').addClass('active')
                    $('.nav-link[data-id="'+entry_id+'"]').addClass('done')
                    entry_id = $('li.nav-link.active').attr('data-id')
                    activateCarousel(entry_id)

                    $('.tab-pane').removeClass('show active')
                    $('.tab-pane[data-tab="'+counter+'"]').addClass('show active')

                    // entry_id = 0
                    $('#flag_reason').val("")
                    $('#abstain_reason').val("")
                    $('#successVoteModal').modal('show')
                    setTimeout(function() {
                        $.LoadingOverlay("hide");
                        $('#successVoteModal').modal('hide')
                    },1000)
                }
            })
        }

        $('#abstain_reason').val("")
        $('#flag_reason').val("")
        
        if ($('li.nav-link.active').hasClass('done')) {
            entry_id = $('li.nav-link.active').next().attr('data-id')
            setTimeout(function() {
                $('input[type="radio"]').removeAttr('checked')
            }, 1000)

        }
        
        getVoteStatus(entry_id);

    })

    function getVoteStatus(entry_id) {
        let params = {
            '_token'   : '{{ csrf_token() }}',
            'entry_id' : entry_id
        }

        fetch("{{url('judge/vote-status-round1')}}", {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(params)
        })
        .then(response => response.json())
        .then((e) => {
            if (e.status_code == 200) {
                setTimeout(function() {
                    if(e.data.vote_status != null) {
                        var status = e.data.vote_status.status
                        var reason = e.data.vote_status.reason

                        $('input[value="'+status+'"]').prop('checked', true)

                        if (status == "flag") {
                            if (reason == "wrong_category" || reason == "wrong_eligibility") {
                                $('input[value="'+reason+'"]').prop('checked', true)
                            } else {
                                $('input[value="other_flag"]').prop('checked', true)
                                $('#flag_reason').val(reason)
                            }
                        }

                        if (status == "abstain") {
                            if (reason == "same_agency" || reason == "involved") {
                                $('input[value="'+reason+'"]').prop('checked', true)
                            } else {
                                $('input[value="other_abstain"]').prop('checked', true)
                                $('#abstain_reason').val(reason)
                            }
                        }
                    } else {
                        $('input[type="radio"]').prop('checked', false); 
                    }

                    var html = "";
                    html += '<h4 class="tab-title mt-5 mb-3">Online links available</h4>'
                    $.each(e.data.references, function(index, ref) {
                        html += '<span class="d-block">'
                        html += '    <a class="link-color" href="'+ref.link+'" target="_blank">'+ref.link+'</a>'
                        html += '</span>'
                    })

                    $('.link-container').html(html)

                }, 1000)
            }
        })
    }

    function getPreview() {
        let params = {
            '_token'        : '{{ csrf_token() }}',
            'cat_id'        : "{{Request::segment(4)}}",
            'sub_id'        : "{{Request::segment(6)}}",
        }

        fetch("{{url('judge/vote-round1')}}", {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(params)
        })
        .then(response => response.json())
        .then((e) => {
            if (e.status_code == 200) {
                var html = "" 
                $.each(e.data.entries, function(index, entry) {
                    html += '<div class="custom-card">';
                    html += '    <div class="custom-card-col">';
                    html += '        <div class="custom-card-col-info">';
                    html += '            <h2>'+entry.entry_name+'</h2>';
                    html += '        </div>';
                    html += '    </div>';
                    html += '    <div class="custom-card-col2">';

                    if (entry.status_round1 != null) {
                        html += '        <span class="status" style="color:#333;">';
                        html += entry.status_round1
                        if (entry.reason_round1 != null) {
                            html += ""+entry.reason_round1
                        }
                        html += '        </span>';
                    }

                    else {
                        html += '        <span class="status text-danger vote-empty">';
                        html += '            You haven\'t scored on this entry yet';
                        html += '        </span>';
                    }

                    var custom_entry = entry.entry_name
                    custom_entry = custom_entry.replace(/\s/g, '').toLowerCase();

                    html += '        <a class="edit-score" href="javascript:void(0)" data-tab="'+custom_entry+'">';
                    html += '            <span>Edit score</span>';
                    html += '        </a> ';
                    html += '    </div>';
                    html += '</div>';

                    if (entry.submitted_round1 == 1) {
                        $('.btn_submit>button').text('BACK TO DASHBOARD')
                    } else {
                        $('.btn_submit>button').text('SUBMIT SCORES') 
                    }
                })

                $('.entries-cards').html(html)
            }
        })
    }

    $(document).on('click', '.edit-score', function() {
        var entry_tab = $(this).attr('data-tab')
        $('.form').show()
        $('#v-pills-'+entry_tab+'-tab').addClass('active')
        $('.nav-link').not($('#v-pills-'+entry_tab+'-tab')).removeClass('active')

        $('#v-pills-'+entry_tab).addClass('show active')
        $('.tab-pane').not($('#v-pills-'+entry_tab)).removeClass('show active')
        entry_id = $('.nav-link.active').attr('data-id')
        setTimeout(function() {
            activateCarousel(entry_id)
            getPreview();
            getVoteStatus(entry_id)
        }, 1000)
    })

    $(document).on('click', '.btn_submit', function() {
        if ($(this).find('button').text() != "BACK TO DASHBOARD") {
             var empty_vote = $('.custom-card-col2 .vote-empty').length;

            if (empty_vote == 0) {
                var entry_ids = [];
                $.each($('.nav-link'), function() {
                    if ($(this).hasClass('done')) {
                        entry_ids.push($(this).attr('data-id'))
                    }
                })

                let params = {
                    '_token'        : '{{ csrf_token() }}',
                    'entry_ids'      : entry_ids,
                }

                fetch("{{url('/judge/submit-allvote-round1')}}", {
                    method: 'POST',
                    headers: {
                        'Content-Type': 'application/json'
                    },
                    body: JSON.stringify(params)
                })
                .then(response => response.json())
                .then((e) => {
                    if (e.status_code == 200) {
                        setTimeout(function() {
                            location.href = "{{url('/judge/success-round-1')}}"
                        },1000)
                    }
                })
            } else {
                setTimeout(function() {
                    $('#notCompleteVoteModal').modal('show')
                }, 1000)
            }
        } else {
            location.href = "{{url('/judge/dashboard')}}"
        }
    })
</script>
<script>
    
</script>
@endpush
@endsection