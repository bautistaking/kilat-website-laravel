@extends('base')  
@push('styles')
    <style>
        .spinner-grow,
        .loading-label {
            display: none;
        }        
    </style>
@endpush
@section('content')
    @include('juror.components.header')
    <div class="bg">
        <div class="wrapper-default pt72">
            <div class="container-fluid py72">
                <div class="state d-flex flex-column justify-content-center align-items-center">
                    <img class="mb-4" src="{{url('images/cheers.png')}}" alt="">
                    <h1 class="title mb-4 text-center fw-bold">
                        <!-- Thank you for your time<br/>
                        and patience!  -->
                        Scores Submitted!
                    </h1>
                    <p class="desc mb-0 text-center">
                        You can proceed to the next category or review <br> your scores in the Dashboard.
                       <!--  We know it's been tough calling those shots. Please wait<br/> 
                        for our next email for the 2nd round on November 9, 2021.
                        <br/> 
                        <br/> 
                        In the meantime, palamig muna tayo. -->
                    </p>
                    <div>
                        <a href="{{url('/judge/dashboard')}}" style="text-decoration: none;">
                            <button type="button" class="btn btn-outline-primary primary lg mt-5 text-uppercase" style="min-width:242px">Back to dashboard</button>
                        </a>

                        @if($next)
                        <a href="{{url('/judge/view-round-2/category/'.$next->category_id.'/subcategory/'.$next->subcategory_id.'')}}" style="text-decoration: none;">
                            <button type="button" class="btn btn-primary primary lg mt-5 text-uppercase ms-3" style="min-width:242px">Start Next Category</button>
                        </a>
                        @endif
                    </div>
                </div>            
            </div>
        </div>
    </div>
    @include('juror.components.footer')
@push('custom-js')
<script>
    $(document).ready(activateQuery())

    function activateQuery() {
        
    }
</script>
@endpush
@endsection