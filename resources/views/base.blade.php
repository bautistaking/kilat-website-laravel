<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <meta name="description" content="Are you ready to unlock the best of the best in Filipino creativity?" />
  <meta name="author" content="EBDigital" />
  <title>Kidlat Awards 2022 - Creativity Recharged</title>
  <meta property="og:title" content="Welcome to Kidlat Awards!">
  <meta property="og:description" content="Are you ready to unlock the best of the best in Filipino creativity?">
  <!-- best size for Facebook: 1300 X 630 px -->
  <meta property="og:image" content="{{url('/images/og-2.jpg')}}"/>
  <meta property="og:url" content="https://www.kidlatawards.com/"/>
  <meta property="og:type" content="website"/>
  <meta property="og:site_name" content="Welcome to Kidlat Awards!" />
  <meta name="twitter:card" content="summary_large_image">
  <meta name="twitter:creator" content=""/>
  <link rel="shortcut icon" href="{{ asset('/images/Favicon.jpg') }}">

  <!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=G-TKL0DQ8D42"></script>
<script> 
  window.dataLayer = window.dataLayer || []; function gtag(){dataLayer.push(arguments);} gtag('js', new Date()); gtag('config', 'G-TKL0DQ8D42');
</script>

  <!-- Google Tag Manager -->
  <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
  new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
  j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
  'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
  })(window,document,'script','dataLayer','GTM-WS7CLVN');</script>
  <!-- End Google Tag Manager -->

  <!--Import Google Icon Font-->
  <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
  <!-- Import Google Poppin Font -->
  <link href="https://fonts.googleapis.com/css?family=Poppins" rel="stylesheet">
  <link rel="shortcut icon" href="{{url('/images/favicon.png')}}">
  <!-- 2021 css -->
  <link rel="stylesheet" type="text/css" href="{{url('/css/main.min.css')}}" />
  <link rel="stylesheet" type="text/css" href="{{url('/css/custom.css')}}" />
  <link rel="stylesheet" type="text/css" href="{{url('/css/component.css')}}" />
  <link rel="stylesheet" type="text/css" href="{{url('/css/splide.min.css')}}" />
  <link rel="stylesheet" type="text/css" href="{{url('/css/themes/splide-default.min.css')}}" />
  <link rel="stylesheet" href="{{url('/css/magnific-popup.css')}}">
  <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-KyZXEAg3QhqLMpG8r+8fhAXLRk2vvoC2f3B09zVXn8CA5QIVfZOJ3BCsw2P0p/We" crossorigin="anonymous">
  <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.5.0/font/bootstrap-icons.css">
  <link rel="preconnect" href="https://fonts.googleapis.com">
  <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
  <link href="https://fonts.googleapis.com/css2?family=Inter:wght@300;400;500;600;700;800;900&display=swap" rel="stylesheet">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.2.0/css/all.min.css">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/dropzone/5.9.2/dropzone.min.css" integrity="sha512-jU/7UFiaW5UBGODEopEqnbIAHOI8fO6T99m7Tsmqs2gkdujByJfkCbbfPSN4Wlqlb9TGnsuC0YgUgWkRBK7B9A==" crossorigin="anonymous" referrerpolicy="no-referrer" />

  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.4/css/all.min.css" integrity="sha512-1ycn6IcaQQ40/MKBW2W4Rhis/DbILU74C1vSrLJxCq57o941Ym01SwNsOMqvEBFlcgUa6xLiPY/NS5R+E6ztJQ==" crossorigin="anonymous" referrerpolicy="no-referrer" />

  <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css"/>
  <!-- end 2021 css -->

  <!-- <link rel="stylesheet" href="{{ mix('css/vendor.css') }}">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/flatpickr/4.5.7/flatpickr.min.css">
  <link rel="stylesheet" href="{{ mix('css/site.css') }}"> -->
  @stack('styles')

</head>
<body class="full-width">
  <!-- Google Tag Manager (noscript) -->
  <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-WS7CLVN"
  height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
  <!-- End Google Tag Manager (noscript) -->
  <div class="main-wrapper">
    @yield('content') 
  </div>
  @stack('dependencies-js')
  <!-- 2021 js -->
  <script src="https://code.jquery.com/jquery-3.6.0.min.js" integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>
  <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.3/dist/umd/popper.min.js" integrity="sha384-eMNCOe7tC1doHpGoWe/6oMVemdAVTMs2xqW4mwXrXsW0L84Iytr2wi5v2QjrP/xp" crossorigin="anonymous"></script>
  <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/js/bootstrap.min.js" integrity="sha384-cn7l7gDp0eyniUwwAZgrzD06kc/tftFf19TOAs2zVinnD/C7E91j9yyk5//jjpt/" crossorigin="anonymous"></script>
  <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/lodash.js/4.17.21/lodash.min.js" integrity="sha512-WFN04846sdKMIP5LKNphMaWzU7YpMyCU245etK3g/2ARYbPK9Ub18eG+ljU96qKRCWh+quCY7yefSmlkQw1ANQ==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.29.1/moment.min.js" integrity="sha512-qTXRIMyZIFb8iQcfjXWCO8+M5Tbc38Qi5WzdPOYZHIlZpzBHG3L3by84BBBOiRGiEb7KKtAOAs5qYdUiZiQNNQ==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/dropzone/5.9.2/min/dropzone.min.js" integrity="sha512-VQQXLthlZQO00P+uEu4mJ4G4OAgqTtKG1hri56kQY1DtdLeIqhKUp9W/lllDDu3uN3SnUNawpW7lBda8+dSi7w==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
  <script type="text/javascript" src="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js"></script>
  <script src="{{url('/js/loadingoverlay.min.js')}}"></script>
  <script src="{{url('/js/jquery.multi-select.js')}}"></script>
  <script src="{{url('/js/modernizr.custom.js')}}"></script>
  <script src="{{url('/js/classie.js')}}"></script>
  <script src="{{url('/js/progressButton.js')}}"></script>
  <script src="{{url('/js/splide.min.js')}}"></script>
  <script src="{{url('/js/magnific-popup.min.js')}}"></script>
  <script>

  [].slice.call( document.querySelectorAll( 'button.progress-button' ) ).forEach( function( bttn ) {
      new ProgressButton( bttn, {
        callback : function( instance ) {
          var progress = 0,
            interval = setInterval( function() {
              progress = Math.min( progress + Math.random() * 0.1, 1 );
              instance._setProgress( progress );

              if( progress === 1 ) {
                instance._stop(1);
                clearInterval( interval );
              }
            }, 200 );
        }
      } );
    } );

    $(document).on('click', '.profile-link', function() {
        Swal.fire({
            // confirmButtonText: 'Profile',
            showConfirmButton: false,
            denyButtonText: 'Logout',
            denyButtonClass: 'btn-logout',
            showDenyButton: true
        }).then((result) => {
            // if (result.isConfirmed) {
            //     location.href = "/profile"
            // }
            if (result.isDenied) {
                location.href = "/logout"
            }
        })
    })

    $(document).on('click', '#btn_set', function() {
        // $.LoadingOverlay("show", {
        //     image: '{{url("/images/loading1.png")}}'
        // },100);

        if ($('#expiry_date').val() == "") {
            $.LoadingOverlay("hide");
            $('#expiry_date').addClass('is-invalid')
            $('.invalid-date').show()
        } else {
            $('#expiry_date').removeClass('is-invalid')
            $('.invalid-date').hide()

            var active = 0;
            if ($('input[name="status"]:checked').val() == "on") {
                active = 1;
            } else if (typeof $('input[name="status"]:checked').val() == "undefined") {
                active = 0;
            }

            let params = {
                '_token': '{{ csrf_token() }}',
                'active': active,
                'expired_at' : $('#expiry_date').val()
            }

            fetch("{{url('admin/expiry-status')}}", {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify(params)
            })
            .then(response => response.json())
            .then((e) => {

              if (e.status_code == 200) {
                    // $.LoadingOverlay("hide");
                    $('#expiry-feedback').show()
                    setTimeout(function() {
                        $('#btn_set').attr('disabled', 'true')
                        location.href = "/admin/entries"
                    }, 2000)
              }
            })
        }
    })
  </script>
  <script src="{{ url('/js/two-way-binding.js') }}"></script>
  <!-- end 2021 js -->
  @stack('custom-js')

</body>
</html>