@extends('base')

@section('content')
<div class="error">
  @include('error.components.header')
  <div class="container text-center">
    <div>
      <h4 class="m-0">Ooops! You caught us in a bad time.</h4>
      <h6>Either something went wrong or were currently updating the website to improve your experience. Try again in few minutes.</h6>
      <div class="spacer"></div>
      <div>
        <a href="" class="waves-effect waves-dark btn pink inverted mx-1">Try Again</a><a href="mailto:4asp@pldtdsl.net,kidlatawards2021@gmail.com?subject=[Kidlat Awards 2021] - Technical Inquiry" class="waves-effect waves-dark btn pink">Contact Us</a>
      </div>
    </div>
  </div>
</div>
@include('error.components.footer')
@endsection