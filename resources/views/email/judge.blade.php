<!doctype html>
<html>
  <head>
    <meta name="viewport" content="width=device-width">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>Kidlat Awards</title>
    <style>
    
    p {
      font-family: sans-serif; font-size: 14px; font-weight: normal; margin: 0; Margin-bottom: 15px;
    }
    /* -------------------------------------
        INLINED WITH htmlemail.io/inline
    ------------------------------------- */
    /* -------------------------------------
        RESPONSIVE AND MOBILE FRIENDLY STYLES
    ------------------------------------- */
    @media only screen and (max-width: 620px) {
      a[href] {
        color: #fff;
      }
      table[class=body] h1 {
        font-size: 28px !important;
        margin-bottom: 10px !important;
      }
      table[class=body] p,
            table[class=body] ul,
            table[class=body] ol,
            table[class=body] td,
            table[class=body] span,
            table[class=body] a {
        font-size: 14px !important;
      }
      table[class=body] .wrapper,
            table[class=body] .article {
        padding: 10px !important;
      }
      table[class=body] .content {
        padding: 0 !important;
      }
      table[class=body] .container {
        padding: 0 !important;
        width: 100% !important;
      }
      table[class=body] .main {
        border-left-width: 0 !important;
        border-radius: 0 !important;
        border-right-width: 0 !important;
      }
      table[class=body] .btn table {
        width: 100% !important;
      }
      table[class=body] .btn a {
        width: 100% !important;
      }
      table[class=body] .img-responsive {
        height: auto !important;
        max-width: 100% !important;
        width: auto !important;
      }
    }

    /* -------------------------------------
        PRESERVE THESE STYLES IN THE HEAD
    ------------------------------------- */
    @media all {
      .ExternalClass {
        width: 100%;
      }
      .ExternalClass,
            .ExternalClass p,
            .ExternalClass span,
            .ExternalClass font,
            .ExternalClass td,
            .ExternalClass div {
        line-height: 100%;
      }
      .apple-link a {
        color: inherit !important;
        font-family: inherit !important;
        font-size: inherit !important;
        font-weight: inherit !important;
        line-height: inherit !important;
        text-decoration: none !important;
      }
      .btn-primary table td:hover {
        background-color: #34495e !important;
      }
      .btn-primary a:hover {
        background-color: #34495e !important;
        border-color: #34495e !important;
      }
    }
    </style>
  </head>
  <body class="" style="background-color: #fff; font-family: sans-serif; -webkit-font-smoothing: antialiased; font-size: 14px; line-height: 1.4; margin: 0; padding: 0; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;">
    <table border="0" cellpadding="0" cellspacing="0" class="body" style="border-collapse: separate; mso-table-lspace: 0pt; mso-table-rspace: 0pt; width: 100%; background-color: #fff;">
        <td style="font-family: sans-serif; font-size: 14px; vertical-align: top;">&nbsp;</td>
        <td class="container" style="font-family: sans-serif; font-size: 14px; vertical-align: top; display: block; Margin: 0 auto; width: 100%;">
          
            <div class="header" style="clear: both;text-align: center; width: 100%;background-color:#5d59a5;">
              <table border="0" cellpadding="0" cellspacing="0" style="border-collapse: separate; mso-table-lspace: 0pt; mso-table-rspace: 0pt; width: 100%;">
                <tr>
                  <td class="content-block powered-by" style="font-family: sans-serif; vertical-align: top; padding-bottom: 10px; padding-top: 10px; font-size: 12px; color: #000; text-align: center;">
                    <a href="{{ url('/') }}" target="_blank"><img src="https://kidlatawards.com.ph/img/kidlat-logo.png" width="70"></a>
                  </td>
                </tr>
              </table>
            </div>
          
          <div class="content" style="box-sizing: border-box; display: block; Margin: 0 auto; padding: 10px;">
            
            <!-- START CENTERED WHITE CONTAINER -->
            <span class="preheader" style="color: transparent; display: none; height: 0; max-height: 0; max-width: 0; opacity: 0; overflow: hidden; mso-hide: all; visibility: hidden; width: 0;"></span>
            <table class="main" style="border-collapse: separate; mso-table-lspace: 0pt; mso-table-rspace: 0pt; width: 100%; background: #fff; border-radius: 3px;max-width: 700px;margin:0 auto;">

              <!-- START MAIN CONTENT AREA -->
              <tr>
                <td class="wrapper" style="font-family: sans-serif; font-size: 14px; vertical-align: top; box-sizing: border-box; padding: 20px;">
                  <table border="0" cellpadding="0" cellspacing="0" style="border-collapse: separate; mso-table-lspace: 0pt; mso-table-rspace: 0pt; width: 100%;">
                    <tr>
                      <td style="font-family: sans-serif; font-size: 14px; vertical-align: top;">
                        <p>Dear <span style="color:#D62888;">{!!$name!!}!</span></p>
                        <p>We are incredibly fortunate to have you judging for the Kidlat Awards this 2019. The amount of talent and experience on the jury is incredibly impressive.</p>
                        <p>We’ve assigned the categories and we wanted to let you know that you will be judging the <strong>{{$category->name}}</strong>  entries.</p>
                        <p >You’ll find more detailed instructions on the process and criteria for judging in the link below. </p>
                        <p>More information on your fellow judges will be available soon too!</p>
                        <p>Looking forward to having your expertise on the evaluation process!</p>
                        <p>Please use the following guidelines when reviewing the entries:</p>
                        <ul class="m-0">
                          <li>5 - Not a good idea</li>
                          <li>4 - It’s okay</li>
                          <li>3 - I’m not sure if it should win or not. Let’s discuss further</li>
                          <li>2 - Great idea. It should get something</li>
                          <li>1 - It’s a sure a winner</li>
                        </ul>
                        <p>Things judges should consider when viewing the work:</p>
                        @if($category->id == 1)
                          <ul class="m-0 circle">
                            <li>Is the work distinctive, fresh, and exceptional in its category?</li>
                            <li>Did it make significant contribution to the brand’s growth or business performance?</li>
                            <li>Is it brave and made your heart beat a little faster?</li>
                            <li>Is it so great, that you find yourself nodding and totally convinced when you see the strong results?</li>
                            <li>Does it showcase brilliant creativity that leads to effectiveness?</li>
                          </ul>
                          @endif

                          @if($category->id == 2)
                          <ul class="m-0 circle">
                            <li>Is the work showcasing an innovative solution for the platform/ medium?</li>
                            <li>Is it brave and made your heart beat a little faster?</li>
                            <li>Does the work make you wish you’ve thought of the digital idea first?</li>
                            <li>Is it a perfect campaign of creativity and technology?</li>
                            <li>Is it something you’ve never thought technology can actually do for a brand?</li>
                          </ul>
                          @endif

                          @if($category->id == 3)
                          <ul class="m-0 circle">
                            <li>Does the work make you wish you’ve thought of the idea first?</li>
                            <li>Is it so awesome that it deserves some serious “slow clapping” ?</li>
                            <li>Is it something you’ve never seen before?</li>
                            <li>Is it brave and made your heart beat a little faster?</li>
                          </ul>
                          @endif

                          @if($category->id == 4)
                          <ul class="m-0 circle">
                            <li>Is it brave and made your heart beat a little faster?</li>
                            <li>Is it so great, that you find yourself touched and totally convinced when you see the effect to humanity?</li>
                            <li>Is it genuine creativity that seeks to help humanity and the world?</li>
                            <li>Is it a perfect campaign of creativity and technology?</li>
                            <li>Does it make you love a brand because of the purpose it represents?</li>
                          </ul>
                          @endif

                          @if($category->id == 4)
                          <ul class="m-0 circle">
                            <li>Is it brave and made your heart beat a little faster?</li>
                            <li>Is it so great, that you find yourself touched and totally convinced when you see the effect to humanity?</li>
                            <li>Is it genuine creativity that seeks to help humanity and the world?</li>
                            <li>Does it make you love a brand because of the purpose it represents?</li>
                          </ul>
                          @endif

                          @if($category->id == 5)
                          <ul class="m-0 circle">
                            <li>Is the work distinctive, fresh, and exceptional in its category?</li>
                            <li>Is  it so awesome that it deserves some serious “slow clapping”?</li>
                            <li>Is it something you’ve never seen before?</li>
                            <li>Is it brave and made your heart beat a little faster?</li>
                            <li>Does it deserve to be rewarded for the painstaking attention to detail and dedication to the craft?</li>
                          </ul>
                          @endif
                        <div style="text-align:center;margin-top:30px;margin-bottom:30px;">
                          <a href="{{ url('/authenticate') }}/{!!$token!!}/{!!$email!!}" style="background-color:#D62888;color:#fff;padding:10px;border:none;text-decoration:none;padding-left:30px;padding-right:30px;">GET STARTED</a>
                        </div>
                        <p>For any clarifications,  send us an e-mail : 4asp@pldtdsl.net</p>                       
                      </td>
                    </tr>
                  </table>
                </td>
              </tr>

            <!-- END MAIN CONTENT AREA -->
            </table>

            <!-- START FOOTER -->
            <div class="footer" style="clear: both; Margin-top: 10px; text-align: center; width: 100%; background-color:#3d3d3d;">
              <table border="0" cellpadding="0" cellspacing="0" style="border-collapse: separate; mso-table-lspace: 0pt; mso-table-rspace: 0pt; width: 100%;">
                <tr>
                  <td class="content-block powered-by" style="font-family: sans-serif; vertical-align: top; padding-bottom: 10px; padding-top: 10px; font-size: 14px; color: #fff; text-align: center;">
                   <strong>CONTACT US</strong> <br>
                   <strong>4as Secretariat:</strong> Vanne Tomada <br>
                   <strong>Address:</strong> Unit 442 Cityland Pasong Tamo Tower, <br>
                   2210 Chino Roces, Makati City <br>
                   <strong>Email:</strong> <span style="color:#fff;">4ASP@pldtdsl.net</span> <br>
                   <strong>Telephone Number:</strong> 8931205/ 7573891/ 8134397 <br>
                   <strong>Mobile Number:</strong> 09175222427
                  </td>
                </tr>
              </table>
            </div>
            <!-- END FOOTER -->

          <!-- END CENTERED WHITE CONTAINER -->
          </div>
        </td>
        <td style="font-family: sans-serif; font-size: 14px; vertical-align: top;">&nbsp;</td>
      </tr>
    </table>
  </body>
</html>
