<!doctype html>
<html>
  <head>
    <meta name="viewport" content="width=device-width">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>Kidlat Awards</title>
    <style>
    
    table.bordered {
      font-family: arial, sans-serif;
      border-collapse: collapse;
      width: 100%;
    }

    table.bordered td, table.bordered th {
      border: 1px solid #dddddd;
      text-align: left;
      padding: 8px;
    }

    table.bordered tr:nth-child(even) {
      background-color: #dddddd;
    }

    /* -------------------------------------
        PRESERVE THESE STYLES IN THE HEAD
    ------------------------------------- */
    @media all {
      .ExternalClass {
        width: 100%;
      }
      .ExternalClass,
            .ExternalClass p,
            .ExternalClass span,
            .ExternalClass font,
            .ExternalClass td,
            .ExternalClass div {
        line-height: 100%;
      }
      .apple-link a {
        color: inherit !important;
        font-family: inherit !important;
        font-size: inherit !important;
        font-weight: inherit !important;
        line-height: inherit !important;
        text-decoration: none !important;
      }
      .btn-primary table td:hover {
        background-color: #34495e !important;
      }
      .btn-primary a:hover {
        background-color: #34495e !important;
        border-color: #34495e !important;
      }
    }
    </style>
  </head>
  <body class="" style="background-color: #fff; font-family: sans-serif; -webkit-font-smoothing: antialiased; font-size: 14px; line-height: 1.4; margin: 0; padding: 0; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;">
    <table border="0" cellpadding="0" cellspacing="0" class="body" style="border-collapse: separate; mso-table-lspace: 0pt; mso-table-rspace: 0pt; width: 100%; background-color: #fff;">
        <td style="font-family: sans-serif; font-size: 14px; vertical-align: top;">&nbsp;</td>
        <td class="container" style="font-family: sans-serif; font-size: 14px; vertical-align: top; display: block; Margin: 0 auto; width: 100%;">
          
            <div class="header" style="clear: both;text-align: center; width: 100%;background-color:#5d59a5;">
              <table border="0" cellpadding="0" cellspacing="0" style="border-collapse: separate; mso-table-lspace: 0pt; mso-table-rspace: 0pt; width: 100%;">
                <tr>
                  <td class="content-block powered-by" style="font-family: sans-serif; vertical-align: top; padding-bottom: 10px; padding-top: 10px; font-size: 12px; color: #000; text-align: center;">
                    <a href="{{ url('/') }}" target="_blank"><img src="https://kidlatawards.com.ph/img/kidlat-logo.png" width="70"></a>
                  </td>
                </tr>
              </table>
            </div>
          
          <div class="content" style="box-sizing: border-box; display: block; Margin: 0 auto; padding: 10px;">
            
            <!-- START CENTERED WHITE CONTAINER -->
            <span class="preheader" style="color: transparent; display: none; height: 0; max-height: 0; max-width: 0; opacity: 0; overflow: hidden; mso-hide: all; visibility: hidden; width: 0;"></span>
            <table class="main" style="border-collapse: separate; mso-table-lspace: 0pt; mso-table-rspace: 0pt; width: 100%; background: #fff; border-radius: 3px;max-width: 700px;margin:0 auto;">

              <!-- START MAIN CONTENT AREA -->
              <tr>
                <td class="wrapper" style="font-family: sans-serif; font-size: 14px; vertical-align: top; box-sizing: border-box; padding: 20px;">
                  <table border="0" cellpadding="0" cellspacing="0" style="border-collapse: separate; mso-table-lspace: 0pt; mso-table-rspace: 0pt; width: 100%;">
                    <tr>
                      <td style="font-family: sans-serif; font-size: 14px; vertical-align: top;">
                        <p style="font-family: sans-serif; font-size: 14px; font-weight: normal; margin: 0; Margin-bottom: 15px;">Hello <span style="color:#D62888;">{!!$name!!}!</span></p>
                        <p style="font-family: sans-serif; font-size: 14px; font-weight: normal; margin: 0; Margin-bottom: 15px;"> The cost for your {{$data['single']}} single entry & {{$data['campaign']}} campaign entry  is Php {{$data['total']}} + 12% VAT.</p>
                        <p style="font-family: sans-serif; font-size: 14px; font-weight: normal; margin: 0; Margin-bottom: 15px;"> Here's a summary of  your entry(ies):</p>
                        <table style="width:100%" class="bordered">
                          <tr>
                            <th>ID</th>
                            <th>Entry Name</th> 
                            <th>Type</th>
                            <th>Category</th>
                          </tr>
                          @foreach($entries as $e)
                          <tr>
                            <td>{{$e->uid}}</td>
                            <td>{{$e->entry_name}}</td> 
                            <td>{{$e->type}}</td>
                            <td>{{$e->category()->name}}</td>
                          </tr>
                          @endforeach
                        </table>
                        <p style="font-family: sans-serif; font-size: 14px; font-weight: normal; margin: 0; Margin-bottom: 15px;"> Your payment options are:</p>
                        <ol>
                          <li>Through a Bank Deposit to the 4As bank account. For bank deposits, kindly email 4asp@pldtdsl.net a scanned copy of the deposit slip.</li>
                          <li>Via a Cheque deposited to the 4As bank account or delivered to the 4As office.</li>
                          <li>For cheque deposits, kindly email 4asp@pldtdsl.net a scanned copy of the deposit slip</li>
                          <li>If cheque is delivered to the 4as office, kindly email 4asp@pldtdsl.net a scanned copy of OR issued by 4as.</li>
                        </ol>
                        <p style="font-family: sans-serif; font-size: 14px; font-weight: normal; margin: 0; Margin-bottom: 15px;padding-left:40px;"> 
                          <b>Bank Details:</b> <br>
                          <b>Account Name</b> - 4As PHILIPPINES or ASSOC. OF ACCREDITED ADVERTISING AGENCIES PHILS. <br>
                          <b>Account Number</b> - 0031-0731-54 <br>
                          <b>Bank</b>: BPI <br>
                          <b>Branch</b>: PHILAMLIFE TOWER, AYALA PASEO BRANCH
                        </p>
                        <p style="font-family: sans-serif; font-size: 14px; font-weight: normal; margin: 0; Margin-bottom: 15px;">For any clarifications,  send us an e-mail : 4asp@pldtdsl.net</p>                       
                      </td>
                    </tr>
                  </table>
                </td>
              </tr>

            <!-- END MAIN CONTENT AREA -->
            </table>

            <!-- START FOOTER -->
            <div class="footer" style="clear: both; Margin-top: 10px; text-align: center; width: 100%; background-color:#3d3d3d;">
              <table border="0" cellpadding="0" cellspacing="0" style="border-collapse: separate; mso-table-lspace: 0pt; mso-table-rspace: 0pt; width: 100%;">
                <tr>
                  <td class="content-block powered-by" style="font-family: sans-serif; vertical-align: top; padding-bottom: 10px; padding-top: 10px; font-size: 14px; color: #fff; text-align: center;">
                   <strong>CONTACT US</strong> <br>
                   <strong>4as Secretariat:</strong> Vanne Tomada <br>
                   <strong>Address:</strong> Unit 442 Cityland Pasong Tamo Tower, <br>
                   2210 Chino Roces, Makati City <br>
                   <strong>Email:</strong> <span style="color:#fff;">4ASP@pldtdsl.net</span> <br>
                   <strong>Telephone Number:</strong> 8931205/ 7573891/ 8134397 <br>
                   <strong>Mobile Number:</strong> 09175222427
                  </td>
                </tr>
              </table>
            </div>
            <!-- END FOOTER -->

          <!-- END CENTERED WHITE CONTAINER -->
          </div>
        </td>
        <td style="font-family: sans-serif; font-size: 14px; vertical-align: top;">&nbsp;</td>
      </tr>
    </table>
  </body>
</html>
