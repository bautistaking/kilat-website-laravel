<!DOCTYPE html>
<html lang="en" xmlns="http://www.w3.org/1999/xhtml" xmlns:o="urn:schemas-microsoft-com:office:office">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width,initial-scale=1">
	<meta name="x-apple-disable-message-reformatting">
	<title></title>
	<style>
		table, td, div, h1, p {font-family: Arial, sans-serif;}
	</style>
</head>
<body style="margin:0;padding:0;">
	<table role="presentation" style="width: 100%;border-collapse:collapse;border:0;border-spacing:0;background:#ffffff;">
		<tr>
			<td align="center" style="padding:0;">
				<table role="presentation" style="width:900px;border-collapse:collapse;border-spacing:0;text-align:left;">
					<tr>
						<td align="center">
							<img src="{{url('/images/kv-email.png')}}" alt="" width="900" style="height:auto;display:block;" />
						</td>
					</tr>
					<tr> 
						<td style="padding:56px 94px 42px 94px;">
							<table role="presentation" style="width:100%;border-collapse:collapse;border:0;border-spacing:0;">
								<tr>
									<td style="color:#153643;">
										<h1 style="font-size:22px;margin:0 0 30px 0;font-family:Arial,sans-serif; font-weight: normal;">Hello <span style="font-weight: bold;">{{$name}},</span></h1>
										<p style="margin:0;line-height:28px;font-family:Arial,sans-serif;">Thank you for submitting an entry. Here’s your invoice request.</p>
									</td>
								</tr>
                                <tr>
                                    <td height="36">
                                </tr>
                                <tr>
                                    <td>
										<p style="font-family: sans-serif;font-weight: normal; margin: 0; Margin-bottom: 15px;"> The cost for your {{$data['single']}} single entry & {{$data['campaign']}} campaign entry  is PHP {{number_format($data['total'], 2, ".", ",")}} (inclusive of 12% VAT) @if($discount) (with 10% discount) @endif.</p>
										<p style="font-family: sans-serif; font-weight: normal; margin: 0; Margin-bottom: 15px;"> Here's a summary of  your entry(ies):</p>
										<table style="width:100%" class="bordered">
											<tr>
												<th style="font-weight:normal;width:150px">Transaction ID</th>
												<th style="font-weight:normal;">Entry Name</th>
												<th style="font-weight:normal;">Type</th>
												<th style="font-weight:normal;">Category</th>
												<th style="font-weight:normal;">Fee VAT EX</th>
											</tr>
											@foreach($data['entries'] as $entry)
												<tr>
													<td>{{$entry->uid}}</td>
													<td>{{$entry->entry_name}}</td>
													<td>{{$entry->type}}</td>
													<td>{{$entry->category()->name}}</td>
													<td style="text-align:right">{{number_format($entry->fee, 2, ".", ",")}}</td>
												</tr>
											@endforeach
										</table>
                                    </td>
								</tr>
                                <tr>
                                    <td height="36">
                                </tr>
								<tr>
									<td>
										<p style="color:#1A2735;margin:0;line-height:28px;font-weight: bold;font-family:Arial,sans-serif;">
											Your payment options are:
                                        </p>
									</td>
								</tr>
								<tr>
                                    <td height="20">
                                </tr>
								<tr>
									<td style="color:#384753;">
										<p style="color:#1A2735;margin:0;line-height:28px;font-weight: bold;font-family:Arial,sans-serif;">
											For check payment
                                        </p>
									</td>
								</tr>
                                <tr>
									<td style="color:#384753;">
										<p style="margin:0 0 0 10px;line-height:28px;font-family:Arial,sans-serif;">
											&#8226; Kindly make check payable to 4As Philippines and remit via bank deposit to 4As account: 
                                        </p>
									</td>
								</tr>
								<tr>
									<td style="color:#384753;">
										<p style="margin:20px 0 0 20px;line-height:28px;font-family:Arial,sans-serif;">
											<span style="color:#1A2735; font-weight: bold; ">Account Name:</span> 
											4As PHILIPPINES or ASSOC. OF ACCREDITED ADVERTISING AGENCIES PHILS.
										</p>
										<p style="margin:0 0 0 20px;line-height:28px;font-family:Arial,sans-serif;">
											<span style="color:#1A2735; font-weight: bold; ">Account Number:</span> 
											0031 0334 03
										</p>
										<p style="margin:0 0 0 20px;line-height:28px;font-family:Arial,sans-serif;">
											<span style="color:#1A2735; font-weight: bold; ">Bank:</span> 
											BPI
										</p>
										<p style="margin:0 0 0 20px;line-height:28px;font-family:Arial,sans-serif;">
											<span style="color:#1A2735; font-weight: bold; ">Branch:</span> 
											PHILAMLIFE TOWER, AYALA PASEO BRANCH
										</p>
									</td>
								</tr>
								<tr>
									<td style="color:#384753;">
										<p style="margin:20px 0 0 10px;line-height:28px;font-family:Arial,sans-serif;">
											&#8226; Please email to 4asp@pldtdsl.net , copy of the bank deposit slip
                                        </p>
									</td>
								</tr>
								<tr>
									<td style="color:#384753;">
										<p style="margin:0 0 0 10px;line-height:28px;font-family:Arial,sans-serif;">
											&#8226; 4As will issue the official receipt upon receipt of proof of payment. 
                                        </p>
									</td>
								</tr>
								<tr>
									<td style="color:#384753;">
										<p style="margin:0 0 0 10px;line-height:28px;font-family:Arial,sans-serif;">
											&#8226; 4As to confirm list of entries via email
                                        </p>
									</td>
								</tr>
								<tr>
                                    <td height="20">
                                </tr>
								<tr>
									<td style="color:#384753;">
										<p style="color:#1A2735;margin:0;line-height:28px;font-weight: bold;font-family:Arial,sans-serif;">
											For cash payments
                                        </p>
									</td>
								</tr>
                                <tr>
									<td style="color:#384753;">
										<p style="margin:0 0 0 10px;line-height:28px;font-family:Arial,sans-serif;">
											&#8226; Kindly remit cash via bank transfers to 4As bank account (same as above)
                                        </p>
									</td>
								</tr>
								<tr>
									<td style="color:#384753;">
										<p style="margin:0 0 0 10px;line-height:28px;font-family:Arial,sans-serif;">
											&#8226; Please email scanned copy of bank transfer transaction receipt to 4asp@pldtdsl.net
                                        </p>
									</td>
								</tr>
								<tr>
									<td style="color:#384753;">
										<p style="margin:0 0 0 10px;line-height:28px;font-family:Arial,sans-serif;">
											&#8226; 4As will issue the official receipt upon receipt of proof of payment
                                        </p>
									</td>
								</tr>
								<tr>
									<td style="color:#384753;">
										<p style="margin:0 0 0 10px;line-height:28px;font-family:Arial,sans-serif;">
											&#8226; 4As to confirm list of entries via email
                                        </p>
									</td>
								</tr>
                                <tr>
                                    <td height="36">
                                </tr>
                                <tr>
									<td style="color:#384753;">
										<p style="margin:0;line-height:28px;font-family:Arial,sans-serif;">
                                            Thank you,
                                            <br>
                                            Your Kidlat Committee
                                        </p>
									</td>
								</tr>
							</table>
						</td>
					</tr>
                    <tr>
                        <td height="60">
                    </tr>
					<tr>
						<td style="padding:38px 60px 80px;background:#43FAFC;" align="center">
							<table role="presentation" style="width:100%;border-collapse:collapse;border:0;border-spacing:0;font-size:9px;font-family:Arial,sans-serif;">
								<tr>
									<td style="padding:0;width:296px;" align="right" valign="top">
                                        <img src="{{url('/images/logos-for-email.png')}}" alt="" width="296" height="151" style="display:block;" />
									</td>
                                    <td style="width:50px;padding:0;font-size:0;line-height:0;">&nbsp;</td>
									<td style="padding:0;width:397px;color:#1A2735;" align="left">
                                        <h2 style="margin:0 0 13px 0;font-family:Arial,sans-serif; font-weight: 700; color: #793694;">Contact us</h1>
                                        <p style="margin:0 0 12px 0;font-size:16px;line-height:24px;font-family:Arial,sans-serif;font-weight: 700; color: #793694;">
											4As Secretariat: <span style="font-weight: 400; color:#1A2735;">Vanne Tomada</span>
										</p>
                                        <p style="margin:0 0 12px 0;font-size:16px;line-height:24px;font-family:Arial,sans-serif;font-weight: 700; color: #793694;">
											Address: <span style="font-weight: 400; color:#1A2735;">Unit 442 Cityland Pasong Tamo Tower, 2210 Chino Roces, Makati City</span>
										</p>
                                        <p style="margin:0 0 12px 0;font-size:16px;line-height:24px;font-family:Arial,sans-serif;font-weight: 700; color: #793694;">
											Email address: <a href="http://www.example.com" style="color:#1A2735; text-decoration:underline;font-weight: 400;" target="_blank">4asp@pldtdsl.net</a>
										</p>
                                        <p style="margin:0 0 12px 0;font-size:16px;line-height:24px;font-family:Arial,sans-serif;font-weight: 700; color: #793694;">
											Telephone number: <a href="http://www.example.com" style="color:#1A2735;text-decoration:underline;font-weight: 400;" target="_blank">8893-1205</a>
										</p>
                                        <p style="margin:0;font-size:16px;line-height:24px;font-family:Arial,sans-serif;font-weight: 700; color: #793694;">
											Mobile number: <a href="http://www.example.com" style="color:#1A2735; text-decoration:underline;font-weight: 400;" target="_blank">0917 5222 427</a>
										</p>			
									</td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
</body>
</html>