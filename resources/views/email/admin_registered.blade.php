<!DOCTYPE html>
<html lang="en" xmlns="http://www.w3.org/1999/xhtml" xmlns:o="urn:schemas-microsoft-com:office:office">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width,initial-scale=1">
    <meta name="x-apple-disable-message-reformatting">
    <title></title>
    <style>
        table, td, div, h1, p {font-family: Arial, sans-serif;}
    </style>
</head>
<body style="margin:0;padding:0;">
    <table role="presentation" style="width: 100%;border-collapse:collapse;border:0;border-spacing:0;background:#ffffff;">
        <tr>
            <td align="center" style="padding:0;">
                <table role="presentation" style="width:900px;border-collapse:collapse;border-spacing:0;text-align:left;">
                    <tr>
                        <td align="center">
                            <img src="{{url('/images/kv-email.png')}}" alt="" width="900" style="height:auto;display:block;" />
                        </td>
                    </tr>
                    <tr> 
                        <td style="padding:56px 94px 42px 94px;">
                            <table role="presentation" style="width:100%;border-collapse:collapse;border:0;border-spacing:0;">
                                <tr>
                                    <td style="color:#153643;">
                                        <h1 style="font-size:22px;margin:0 0 30px 0;font-family:Arial,sans-serif; font-weight: normal;">Hello {!!$name!!}<span style="font-weight: bold;">,</span></h1>
                                        <p style="margin:0;font-size:18px;line-height:28px;font-family:Arial,sans-serif;">Here is your sign-in credentials, do not share this credentials to anyone.
                                            <br><br>
                                            <span>Admin URL: <strong>{!!$url!!}</strong></span><br>
                                            <span>Email: <strong>{!!$email!!}</strong></span><br>
                                            <span>Password: <strong>{!!$password!!}</strong></span>
                                        </p>
                                    </td>
                                </tr>
                                <tr>
                                    <td height="36">
                                </tr>
                                <tr>
                                    <td style="color:#384753;">
                                        <p style="margin:0;font-size:18px;line-height:28px;font-family:Arial,sans-serif;">
                                            Thank you,
                                            <br>
                                            Your Kidlat Committee
                                        </p>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td height="60">
                    </tr>
                    <tr>
                        <td style="padding:38px 60px 80px;background:#43FAFC;" align="center">
                            <table role="presentation" style="width:100%;border-collapse:collapse;border:0;border-spacing:0;font-size:9px;font-family:Arial,sans-serif;">
                                <tr>
                                    <td style="padding:0;width:296px;" align="right" valign="top">
                                        <img src="{{url('/images/logos-for-email.png')}}" alt="" width="296" height="151" style="display:block;" />
                                    </td>
                                    <td style="width:50px;padding:0;font-size:0;line-height:0;">&nbsp;</td>
                                    <td style="padding:0;width:397px;color:#DCE3EA;" align="left">
                                        <h2 style="font-size:18px;margin:0 0 13px 0;font-family:Arial,sans-serif; font-weight: 700; color: #D7A333;">Contact us</h1>
                                        <p style="margin:0 0 12px 0;font-size:16px;line-height:24px;font-family:Arial,sans-serif;font-weight: 700;">
                                            4As Secretariat: <span style="font-weight: 400;">Vanne Tomada</span>
                                        </p>
                                        <p style="margin:0 0 12px 0;font-size:16px;line-height:24px;font-family:Arial,sans-serif;font-weight: 700;">
                                            Address: <span style="font-weight: 400;">Unit 442 Cityland Pasong Tamo Tower, 2210 Chino Roces, Makati City</span>
                                        </p>
                                        <p style="margin:0 0 12px 0;font-size:16px;line-height:24px;font-family:Arial,sans-serif;font-weight: 700;">
                                            Email address: <a href="http://www.example.com" style="color:#DCE3EA;text-decoration:underline;font-weight: 400;" target="_blank">4asp@pldtdsl.net</a>
                                        </p>
                                        <p style="margin:0 0 12px 0;font-size:16px;line-height:24px;font-family:Arial,sans-serif;font-weight: 700;">
                                            Telephone number: <a href="http://www.example.com" style="color:#DCE3EA;text-decoration:underline;font-weight: 400;" target="_blank">8893-1205</a>
                                        </p>
                                        <p style="margin:0;font-size:16px;line-height:24px;font-family:Arial,sans-serif;font-weight: 700;">
                                            Mobile number: <a href="http://www.example.com" style="color:#DCE3EA;text-decoration:underline;font-weight: 400;" target="_blank">0917 5222 427</a>
                                        </p>            
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</body>
</html>