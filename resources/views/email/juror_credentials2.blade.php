<!DOCTYPE html>
<html lang="en" xmlns="http://www.w3.org/1999/xhtml" xmlns:o="urn:schemas-microsoft-com:office:office">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width,initial-scale=1">
    <meta name="x-apple-disable-message-reformatting">
    <title></title>
    <style>
        table, td, div, h1, p {font-family: Arial, sans-serif;}
    </style>
</head>
<body style="margin:0;padding:0;">
    <table role="presentation" style="width: 100%;border-collapse:collapse;border:0;border-spacing:0;background:#ffffff;">
        <tr>
            <td align="center" style="padding:0;">
                <table role="presentation" style="width:900px;border-collapse:collapse;border-spacing:0;text-align:left;">
                    <tr>
                        <td align="center">
                            <img src="{{url('/images/kv-email.png')}}" alt="" width="900" style="height:auto;display:block;" />
                        </td>
                    </tr>
                    <tr> 
                        <td style="padding:56px 94px 42px 94px;">
                            <table role="presentation" style="width:100%;border-collapse:collapse;border:0;border-spacing:0;">
                                <tr>
                                    <td style="color:#153643;">
                                        <h1 style="font-size:22px;margin:0 0 30px 0;font-family:Arial,sans-serif; font-weight: normal;">Hello <span style="font-weight: bold;">{!!$name!!},</span></h1>
                                        <p style="margin:0;font-size:18px;line-height:28px;font-family:Arial,sans-serif;">This is it! Welcome to the metal round. Once again, thank you for sharing your time and expertise with us.
                                        </p><br>
                                        <p style="margin:0;font-size:18px;line-height:28px;font-family:Arial,sans-serif;">
                                            You will be judging the following category:
                                        </p><br><br>
                                        <h2 style="font-size:18px;margin:0 0 30px 0;font-family:Arial,sans-serif; font-weight: normal;">
                                            @foreach($categories as $category)
                                                <p><strong>{!!$category!!}</strong></p>
                                            @endforeach
                                        </h2>
                                        <br><br>
                                        <p>You will find detailed instructions on the process and criteria for judging in the link below</p>
                                        <p>
                                            <a class="link-color" href="https://youtu.be/7W5SJLPAETc" target="_blank">https://youtu.be/7W5SJLPAETc</a>
                                        </p><br><br>

                                        <p>For <strong>Round 2</strong>, please use the following guidelines when reviewing the entries:</p><br>
                                        <p><strong>4</strong> - Gold</p>
                                        <p><strong>3</strong> - Silver</p>
                                        <p><strong>2</strong> - Bronze</p>
                                        <p><strong>1</strong> - Finalist</p><br>

                                        <p>Things judges should consider when viewing the work:</p>
                                        <ul>
                                            <li>I want the people behind this in my team.</li>
                                            <li>That was so simple yet so brilliant.</li> 
                                            <li>This reminds me of something else, only this one's way better!</li>
                                            <li>This campaign gave me chills.</li>
                                            <li>Tinamaan ako ng Kidlat dito.</li>
                                        </ul>
                                        <br>
                                        <p>For the best experience, please make sure you are using your desktop device and are browsing on either Firefox or Chrome before clicking on the link below</p>
                                    </td>
                                </tr>
                                <tr>
                                    <td height="36"></td>
                                </tr>
                                <tr>
                                    <td style="color:#384753;">
                                        <p style="margin:0;font-size:18px;line-height:28px;font-family:Arial,sans-serif;">
                                            Thank you and may the Kidlat be with you!
                                            <br><br>
                                            Your Kidlat Committee
                                        </p>
                                    </td>
                                </tr>
                                <tr>
                                    <td height="36"></td>
                                </tr>
                                <tr>
                                    <td align="center">
                                        <p>Or copy and paste this link into your browser: {{ url('/authenticate') }}/{!!$token!!}/{!!$email!!}</p><br>
                                        <a href="{{ url('/authenticate') }}/{!!$token!!}/{!!$email!!}" target="_blank">
                                            <img src="{{url('/images/btn-judging-email.png')}}" alt="" width="360" style="height:auto;display:block;" />
                                        </a>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td height="60">
                    </tr>
                    <tr>
                        <td style="padding:38px 60px 80px;background:#27313A;" align="center">
                            <table role="presentation" style="width:100%;border-collapse:collapse;border:0;border-spacing:0;font-size:9px;font-family:Arial,sans-serif;">
                                <tr>
                                    <td style="padding:0;width:296px;" align="right" valign="top">
                                        <img src="{{url('/images/logos-for-email.png')}}" alt="" width="296" height="151" style="display:block;" />
                                    </td>
                                    <td style="width:50px;padding:0;font-size:0;line-height:0;">&nbsp;</td>
                                    <td style="padding:0;width:397px;color:#DCE3EA;" align="left">
                                        <h2 style="font-size:18px;margin:0 0 13px 0;font-family:Arial,sans-serif; font-weight: 700; color: #D7A333;">Contact us</h1>
                                        <p style="margin:0 0 12px 0;font-size:16px;line-height:24px;font-family:Arial,sans-serif;font-weight: 700;">
                                            4As Secretariat: <span style="font-weight: 400;">Vanne Tomada</span>
                                        </p>
                                        <p style="margin:0 0 12px 0;font-size:16px;line-height:24px;font-family:Arial,sans-serif;font-weight: 700;">
                                            Address: <span style="font-weight: 400;">Unit 442 Cityland Pasong Tamo Tower, 2210 Chino Roces, Makati City</span>
                                        </p>
                                        <p style="margin:0 0 12px 0;font-size:16px;line-height:24px;font-family:Arial,sans-serif;font-weight: 700;">
                                            Email address: <a href="http://www.example.com" style="color:#DCE3EA;text-decoration:underline;font-weight: 400;" target="_blank">4asp@pldtdsl.net</a>
                                        </p>
                                        <p style="margin:0 0 12px 0;font-size:16px;line-height:24px;font-family:Arial,sans-serif;font-weight: 700;">
                                            Telephone number: <a href="http://www.example.com" style="color:#DCE3EA;text-decoration:underline;font-weight: 400;" target="_blank">8893-1205</a>
                                        </p>
                                        <p style="margin:0;font-size:16px;line-height:24px;font-family:Arial,sans-serif;font-weight: 700;">
                                            Mobile number: <a href="http://www.example.com" style="color:#DCE3EA;text-decoration:underline;font-weight: 400;" target="_blank">0917 5222 427</a>
                                        </p>            
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</body>
</html>