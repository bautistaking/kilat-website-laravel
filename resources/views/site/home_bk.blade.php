@extends('base')

@push('styles')
  <style>
    body {
      background-color: #5d59a5;
    }
  </style>
@endpush

@section('content')
<div class="sub-container">
@include('site.components.nav')
<div class="container home">
  <h1 class="_welcome m-0">Welcome To</h1>
  <h1 class="_highlight m-0">Kidlat Awards 2019</h1>
  <p class="blurb">
    It’s time to remind everyone that in all great <br> advertising, Creativity Rules.
  </p>
  <span><img src="{{ asset('/images/4as.png') }}" width="140" alt=""></span>
  <div>
    <a href="{{asset('/files/Finalists-Kidlat-Awards-2019.pdf')}}" target="_blank" class="waves-effect waves-dark btn pink">Check out this year's finalists</a>
  </div>
</div>
</div>
@include('site.components.footer')
<!-- Modal Structure -->
<div id="rules" class="modal modal-fixed-footer">
  <div class="modal-content">
    @include('site.components.rules')
  </div>
  <div class="modal-footer">
    <a href="#!" class="modal-close waves-effect waves-dark btn pink">Close</a>
  </div>
</div>
<div id="faqs" class="modal modal-fixed-footer">
  <div class="modal-content">
    @include('site.components.faqs')
  </div>
  <div class="modal-footer">
    <a href="#!" class="modal-close waves-effect waves-dark btn pink">Close</a>
  </div>
</div>
@push('custom-js')
<script>
@if (session('success'))
  $( document ).ready(function() {
    Swal.fire({
      title: "{{ session('success')['header'] }}",
      text: "{{ session('success')['message'] }}",
      imageUrl: "/images/Success.svg",
      imageWidth: 100,
      imageHeight: 111,
    });
  });
@endif
@if (session('error'))
  $( document ).ready(function() {
    Swal.fire({
      title: "{{ session('error')['header'] }}",
      text: "{{ session('error')['message'] }}",
      imageUrl: "/images/Error.svg",
      imageWidth: 100,
      imageHeight: 124,
    });
  });
@endif
@if ($modal == 'faq')
  $( document ).ready(function() {
    $('#faqs').modal('open');
  });
@endif
</script>
@endpush
@endsection