<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en" class="no-js">
<!--<![endif]-->

    <head>
          <meta charset="UTF-8">
          <meta name="csrf-token" content="{{ csrf_token() }}">
          <meta name="viewport" content="width=device-width, initial-scale=1.0">
          <meta http-equiv="X-UA-Compatible" content="ie=edge">
          <meta name="description" content="Are you ready to unlock the best of the best in Filipino creativity?" />
          <meta name="author" content="EBDigital" />
          <title>Kidlat Awards 2021 - Creativity Rules</title>
          <meta property="og:title" content="Welcome to Kidlat Awards!">
          <meta property="og:description" content="Are you ready to unlock the best of the best in Filipino creativity?">
          <!-- best size for Facebook: 1300 X 630 px -->
          <meta property="og:image" content="{{url('/images/og.jpg')}}"/>
          <meta property="og:url" content="https://www.kidlatawards.com"/>
          <meta property="og:type" content="website"/>
          <meta property="og:site_name" content="Welcome to Kidlat Awards!" />
          <meta name="twitter:card" content="summary_large_image">
          <meta name="twitter:creator" content=""/>
          <link rel="shortcut icon" href="{{ asset('/images/favicon.ico') }}">
        <title>Kidlat Awards 2021</title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1" name="viewport"/>

        <link rel="shortcut icon" href="{{url('/images/favicon.png')}}">
        <link rel="stylesheet" type="text/css" href="{{url('/css/main.min.css')}}" />
        <link rel="stylesheet" type="text/css" href="{{url('/css/custom.css')}}" />
        
        <!-- Bootstrap CSS -->
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-KyZXEAg3QhqLMpG8r+8fhAXLRk2vvoC2f3B09zVXn8CA5QIVfZOJ3BCsw2P0p/We" crossorigin="anonymous">
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.5.0/font/bootstrap-icons.css">
        <link rel="preconnect" href="https://fonts.googleapis.com">
        <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
        <link href="https://fonts.googleapis.com/css2?family=Inter:wght@300;400;500;600;700;800;900&display=swap" rel="stylesheet">

        <!-- Global site tag (gtag.js) - Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=G-TKL0DQ8D42"></script>
        <script> 
          window.dataLayer = window.dataLayer || []; function gtag(){dataLayer.push(arguments);} gtag('js', new Date()); gtag('config', 'G-TKL0DQ8D42');
        </script>

        <!-- Google Tag Manager -->
          <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
          new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
          j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
          'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
          })(window,document,'script','dataLayer','GTM-WS7CLVN');</script>
          <!-- End Google Tag Manager -->

          <style>
            #confetti{
              position: absolute;
              left: 0;
              top: 0;
              height: 100%;
              width: 100%;
            }


            a.iprodev {
              line-height: normal;
              font-family: Varela Round, sans-serif;
              font-weight: 600;
              text-decoration: none;
              font-size: 13px;
              color: #A7AAAE;
              position: absolute;
              left: 20px;
              bottom: 20px;
              border: 1px solid #A7AAAE;
              padding: 12px 20px 10px;
              border-radius: 50px;
              transition: all .1s ease-in-out;
              text-transform: uppercase;
            }
          </style>
    </head>

<body class="full-width">
  <!-- Google Tag Manager (noscript) -->
  <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-WS7CLVN"
  height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
  <!-- End Google Tag Manager (noscript) -->
  <canvas id="confetti" style="width:1;height:1"></canvas>
    <div class="d-flex flex-grow-1 w-100 h-100">
        <div class="landing">
            <div class="d-flex flex-column justify-content-between align-items-center h-100">
                <div class="wrapper-default py-30 m-0" style="z-index:4;">
                    <div class="d-flex justify-content-between align-items-center w-100">
                        <div class="d-flex align-items-center">
                            <img class="logo-left" src="{{url('images/logo-white.svg')}}" alt="...">
                            <div class="horizontal-line"></div>
                            <a class="contact-us" data-bs-toggle="modal" data-bs-target="#contactusModal"><span>Contact us</span></a>
                        </div>
                        <img src="{{url('images/CREATIVEFEST_new 1.svg')}}" alt="...">
                    </div>
                </div>
                <div class="lower-part">
                    <img src="{{url('images/house-center.png')}}" alt="" class="house-center">
                    <img src="{{url('images/house-vertical.png')}}" alt="" class="house-vertical">
                    <!--
                    SIGN UP AND DOWNLOAD RULES
                    -->
                    <div class="lower-part-content">
                        <p><span class="fw-bold text-uppercase">2021 Shortlisted Entries</p>
                        <p style="margin:0;font-size: 18px">Watch the <strong>Kidlat Awards Night</strong></p>
                        <p style="margin:0;font-size: 18px">November 6, 2021</p>
                        <p style="margin:0;font-size: 18px">https://www.sample.com</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- modal -->
    <div class="voting-modal modal fade" id="contactusModal" tabindex="-1" aria-labelledby="flag" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered ">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body p-0 pt-4">
                    <div class="selection">
                        <p class="question contact-title">Contact 4As</p>
                        <div class="footer-links contact flex-column">
                            <p class="label">
                                4As Secretariat:&nbsp;<span class="value">Vanne Tomada</span>
                            </p>
                            <p class="label">
                                Address:&nbsp;<span class="value">Unit 442 Cityland Pasong Tamo Tower, 2210 Chino Roces, Makati City</span>
                            </p>
                            <p class="label">
                                Email address:&nbsp;<a href="mailto:4asp@pldtdsl.net" class="value" target="_blank">4asp@pldtdsl.net</a>
                            </p>
                            <div class="d-flex label">
                                Telephone number:&nbsp;<div class="separator"><a class="value">893-1205</a><a class="value">757-3891</a><a class="value">812-4397</a></div>
                            </div>
                            <p class="label">
                                Mobile number:&nbsp;<a href="#" class="value" target="_blank">0917 5222 427</a>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
  <script src="https://code.jquery.com/jquery-3.6.0.min.js" integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>
  <script src="{{url('/js/loadingoverlay.min.js')}}"></script>
  <script>
    $(document).on('click', '.profile-link', function() {
        Swal.fire({
            // confirmButtonText: 'Profile',
            showConfirmButton: false,
            denyButtonText: 'Logout',
            denyButtonClass: 'btn-logout',
            showDenyButton: true
        }).then((result) => {
            // if (result.isConfirmed) {
            //     location.href = "/profile"
            // }
            if (result.isDenied) {
                location.href = "/logout"
            }
        })
    })
  </script>
  <!-- end 2021 js -->  
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.3/dist/umd/popper.min.js" integrity="sha384-eMNCOe7tC1doHpGoWe/6oMVemdAVTMs2xqW4mwXrXsW0L84Iytr2wi5v2QjrP/xp" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/js/bootstrap.min.js" integrity="sha384-cn7l7gDp0eyniUwwAZgrzD06kc/tftFf19TOAs2zVinnD/C7E91j9yyk5//jjpt/" crossorigin="anonymous"></script>

    <script>
        $(document).ready(activateQuery())

        function activateQuery() {
            
        }

        var retina = window.devicePixelRatio,

        // Math shorthands
        PI = Math.PI,
        sqrt = Math.sqrt,
        round = Math.round,
        random = Math.random,
        cos = Math.cos,
        sin = Math.sin,

        // Local WindowAnimationTiming interface
        rAF = window.requestAnimationFrame,
        cAF = window.cancelAnimationFrame || window.cancelRequestAnimationFrame,
        _now = Date.now || function () {return new Date().getTime();};

    // Local WindowAnimationTiming interface polyfill
    (function (w) {
      /**
            * Fallback implementation.
            */
      var prev = _now();
      function fallback(fn) {
        var curr = _now();
        var ms = Math.max(0, 16 - (curr - prev));
        var req = setTimeout(fn, ms);
        prev = curr;
        return req;
      }

      /**
            * Cancel.
            */
      var cancel = w.cancelAnimationFrame
      || w.webkitCancelAnimationFrame
      || w.clearTimeout;

      rAF = w.requestAnimationFrame
      || w.webkitRequestAnimationFrame
      || fallback;

      cAF = function(id){
        cancel.call(w, id);
      };
    }(window));

    document.addEventListener("DOMContentLoaded", function() {
      var speed = 50,
          duration = (1.0 / speed),
          confettiRibbonCount = 11,
          ribbonPaperCount = 30,
          ribbonPaperDist = 8.0,
          ribbonPaperThick = 8.0,
          confettiPaperCount = 95,
          DEG_TO_RAD = PI / 180,
          RAD_TO_DEG = 180 / PI,
          colors = [
            ["#df0049", "#660671"],
            ["#00e857", "#005291"],
            ["#2bebbc", "#05798a"],
            ["#ffd200", "#b06c00"]
          ];

      function Vector2(_x, _y) {
        this.x = _x, this.y = _y;
        this.Length = function() {
          return sqrt(this.SqrLength());
        }
        this.SqrLength = function() {
          return this.x * this.x + this.y * this.y;
        }
        this.Add = function(_vec) {
          this.x += _vec.x;
          this.y += _vec.y;
        }
        this.Sub = function(_vec) {
          this.x -= _vec.x;
          this.y -= _vec.y;
        }
        this.Div = function(_f) {
          this.x /= _f;
          this.y /= _f;
        }
        this.Mul = function(_f) {
          this.x *= _f;
          this.y *= _f;
        }
        this.Normalize = function() {
          var sqrLen = this.SqrLength();
          if (sqrLen != 0) {
            var factor = 1.0 / sqrt(sqrLen);
            this.x *= factor;
            this.y *= factor;
          }
        }
        this.Normalized = function() {
          var sqrLen = this.SqrLength();
          if (sqrLen != 0) {
            var factor = 1.0 / sqrt(sqrLen);
            return new Vector2(this.x * factor, this.y * factor);
          }
          return new Vector2(0, 0);
        }
      }
      Vector2.Lerp = function(_vec0, _vec1, _t) {
        return new Vector2((_vec1.x - _vec0.x) * _t + _vec0.x, (_vec1.y - _vec0.y) * _t + _vec0.y);
      }
      Vector2.Distance = function(_vec0, _vec1) {
        return sqrt(Vector2.SqrDistance(_vec0, _vec1));
      }
      Vector2.SqrDistance = function(_vec0, _vec1) {
        var x = _vec0.x - _vec1.x;
        var y = _vec0.y - _vec1.y;
        return (x * x + y * y + z * z);
      }
      Vector2.Scale = function(_vec0, _vec1) {
        return new Vector2(_vec0.x * _vec1.x, _vec0.y * _vec1.y);
      }
      Vector2.Min = function(_vec0, _vec1) {
        return new Vector2(Math.min(_vec0.x, _vec1.x), Math.min(_vec0.y, _vec1.y));
      }
      Vector2.Max = function(_vec0, _vec1) {
        return new Vector2(Math.max(_vec0.x, _vec1.x), Math.max(_vec0.y, _vec1.y));
      }
      Vector2.ClampMagnitude = function(_vec0, _len) {
        var vecNorm = _vec0.Normalized;
        return new Vector2(vecNorm.x * _len, vecNorm.y * _len);
      }
      Vector2.Sub = function(_vec0, _vec1) {
        return new Vector2(_vec0.x - _vec1.x, _vec0.y - _vec1.y, _vec0.z - _vec1.z);
      }

      function EulerMass(_x, _y, _mass, _drag) {
        this.position = new Vector2(_x, _y);
        this.mass = _mass;
        this.drag = _drag;
        this.force = new Vector2(0, 0);
        this.velocity = new Vector2(0, 0);
        this.AddForce = function(_f) {
          this.force.Add(_f);
        }
        this.Integrate = function(_dt) {
          var acc = this.CurrentForce(this.position);
          acc.Div(this.mass);
          var posDelta = new Vector2(this.velocity.x, this.velocity.y);
          posDelta.Mul(_dt);
          this.position.Add(posDelta);
          acc.Mul(_dt);
          this.velocity.Add(acc);
          this.force = new Vector2(0, 0);
        }
        this.CurrentForce = function(_pos, _vel) {
          var totalForce = new Vector2(this.force.x, this.force.y);
          var speed = this.velocity.Length();
          var dragVel = new Vector2(this.velocity.x, this.velocity.y);
          dragVel.Mul(this.drag * this.mass * speed);
          totalForce.Sub(dragVel);
          return totalForce;
        }
      }

      function ConfettiPaper(_x, _y) {
        this.pos = new Vector2(_x, _y);
        this.rotationSpeed = (random() * 600 + 800);
        this.angle = DEG_TO_RAD * random() * 360;
        this.rotation = DEG_TO_RAD * random() * 360;
        this.cosA = 1.0;
        this.size = 5.0;
        this.oscillationSpeed = (random() * 1.5 + 0.5);
        this.xSpeed = 40.0;
        this.ySpeed = (random() * 60 + 50.0);
        this.corners = new Array();
        this.time = random();
        var ci = round(random() * (colors.length - 1));
        this.frontColor = colors[ci][0];
        this.backColor = colors[ci][1];
        for (var i = 0; i < 4; i++) {
          var dx = cos(this.angle + DEG_TO_RAD * (i * 90 + 45));
          var dy = sin(this.angle + DEG_TO_RAD * (i * 90 + 45));
          this.corners[i] = new Vector2(dx, dy);
        }
        this.Update = function(_dt) {
          this.time += _dt;
          this.rotation += this.rotationSpeed * _dt;
          this.cosA = cos(DEG_TO_RAD * this.rotation);
          this.pos.x += cos(this.time * this.oscillationSpeed) * this.xSpeed * _dt
          this.pos.y += this.ySpeed * _dt;
          if (this.pos.y > ConfettiPaper.bounds.y) {
            this.pos.x = random() * ConfettiPaper.bounds.x;
            this.pos.y = 0;
          }
        }
        this.Draw = function(_g) {
          if (this.cosA > 0) {
            _g.fillStyle = this.frontColor;
          } else {
            _g.fillStyle = this.backColor;
          }
          _g.beginPath();
          _g.moveTo((this.pos.x + this.corners[0].x * this.size) * retina, (this.pos.y + this.corners[0].y * this.size * this.cosA) * retina);
          for (var i = 1; i < 4; i++) {
            _g.lineTo((this.pos.x + this.corners[i].x * this.size) * retina, (this.pos.y + this.corners[i].y * this.size * this.cosA) * retina);
          }
          _g.closePath();
          _g.fill();
        }
      }
      ConfettiPaper.bounds = new Vector2(0, 0);

      function ConfettiRibbon(_x, _y, _count, _dist, _thickness, _angle, _mass, _drag) {
        this.particleDist = _dist;
        this.particleCount = _count;
        this.particleMass = _mass;
        this.particleDrag = _drag;
        this.particles = new Array();
        var ci = round(random() * (colors.length - 1));
        this.frontColor = colors[ci][0];
        this.backColor = colors[ci][1];
        this.xOff = (cos(DEG_TO_RAD * _angle) * _thickness);
        this.yOff = (sin(DEG_TO_RAD * _angle) * _thickness);
        this.position = new Vector2(_x, _y);
        this.prevPosition = new Vector2(_x, _y);
        this.velocityInherit = (random() * 2 + 4);
        this.time = random() * 100;
        this.oscillationSpeed = (random() * 2 + 2);
        this.oscillationDistance = (random() * 40 + 40);
        this.ySpeed = (random() * 40 + 80);
        for (var i = 0; i < this.particleCount; i++) {
          this.particles[i] = new EulerMass(_x, _y - i * this.particleDist, this.particleMass, this.particleDrag);
        }
        this.Update = function(_dt) {
          var i = 0;
          this.time += _dt * this.oscillationSpeed;
          this.position.y += this.ySpeed * _dt;
          this.position.x += cos(this.time) * this.oscillationDistance * _dt;
          this.particles[0].position = this.position;
          var dX = this.prevPosition.x - this.position.x;
          var dY = this.prevPosition.y - this.position.y;
          var delta = sqrt(dX * dX + dY * dY);
          this.prevPosition = new Vector2(this.position.x, this.position.y);
          for (i = 1; i < this.particleCount; i++) {
            var dirP = Vector2.Sub(this.particles[i - 1].position, this.particles[i].position);
            dirP.Normalize();
            dirP.Mul((delta / _dt) * this.velocityInherit);
            this.particles[i].AddForce(dirP);
          }
          for (i = 1; i < this.particleCount; i++) {
            this.particles[i].Integrate(_dt);
          }
          for (i = 1; i < this.particleCount; i++) {
            var rp2 = new Vector2(this.particles[i].position.x, this.particles[i].position.y);
            rp2.Sub(this.particles[i - 1].position);
            rp2.Normalize();
            rp2.Mul(this.particleDist);
            rp2.Add(this.particles[i - 1].position);
            this.particles[i].position = rp2;
          }
          if (this.position.y > ConfettiRibbon.bounds.y + this.particleDist * this.particleCount) {
            this.Reset();
          }
        }
        this.Reset = function() {
          this.position.y = -random() * ConfettiRibbon.bounds.y;
          this.position.x = random() * ConfettiRibbon.bounds.x;
          this.prevPosition = new Vector2(this.position.x, this.position.y);
          this.velocityInherit = random() * 2 + 4;
          this.time = random() * 100;
          this.oscillationSpeed = random() * 2.0 + 1.5;
          this.oscillationDistance = (random() * 40 + 40);
          this.ySpeed = random() * 40 + 80;
          var ci = round(random() * (colors.length - 1));
          this.frontColor = colors[ci][0];
          this.backColor = colors[ci][1];
          this.particles = new Array();
          for (var i = 0; i < this.particleCount; i++) {
            this.particles[i] = new EulerMass(this.position.x, this.position.y - i * this.particleDist, this.particleMass, this.particleDrag);
          }
        }
        this.Draw = function(_g) {
          for (var i = 0; i < this.particleCount - 1; i++) {
            var p0 = new Vector2(this.particles[i].position.x + this.xOff, this.particles[i].position.y + this.yOff);
            var p1 = new Vector2(this.particles[i + 1].position.x + this.xOff, this.particles[i + 1].position.y + this.yOff);
            if (this.Side(this.particles[i].position.x, this.particles[i].position.y, this.particles[i + 1].position.x, this.particles[i + 1].position.y, p1.x, p1.y) < 0) {
              _g.fillStyle = this.frontColor;
              _g.strokeStyle = this.frontColor;
            } else {
              _g.fillStyle = this.backColor;
              _g.strokeStyle = this.backColor;
            }
            if (i == 0) {
              _g.beginPath();
              _g.moveTo(this.particles[i].position.x * retina, this.particles[i].position.y * retina);
              _g.lineTo(this.particles[i + 1].position.x * retina, this.particles[i + 1].position.y * retina);
              _g.lineTo(((this.particles[i + 1].position.x + p1.x) * 0.5) * retina, ((this.particles[i + 1].position.y + p1.y) * 0.5) * retina);
              _g.closePath();
              _g.stroke();
              _g.fill();
              _g.beginPath();
              _g.moveTo(p1.x * retina, p1.y * retina);
              _g.lineTo(p0.x * retina, p0.y * retina);
              _g.lineTo(((this.particles[i + 1].position.x + p1.x) * 0.5) * retina, ((this.particles[i + 1].position.y + p1.y) * 0.5) * retina);
              _g.closePath();
              _g.stroke();
              _g.fill();
            } else if (i == this.particleCount - 2) {
              _g.beginPath();
              _g.moveTo(this.particles[i].position.x * retina, this.particles[i].position.y * retina);
              _g.lineTo(this.particles[i + 1].position.x * retina, this.particles[i + 1].position.y * retina);
              _g.lineTo(((this.particles[i].position.x + p0.x) * 0.5) * retina, ((this.particles[i].position.y + p0.y) * 0.5) * retina);
              _g.closePath();
              _g.stroke();
              _g.fill();
              _g.beginPath();
              _g.moveTo(p1.x * retina, p1.y * retina);
              _g.lineTo(p0.x * retina, p0.y * retina);
              _g.lineTo(((this.particles[i].position.x + p0.x) * 0.5) * retina, ((this.particles[i].position.y + p0.y) * 0.5) * retina);
              _g.closePath();
              _g.stroke();
              _g.fill();
            } else {
              _g.beginPath();
              _g.moveTo(this.particles[i].position.x * retina, this.particles[i].position.y * retina);
              _g.lineTo(this.particles[i + 1].position.x * retina, this.particles[i + 1].position.y * retina);
              _g.lineTo(p1.x * retina, p1.y * retina);
              _g.lineTo(p0.x * retina, p0.y * retina);
              _g.closePath();
              _g.stroke();
              _g.fill();
            }
          }
        }
        this.Side = function(x1, y1, x2, y2, x3, y3) {
          return ((x1 - x2) * (y3 - y2) - (y1 - y2) * (x3 - x2));
        }
      }
      ConfettiRibbon.bounds = new Vector2(0, 0);
      confetti = {};
      confetti.Context = function(id) {
        var i = 0;
        var canvas = document.getElementById(id);
        var canvasParent = canvas.parentNode;
        var canvasWidth = canvasParent.offsetWidth;
        var canvasHeight = canvasParent.offsetHeight;
        canvas.width = canvasWidth * retina;
        canvas.height = canvasHeight * retina;
        var context = canvas.getContext('2d');
        var interval = null;
        var confettiRibbons = new Array();
        ConfettiRibbon.bounds = new Vector2(canvasWidth, canvasHeight);
        for (i = 0; i < confettiRibbonCount; i++) {
          confettiRibbons[i] = new ConfettiRibbon(random() * canvasWidth, -random() * canvasHeight * 2, ribbonPaperCount, ribbonPaperDist, ribbonPaperThick, 45, 1, 0.05);
        }
        var confettiPapers = new Array();
        ConfettiPaper.bounds = new Vector2(canvasWidth, canvasHeight);
        for (i = 0; i < confettiPaperCount; i++) {
          confettiPapers[i] = new ConfettiPaper(random() * canvasWidth, random() * canvasHeight);
        }
        this.resize = function() {
          canvasWidth = canvasParent.offsetWidth;
          canvasHeight = canvasParent.offsetHeight;
          canvas.width = canvasWidth * retina;
          canvas.height = canvasHeight * retina;
          ConfettiPaper.bounds = new Vector2(canvasWidth, canvasHeight);
          ConfettiRibbon.bounds = new Vector2(canvasWidth, canvasHeight);
        }
        this.start = function() {
          this.stop()
          var context = this;
          this.update();
        }
        this.stop = function() {
          cAF(this.interval);
        }
        this.update = function() {
          var i = 0;
          context.clearRect(0, 0, canvas.width, canvas.height);
          for (i = 0; i < confettiPaperCount; i++) {
            confettiPapers[i].Update(duration);
            confettiPapers[i].Draw(context);
          }
          for (i = 0; i < confettiRibbonCount; i++) {
            confettiRibbons[i].Update(duration);
            confettiRibbons[i].Draw(context);
          }
          this.interval = rAF(function() {
            confetti.update();
          });
        }
      }
      var confetti = new confetti.Context('confetti');
      confetti.start();
      window.addEventListener('resize', function(event){
        confetti.resize();
      });
    });
    </script>
</body>
</html>