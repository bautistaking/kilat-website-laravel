@extends('base')  
@push('styles')
    <style>
        .spinner-grow,
        .loading-label {
            display: none;
        }
    </style>
@endpush
@section('content')
<div class="d-flex flex-grow-1 w-100 h-100">
    <div class="signin wrapper">
        <div class="d-flex flex-column flex-lg-row justify-content-center flex-grow-1">
            <div class="img-banner d-flex">
                <img class="banner img-fluid" src="{{url('/images/banner.png')}}" alt="...">
            </div>
            <div class="signin-form d-flex flex-column">
                <div class="d-flex align-items-center justify-content-between mb-3">
                    <a href="https://www.kidlatawards.com/" class="top-action">
                        <i class="bi bi-chevron-left me-2"></i><span style="color:#1a2735;">Back</span>
                    </a>
                </div>
                <div class="d-flex flex-column flex-grow-1 justify-content-center mb-5">
                    <div class="text-center header-wrapper">
                       <h2>Sign in with just your email</h2>
                        <p>We’ll send you a link that you can use to sign in</p>
                    </div>
                    <div class="form">
                        <input type="hidden" name="signin" value="1">
                        <div class="form-group">
                            <label for="signin_email" class="mb-2">Email address</label>
                            <input type="email" class="form-control lg" id="signin_email" name="email" placeholder="name@email.com">
                        </div>
                        <div id="invalid-email" class="invalid-feedback"></div>
                        <div class="text-center mt-46">
                            <button class="btn btn-primary primary lg mb-4" id="btn_signin">
                                <span class="spinner-grow spinner-grow-sm" role="status"></span>
                                <span class="spinner-grow spinner-grow-sm" role="status"></span>
                                <span class="spinner-grow spinner-grow-sm" role="status"></span>
                                <span class="signin-label text-uppercase">Send sign-in link</span>
                            </button>
                            <p>Don't have an account?</p>
                            <a href="javascript:void(0)" class="link-color" id="btn_signup">Sign up</a>
                      <!--       <button class="btn btn-outline-primary primary brown lg" id="btn_download_rules">Download rules</button> -->
                            <!-- <button class="btn btn-outline-primary primary brown lg" id="btn_signup">Sign-up</button> -->
                        </div>
                    </div>
                </div>
                <div class="signin-form-footer justify-content-end">
                    <h3 class="title">Need help? Contact 4As</h3>
                    <ul class="info">
                        <li>
                            <img src="images/email.png" alt="" />
                            <a href="mailto:4asp@pldtdsl.net">4asp@pldtdsl.net</a>
                        </li>
                        <li>
                            <img src="images/call.png" alt="" />
                            <a href="#">8893-1205</a>
                        </li>
                        <li>
                            <img src="images/mobile-phone.png" alt="" />
                            <a href="#">0917 5222 427</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
@push('custom-js')
<script>
    $(document).ready(activateQuery())

    function activateQuery() {
        $('#btn_signin').on('click', function() {
            $.LoadingOverlay("show", {
                image: 'images/loading1.png'
            },100);

            $('.signin-label').hide()
            $('.spinner-grow').addClass('d-inline-block')

            let params = {
                '_token': '{{ csrf_token() }}',
                'email': $('#signin_email').val()
            }

            fetch("/login", {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify(params)
            })
            .then(response => response.json())
            .then((e) => {
                let html = ""

                $('#signin_email').removeClass('is-invalid')
                $('#invalid-email').hide()

                if (e.status_code == 400) {
                    $.LoadingOverlay("hide");

                    $.each(e.message, function(a,b) {
                        html += b
                    })

                    $('#invalid-email').html(html).show()
                    $('#signin_email').addClass('is-invalid')

                    // Swal.fire({
                    //     icon: 'error',
                    //     title: 'Oops...',
                    //     html: html,
                    //     confirmButtonText: 'Close',
                    //     confirmButonColor: 'btn-primary'
                    // })

                    $('.signin-label').show()
                    $('.spinner-grow').removeClass('d-inline-block')
                }

                if (e.status_code == 200) {
                    $.LoadingOverlay("hide");
                    
                    $.each(e.message, function(a,b) {
                        html += "<ul class='list-group'>"
                        html += "<li class='list-group-item'>"+b+"</li>"
                        html += "</ul>"
                    })

                    // Swal.fire({
                    //     icon: 'success',
                    //     title: e.header,
                    //     html: html,
                    //     confirmButtonText: 'Close',
                    //     confirmButonColor: 'btn-primary'
                    // })

                    localStorage.setItem("email", params.email)
                    setTimeout(function() {
                        location.href = "/login-email-sent"
                    }, 100)
                }
            })
            .catch((error) => {

            })
        })

        $(document).on('click', '#btn_signup', function() {
            location.href = "/register"
        })
    }
</script>
@endpush
@endsection