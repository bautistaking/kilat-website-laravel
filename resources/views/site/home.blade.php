<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en" class="no-js">
<!--<![endif]-->

    <head>
        <meta charset="UTF-8">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <meta name="description" content="Are you ready to unlock the best of the best in Filipino creativity?" />
        <meta name="author" content="EBDigital" />
        <title>Kidlat Awards 2022 - Creativity Recharged</title>
        <meta property="og:title" content="Welcome to Kidlat Awards!">
        <meta property="og:description" content="Are you ready to unlock the best of the best in Filipino creativity?">
        <!-- best size for Facebook: 1300 X 630 px -->
        <meta property="og:image" content="{{url('/images/og-2.jpg')}}"/>
        <meta property="og:url" content="https://www.kidlatawards.com"/>
        <meta property="og:type" content="website"/>
        <meta property="og:site_name" content="Welcome to Kidlat Awards!" />
        <meta name="twitter:card" content="summary_large_image">
        <meta name="twitter:creator" content=""/>
        <link rel="shortcut icon" href="{{ asset('/images/Favicon.jpg') }}">
        <title>Kidlat Awards 2022</title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1" name="viewport"/>

        <link rel="shortcut icon" href="{{url('/images/Favicon.jpg')}}">
        <link rel="stylesheet" type="text/css" href="{{url('/css/main.min.css')}}" />
        <link rel="stylesheet" type="text/css" href="{{url('/css/custom.css')}}" />
        
        <!-- Bootstrap CSS -->
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-KyZXEAg3QhqLMpG8r+8fhAXLRk2vvoC2f3B09zVXn8CA5QIVfZOJ3BCsw2P0p/We" crossorigin="anonymous">
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.5.0/font/bootstrap-icons.css">
        <link rel="preconnect" href="https://fonts.googleapis.com">
        <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
        <link href="https://fonts.googleapis.com/css2?family=Inter:wght@300;400;500;600;700;800;900&display=swap" rel="stylesheet">

        <!-- Global site tag (gtag.js) - Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=G-TKL0DQ8D42"></script>
        <script> 
          window.dataLayer = window.dataLayer || []; function gtag(){dataLayer.push(arguments);} gtag('js', new Date()); gtag('config', 'G-TKL0DQ8D42');
        </script>

        <!-- Google Tag Manager -->
        <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
        new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
        })(window,document,'script','dataLayer','GTM-WS7CLVN');</script>
        <!-- End Google Tag Manager -->
    </head>

<body class="full-width">
    <!-- Google Tag Manager (noscript) -->
    <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-WS7CLVN"
    height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
    <!-- End Google Tag Manager (noscript) -->

    <div class="d-flex flex-grow-1 w-100 h-100">
        <div class="landing">
            <div class="d-flex flex-column justify-content-between align-items-center h-100">
                <div class="wrapper-default py-30 m-0" style="z-index:4;">
                    <div class="d-flex justify-content-between align-items-center w-100">
                        <div class="d-flex align-items-center">
                            <img class="logo-left" src="{{url('images/logo-white.svg')}}" alt="...">
                            <div class="horizontal-line"></div>
                            <a class="contact-us" data-bs-toggle="modal" data-bs-target="#contactusModal"><span>Contact us</span></a>
                        </div>
                        <img src="{{url('images/kidlat-logo-horizontal.svg')}}" alt="...">
                    </div>
                </div>
                <div class="lower-part">
                    <img src="{{url('images/kidlat-center.png')}}" alt="" class="house-center">
                    <!--
                    SIGN UP AND DOWNLOAD RULES
                    -->
                    <div class="lower-part-content">
                        <p><span class="fw-bold text-uppercase">Submission of Entries</p>
                        <p style="font-size: 24px">January 10 to January 27, 2023</p>
                        <div class="buttons mt-3">
                            <a href="{{url('/register')}}">
                                <button type="button" class="btn btn-primary primary small purple-btn" style="min-width:231px">REGISTER</button>
                            </a>
                            <a href="{{url('/documents/kidlat_rules.pdf')}}" target="_blank">
                                <button type="button" class="btn btn-primary primary small purple-btn" style="min-width:231px">DOWNLOAD RULES</button>
                            </a>
                        </div>
                    </div>
                    <!--
                    END OF SIGN UP AND DOWNLOAD RULES
                    -->
                    <!--
                    DOWNLOAD RULES BUTTON ONLY
                    -->
<!--                     <div class="lower-part-content dl-rules-only">
                        <a href="{{url('/documents/kidlat_rules.pdf')}}" target="_blank">
                            <button type="button" class="btn btn-primary primary small" style="min-width:231px">DOWNLOAD RULES</button>
                        </a>
                    </div> -->
                    <!--
                    END OF DOWNLOAD RULES BUTTON ONLY
                    -->
                </div>
            </div>
        </div>
    </div>
    <!-- modal -->
    <div class="voting-modal modal fade" id="contactusModal" tabindex="-1" aria-labelledby="flag" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered ">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body p-0 pt-4">
                    <div class="selection">
                        <p class="question contact-title">Contact 4As</p>
                        <div class="footer-links contact flex-column">
                            <p class="label">
                                4As Secretariat:&nbsp;<span class="value">Vanne Tomada</span>
                            </p>
                            <p class="label">
                                Address:&nbsp;<span class="value">Unit 442 Cityland Pasong Tamo Tower, 2210 Chino Roces, Makati City</span>
                            </p>
                            <p class="label">
                                Email address:&nbsp;<a href="mailto:4asp@pldtdsl.net" class="value" target="_blank">4asp@pldtdsl.net</a>
                            </p>
                            <div class="d-flex label">
                                Telephone number:&nbsp;<div class="separator"><a class="value">893-1205</a><a class="value">757-3891</a><a class="value">812-4397</a></div>
                            </div>
                            <p class="label">
                                Mobile number:&nbsp;<a href="#" class="value" target="_blank">0917 5222 427</a>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
  <script src="https://code.jquery.com/jquery-3.6.0.min.js" integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>
  <script src="{{url('/js/loadingoverlay.min.js')}}"></script>
  <script>
    $(document).on('click', '.profile-link', function() {
        Swal.fire({
            // confirmButtonText: 'Profile',
            showConfirmButton: false,
            denyButtonText: 'Logout',
            denyButtonClass: 'btn-logout',
            showDenyButton: true
        }).then((result) => {
            // if (result.isConfirmed) {
            //     location.href = "/profile"
            // }
            if (result.isDenied) {
                location.href = "/logout"
            }
        })
    })
  </script>
  <!-- end 2021 js -->  
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.3/dist/umd/popper.min.js" integrity="sha384-eMNCOe7tC1doHpGoWe/6oMVemdAVTMs2xqW4mwXrXsW0L84Iytr2wi5v2QjrP/xp" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/js/bootstrap.min.js" integrity="sha384-cn7l7gDp0eyniUwwAZgrzD06kc/tftFf19TOAs2zVinnD/C7E91j9yyk5//jjpt/" crossorigin="anonymous"></script>

    <script>
        $(document).ready(activateQuery())

        function activateQuery() {
            $(document).on('click', '#btn_signup', function() {
                location.href = "/register"
            })
        }
    </script>
</body>
</html>