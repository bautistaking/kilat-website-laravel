<nav>
  <div class="container">
    <div class="nav-wrapper">
      <a href="#" class="brand-logo left"><img src="{{ asset('/images/logo.png') }}" alt=""></a>
      <a href="#" data-target="mobile-nav" class="sidenav-trigger"><i class="material-icons">menu</i></a>
      <ul class="left hide-on-med-and-down desktop-nav">
        <li><a class="modal-trigger" href="#rules">Competition Rules</a></li>
        <li><a class="modal-trigger" href="#faqs">FAQs</a></li>
        <li><a href="mailto:4asp@pldtdsl.net,kidlatawards2019@gmail.com?subject=[Kidlat Awards 2019] - Technical Inquiry">Contact Us</a></li>
        @if (Auth::check())
        @if(Auth::user()->role == "User")
        <li><a href="{{ url('/dashboard') }}">Dashboard</a></li>
        @endif
        @else
        @if($expired == false)
        <li><a href="#" class="nav-signin sign-up">Sign in</a></li>
        <li><a href="#" class="nav-signup sign-in hidden">Sign up</a></li>
        @endif
        @endif
      </ul>
      
      <ul class="sidenav" id="mobile-nav">
        <li><a class="modal-trigger" href="#rules">Competition Rules</a></li>
        <li><a class="modal-trigger" href="#faqs">FAQS</a></li>
        <li><a href="mailto:4asp@pldtdsl.net,kidlatawards2019@gmail.com?subject=[Kidlat Awards 2019] - Technical Inquiry">Contact Us</a></li>
        @if (Auth::check())
        @if(Auth::user()->role == "User")
        <li><a href="{{ url('/dashboard') }}">Dashboard</a></li>
        @endif
        @else
        @if($expired == false)
        <li><a href="#" class="nav-signin sign-up">Sign in</a></li>
        <li><a href="#" class="nav-signup sign-in hidden">Sign up</a></li>
        @endif
        @endif
      </ul>
    </div>
  </div>
</nav>