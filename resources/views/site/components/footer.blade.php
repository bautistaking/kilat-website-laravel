<footer class="page-footer">
  <div class="container text-center">
    @if (Auth::check())
    <h5 class="m-0">Hello, <strong>{{Auth::user()->name}}</strong>!</h5>
    @if(Auth::user()->role == "User")
    <a href="{{url('/dashboard')}}" target="_blank" class="waves-effect waves-dark btn pink mt-1">View Dashboard</a>
    @endif
    @else
    @if($expired == false)
    <form action="{{ url('/signUp') }}" method="post" class="sign-up">
    @csrf
      <div class="row">
        <div class="input-field col m3 s6 offset-m2 flex m-0">
            <input type="hidden" name="signin" value="0">
            <input type="text" name="name" class="validate" autocomplete="off">
            <label for="">Agency Name</label>
        </div>
        <div class="input-field col m3 s6 flex m-0">
            <input type="text" name="email" class="validate" autocomplete="off">
            <label for="">Enter your email</label>
        </div>
        <div class="input-field col m3 s12 flex justify-center m-0">
            <input type="image" src="{{ asset('/images/sign-up.png') }}" height="46" class="mx-1" alt="Sign Up">
        </div>
      </div>
    </form>
    @endif
    <form action="{{ url('/signUp') }}" method="post" class="@if($expired == false) hidden @endif sign-in">
    @csrf
      <div class="row">
        <div class="input-field col m3 s6 flex m-0 offset-m4 ">
            <input type="hidden" name="signin" value="1">
            <input type="text" name="email" class="validate" autocomplete="off">
            <label for="">Enter your email</label>
        </div>
        <div class="input-field col m3 s6 flex justify-center m-0">
            <input type="image" src="{{ asset('/images/sign-in.png') }}" height="46" class="mx-1" alt="Sign Up">
        </div>
      </div>
    </form>
    @endif
  </div>
</footer>