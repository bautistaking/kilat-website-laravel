<div>
  <h5 style="color:#5d59a5;">FAQs</h5>
  <ol>

    <li>
      <div>
        Where can I fill out the PDF entry form for Kidlat Awards 2019?
        <ul class="circle">
          <li>No need to fill out a PDF form.</li>
          <li>Simply sign up to Kidlat Awards 2019 by going to <a href="https://enter.kidlatawards.com.ph/">https://enter.kidlatawards.com.ph/</a>.</li>
          <li>Fill out your Agency Name and email address and we’ll send you a sign-in link where you can access the entry form to submit your entry.</li>
        </ul>
      </div>
    </li>

    <li>
      <div>
        Where can I view and edit my encoded entries before the final submission?
        <ul class="circle">
          <li> You can access the dashboard for your entries by using the sign-in link sent via email.</li>
          <li>Make sure your entry requirements are complete before submitting. If you accidentally submitted with incomplete or incorrect information, you can submit another entry. We are currently reviewing an option to manage or edit an entry in our future updates.</li>
          <li>Only entries that are submitted in the portal, requested for invoice, paid, and verified by 4As will only be counted as valid entries for judging.</li>
        </ul>
      </div>
    </li>

    <li>
      <div>
        Where can I get a Client Certificate Template?
        <ul class="circle">
          <li>You can use this template. Download <a href="{{ asset('/files/Client Certification Template - Kidlat Awards 2019.pdf') }}" download>here</a>.</li>
        </ul>
      </div>
    </li>

    <li>
      <div>
        Can I pay online? How can I request an invoice?
        <ul class="circle">
          <li>Available payment options are via Bank or Check Deposit. Check the payment instructions sent via E-mail after your select entries for Invoice Request.</li>
          <li>An email with amount and payment instructions will be sent after.</li>
          <li>Your entry status will also change to "Pending Payment".</li>
        </ul>
      </div>
    </li>

    <li>
      <div>
        I encountered an issue. Who can I contact? <br>
        We’re here to help. Simply contact us <a href="mailto:4asp@pldtdsl.net,kidlatawards2019@gmail.com?subject=[Kidlat Awards 2019] - Technical Inquiry">here</a>
      </div>
    </li>

  </ol>
</div>