<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en" class="no-js">
<!--<![endif]-->

    <head>
          <meta charset="UTF-8">
          <meta name="csrf-token" content="{{ csrf_token() }}">
          <meta name="viewport" content="width=device-width, initial-scale=1.0">
          <meta http-equiv="X-UA-Compatible" content="ie=edge">
          <meta name="description" content="Are you ready to unlock the best of the best in Filipino creativity?" />
          <meta name="author" content="EBDigital" />
          <title>Kidlat Awards 2021 - Creativity Rules</title>
          <meta property="og:title" content="Welcome to Kidlat Awards!">
          <meta property="og:description" content="Are you ready to unlock the best of the best in Filipino creativity?">
          <!-- best size for Facebook: 1300 X 630 px -->
          <meta property="og:image" content="{{url('/images/og.jpg')}}"/>
          <meta property="og:url" content="https://www.kidlatawards.com"/>
          <meta property="og:type" content="website"/>
          <meta property="og:site_name" content="Welcome to Kidlat Awards!" />
          <meta name="twitter:card" content="summary_large_image">
          <meta name="twitter:creator" content=""/>
          <link rel="shortcut icon" href="{{ asset('/images/favicon.ico') }}">
        <title>Kidlat Awards 2021</title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1" name="viewport"/>

        <link rel="shortcut icon" href="{{url('/images/favicon.png')}}">
        <link rel="stylesheet" type="text/css" href="{{url('/css/main.min.css')}}" />
        <link rel="stylesheet" type="text/css" href="{{url('/css/custom.css')}}" />
        
        <!-- Bootstrap CSS -->
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-KyZXEAg3QhqLMpG8r+8fhAXLRk2vvoC2f3B09zVXn8CA5QIVfZOJ3BCsw2P0p/We" crossorigin="anonymous">
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.5.0/font/bootstrap-icons.css">
        <link rel="preconnect" href="https://fonts.googleapis.com">
        <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
        <link href="https://fonts.googleapis.com/css2?family=Inter:wght@300;400;500;600;700;800;900&display=swap" rel="stylesheet">

        <!-- Global site tag (gtag.js) - Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=G-TKL0DQ8D42"></script>
        <script> 
          window.dataLayer = window.dataLayer || []; function gtag(){dataLayer.push(arguments);} gtag('js', new Date()); gtag('config', 'G-TKL0DQ8D42');
        </script>

        <!-- Google Tag Manager -->
          <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
          new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
          j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
          'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
          })(window,document,'script','dataLayer','GTM-WS7CLVN');</script>
          <!-- End Google Tag Manager -->

          <style>
            #confetti{
              position: absolute;
              left: 0;
              top: 0;
              height: 100%;
              width: 100%;
            }


            a.iprodev {
              line-height: normal;
              font-family: Varela Round, sans-serif;
              font-weight: 600;
              text-decoration: none;
              font-size: 13px;
              color: #A7AAAE;
              position: absolute;
              left: 20px;
              bottom: 20px;
              border: 1px solid #A7AAAE;
              padding: 12px 20px 10px;
              border-radius: 50px;
              transition: all .1s ease-in-out;
              text-transform: uppercase;
            }

            section#section-announcement{
              background: rgb(23,32,42);
              background: linear-gradient(180deg, rgba(23,32,42,1) 0%, rgba(46,74,98,1) 100%);
              min-height: 300px;
              padding-top: 50px;
              padding-bottom: 100px;
            }

            .accordion .accordion-item{
              background: transparent;
              color: var(--bs-white);
              border-bottom: 1px solid #94A6B5;
            }

            .accordion-flush .accordion-item .accordion-button {
                border-radius: 0;
                background-color: transparent;
                color: #d7a333;
                font-size: 30px;
                font-weight: bold;
                box-shadow: none;
                padding-top: 50px;
                padding-bottom: 50px;
            }

            .accordion-button:after {
                background-image: url("data:image/svg+xml,%3Csvg width='44' height='45' viewBox='0 0 44 45' fill='none' xmlns='http://www.w3.org/2000/svg'%3E%3Ccircle cx='22' cy='22.0447' r='21.5' stroke='%23D7A333'/%3E%3Cpath d='M30.0954 17.0447L31.1943 18.1495L22.0972 27.2958L13 18.1495L14.0989 17.0447L22.0972 25.0854L30.0954 17.0447Z' fill='%23D7A333'/%3E%3C/svg%3E%0A");
                width: 3rem;
                height: 3rem;
                background-size: cover;
            }

            .accordion-button:not(.collapsed)::after {
                background-image: url("data:image/svg+xml,%3Csvg width='44' height='45' viewBox='0 0 44 45' fill='none' xmlns='http://www.w3.org/2000/svg'%3E%3Ccircle cx='22' cy='22.0447' r='21.5' stroke='%23D7A333'/%3E%3Cpath d='M30.0954 17.0447L31.1943 18.1495L22.0972 27.2958L13 18.1495L14.0989 17.0447L22.0972 25.0854L30.0954 17.0447Z' fill='%23D7A333'/%3E%3C/svg%3E%0A");
                transform: rotate(-180deg);
            }

            .table{
              color: #fff;
            }

            .table thead th{
                color: #d7a333;
            }

            .table>:not(:last-child)>:last-child>* {
                border-bottom-color: #94A6B5;
            }

            .table tbody td{
                padding-top: 30px;
                padding-bottom: 30px;
                min-width: 200px;
            }

            .table tbody tr, .table tbody td{
              border-bottom: none;
            }

            .table tbody tr:hover{
              background: #d7a333;
              color: #fff;
            }

            .bi-check-circle-fill{
              color: #D7A333;
            }

            #row-auditor{
              padding-top: 50px;
              padding-bottom: 50px;
            }

            .sub-head{
              margin:0;
              font-size: 26px;
              text-align: center;
            }

            .icon-circle{ font-size: 16px!important; color: #fff!important;}

            /**Filters**/
            .btn-group-filters .btn-primary{
              min-width: 180px;
              margin: 5px;
              background: transparent;
              border: 1px solid #D7A333;
              border-radius: 30px;
              font-weight: bold;
            }

            .btn-group-filters .btn-primary:first-child{
              margin-left: 0;
            }

            .btn-group-filters .btn-primary:last-child{
              margin-right: 0;
            }

            .btn-group-filters .btn-primary.active{
              background: #D7A333;
              color: #5D2800;
            }

            .text-nowin{
              margin-top: 50px;
              margin-bottom: 100px;
              display: none;
            }

            .table-winners{display:none;}

            .btn-group-filters-xs{
                width: 100%;
                bottom: 10px;
            }

            .btn-group-filters-xs .btn-secondary{
                min-width: 180px;
                background: #D7A333;
                border: 1px solid #D7A333;
                border-radius: 30px;
                font-weight: bold;
                box-shadow: 0px -5px 15px rgb(0 0 0 / 30%);
            }

            .btn-group-filters-xs .btn-group.dropup{
              width: 100%;
              margin-top: -50px;
            }

            .btn-group-filters-xs .dropdown-menu.show {
              display: block;
              width: 100%;
            }

            .btn-group-filters-xs .btn-group-filters .btn-primary {
              margin: 15px 0;
              background: transparent;
              border: none;
              border-radius: 0; 
              font-size: 1.2rem;
              text-align: center;
            }

            .btn-group-filters-xs .btn-group-filters .btn-primary.active {
              background: #D7A333;
              color: #5D2800;
            }


            /** X-Small devices **/
            @media (max-width: 575.98px) { 

              .wrapper-default{
                padding-left: 15px;
                padding-right: 15px;
                padding-bottom: 50px;
              }

              .sub-head{
                font-size: 20px;
              }

              .accordion-flush .accordion-item .accordion-button{
                  padding-top: 30px;
                  padding-bottom: 30px;
                  padding-left: 0;
                  padding-right: 0;
                  font-size: 20px;
              }

              section#section-announcement{
                padding-top: 0;
                padding-bottom: 50px;
              }

              .accordion-body{
                padding-left: 0;
                padding-right: 0;
              }


            }


          </style>
    </head>

<body class="full-width">
  <!-- Google Tag Manager (noscript) -->
  <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-WS7CLVN"
  height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
  <!-- End Google Tag Manager (noscript) -->
  <canvas id="confetti" style="width:1;height:1"></canvas>
    <div class="d-flex flex-grow-1 w-100 h-100">
        <div class="landing">
            <div class="d-flex flex-column justify-content-between align-items-center h-100">
                <div class="wrapper-default py-30 m-0" style="z-index:4;">
                    <div class="d-flex justify-content-between align-items-center w-100">
                        <div class="d-flex align-items-center">
                            <img class="logo-left" src="{{url('images/logo-white.svg')}}" alt="...">
                            <div class="horizontal-line"></div>
                            <a class="contact-us" data-bs-toggle="modal" data-bs-target="#contactusModal"><span>Contact us</span></a>
                        </div>
                        <img src="{{url('images/CREATIVEFEST_new 1.svg')}}" alt="...">
                    </div>
                </div>
                <div class="lower-part">
                    <img src="{{url('images/house-center.png')}}" alt="" class="house-center">
                    <img src="{{url('images/house-vertical.png')}}" alt="" class="house-vertical">
                    <!--
                    SIGN UP AND DOWNLOAD RULES
                    -->
                    <div class="lower-part-content">
                        <h2 class="text-center"><span class="fw-bold text-uppercase">2021 Winners</h2>
                        <p class="fw-light sub-head">Congratulations to all our winners for riding <br class="d-none d-md-block">the lightning and coming out victorious!</p>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!--Announcement-->
    <section class="container-fluid" id="section-announcement">
      <div class="container">
          <div class="row">
            <div class="col-12 text-center">
              <div class="btn-group-filters d-none d-sm-block">
                <button type="button" class="btn btn-lg btn-primary d-none" data-filter-value="metal-finalist">Finalist</button>
                <button type="button" class="btn btn-lg btn-primary active" data-filter-value="metal-bronze">Bronze</button>
                <button type="button" class="btn btn-lg btn-primary" data-filter-value="metal-silver">Silver</button>
                <button type="button" class="btn btn-lg btn-primary" data-filter-value="metal-gold">Gold</button>
                <button type="button" class="btn btn-lg btn-primary" data-filter-value="metal-grand">Grand Kidlat</button>          
              </div>
            </div>
            <div class="btn-group-filters-xs fixed-bottom d-block d-sm-none">
              <!-- Default dropup button -->
              <div class="btn-group dropup">
                <button type="button" class="btn btn-secondary btn-lg dropdown-toggle" data-bs-toggle="dropdown" aria-expanded="false">
                  <span id="filter-drop-text">Bronze</span>
                </button>
                <ul class="dropdown-menu btn-group-filters">
                  <!-- Dropdown menu links -->
                  <li><a class="dropdown-item btn-primary d-none" data-filter-value="metal-finalist" href="#winners-list">Finalist</a></li>
                  <li><a class="dropdown-item btn-primary active" data-filter-value="metal-bronze" href="#winners-list">Bronze</a></li>
                  <li><a class="dropdown-item btn-primary" data-filter-value="metal-silver" href="#winners-list">Silver</a></li>
                  <li><a class="dropdown-item btn-primary" data-filter-value="metal-gold" href="#winners-list">Gold</a></li>
                  <li><a class="dropdown-item btn-primary" data-filter-value="metal-grand" href="#winners-list">Grand Kidlat</a></li>
                </ul>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-12 col-md-12" id="winners-list">
             

              <div class="accordion accordion-flush" id="accordionShortlist">
                
                <div class="accordion-item">
                  <h2 class="accordion-header" id="acc-head-1">
                    <button class="accordion-button" type="button" data-bs-toggle="collapse" data-bs-target="#acc-con-1" aria-expanded="true" aria-controls="acc-con-1">
                      Creative Effectiveness
                    </button>
                  </h2>
                  <div id="acc-con-1" class="accordion-collapse collapse show" aria-labelledby="acc-head-1" data-bs-parent="#accordionShortlist">
                    <div class="accordion-body">
                      <h4 class="text-nowin text-center"></h4>
                      <div class="table-responsive">
                        <table class="table table-hover align-middle table-winners">
                          <thead>
                            <tr>
                              <th scope="col">SUBCATEGORY</th>
                              <th scope="col">TITLE</th>
                              <th scope="col">AGENCY</th>
                              <th scope="col">BRAND</th>
                            </tr>
                          </thead>
                          <tbody>
                            <tr class="metal-finalist">
                              <td scope="row">Film</th>
                              <td>A Frontliner's Sacrifice</td>
                              <td>Leo Burnett Group Manila</td>
                              <td>Procter & Gamble</td>
                            </tr>
                            <tr class="metal-finalist">
                              <td scope="row">Film</th>
                              <td>Applause</td>
                              <td>FCBManila</td>
                              <td>Union Bank of the Phiippines</td>
                            </tr>
                             <tr class="metal-finalist">
                              <td scope="row">Film</th>
                              <td>Blackout</td>
                              <td>Leo Burnett Group Manila</td>
                              <td>World Wilde Fund for Nature Philippines</td>
                            </tr>
                            <tr class="metal-finalist">
                              <td scope="row">Film</th>
                              <td>Connections Should Have No Limits</td>
                              <td>Petch&Partners</td>
                              <td>VIVO</td>
                            </tr>
                            <tr class="metal-finalist">
                              <td scope="row">Film</th>
                              <td>Lift</td>
                              <td>Leo Burnett Group Manila</td>
                              <td>McDonald's Philippines</td>
                            </tr>
                            <tr class="metal-silver">
                              <td scope="row">Film</th>
                              <td>A Star Wars Experience for All</td>
                              <td>Wunderman Thompson Philippines</td>
                              <td>Globe Telecom Inc.</td>
                            </tr>
                            <tr class="metal-bronze">
                              <td scope="row">Film</th>
                              <td>TNT - Free Games For All Campaign</td>
                              <td>Ace Saatchi & Saatchi Advertising</td>
                              <td>Smart Communications, Inc.</td>
                            </tr>
                            <tr class="metal-finalist">
                              <td scope="row">Film</th>
                              <td>Nissin Ramen Time ASMR: Making a lot of noise with so little sound</td>
                              <td>dentsu one manila</td>
                              <td>Nissin Universal Robina Corporation</td>
                            </tr>
                            <tr class="metal-finalist">
                              <td scope="row">Film</th>
                              <td>SaferKidsPH "Toys"</td>
                              <td>TBWA\Santiago Mangada Puno</td>
                              <td>UNICEF Philippines</td>
                            </tr>
                            <tr class="metal-finalist">
                              <td scope="row">Film</th>
                              <td>Story of Us</td>
                              <td>Leo Burnett Group Manila</td>
                              <td>McDonald's Philippines</td>
                            </tr>
                            <tr class="metal-finalist">
                              <td scope="row">Public Relations</th>
                              <td>A Star Wars Experience for All</td>
                              <td>Wunderman Thompson Philippines</td>
                              <td>Globe Telecom Inc.</td>
                            </tr>
                            <tr class="metal-silver">
                              <td scope="row">Public Relations</th>
                              <td>Ingat Angat Tayong Lahat (Carefully, We Rise)</td>
                              <td>TBWA\Santiago Mangada Puno</td>
                              <td>Task Force T3</td>
                            </tr>
                            <tr class="metal-finalist">
                              <td scope="row">Public Relations</th>
                              <td>McDeliverback Breakfast for Heroes</td>
                              <td>Leo Burnett Group Manila</td>
                              <td>McDonald's Philippines</td>
                            </tr>
                            <tr class="metal-finalist">
                              <td scope="row">Public Relations</th>
                              <td>McDonald's M Safe Campaign</td>
                              <td>TBWA\Santiago Mangada Puno</td>
                              <td>McDonald's Philippines</td>
                            </tr>
                            <tr class="metal-gold">
                              <td scope="row">Public Relations</th>
                              <td>McClassroom</td>
                              <td>Leo Burnett Group Manila</td>
                              <td>McDonald's Philippines</td>
                            </tr>
                            <tr class="metal-silver">
                              <td scope="row">Public Relations</th>
                              <td>Oplan Basher Win-Back</td>
                              <td>Publicis JimenezBasic</td>
                              <td>Monde Nissin</td>
                            </tr>
                            <tr class="metal-bronze">
                              <td scope="row">Digital Activation & Campaigns</th>
                              <td>Kentucky Fried Crossing</td>
                              <td>Ogilvy & Mather Philippines</td>
                              <td>Grantline, Inc.</td>
                            </tr>
                            <tr class="metal-silver">
                              <td scope="row">Digital Activation & Campaigns</th>
                              <td>MILO Homecourt</td>
                              <td>Ogilvy & Mather Philippines</td>
                              <td>Nestle Philippines, Inc.</td>
                            </tr>
                            <tr class="metal-gold">
                              <td scope="row">Digital Activation & Campaigns</th>
                              <td>Bank the Way You Live</td>
                              <td>FCBManila</td>
                              <td>Union Bank of the Phiippines</td>
                            </tr>
                            <tr class="metal-finalist">
                              <td scope="row">Digital Activation & Campaigns</th>
                              <td>The First Shrimp-fluencer</td>
                              <td>TBWA\Santiago Mangada Puno</td>
                              <td>Liwayway Manufacturing Corp.</td>
                            </tr>
                            <tr class="metal-finalist">
                              <td scope="row">Digital Solutions</th>
                              <td>MILO Homecourt</td>
                              <td>Ogilvy & Mather Philippines</td>
                              <td>Nestlé Philippines, Inc.</td>
                            </tr>
                            <tr class="metal-gold">
                              <td scope="row">Digital Solutions</th>
                              <td>Kentucky Fried Crossing</td>
                              <td>Ogilvy & Mather Philippines</td>
                              <td>Grantline, Inc.</td>
                            </tr>
                            <tr class="metal-bronze">
                              <td scope="row">Promo & Activation</th>
                              <td>The Think Fast Sale</td>
                              <td>Digitas Philippines</td>
                              <td>Wyeth Nutrition Philippines</td>
                            </tr>
                            <tr class="metal-bronze">
                              <td scope="row">Direct</th>
                              <td>MILO Homecourt</td>
                              <td>Ogilvy & Mather Philippines</td>
                              <td>Nestlé Philippines, Inc.</td>
                            </tr>
                            <tr class="metal-bronze">
                              <td scope="row">Direct</th>
                              <td>The Think Fast Sale</td>
                              <td>Digitas Philippines</td>
                              <td>Wyeth Nutrition Philippines</td>
                            </tr>

                          </tbody>
                        </table>
                      </div>
                    </div>
                  </div>
                </div>

                <div class="accordion-item">
                  <h2 class="accordion-header" id="acc-head-2">
                    <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#acc-con-2" aria-expanded="false" aria-controls="acc-con-2">
                      Creative Technology & Media
                    </button>
                  </h2>
                  <div id="acc-con-2" class="accordion-collapse collapse" aria-labelledby="acc-head-2" data-bs-parent="#accordionShortlist">
                    <div class="accordion-body">
                      <h4 class="text-nowin text-center"></h4>
                      <div class="table-responsive">
                        <table class="table table-hover align-middle table-winners">
                          <thead>
                            <tr>
                              <th scope="col">SUBCATEGORY</th>
                              <th scope="col">TITLE</th>
                              <th scope="col">AGENCY</th>
                              <th scope="col">BRAND</th>
                            </tr>
                          </thead>
                          <tbody>
                            <tr class="metal-finalist">
                              <td scope="row">Creativity in Social Media: Social Post</th>
                              <td>Love Verses Hate</td>
                              <td>Propel Manila</td>
                              <td>Love Is All We Need</td>
                            </tr>
                            <tr class="metal-bronze">
                              <td scope="row">Creativity in Social Media: Social Post</th>
                              <td>Dove Blackout Mirror</td>
                              <td>Ogilvy & Mather Philippines, Inc.</td>
                              <td>Unilever Philippines, Inc.</td>
                            </tr>
                            <tr class="metal-silver">
                              <td scope="row">Creativity in Social Media: Social Post</th>
                              <td>GiF Learning Library</td>
                              <td>BBDO Guerrero</td>
                              <td>Facebook</td>
                            </tr>
                            <tr class="metal-finalist">
                              <td scope="row">Creativity in Social Media: Social Post</th>
                              <td>Targeting Little Ones On YouTube</td>
                              <td>Petch&Partners</td>
                              <td>Dole Philippines</td>
                            </tr>
                            <tr class="metal-finalist">
                              <td scope="row">Creative Use of Technology: Websites & E-commerce</th>
                              <td>Swift: The People's Suking Tech-Dahan</td>
                              <td>NuWorks Interactive Labs, Inc.</td>
                              <td>The Pacific Meat Company, Inc.</td>
                            </tr>
                            <tr class="metal-silver">
                              <td scope="row">Creative Use of Technology: Websites & E-commerce</th>
                              <td>The Think Fast Sale</td>
                              <td>Digitas Philippines</td>
                              <td>Wyeth Nutrition Philippines</td>
                            </tr>
                            <tr class="metal-gold">
                              <td scope="row">Creative Use of Technology: Websites & E-commerce</th>
                              <td>Sakay Halfway</td>
                              <td>MullenLowe TREYNA</td>
                              <td>Sakay.ph</td>
                            </tr>
                            <tr class="metal-bronze">
                              <td scope="row">Creative Use of Media</th>
                              <td>Be Part of the Font</td>
                              <td>BBDO Guerrero</td>
                              <td>Department of Tourism</td>
                            </tr>
                            <tr class="metal-silver">
                              <td scope="row">Creative Use of Media</th>
                              <td>GiF Learning Library</td>
                              <td>BBDO Guerrero</td>
                              <td>Facebook</td>
                            </tr>
                            <tr class="metal-silver">
                              <td scope="row">Creative Use of Media</th>
                              <td>The Think Fast Sale</td>
                              <td>Digitas Philippines</td>
                              <td>Wyeth Nutrition Philippines</td>
                            </tr>
                            <tr class="metal-bronze">
                              <td scope="row">Creative Use of Media</th>
                              <td>CAPTCHAring Harassment</td>
                              <td>Dentsu Jayme Syfu</td>
                              <td>Gabriela Philippines</td>
                            </tr>
                            <tr class="metal-bronze">
                              <td scope="row">Creative Use of Media</th>
                              <td>Font Books</td>
                              <td>BBDO Guerrero</td>
                              <td>Reading Association of the Philippines</td>
                            </tr>
                            <tr class="metal-bronze">
                              <td scope="row">Creative Use of Media</th>
                              <td>Side By Side</td>
                              <td>McCann Worldgroup</td>
                              <td>San Miguel Brewery Inc.</td>
                            </tr>
                            <tr class="metal-bronze">
                              <td scope="row">Creative Use of Data: Storytelling</th>
                              <td>Lazada's Hit Jingle</td>
                              <td>Publicis JimenezBasic</td>
                              <td>Lazada PH</td>
                            </tr>
                            <tr class="metal-bronze">
                              <td scope="row">Creativity in Social Media: Real-time Response</th>
                              <td>Pride March Anywhere</td>
                              <td>Propel Manila</td>
                              <td>Globe Telecoms</td>
                            </tr>
                            <tr class="metal-bronze">
                              <td scope="row">Creativity in Social Media: User-generated Content</th>
                              <td>Be Part of the Font</td>
                              <td>BBDO Guerrero</td>
                              <td>Department of Tourism</td>
                            </tr>
                            <tr class="metal-bronze">
                              <td scope="row">Creativity in Social Media: User-generated Content</th>
                              <td>Pride March Anywhere</td>
                              <td>Propel Manila</td>
                              <td>Globe Telecoms</td>
                            </tr>
                            <tr class="metal-bronze">
                              <td scope="row">Creativity in Social Media: Influencer Campaign</th>
                              <td>Oplan Basher Win-Back</td>
                              <td>Publicis JimenezBasic</td>
                              <td>Monde Nissin</td>
                            </tr>
                            <tr class="metal-bronze">
                              <td scope="row">Creativity in Social Media: Influencer Campaign</th>
                              <td>The First Shrimp-fluencer</td>
                              <td>TBWA\Santiago Mangada Puno</td>
                              <td>Liwayway Manufacturing Corp.</td>
                            </tr>
                            <tr class="metal-bronze">
                              <td scope="row">Creativity in Social Media: Influencer Campaign</th>
                              <td>Tutorials Takeover</td>
                              <td>VMLY&R Philippines</td>
                              <td>Quezon City Science High School Alumni Association</td>
                            </tr>
                            <tr class="metal-bronze">
                              <td scope="row">Creative Use of Technology: App</th>
                              <td>Stop The Spread</td>
                              <td>BBDO Guerrero</td>
                              <td>National Union of Journalists of the Philippines</td>
                            </tr>
                            <tr class="metal-gold metal-grand">
                              <td scope="row">Creativity in Experiential & Immersive: Brand-owned Experiences</th>
                              <td>McClassroom</td>
                              <td>Leo Burnett Group Manila</td>
                              <td>McDonald's Philippines</td>
                            </tr>

                          </tbody>
                        </table>
                      </div>
                    </div>
                  </div>
                </div>

                <div class="accordion-item">
                  <h2 class="accordion-header" id="acc-head-3">
                    <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#acc-con-3" aria-expanded="false" aria-controls="acc-con-3">
                      Creative Storytelling
                    </button>
                  </h2>
                  <div id="acc-con-3" class="accordion-collapse collapse" aria-labelledby="acc-head-3" data-bs-parent="#accordionShortlist">
                    <div class="accordion-body">
                      <h4 class="text-nowin text-center"></h4>
                      <div class="table-responsive">
                        <table class="table table-hover align-middle table-winners">
                          <thead>
                            <tr>
                              <th scope="col">SUBCATEGORY</th>
                              <th scope="col">TITLE</th>
                              <th scope="col">AGENCY</th>
                              <th scope="col">BRAND</th>
                            </tr>
                          </thead>
                          <tbody>
                          	 <tr class="metal-bronze">
                              <td scope="row">Outdoor/Ambient</th>
                              <td>Tubbataha Coral Rip</td>
                              <td>TBWA\Santiago Mangada Puno</td>
                              <td>Tubbataha Reefs Natural Park and World Heritage Site</td>
                            </tr>
                            <tr class="metal-finalist">
                              <td scope="row">Outdoor/Ambient</th>
                              <td>BayBayan</td>
                              <td>TBWA\Santiago Mangada Puno</td>
                              <td>Cultural Center of the Philippines</td>
                            </tr>
                            <tr class="metal-silver">
                              <td scope="row">Outdoor/Ambient</th>
                              <td>The Dissolving Bottle</td>
                              <td>BBDO Guerrero</td>
                              <td>The Naturale Market</td>
                            </tr>
                            <tr class="metal-bronze">
                              <td scope="row">Outdoor/Ambient</th>
                              <td>Take Me to Tokyo</td>
                              <td>Ogilvy & Mather Philippines</td>
                              <td>Hansbury, Inc.</td>
                            </tr>
                            <tr class="metal-finalist">
                              <td scope="row">Outdoor/Ambient</th>
                              <td>Bench Be The Leading Men</td>
                              <td>TBWA\Santiago Mangada Puno</td>
                              <td>Suyen Corporation</td>
                            </tr>
                            <tr class="metal-finalist">
                              <td scope="row">Film</th>
                              <td>(Not A) Barbie Girl</td>
                              <td>Petch&Partners</td>
                              <td>Splash Corporation</td>
                            </tr>
                            <tr class="metal-bronze">
                              <td scope="row">Film</th>
                              <td>A Frontliner's Sacrifice</td>
                              <td>Leo Burnett Group Manila</td>
                              <td>Procter & Gamble</td>
                            </tr>
                            <tr class="metal-gold">
                              <td scope="row">Film</th>
                              <td>Connections Should Have No Limits</td>
                              <td>Petch&Partners</td>
                              <td>VIVO</td>
                            </tr>
                            <tr class="metal-finalist">
                              <td scope="row">Film</th>
                              <td>Lift</td>
                              <td>Leo Burnett Group Manila</td>
                              <td>McDonald's Philippines</td>
                            </tr>
                            <tr class="metal-silver">
                              <td scope="row">Film</th>
                              <td>A Star Wars Experience for All</td>
                              <td>Wunderman Thompson Philippines</td>
                              <td>Globe Telecom</td>
                            </tr>
                            <tr class="metal-bronze">
                              <td scope="row">Film</th>
                              <td>Applause</td>
                              <td>FCBManila</td>
                              <td>Union Bank of the Phiippines</td>
                            </tr>
                            <tr class="metal-finalist">
                              <td scope="row">Film</th>
                              <td>PSBank Aksyon</td>
                              <td>Seven A.D.</td>
                              <td>Philippine Savings Bank (PSBank)</td>
                            </tr>
                            <tr class="metal-finalist">
                              <td scope="row">Film</th>
                              <td>Side By Side</td>
                              <td>McCann Worldgroup</td>
                              <td>San Miguel Brewery Inc.</td>
                            </tr>
                            <tr class="metal-gold">
                              <td scope="row">Film</th>
                              <td>SaferKidsPH "Toys"</td>
                              <td>TBWA\Santiago Mangada Puno</td>
                              <td>UNICEF Philippines</td>
                            </tr>
                            <tr class="metal-finalist">
                              <td scope="row">Film</th>
                              <td>TNT - Free Games For All</td>
                              <td>Ace Saatchi & Saatchi Advertising</td>
                              <td>Smart Communications, Inc.</td>
                            </tr>
                            <tr class="metal-bronze">
                              <td scope="row">Film</th>
                              <td>Every Kind of Family</td>
                              <td>Petch&Partners</td>
                              <td>Max's Group Inc.</td>
                            </tr>
                            <tr class="metal-gold metal-grand">
                              <td scope="row">Film</th>
                              <td>Story of Us</td>
                              <td>Leo Burnett Group Manila</td>
                              <td>McDonald's Philippines</td>
                            </tr>
                            <tr class="metal-bronze">
                              <td scope="row">Film</th>
                              <td>Every Kind of World</td>
                              <td>Petch&Partners</td>
                              <td>Max's Group Inc.</td>
                            </tr>
                            <tr class="metal-finalist">
                              <td scope="row">Audio</th>
                              <td>Insta-Scares</td>
                              <td>Wunderman Thompson Philippines</td>
                              <td>Globe Telecom Inc.</td>
                            </tr>
                            <tr class="metal-silver">
                              <td scope="row">Audio</th>
                              <td>Life Inside Of You</td>
                              <td>McCann Worldgroup</td>
                              <td>Johnson & Johnson Phils. Inc.</td>
                            </tr>
                            <tr class="metal-bronze">
                              <td scope="row">Promo & Activation</th>
                              <td>Kentucky Fried Crossing</td>
                              <td>Ogilvy & Mather Philippines</td>
                              <td>Grantline, Inc.</td>
                            </tr>
                            <tr class="metal-finalist">
                              <td scope="row">Promo & Activation</th>
                              <td>McDonald's Ride-Thru</td>
                              <td>TBWA\Santiago Mangada Puno</td>
                              <td>McDonald's Philippines</td>
                            </tr>
                            <tr class="metal-silver">
                              <td scope="row">Promo & Activation</th>
                              <td>McClassroom</td>
                              <td>Leo Burnett Group Manila</td>
                              <td>McDonald's Philippines</td>
                            </tr>
                            <tr class="metal-silver">
                              <td scope="row">Promo & Activation</th>
                              <td>Unusually Usual Father's Day Gifts</td>
                              <td>McCann Worldgroup</td>
                              <td>San Miguel Brewery Inc.</td>
                            </tr>
                            <tr class="metal-finalist">
                              <td scope="row">Direct</th>
                              <td>#SuitsForHeroes</td>
                              <td>Wunderman Thompson Philippines</td>
                              <td>Comic Odyssey</td>
                            </tr>
                            <tr class="metal-finalist">
                              <td scope="row">Direct</th>
                              <td>Complex Emojis</td>
                              <td>Propel Manila</td>
                              <td>MindNation</td>
                            </tr>
                            <tr class="metal-bronze">
                              <td scope="row">Direct</th>
                              <td>Unusually Usual Father's Day Gifts</td>
                              <td>McCann Worldgroup</td>
                              <td>San Miguel Brewery Inc.</td>
                            </tr>
                            <tr class="metal-finalist">
                              <td scope="row">Direct</th>
                              <td>Thrilltok</td>
                              <td>BBDO Guerrero</td>
                              <td>PepsiCo Far East Trade Dev Co Inc.</td>
                            </tr>
                            <tr class="metal-bronze">
                              <td scope="row">Direct</th>
                              <td>The Dissolving Bottle</td>
                              <td>BBDO Guerrero</td>
                              <td>The Naturale Market</td>
                            </tr>
                            <tr class="metal-finalist">
                              <td scope="row">Public Relations</th>
                              <td>A Star Wars Experience for All</td>
                              <td>Wunderman Thompson Philippines</td>
                              <td>Globe Telecom Inc.</td>
                            </tr>
                            <tr class="metal-bronze">
                              <td scope="row">Public Relations</th>
                              <td>Ginebra Frontliner Labels</td>
                              <td>dentsu one manila</td>
                              <td>Ginebra San Miguel Inc.</td>
                            </tr>
                            <tr class="metal-gold">
                              <td scope="row">Public Relations</th>
                              <td>McClassroom</td>
                              <td>Leo Burnett Group Manila</td>
                              <td>McDonald's Philippines</td>
                            </tr>
                            <tr class="metal-finalist">
                              <td scope="row">Public Relations</th>
                              <td>Every Kind of Family</td>
                              <td>Petch&Partners</td>
                              <td>Max's Group Inc.</td>
                            </tr>
                             <tr class="metal-silver">
                              <td scope="row">Public Relations</th>
                              <td>NUJP Masked Media</td>
                              <td>Ace Saatchi & Saatchi Advertising</td>
                              <td>National Union of Journalists of the Philippines</td>
                            </tr>
                            <tr class="metal-bronze">
                              <td scope="row">Public Relations</th>
                              <td>Baybayan</td>
                              <td>TBWA\Santiago Mangada Puno</td>
                              <td>Cultural Center of the Philippines</td>
                            </tr>
                            <tr class="metal-finalist">
                              <td scope="row">Digital Activation & Campaigns</th>
                              <td>Ginebra Frontliner Labels</td>
                              <td>dentsu one manila</td>
                              <td>Ginebra San Miguel Inc.</td>
                            </tr>
                            <tr class="metal-bronze">
                              <td scope="row">Digital Activation & Campaigns</th>
                              <td>Pride March Anywhere</td>
                              <td>Propel Manila</td>
                              <td>Globe Telecom Inc.</td>
                            </tr>
                            <tr class="metal-silver">
                              <td scope="row">Digital Activation & Campaigns</th>
                              <td>Baybayan</td>
                              <td>TBWA\Santiago Mangada Puno</td>
                              <td>Cultural Center of the Philippines</td>
                            </tr>
                            <tr class="metal-silver">
                              <td scope="row">Digital Activation & Campaigns</th>
                              <td>GiF Learning Library</td>
                              <td>BBDO Guerrero</td>
                              <td>Facebook</td>
                            </tr>
                            <tr class="metal-silver">
                              <td scope="row">Digital Activation & Campaigns</th>
                              <td>Kentucky Fried Crossing</td>
                              <td>Ogilvy & Mather Philippines</td>
                              <td>Grantline, Inc.</td>
                            </tr>
                            <tr class="metal-bronze">
                              <td scope="row">Digital Solutions</th>
                              <td>Kentucky Fried Crossing</td>
                              <td>Ogilvy & Mather Philippines</td>
                              <td>Grantline, Inc.</td>
                            </tr>
                            <tr class="metal-gold">
                              <td scope="row">Digital Solutions</th>
                              <td>Sakay Halfway</td>
                              <td>MullenLowe TREYNA</td>
                              <td>Sakay.ph</td>
                            </tr>
                            <tr class="metal-silver">
                              <td scope="row">Digital Solutions</th>
                              <td>Font Books</td>
                              <td>BBDO Guerrero</td>
                              <td>Reading Association of the Philippines</td>
                            </tr>
                          
                          </tbody>
                        </table>
                      </div>
                    </div>
                  </div>
                </div>

                <div class="accordion-item">
                  <h2 class="accordion-header" id="acc-head-4">
                    <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#acc-con-4" aria-expanded="false" aria-controls="acc-con-4">
                      Creative Purpose
                    </button>
                  </h2>
                  <div id="acc-con-4" class="accordion-collapse collapse" aria-labelledby="acc-head-4" data-bs-parent="#accordionShortlist">
                    <div class="accordion-body">
                      <h4 class="text-nowin text-center"></h4>
                      <div class="table-responsive">
                        <table class="table table-hover align-middle table-winners">
                          <thead>
                            <tr>
                              <th scope="col">SUBCATEGORY</th>
                              <th scope="col">TITLE</th>
                              <th scope="col">AGENCY</th>
                              <th scope="col">BRAND</th>
                            </tr>
                          </thead>
                          <tbody>
                          	<tr class="metal-gold metal-grand">
                              <td scope="row">Corporate Social Responsibility</th>
                              <td>McClassroom</td>
                              <td>Leo Burnett Group Manila</td>
                              <td>McDonald's Philippines</td>
                            </tr>
                          	<tr class="metal-bronze">
                              <td scope="row">Corporate Social Responsibility</th>
                              <td>#SuitsForHeroes</td>
                              <td>Wunderman Thompson Philippines</td>
                              <td>Comic Odyssey</td>
                            </tr>
                            <tr class="metal-finalist">
                              <td scope="row">Corporate Social Responsibility</th>
                              <td>Complex Emojis</td>
                              <td>Propel Manila</td>
                              <td>MindNation</td>
                            </tr>
                            <tr class="metal-finalist">
                              <td scope="row">Corporate Social Responsibility</th>
                              <td>GiF Learning Library</td>
                              <td>BBDO Guerrero</td>
                              <td>Facebook</td>
                            </tr>
                            <tr class="metal-silver">
                              <td scope="row">Corporate Social Responsibility</th>
                              <td>The Dissolving Bottle</td>
                              <td>BBDO Guerrero</td>
                              <td>The Naturale Market</td>
                            </tr>
                            <tr class="metal-bronze">
                              <td scope="row">Corporate Social Responsibility</th>
                              <td>Closeup Where #LoveRules Live Map</td>
                              <td>MullenLowe TREYNA</td>
                              <td>Unilever Pilippines</td>
                            </tr>
                            <tr class="metal-finalist">
                              <td scope="row">Corporate Social Responsibility</th>
                              <td>McDonald's Kindness Kitchen</td>
                              <td>TBWA\Santiago Mangada Puno</td>
                              <td>McDonald's Philippines</td>
                            </tr>
                            <tr class="metal-bronze">
                              <td scope="row">Corporate Social Responsibility</th>
                              <td>Pride March Anywhere</td>
                              <td>Propel Manila</td>
                              <td>Globe</td>
                            </tr>
                            <tr class="metal-finalist">
                              <td scope="row">Corporate Social Responsibility</th>
                              <td>The First Filipino</td>
                              <td>BBDO Guerrero</td>
                              <td>Anvil Publishing, Inc.</td>
                            </tr>
                            <tr class="metal-finalist">
                              <td scope="row">Public Services & Cause Appeals</th>
                              <td>#iamnotavirus</td>
                              <td>TBWA\Santiago Mangada Puno</td>
                              <td>iamnotavirus.info</td>
                            </tr>
                            <tr class="metal-finalist">
                              <td scope="row">Public Services & Cause Appeals</th>
                              <td>2nd First Birthday</td>
                              <td>Dentsu Jayme Syfu</td>
                              <td>Sweet Spot Inc.</td>
                            </tr>
                            <tr class="metal-finalist">
                              <td scope="row">Public Services & Cause Appeals</th>
                              <td>Complex Emojis</td>
                              <td>Propel Manila</td>
                              <td>MindNation</td>
                            </tr>
                            <tr class="metal-finalist">
                              <td scope="row">Public Services & Cause Appeals</th>
                              <td>Love Verses Hate</td>
                              <td>Propel Manila</td>
                              <td>Love Is All We Need</td>
                            </tr>
                            <tr class="metal-bronze">
                              <td scope="row">Non-Profit, Charity and Government</th>
                              <td>Stop The Spread</td>
                              <td>BBDO Guerrero</td>
                              <td>National Union of Journalists of the Philippines</td>
                            </tr>
                            <tr class="metal-gold">
                              <td scope="row">Non-Profit, Charity and Government</th>
                              <td>SaferKidsPH "Toys"</td>
                              <td>TBWA\Santiago Mangada Puno</td>
                              <td>UNICEF Philippines</td>
                            </tr>
                            <tr class="metal-bronze">
                              <td scope="row">Non-Profit, Charity and Government</th>
                              <td>Tubbataha Coral Rip</td>
                              <td>TBWA\Santiago Mangada Puno</td>
                              <td>Tubbataha Reefs Natural Park and World Heritage Site</td>
                            </tr>
                            <tr class="metal-silver">
                              <td scope="row">Non-Profit, Charity and Government</th>
                              <td>BayBayan</td>
                              <td>TBWA\Santiago Mangada Puno</td>
                              <td>Cultural Center of the Philippines</td>
                            </tr>
                            <tr class="metal-silver">
                              <td scope="row">Non-Profit, Charity and Government</th>
                              <td>NUJP Masked Media</td>
                              <td>Ace Saatchi & Saatchi Advertising</td>
                              <td>National Union of Journalists of the Philippines</td>
                            </tr>
                            <tr class="metal-silver">
                              <td scope="row">Non-Profit, Charity and Government</th>
                              <td>Font Books</td>
                              <td>BBDO Guerrero</td>
                              <td>Reading Association of the Philippines</td>
                            </tr>
                            <tr class="metal-bronze">
                              <td scope="row">Public Services & Cause Appeals</th>
                              <td>Dove Blackout Mirror</td>
                              <td>Ogilvy & Mather Philippines, Inc.</td>
                              <td>Unilever Pilippines</td>
                            </tr>
                            <tr class="metal-bronze">
                              <td scope="row">Public Services & Cause Appeals</th>
                              <td>Ingat Angat Tayong Lahat (Carefully, We Rise)</td>
                              <td>TBWA\Santiago Mangada Puno</td>
                              <td>Task Force T3</td>
                            </tr>
                            <tr class="metal-silver">
                              <td scope="row">Public Services & Cause Appeals</th>
                              <td>SaferKidsPH "Toys"</td>
                              <td>TBWA\Santiago Mangada Puno</td>
                              <td>UNICEF Philippines</td>
                            </tr>
                            <tr class="metal-bronze">
                              <td scope="row">Public Services & Cause Appeals</th>
                              <td>NUJP Masked Media</td>
                              <td>Ace Saatchi & Saatchi Advertising</td>
                              <td>National Union of Journalists of the Philippines</td>
                            </tr>
                            <tr class="metal-bronze">
                              <td scope="row">Public Services & Cause Appeals</th>
                              <td>Tubbataha Coral Rip</td>
                              <td>TBWA\Santiago Mangada Puno</td>
                              <td>Tubbataha Reefs Natural Park and World Heritage Site</td>
                            </tr>
                            <tr class="metal-bronze">
                              <td scope="row">Public Services & Cause Appeals</th>
                              <td>CAPTCHAring Harassment</td>
                              <td>Dentsu Jayme Syfu</td>
                              <td>Gabriela Philippines</td>
                            </tr>
                            
                          </tbody>
                        </table>
                      </div>
                    </div>
                  </div>
                </div>

                <div class="accordion-item">
                  <h2 class="accordion-header" id="acc-head-5">
                    <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#acc-con-5" aria-expanded="false" aria-controls="acc-con-5">
                      Craft
                    </button>
                  </h2>
                  <div id="acc-con-5" class="accordion-collapse collapse" aria-labelledby="acc-head-5" data-bs-parent="#accordionShortlist">
                    <div class="accordion-body">
                      <h4 class="text-nowin text-center"></h4>
                      <div class="table-responsive">
                        <table class="table table-hover align-middle table-winners">
                          <thead>
                            <tr>
                              <th scope="col">SUBCATEGORY</th>
                              <th scope="col">TITLE</th>
                              <th scope="col">AGENCY/PRODUCTION HOUSE</th>
                              <th scope="col">BRAND</th>
                            </tr>
                          </thead>
                          <tbody>
                          	<tr class="metal-bronze">
                              <td scope="row">Digital: Best UX/UI</th>
                              <td>Sakay Halfway</td>
                              <td>MullenLowe TREYNA</td>
                              <td>Sakay.ph</td>
                            </tr>
                            <tr class="metal-bronze">
                              <td scope="row">Digital: Best UX/UI</th>
                              <td>The Think Fast Sale</td>
                              <td>Digitas Philippines</td>
                              <td>Wyeth Nutrition Philippines</td>
                            </tr>
                          	<tr class="metal-bronze">
                              <td scope="row">Digital (Excluding Films) – Best Character Design</th>
                              <td>Ginebra Frontliner Labels</td>
                              <td>dentsu one manila</td>
                              <td>Ginebra San Miguel Inc.</td>
                            </tr>
                            <tr class="metal-silver">
                              <td scope="row">Digital (Excluding Films) – Best Art Direction</th>
                              <td>GiF Learning Library</td>
                              <td>BBDO Guerrero</td>
                              <td>Facebook</td>
                            </tr>
                            <tr class="metal-silver">
                              <td scope="row">Digital (Excluding Films) – Best Art Direction</th>
                              <td>The First Filipino</td>
                              <td>BBDO Guerrero</td>
                              <td>Anvil Publishing, Inc.</td>
                            </tr>
                            <tr class="metal-silver">
                              <td scope="row">Digital (Excluding Films) – Best Copywriting</th>
                              <td>BIBIGkasin Challenge</td>
                              <td>Wunderman Thompson Philippines</td>
                              <td>Johnson & Johnson Phils. Inc.</td>
                            </tr>
                            <tr class="metal-bronze">
                              <td scope="row">Digital (Excluding Films) – Best Typography</th>
                              <td>Be Part of the Font</td>
                              <td>BBDO Guerrero</td>
                              <td>Department of Tourism</td>
                            </tr>
                            <tr class="metal-bronze">
                              <td scope="row">Digital (Excluding Films) – Best Best Art Direction</th>
                              <td>Be Part of the Font</td>
                              <td>BBDO Guerrero</td>
                              <td>Department of Tourism</td>
                            </tr>
                            <tr class="metal-bronze">
                              <td scope="row">Digital (Excluding Films) – Best Best Art Direction</th>
                              <td>The Dissolving Bottle</td>
                              <td>BBDO Guerrero</td>
                              <td>The Naturale Market</td>
                            </tr>
                            <tr class="metal-silver">
                              <td scope="row">Digital (Excluding Films) – Best Typography</th>
                              <td>Obra Typeface</td>
                              <td>Wunderman Thompson Philippines</td>
                              <td>Ayala Museum</td>
                            </tr>
                            <tr>
                              <td scope="row">Digital (Excluding Films) – Best Typography</th>
                              <td>BayBayan</td>
                              <td>TBWA\Santiago Mangada Puno</td>
                              <td>Cultural Center of the Philippines</td>
                            </tr>    
                            <tr class="metal-bronze">
                              <td scope="row">Digital (Excluding Films) – Best Typography</th>
                              <td>Font Books</td>
                              <td>BBDO Guerrero</td>
                              <td>Reading Association of the Philippines</td>
                            </tr>
                            <tr class="metal-silver">
                              <td scope="row">Digital (Excluding Films) – Best Illustration</th>
                              <td>Pride for Good</td>
                              <td>Publicis JimenezBasic</td>
                              <td>Lazada Philippines</td>
                            </tr>  
                            <tr class="metal-gold">
                              <td scope="row">Digital (Excluding Films) – Best Art Direction</th>
                              <td>BayBayan</td>
                              <td>TBWA\Santiago Mangada Puno</td>
                              <td>Cultural Center of the Philippines</td>
                            </tr> 
                            <tr class="metal-gold">
                              <td scope="row">Digital (Excluding Films) – Best Typography</th>
                              <td>BayBayan</td>
                              <td>TBWA\Santiago Mangada Puno</td>
                              <td>Cultural Center of the Philippines</td>
                            </tr>    
                            <tr class="metal-gold">
                              <td scope="row">Design/Outdoor/Print – Best Typography</th>
                              <td>BayBayan</td>
                              <td>TBWA\Santiago Mangada Puno</td>
                              <td>Cultural Center of the Philippines</td>
                            </tr>                      
                            <tr class="metal-bronze">
                              <td scope="row">Design/Outdoor/Print – Best Typography</th>
                              <td>Adobo Design Awards</td>
                              <td>Wunderman Thompson Philippines</td>
                              <td>Adobo Magazine</td>
                            </tr>
                            <tr class="metal-bronze">
                              <td scope="row">Design/Outdoor/Print – Best Typography</th>
                              <td>Font Books</td>
                              <td>BBDO Guerrero</td>
                              <td>Reading Association of the Philippines</td>
                            </tr>
                          	<tr class="metal-bronze">
                              <td scope="row">Design/Outdoor/Print – Best Digital Imaging</th>
                              <td>Bench Be The Leading Men</td>
                              <td>TBWA\Santiago Mangada Puno</td>
                              <td>Suyen Corporation</td>
                            </tr>
                          	<tr class="metal-bronze">
                              <td scope="row">Design/Outdoor/Print – Best Art Direction</th>
                              <td>Bench Be The Leading Men</td>
                              <td>TBWA\Santiago Mangada Puno</td>
                              <td>Suyen Corporation</td>
                            </tr>
                            <tr>
                              <td scope="row">Design/Outdoor/Print – Best Art Direction</th>
                              <td>Be Part of the Font</td>
                              <td>BBDO Guerrero</td>
                              <td>Department of Tourism</td>
                            </tr>
                            <tr class="metal-gold">
                              <td scope="row">Design/Outdoor/Print – Best Art Direction</th>
                              <td>BayBayan</td>
                              <td>TBWA\Santiago Mangada Puno</td>
                              <td>Cultural Center of the Philippines</td>
                            </tr>
                            <tr class="metal-silver">
                              <td scope="row">Design/Outdoor/Print – Best Art Direction</th>
                              <td>Take Me to Tokyo</td>
                              <td>Ogilvy & Mather Philippines</td>
                              <td>Hansbury Inc.</td>
                            </tr>
                            <tr class="metal-gold">
                              <td scope="row">Design/Outdoor/Print – Best Art Direction</th>
                              <td>Tubbataha Coral Rip</td>
                              <td>TBWA\Santiago Mangada Puno</td>
                              <td>Tubbataha Reefs Natural Park and World Heritage Site</td>
                            </tr>
                            <tr>
                              <td scope="row">Design/Outdoor/Print – Best Art Direction</th>
                              <td>The Dissolving Bottle</td>
                              <td>BBDO Guerrero</td>
                              <td>The Naturale Market</td>
                            </tr>
                            <tr class="metal-finalist">
                              <td scope="row">Design/Outdoor/Print – Best Copywriting</th>
                              <td>Love Verses Hate</td>
                              <td>Propel Manila</td>
                              <td>Love is All We Need</td>
                            </tr>
                            <tr class="metal-bronze">
                              <td scope="row">Design/Outdoor/Print – Best Copywriting</th>
                              <td>Filename Posters</td>
                              <td>McCann Worldgroup</td>
                              <td>Loudbox Studios Inc.</td>
                            </tr>
                            <tr class="metal-bronze">
                              <td scope="row">Digital (Excluding Films) – Best Illustration</th>
                              <td>Ginebra Frontliner Labels</td>
                              <td>dentsu one manila</td>
                              <td>Ginebra San Miguel Inc.</td>
                            </tr>
                            <tr class="metal-bronze">
                              <td scope="row">Design/Outdoor/Print – Best Illustration</th>
                              <td>Red Horse Beer 2020 Calendar</td>
                              <td>NuWorks Interactive Labs, Inc.</td>
                              <td>San Miguel Brewery Inc.</td>
                            </tr>
                            <tr class="metal-gold">
                              <td scope="row">Design/Outdoor/Print – Best Illustration</th>
                              <td>Tubbataha Coral Rip</td>
                              <td>TBWA\Santiago Mangada Puno</td>
                              <td>Tubbataha Reefs Natural Park and World Heritage Site</td>
                            </tr>
                            <tr class="metal-silver">
                              <td scope="row">Design/Outdoor/Print – Best Illustration</th>
                              <td>Take Me to Tokyo</td>
                              <td>Ogilvy & Mather Philippines</td>
                              <td>Hansbury Inc.</td>
                            </tr>
                            <tr class="metal-bronze">
                              <td scope="row">Design/Outdoor/Print – Best Illustration</th>
                              <td>#SuitsForHeroes</td>
                              <td>Wunderman Thompson Philippines</td>
                              <td>Comic Odyssey</td>
                          	</td>
                            <tr class="metal-finalist">
                              <td scope="row">Digital (Excluding Films) – Best Copywriting</th>
                              <td>Love Verses Hate</td>
                              <td>Propel Manila</td>
                              <td>Love is All We Need</td>
                            </tr>
                            <tr class="metal-finalist">
                              <td scope="row">Audio: Best Direction</th>
                              <td>2nd First Birthday</td>
                              <td>Soundesign Manila</td>
                              <td>Cakeshop by Sonja</td>
                            </tr>
                            <tr class="metal-bronze">
                              <td scope="row">Audio: Best Direction</th>
                              <td>Cloud</td>
                              <td>Soundesign Manila</td>
                              <td>PLDT</td>
                            </tr>
                            <tr class="metal-finalist">
                              <td scope="row">Audio: Best Direction</th>
                              <td>Spicy Birit</td>
                              <td>Soundesign Manila</td>
                              <td>Lucky Me</td>
                            </tr>
                            <tr class="metal-finalist">
                              <td scope="row">Audio: Best Direction</th>
                              <td>Phoenix</td>
                              <td>Soundesign Manila</td>
                              <td>Smart Communications Inc.</td>
                            </tr>
                            <tr class="metal-silver">
                              <td scope="row">Audio: Best Direction</th>
                              <td>Fiesta Filipinas: Experience Filipino Festivals through Auditory Experiences (AUDEX)</td>
                              <td>Served Manila & Wonder Collab Studios</td>
                              <td>Department of Foreign Affairs</td>
                            </tr>
                            <tr class="metal-finalist">
                              <td scope="row">Audio: Best Music/Sound Design</th>
                              <td>2nd First Birthday</td>
                              <td>Soundesign Manila</td>
                              <td>Cakeshop by Sonja</td>
                            </tr>
                            <tr class="metal-finalist">
                              <td scope="row">Audio: Best Music/Sound Design</th>
                              <td>Nescafé Bangon Tayo</td>
                              <td>Soundesign Manila</td>
                              <td>Nestlé Philippines, Inc.</td>
                            </tr>
                            <tr class="metal-bronze">
                              <td scope="row">Audio: Best Music/Sound Design</th>
                              <td>Cloud</td>
                              <td>Soundesign Manila</td>
                              <td>PLDT</td>
                            </tr>
                            <tr class="metal-gold">
                              <td scope="row">Audio: Best Music/Sound Design</th>
                              <td>Side By Side</td>
                              <td>Loudbox Studios</td>
                              <td>San Miguel Brewery Inc.</td>
                            </tr>
                            <tr class="metal-finalist">
                              <td scope="row">Audio: Best Music/Sound Design</th>
                              <td>Phoenix</td>
                              <td>Soundesign Manila</td>
                              <td>Smart Communications Inc.</td>
                            </tr>
                            <tr class="metal-bronze">
                              <td scope="row">Audio: Best Music/Sound Design</th>
                              <td>Shirt</td>
                              <td>Soundesign Manila</td>
                              <td>Shipping Cart</td>
                            </tr>
                            <tr class="metal-bronze">
                              <td scope="row">Audio: Best Music/Sound Design</th>
                              <td>Chorale</td>
                              <td>Soundesign Manila</td>
                              <td>Seaoil</td>
                            </tr>
                            <tr class="metal-finalist">
                              <td scope="row">Audio: Best Music/Sound Design</th>
                              <td>Pass Ako</td>
                              <td>Loudbox Studios</td>
                              <td>San Miguel Brewery Inc.</td>
                            </tr>
                            <tr class="metal-finalist">
                              <td scope="row">Audio: Best Music/Sound Design</th>
                              <td>Tourism Decade Ten</td>
                              <td>Loudbox Studios</td>
                              <td>Department of Tourism</td>
                            </tr>
                            <tr class="metal-bronze">
                              <td scope="row">Audio: Best Music/Sound Design</th>
                              <td>Hugas Na Rin Yan - Fred d' Germ</td>
                              <td>Loudbox Studios</td>
                              <td>P&G Singapore</td>
                            </tr>
                            <tr class="metal-bronze">
                              <td scope="row">Audio: Best Music/Sound Design</th>
                              <td>Journey aka On & On Song</td>
                              <td>Loudbox Studios</td>
                              <td>Chevron Philippines Inc.</td>
                            </tr>
                            <tr>
                              <td scope="row">Audio: Best Voice Performance</th>
                              <td>Chorale</td>
                              <td>Soundesign Manila</td>
                              <td>Seaoil</td>
                            </tr>
                            <tr class="metal-bronze">
                              <td scope="row">Audio: Best Voice Performance</th>
                              <td>BLACKPINK Reinvent Your World</td>
                              <td>Kingkong Sound South Korea</td>
                              <td>Globe Telecom</td>
                            </tr>
                            <tr class="metal-bronze">
                              <td scope="row">Audio: Best Voice Performance</th>
                              <td>Lullaby</td>
                              <td>Soundesign Manila</td>
                              <td>Kamillosan</td>
                            </tr>
                            <tr class="metal-silver">
                              <td scope="row">Audio: Best Copywriting</th>
                              <td>Life Inside Of You</td>
                              <td>McCann Worldgroup</td>
                              <td>Johnson & Johnson Phils. Inc.</td>
                            </tr>
                            <tr class="metal-finalist">
                              <td scope="row">Film: Best Art Direction</th>
                              <td>Applause</td>
                              <td>FCBManila</td>
                              <td>Union Bank of the Philippines</td>
                            </tr>
                            <tr class="metal-finalist">
                              <td scope="row">Film: Best Art Direction</th>
                              <td>Connections Should Have No Limits</td>
                              <td>Petch&Partners</td>
                              <td>VIVO</td>
                            </tr>
                            <tr class="metal-bronze">
                              <td scope="row">Film: Best Art Direction</th>
                              <td>Adobo Festival of Ideas</td>
                              <td>Wunderman Thompson Philippines</td>
                              <td>Adobo Magazine</td>
                            </tr>
                            <tr class="metal-finalist">
                              <td scope="row">Film: Best Art Direction</th>
                              <td>Mindhunter – Waiting in Vain</td>
                              <td>Dentsu Jayme Syfu</td>
                              <td>Smart Communications Inc.</td>
                            </tr>
                            <tr class="metal-finalist">
                              <td scope="row">Film: Best Art Direction</th>
                              <td>PSBank Aksyon</td>
                              <td>Seven A.D.</td>
                              <td>Philippine Savings Bank</td>
                            </tr>
                            <tr class="metal-silver">
                              <td scope="row">Film: Best Art Direction</th>
                              <td>Small Moments</td>
                              <td>McCann Worldgroup</td>
                              <td>Ayala Malls</td>
                            </tr>
                            <tr class="metal-bronze">
                              <td scope="row">Film: Best Art Direction</th>
                              <td>Side By Side</td>
                              <td>McCann Worldgroup</td>
                              <td>San Miguel Brewery Inc.</td>
                            </tr>
                            <tr class="metal-silver">
                              <td scope="row">Film: Best Art Direction</th>
                              <td>Every Kind of Family</td>
                              <td>Petch&Partners</td>
                              <td>Max's Group Inc</td>
                            </tr>
                            <tr class="metal-finalist">
                              <td scope="row">Film: Best Art Direction</th>
                              <td>The AlphaBURP</td>
                              <td>Petch&Partners</td>
                              <td>Dole Philippines</td>
                            </tr>
                            <tr class="metal-bronze">
                              <td scope="row">Film: Best Copywriting</th>
                              <td>Distance/Sama-Sama Sa Distansya</td>
                              <td>Publicis JimenezBasic</td>
                              <td>Globe Telecom</td>
                            </tr>
                            <tr class="metal-bronze">
                              <td scope="row">Film: Best Copywriting</th>
                              <td>Na Lockdown Tayo, Ma</td>
                              <td>Leo Burnett Group Manila</td>
                              <td>McDonald's Philippines</td>
                            </tr>
                            <tr class="metal-silver">
                              <td scope="row">Film: Best Copywriting</th>
                              <td>CADP's Death Threat</td>
                              <td>MullenLowe Treyna</td>
                              <td>Coalition Against Death Penalty</td>
                            </tr>
                            <tr class="metal-bronze">
                              <td scope="row">Film: Best Copywriting</th>
                              <td>Side By Side</td>
                              <td>McCann Worldgroup</td>
                              <td>San Miguel Brewery Inc.</td>
                            </tr>
                            <tr class="metal-finalist">
                              <td scope="row">Film: Best Audio Craft</th>
                              <td>Phoenix</td>
                              <td>Soundesign Manila</td>
                              <td>Smart Communications Inc.</td>
                            </tr>
                            <tr class="metal-bronze">
                              <td scope="row">Film: Best Audio Craft</th>
                              <td>Cloud</td>
                              <td>Soundesign Manila</td>
                              <td>PLDT</td>
                            </tr>
                            <tr class="metal-finalist">
                              <td scope="row">Film: Best Audio Craft</th>
                              <td>Y-Connect Possible The Link</td>
                              <td>Loudbox Studios</td>
                              <td>Yamaha Motors Philippines Inc.</td>
                            </tr>
                            <tr class="metal-bronze">
                              <td scope="row">Film: Best Audio Craft</th>
                              <td>2nd First Birthday</td>
                              <td>Soundesign Manila</td>
                              <td>Cakeshop by Sonja</td>
                            </tr>
                            <tr class="metal-finalist">
                              <td scope="row">Film: Best Direction</th>
                              <td>Batang Matibay Awards 2019 - Suhel</td>
                              <td>UxS, Inc.</td>
                              <td>Nestlé Philippines, Inc.</td>
                            </tr>
                            <tr class="metal-silver">
                              <td scope="row">Film: Best Direction</th>
                              <td>A Frontliner's Sacrifice</td>
                              <td>FILMPABRIKA INC.</td>
                              <td>P&G Ariel</td>
                            </tr>
                            <tr class="metal-silver">
                              <td scope="row">Film: Best Direction</th>
                              <td>Applause</td>
                              <td>FILMPABRIKA INC.</td>
                              <td>Union Bank of the Philippines</td>
                            </tr>
                            <tr class="metal-silver">
                              <td scope="row">Film: Best Direction</th>
                              <td>Darkula</td>
                              <td>FILMPABRIKA INC.</td>
                              <td>Philippine Seven Corporation</td>
                            </tr>
                            <tr class="metal-silver">
                              <td scope="row">Film: Best Direction</th>
                              <td>Lift</td>
                              <td>FILMPABRIKA INC.</td>
                              <td>McDonald's Philippines</td>
                            </tr>
                            <tr class="metal-finalist">
                              <td scope="row">Film: Best Direction</th>
                              <td>Bigas</td>
                              <td>Arcade Film Factory</td>
                              <td>Smart Communications Inc.</td>
                            </tr>
                            <tr class="metal-finalist">
                              <td scope="row">Film: Best Direction</th>
                              <td>Hero</td>
                              <td>UxS, Inc.</td>
                              <td>Jollibee Foods Corporation</td>
                            </tr>
                            <tr class="metal-bronze">
                              <td scope="row">Film: Best Direction</th>
                              <td>Abakada</td>
                              <td>FILMPABRIKA INC.</td>
                              <td>Department of Tourism</td>
                            </tr>
                            <tr class="metal-bronze">
                              <td scope="row">Film: Best Direction</th>
                              <td>Cupid</td>
                              <td>Arcade Film Factory</td>
                              <td>Netflix Pte Ltd.</td>
                            </tr>
                            <tr class="metal-bronze">
                              <td scope="row">Film: Best Direction</th>
                              <td>Gong</td>
                              <td>Arcade Film Factory</td>
                              <td>Wellmade Manufacturing Corp.</td>
                            </tr>
                            <tr class="metal-gold">
                              <td scope="row">Film: Best Direction</th>
                              <td>Gold</td>
                              <td>Arcade Film Factory</td>
                              <td>ARC Refreshments Corp.</td>
                            </tr>
                            <tr class="metal-bronze">
                              <td scope="row">Film: Best Direction</th>
                              <td>Judith</td>
                              <td>Arcade Film Factory</td>
                              <td>Philippine Seven Corporation</td>
                            </tr>
                            <tr class="metal-gold">
                              <td scope="row">Film: Best Direction</th>
                              <td>Tokyo</td>
                              <td>FILMPABRIKA INC.</td>
                              <td>VIVO</td>
                            </tr>
                            <tr class="metal-gold">
                              <td scope="row">Film: Best Direction</th>
                              <td>Us</td>
                              <td>FILMPABRIKA INC.</td>
                              <td>McDonald's Philippines</td>
                            </tr>
                            <tr class="metal-bronze">
                              <td scope="row">Film: Best Direction</th>
                              <td>Snacks</td>
                              <td>Arcade Film Factory</td>
                              <td>CDO Foodsphere Inc.</td>
                            </tr>
                            <tr class="metal-gold">
                              <td scope="row">Film: Best Direction</th>
                              <td>Grizzer/Star Wars</td>
                              <td>FILMPABRIKA INC.</td>
                              <td>Globe Telecom</td>
                            </tr>
                            <tr class="metal-finalist">
                              <td scope="row">Film: Best Editing</th>
                              <td>Bigas</td>
                              <td>Arcade Film Factory</td>
                              <td>Smart Communications Inc.</td>
                            </tr>
                            <tr class="metal-bronze">
                              <td scope="row">Film: Best Editing</th>
                              <td>Doors</td>
                              <td>Arcade Film Factory</td>
                              <td>Smart Communications Inc.</td>
                            </tr>
                            <tr class="metal-bronze">
                              <td scope="row">Film: Best Editing</th>
                              <td>Ten</td>
                              <td>Arcade Film Factory</td>
                              <td>Department of Tourism</td>
                            </tr>
                            <tr class="metal-silver">
                              <td scope="row">Film: Best Editing</th>
                              <td>Abakada</td>
                              <td>FILMPABRIKA INC.</td>
                              <td>Department of Tourism</td>
                            </tr>
                            <tr class="metal-silver">
                              <td scope="row">Film: Best Editing</th>
                              <td>Grizzer/Star Wars</td>
                              <td>FILMPABRIKA INC.</td>
                              <td>Globe Telecom</td>
                            </tr>
                            <tr class="metal-silver">
                              <td scope="row">Film: Best Editing</th>
                              <td>Tokyo</td>
                              <td>FILMPABRIKA INC.</td>
                              <td>VIVO</td>
                            </tr>
                            <tr class="metal-silver">
                              <td scope="row">Film: Best Editing</th>
                              <td>Batang Matibay Awards 2019 - Suhel</td>
                              <td>UxS, Inc.</td>
                              <td>Nestlé Philippines, Inc.</td>
                            </tr>
                            <tr class="metal-silver">
                              <td scope="row">Film: Best Editing</th>
                              <td>Goodbye, Hello</td>
                              <td>FILMPABRIKA INC.</td>
                              <td>Manulife</td>
                            </tr>
                            <tr class="metal-finalist">
                              <td scope="row">Film: Best Production Design</th>
                              <td>Dark or White</td>
                              <td>FILMPABRIKA INC.</td>
                              <td>Splash Corp</td>
                            </tr>
                            <tr class="metal-bronze">
                              <td scope="row">Film: Best Production Design</th>
                              <td>Dawn</td>
                              <td>FILMPABRIKA INC.</td>
                              <td>Globe Telecom</td>
                            </tr>
                            <tr class="metal-gold">
                              <td scope="row">Film: Best Production Design</th>
                              <td>Small Moments</td>
                              <td>FILMPABRIKA INC.</td>
                              <td>Ayala Malls</td>
                            </tr>
                            <tr class="metal-silver">
                              <td scope="row">Film: Best Production Design</th>
                              <td>Abakada</td>
                              <td>FILMPABRIKA INC.</td>
                              <td>Department of Tourism</td>
                            </tr>
                            <tr class="metal-bronze">
                              <td scope="row">Film: Best Cinematography</th>
                              <td>Batang Matibay Awards 2019 - Suhel</td>
                              <td>UxS, Inc.</td>
                              <td>Nestlé Philippines, Inc.</td>
                            </tr>
                            <tr class="metal-bronze">
                              <td scope="row">Film: Best Cinematography</th>
                              <td>Play</td>
                              <td>Arcade Film Factory</td>
                              <td>Smart Communications Inc.</td>
                            </tr>
                            <tr class="metal-bronze">
                              <td scope="row">Film: Best Cinematography</th>
                              <td>Ten</td>
                              <td>Arcade Film Factory</td>
                              <td>Department of Tourism</td>
                            </tr>
                            <tr class="metal-bronze">
                              <td scope="row">Film: Best Cinematography</th>
                              <td>Wake Up in Eastern Visayas</td>
                              <td>UxS, Inc.</td>
                              <td>Department of Tourism</td>
                            </tr>
                            <tr class="metal-gold">
                              <td scope="row">Film: Best Cinematography</th>
                              <td>Tokyo</td>
                              <td>FILMPABRIKA INC.</td>
                              <td>VIVO</td>
                            </tr>
                            <tr class="metal-finalist">
                              <td scope="row">Film: Best Visual Effects and Digital Imaging</th>
                              <td>Caveman</td>
                              <td>Arcade Film Factory</td>
                              <td>JG Summit Holdings, Inc/URC</td>
                            </tr>
                            <tr class="metal-gold metal-grand">
                              <td scope="row">Film: Best Visual Effects and Digital Imaging</th>
                              <td>Gold</td>
                              <td>Arcade Film Factory</td>
                              <td>ARC Refreshments Corp</td>
                            </tr>
                            <tr class="metal-bronze">
                              <td scope="row">Film: Best Visual Effects and Digital Imaging</th>
                              <td>Doors</td>
                              <td>Arcade Film Factory</td>
                              <td>Smart Communications Inc.</td>
                            </tr>
                            <tr class="metal-bronze">
                              <td scope="row">Film: Best Visual Effects and Digital Imaging</th>
                              <td>Luzon</td>
                              <td>Arcade Film Factory</td>
                              <td>Prifood Corporation</td>
                            </tr>
                            
                          </tbody>
                        </table>
                      </div>
                    </div>
                  </div>
                </div>


              </div>


            </div>
          </div>

          <div class="row" id="row-auditor">
            <div class="col-12 text-center">
              <h4 style="color: #fff">
                <svg width="25" height="26" viewBox="0 0 25 26" fill="none" xmlns="http://www.w3.org/2000/svg">
                  <g clip-path="url(#clip0_2245:17531)">
                  <circle cx="13" cy="13" r="8" fill="white"/>
                  <path d="M12.5 0.5C5.63428 0.5 0 6.13428 0 13C0 19.8657 5.63428 25.5 12.5 25.5C19.3657 25.5 25 19.8657 25 13C25 6.13428 19.3657 0.5 12.5 0.5ZM10.9873 18.6855L5.51338 13.2117L7.58477 11.1403L11.0831 14.6387L18.1062 8.2543L20.0774 10.4215L10.9873 18.6855Z" fill="#D7A333"/>
                  </g>
                  <defs>
                  <clipPath id="clip0_2245:17531">
                  <rect width="25" height="25" fill="white" transform="translate(0 0.5)"/>
                  </clipPath>
                  </defs>
                  </svg>
                &nbsp;Audited and Verified by
              </h4>
              <img src="{{url('images/rtco-logo.png')}}" alt="" class="img-responsive" style="margin-top: 5px; margin-bottom: 10px; max-width: 100%">
              <br>
              <!--<h4 style="color: #fff">Reyes Tacandong & Co.</h4>-->
            </div>
          </div>
      </div>
    </section>
  
    <!-- modal -->
    <div class="voting-modal modal fade" id="contactusModal" tabindex="-1" aria-labelledby="flag" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered ">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body p-0 pt-4">
                    <div class="selection">
                        <p class="question contact-title">Contact 4As</p>
                        <div class="footer-links contact flex-column">
                            <p class="label">
                                4As Secretariat:&nbsp;<span class="value">Vanne Tomada</span>
                            </p>
                            <p class="label">
                                Address:&nbsp;<span class="value">Unit 442 Cityland Pasong Tamo Tower, 2210 Chino Roces, Makati City</span>
                            </p>
                            <p class="label">
                                Email address:&nbsp;<a href="mailto:4asp@pldtdsl.net" class="value" target="_blank">4asp@pldtdsl.net</a>
                            </p>
                            <div class="d-flex label">
                                Telephone number:&nbsp;<div class="separator"><a class="value">893-1205</a><a class="value">757-3891</a><a class="value">812-4397</a></div>
                            </div>
                            <p class="label">
                                Mobile number:&nbsp;<a href="#" class="value" target="_blank">0917 5222 427</a>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
  <script src="https://code.jquery.com/jquery-3.6.0.min.js" integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>
  <script src="{{url('/js/loadingoverlay.min.js')}}"></script>
  <script>
    //Metal filters
    var noWinCopy1 = "There is no";
    var noWinCopy2 = "winner for this category.";
    var $targetRow = $('.table-winners tbody tr');

    //initialize
    $targetRow.each(function() {
      if($(this).hasClass("metal-bronze")){
         $(this).show();
      }else{
        $(this).hide();
      }
    });

    //$('.table-winners').show();

    checkWinCounts('metal-bronze' , 'Bronze');


    function checkWinCounts(metalClass, metalText){
      var ctrWin = 0;

      $('.text-nowin').hide();

      //1
      $('#acc-con-1 .table-winners tbody tr').each(function(){
        if($(this).hasClass(metalClass)){
            ctrWin++;
        }     
      });

      if(ctrWin == 0){
        $('#acc-con-1 .text-nowin').text(noWinCopy1 + " " + metalText + " "+ noWinCopy2);
        $('#acc-con-1 .text-nowin').show();
        $('#acc-con-1 .table-winners').hide();
      }else{
        $('#acc-con-1 .table-winners').show();
      }
      ctrWin = 0;

      //2
      $('#acc-con-2 .table-winners tbody tr').each(function(){
        if($(this).hasClass(metalClass)){
            ctrWin++;
        }     
      });

      if(ctrWin == 0){
        $('#acc-con-2 .text-nowin').text(noWinCopy1 + " " + metalText + " "+ noWinCopy2);
        $('#acc-con-2 .text-nowin').show();
        $('#acc-con-2 .table-winners').hide();
      }else{
        $('#acc-con-2 .table-winners').show();
      }
      ctrWin = 0;

      //3
      $('#acc-con-3 .table-winners tbody tr').each(function(){
        if($(this).hasClass(metalClass)){
            ctrWin++;
        }     
      });

      if(ctrWin == 0){
        $('#acc-con-3 .text-nowin').text(noWinCopy1 + " " + metalText + " "+ noWinCopy2);
        $('#acc-con-3 .text-nowin').show();
        $('#acc-con-3 .table-winners').hide();
      }else{
        $('#acc-con-3 .table-winners').show();
      }
      ctrWin = 0;

      //4
      $('#acc-con-4 .table-winners tbody tr').each(function(){
        if($(this).hasClass(metalClass)){
            ctrWin++;
        }     
      });

      if(ctrWin == 0){
        $('#acc-con-4 .text-nowin').text(noWinCopy1 + " " + metalText + " "+ noWinCopy2);
        $('#acc-con-4 .text-nowin').show();
        $('#acc-con-4 .table-winners').hide();
      }else{
        $('#acc-con-4 .table-winners').show();
      }
      ctrWin = 0;

      //5
      $('#acc-con-5 .table-winners tbody tr').each(function(){
        if($(this).hasClass(metalClass)){
            ctrWin++;
        }     
      });

      if(ctrWin == 0){
        $('#acc-con-5 .text-nowin').text(noWinCopy1 + " " + metalText + " "+ noWinCopy2);
        $('#acc-con-5 .text-nowin').show();
        $('#acc-con-5 .table-winners').hide();
      }else{
        $('#acc-con-5 .table-winners').show();
      }
      ctrWin = 0;

    }


    //on filter
    $('.btn-group-filters .btn-primary').click(function(){
      var $thisBtn = $(this);
      var selectedVal = $thisBtn.data("filter-value");

      $('.btn-group-filters .btn-primary').removeClass('active');
      $thisBtn.addClass('active');

      $('#filter-drop-text').text($thisBtn.text());

      //$targetRow.hide();

      $targetRow.each(function() {
        if($(this).hasClass(selectedVal)){
           $(this).show();
        }else{
           $(this).hide();
        }
      });

      console.log('val '+selectedVal);
      checkWinCounts(selectedVal , $thisBtn.text());

    });
  </script>
  <script>
    $(document).on('click', '.profile-link', function() {
        Swal.fire({
            // confirmButtonText: 'Profile',
            showConfirmButton: false,
            denyButtonText: 'Logout',
            denyButtonClass: 'btn-logout',
            showDenyButton: true
        }).then((result) => {
            // if (result.isConfirmed) {
            //     location.href = "/profile"
            // }
            if (result.isDenied) {
                location.href = "/logout"
            }
        })
    })
  </script>
  <!-- end 2021 js -->  
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.3/dist/umd/popper.min.js" integrity="sha384-eMNCOe7tC1doHpGoWe/6oMVemdAVTMs2xqW4mwXrXsW0L84Iytr2wi5v2QjrP/xp" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/js/bootstrap.min.js" integrity="sha384-cn7l7gDp0eyniUwwAZgrzD06kc/tftFf19TOAs2zVinnD/C7E91j9yyk5//jjpt/" crossorigin="anonymous"></script>

    <script>
        $(document).ready(activateQuery())

        function activateQuery() {
            
        }

        var retina = window.devicePixelRatio,

        // Math shorthands
        PI = Math.PI,
        sqrt = Math.sqrt,
        round = Math.round,
        random = Math.random,
        cos = Math.cos,
        sin = Math.sin,

        // Local WindowAnimationTiming interface
        rAF = window.requestAnimationFrame,
        cAF = window.cancelAnimationFrame || window.cancelRequestAnimationFrame,
        _now = Date.now || function () {return new Date().getTime();};

    // Local WindowAnimationTiming interface polyfill
    (function (w) {
      /**
            * Fallback implementation.
            */
      var prev = _now();
      function fallback(fn) {
        var curr = _now();
        var ms = Math.max(0, 16 - (curr - prev));
        var req = setTimeout(fn, ms);
        prev = curr;
        return req;
      }

      /**
            * Cancel.
            */
      var cancel = w.cancelAnimationFrame
      || w.webkitCancelAnimationFrame
      || w.clearTimeout;

      rAF = w.requestAnimationFrame
      || w.webkitRequestAnimationFrame
      || fallback;

      cAF = function(id){
        cancel.call(w, id);
      };
    }(window));

    document.addEventListener("DOMContentLoaded", function() {
      var speed = 50,
          duration = (1.0 / speed),
          confettiRibbonCount = 11,
          ribbonPaperCount = 30,
          ribbonPaperDist = 8.0,
          ribbonPaperThick = 8.0,
          confettiPaperCount = 95,
          DEG_TO_RAD = PI / 180,
          RAD_TO_DEG = 180 / PI,
          colors = [
            ["#df0049", "#660671"],
            ["#00e857", "#005291"],
            ["#2bebbc", "#05798a"],
            ["#ffd200", "#b06c00"]
          ];

      function Vector2(_x, _y) {
        this.x = _x, this.y = _y;
        this.Length = function() {
          return sqrt(this.SqrLength());
        }
        this.SqrLength = function() {
          return this.x * this.x + this.y * this.y;
        }
        this.Add = function(_vec) {
          this.x += _vec.x;
          this.y += _vec.y;
        }
        this.Sub = function(_vec) {
          this.x -= _vec.x;
          this.y -= _vec.y;
        }
        this.Div = function(_f) {
          this.x /= _f;
          this.y /= _f;
        }
        this.Mul = function(_f) {
          this.x *= _f;
          this.y *= _f;
        }
        this.Normalize = function() {
          var sqrLen = this.SqrLength();
          if (sqrLen != 0) {
            var factor = 1.0 / sqrt(sqrLen);
            this.x *= factor;
            this.y *= factor;
          }
        }
        this.Normalized = function() {
          var sqrLen = this.SqrLength();
          if (sqrLen != 0) {
            var factor = 1.0 / sqrt(sqrLen);
            return new Vector2(this.x * factor, this.y * factor);
          }
          return new Vector2(0, 0);
        }
      }
      Vector2.Lerp = function(_vec0, _vec1, _t) {
        return new Vector2((_vec1.x - _vec0.x) * _t + _vec0.x, (_vec1.y - _vec0.y) * _t + _vec0.y);
      }
      Vector2.Distance = function(_vec0, _vec1) {
        return sqrt(Vector2.SqrDistance(_vec0, _vec1));
      }
      Vector2.SqrDistance = function(_vec0, _vec1) {
        var x = _vec0.x - _vec1.x;
        var y = _vec0.y - _vec1.y;
        return (x * x + y * y + z * z);
      }
      Vector2.Scale = function(_vec0, _vec1) {
        return new Vector2(_vec0.x * _vec1.x, _vec0.y * _vec1.y);
      }
      Vector2.Min = function(_vec0, _vec1) {
        return new Vector2(Math.min(_vec0.x, _vec1.x), Math.min(_vec0.y, _vec1.y));
      }
      Vector2.Max = function(_vec0, _vec1) {
        return new Vector2(Math.max(_vec0.x, _vec1.x), Math.max(_vec0.y, _vec1.y));
      }
      Vector2.ClampMagnitude = function(_vec0, _len) {
        var vecNorm = _vec0.Normalized;
        return new Vector2(vecNorm.x * _len, vecNorm.y * _len);
      }
      Vector2.Sub = function(_vec0, _vec1) {
        return new Vector2(_vec0.x - _vec1.x, _vec0.y - _vec1.y, _vec0.z - _vec1.z);
      }

      function EulerMass(_x, _y, _mass, _drag) {
        this.position = new Vector2(_x, _y);
        this.mass = _mass;
        this.drag = _drag;
        this.force = new Vector2(0, 0);
        this.velocity = new Vector2(0, 0);
        this.AddForce = function(_f) {
          this.force.Add(_f);
        }
        this.Integrate = function(_dt) {
          var acc = this.CurrentForce(this.position);
          acc.Div(this.mass);
          var posDelta = new Vector2(this.velocity.x, this.velocity.y);
          posDelta.Mul(_dt);
          this.position.Add(posDelta);
          acc.Mul(_dt);
          this.velocity.Add(acc);
          this.force = new Vector2(0, 0);
        }
        this.CurrentForce = function(_pos, _vel) {
          var totalForce = new Vector2(this.force.x, this.force.y);
          var speed = this.velocity.Length();
          var dragVel = new Vector2(this.velocity.x, this.velocity.y);
          dragVel.Mul(this.drag * this.mass * speed);
          totalForce.Sub(dragVel);
          return totalForce;
        }
      }

      function ConfettiPaper(_x, _y) {
        this.pos = new Vector2(_x, _y);
        this.rotationSpeed = (random() * 600 + 800);
        this.angle = DEG_TO_RAD * random() * 360;
        this.rotation = DEG_TO_RAD * random() * 360;
        this.cosA = 1.0;
        this.size = 5.0;
        this.oscillationSpeed = (random() * 1.5 + 0.5);
        this.xSpeed = 40.0;
        this.ySpeed = (random() * 60 + 50.0);
        this.corners = new Array();
        this.time = random();
        var ci = round(random() * (colors.length - 1));
        this.frontColor = colors[ci][0];
        this.backColor = colors[ci][1];
        for (var i = 0; i < 4; i++) {
          var dx = cos(this.angle + DEG_TO_RAD * (i * 90 + 45));
          var dy = sin(this.angle + DEG_TO_RAD * (i * 90 + 45));
          this.corners[i] = new Vector2(dx, dy);
        }
        this.Update = function(_dt) {
          this.time += _dt;
          this.rotation += this.rotationSpeed * _dt;
          this.cosA = cos(DEG_TO_RAD * this.rotation);
          this.pos.x += cos(this.time * this.oscillationSpeed) * this.xSpeed * _dt
          this.pos.y += this.ySpeed * _dt;
          if (this.pos.y > ConfettiPaper.bounds.y) {
            this.pos.x = random() * ConfettiPaper.bounds.x;
            this.pos.y = 0;
          }
        }
        this.Draw = function(_g) {
          if (this.cosA > 0) {
            _g.fillStyle = this.frontColor;
          } else {
            _g.fillStyle = this.backColor;
          }
          _g.beginPath();
          _g.moveTo((this.pos.x + this.corners[0].x * this.size) * retina, (this.pos.y + this.corners[0].y * this.size * this.cosA) * retina);
          for (var i = 1; i < 4; i++) {
            _g.lineTo((this.pos.x + this.corners[i].x * this.size) * retina, (this.pos.y + this.corners[i].y * this.size * this.cosA) * retina);
          }
          _g.closePath();
          _g.fill();
        }
      }
      ConfettiPaper.bounds = new Vector2(0, 0);

      function ConfettiRibbon(_x, _y, _count, _dist, _thickness, _angle, _mass, _drag) {
        this.particleDist = _dist;
        this.particleCount = _count;
        this.particleMass = _mass;
        this.particleDrag = _drag;
        this.particles = new Array();
        var ci = round(random() * (colors.length - 1));
        this.frontColor = colors[ci][0];
        this.backColor = colors[ci][1];
        this.xOff = (cos(DEG_TO_RAD * _angle) * _thickness);
        this.yOff = (sin(DEG_TO_RAD * _angle) * _thickness);
        this.position = new Vector2(_x, _y);
        this.prevPosition = new Vector2(_x, _y);
        this.velocityInherit = (random() * 2 + 4);
        this.time = random() * 100;
        this.oscillationSpeed = (random() * 2 + 2);
        this.oscillationDistance = (random() * 40 + 40);
        this.ySpeed = (random() * 40 + 80);
        for (var i = 0; i < this.particleCount; i++) {
          this.particles[i] = new EulerMass(_x, _y - i * this.particleDist, this.particleMass, this.particleDrag);
        }
        this.Update = function(_dt) {
          var i = 0;
          this.time += _dt * this.oscillationSpeed;
          this.position.y += this.ySpeed * _dt;
          this.position.x += cos(this.time) * this.oscillationDistance * _dt;
          this.particles[0].position = this.position;
          var dX = this.prevPosition.x - this.position.x;
          var dY = this.prevPosition.y - this.position.y;
          var delta = sqrt(dX * dX + dY * dY);
          this.prevPosition = new Vector2(this.position.x, this.position.y);
          for (i = 1; i < this.particleCount; i++) {
            var dirP = Vector2.Sub(this.particles[i - 1].position, this.particles[i].position);
            dirP.Normalize();
            dirP.Mul((delta / _dt) * this.velocityInherit);
            this.particles[i].AddForce(dirP);
          }
          for (i = 1; i < this.particleCount; i++) {
            this.particles[i].Integrate(_dt);
          }
          for (i = 1; i < this.particleCount; i++) {
            var rp2 = new Vector2(this.particles[i].position.x, this.particles[i].position.y);
            rp2.Sub(this.particles[i - 1].position);
            rp2.Normalize();
            rp2.Mul(this.particleDist);
            rp2.Add(this.particles[i - 1].position);
            this.particles[i].position = rp2;
          }
          if (this.position.y > ConfettiRibbon.bounds.y + this.particleDist * this.particleCount) {
            this.Reset();
          }
        }
        this.Reset = function() {
          this.position.y = -random() * ConfettiRibbon.bounds.y;
          this.position.x = random() * ConfettiRibbon.bounds.x;
          this.prevPosition = new Vector2(this.position.x, this.position.y);
          this.velocityInherit = random() * 2 + 4;
          this.time = random() * 100;
          this.oscillationSpeed = random() * 2.0 + 1.5;
          this.oscillationDistance = (random() * 40 + 40);
          this.ySpeed = random() * 40 + 80;
          var ci = round(random() * (colors.length - 1));
          this.frontColor = colors[ci][0];
          this.backColor = colors[ci][1];
          this.particles = new Array();
          for (var i = 0; i < this.particleCount; i++) {
            this.particles[i] = new EulerMass(this.position.x, this.position.y - i * this.particleDist, this.particleMass, this.particleDrag);
          }
        }
        this.Draw = function(_g) {
          for (var i = 0; i < this.particleCount - 1; i++) {
            var p0 = new Vector2(this.particles[i].position.x + this.xOff, this.particles[i].position.y + this.yOff);
            var p1 = new Vector2(this.particles[i + 1].position.x + this.xOff, this.particles[i + 1].position.y + this.yOff);
            if (this.Side(this.particles[i].position.x, this.particles[i].position.y, this.particles[i + 1].position.x, this.particles[i + 1].position.y, p1.x, p1.y) < 0) {
              _g.fillStyle = this.frontColor;
              _g.strokeStyle = this.frontColor;
            } else {
              _g.fillStyle = this.backColor;
              _g.strokeStyle = this.backColor;
            }
            if (i == 0) {
              _g.beginPath();
              _g.moveTo(this.particles[i].position.x * retina, this.particles[i].position.y * retina);
              _g.lineTo(this.particles[i + 1].position.x * retina, this.particles[i + 1].position.y * retina);
              _g.lineTo(((this.particles[i + 1].position.x + p1.x) * 0.5) * retina, ((this.particles[i + 1].position.y + p1.y) * 0.5) * retina);
              _g.closePath();
              _g.stroke();
              _g.fill();
              _g.beginPath();
              _g.moveTo(p1.x * retina, p1.y * retina);
              _g.lineTo(p0.x * retina, p0.y * retina);
              _g.lineTo(((this.particles[i + 1].position.x + p1.x) * 0.5) * retina, ((this.particles[i + 1].position.y + p1.y) * 0.5) * retina);
              _g.closePath();
              _g.stroke();
              _g.fill();
            } else if (i == this.particleCount - 2) {
              _g.beginPath();
              _g.moveTo(this.particles[i].position.x * retina, this.particles[i].position.y * retina);
              _g.lineTo(this.particles[i + 1].position.x * retina, this.particles[i + 1].position.y * retina);
              _g.lineTo(((this.particles[i].position.x + p0.x) * 0.5) * retina, ((this.particles[i].position.y + p0.y) * 0.5) * retina);
              _g.closePath();
              _g.stroke();
              _g.fill();
              _g.beginPath();
              _g.moveTo(p1.x * retina, p1.y * retina);
              _g.lineTo(p0.x * retina, p0.y * retina);
              _g.lineTo(((this.particles[i].position.x + p0.x) * 0.5) * retina, ((this.particles[i].position.y + p0.y) * 0.5) * retina);
              _g.closePath();
              _g.stroke();
              _g.fill();
            } else {
              _g.beginPath();
              _g.moveTo(this.particles[i].position.x * retina, this.particles[i].position.y * retina);
              _g.lineTo(this.particles[i + 1].position.x * retina, this.particles[i + 1].position.y * retina);
              _g.lineTo(p1.x * retina, p1.y * retina);
              _g.lineTo(p0.x * retina, p0.y * retina);
              _g.closePath();
              _g.stroke();
              _g.fill();
            }
          }
        }
        this.Side = function(x1, y1, x2, y2, x3, y3) {
          return ((x1 - x2) * (y3 - y2) - (y1 - y2) * (x3 - x2));
        }
      }
      ConfettiRibbon.bounds = new Vector2(0, 0);
      confetti = {};
      confetti.Context = function(id) {
        var i = 0;
        var canvas = document.getElementById(id);
        var canvasParent = canvas.parentNode;
        var canvasWidth = canvasParent.offsetWidth;
        var canvasHeight = canvasParent.offsetHeight;
        canvas.width = canvasWidth * retina;
        canvas.height = canvasHeight * retina;
        var context = canvas.getContext('2d');
        var interval = null;
        var confettiRibbons = new Array();
        ConfettiRibbon.bounds = new Vector2(canvasWidth, canvasHeight);
        for (i = 0; i < confettiRibbonCount; i++) {
          confettiRibbons[i] = new ConfettiRibbon(random() * canvasWidth, -random() * canvasHeight * 2, ribbonPaperCount, ribbonPaperDist, ribbonPaperThick, 45, 1, 0.05);
        }
        var confettiPapers = new Array();
        ConfettiPaper.bounds = new Vector2(canvasWidth, canvasHeight);
        for (i = 0; i < confettiPaperCount; i++) {
          confettiPapers[i] = new ConfettiPaper(random() * canvasWidth, random() * canvasHeight);
        }
        this.resize = function() {
          canvasWidth = canvasParent.offsetWidth;
          canvasHeight = canvasParent.offsetHeight;
          canvas.width = canvasWidth * retina;
          canvas.height = canvasHeight * retina;
          ConfettiPaper.bounds = new Vector2(canvasWidth, canvasHeight);
          ConfettiRibbon.bounds = new Vector2(canvasWidth, canvasHeight);
        }
        this.start = function() {
          this.stop()
          var context = this;
          this.update();
        }
        this.stop = function() {
          cAF(this.interval);
        }
        this.update = function() {
          var i = 0;
          context.clearRect(0, 0, canvas.width, canvas.height);
          for (i = 0; i < confettiPaperCount; i++) {
            confettiPapers[i].Update(duration);
            confettiPapers[i].Draw(context);
          }
          for (i = 0; i < confettiRibbonCount; i++) {
            confettiRibbons[i].Update(duration);
            confettiRibbons[i].Draw(context);
          }
          this.interval = rAF(function() {
            confetti.update();
          });
        }
      }
      var confetti = new confetti.Context('confetti');
      confetti.start();
      window.addEventListener('resize', function(event){
        confetti.resize();
      });
    });
    </script>
</body>
</html>