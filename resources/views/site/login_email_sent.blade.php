@extends('base')  
@push('styles')
    <style>

    </style>
@endpush
@section('content')
<div class="d-flex flex-grow-1 w-100 h-100">
    <div class="signin wrapper">
        <div class="d-flex flex-column flex-lg-row justify-content-center flex-grow-1">
            <div class="img-banner d-flex">
                <img class="banner img-fluid" src="images/banner.png" alt="...">
            </div>
            <div class="signin-form d-flex flex-column">
                <div class="d-flex flex-column flex-grow-1 justify-content-center mb-5">
                    <div class="text-center">
                        <img class="mb-4" src="images/img-mailbox.png" width="144" alt="">
                        <h2>Check your mailbox!</h2>
                        <p class="mb-4">We sent an email to <span class="font-weight-bold email-label">[email]</span>. You’ll find a link that will sign you into Kidlat Awards.</p>
                        <p>This may take up to 5 minutes, or the message may have been sent to your spam folder</p>
                    </div>
                    <div class="text-center" style="margin: 30px auto">
                        <a href="/login">
                            <button type="button" class="btn btn-primary primary lg btn_back text-uppercase" style="min-width: 202px">Back to home</button>
                        </a>
                    </div>
                    <div class="text-center">
                        <p class="parag">Entered a wrong email address?</p>
                        <a href="/login" class="font-weight-bold link-signin">Sign in again</a>
                    </div>
                </div>
                <div class="signin-form-footer justify-content-end">
                    <h3 class="title">Need help? Contact 4As</h3>
                    <ul class="info">
                        <li>
                            <img src="images/email.png" alt="" />  
                            <a href="mailto:4asp@pldtdsl.net">4asp@pldtdsl.net</a>
                        </li>
                        <li>
                            <img src="images/call.png" alt="" />
                            <a href="#">8893-1205</a>
                        </li>
                        <li>
                            <img src="images/mobile-phone.png" alt="" />
                            <a href="#">0917 5222 427</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
@push('custom-js')
    <script>
        let email = localStorage.getItem('email')
        $('.email-label').text(email)

        if (!localStorage.hasOwnProperty('email')) {
            location.href = "/login"
        }

        $(document).on('click', '.btn_back', function() {
            localStorage.removeItem('email')
            setTimeout(function() {
                location.href = "/login"
            },1000)
        })
    </script>
@endpush
@endsection