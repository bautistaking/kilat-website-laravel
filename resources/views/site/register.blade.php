@extends('base')  
@push('styles')
    <style>
        .spinner-grow,
        .loading-label {
            display: none;
        }

    </style>
@endpush
@section('content')

<div class="d-flex flex-grow-1 w-100 h-100">
    <div class="signin wrapper">
        <div class="d-flex flex-column flex-lg-row justify-content-center flex-grow-1">
            <div class="img-banner d-flex">
                <img class="banner img-fluid" src="images/banner.png" alt="...">
            </div>
            <div class="signin-form d-flex flex-column">
                <div class="d-flex align-items-center justify-content-between mb-3">
                    <a href="https://www.kidlatawards.com/" class="top-action">
                        <i class="bi bi-chevron-left me-2"></i><span style="color:#1a2735;">Back</span>
                    </a>
                </div>
                <div class="d-flex flex-column flex-grow-1 justify-content-center mb-5">
                    <div class="text-center header-wrapper">
                        <h2>Register with just your email</h2>
                        <p>We’ll send you a link that you can use to register</p>
                    </div>
                    <div class="form">
                        <input type="hidden" name="signin" value="0">
                       <!--  <div class="form-group mb-3">
                            <label for="agency">Agency Name</label>
                            <input type="text" class="form-control lg" id="signup_agency" name="agency" placeholder="Agency name">
                        </div> -->
                        <div class="form-group">
                            <label for="email" class="mb-2">Email address</label>
                            <input type="email" class="form-control lg" id="signup_email" name="email" placeholder="name@email.com">
                        </div>
                        <div id="invalid-email" class="invalid-feedback"></div>
                        <div class="text-center mt-46">
                            <button class="btn btn-primary primary lg mb-4" id="btn_signup">
                                <span class="spinner-grow spinner-grow-sm" role="status"></span>
                                <span class="spinner-grow spinner-grow-sm" role="status"></span>
                                <span class="spinner-grow spinner-grow-sm" role="status"></span>
                                <span class="signup-label text-uppercase">Send sign-up link</span>
                            </button>
                            <p>Already have an account?</p>
                            <a href="javascript:void(0)" class="link-color" id="btn_signin">Sign in</a>
                        </div>
                    </div>
                </div>
                <div class="signin-form-footer justify-content-end">
                    <h3 class="title">Need help? Contact 4As</h3>
                    <ul class="info">
                        <li>
                            <img src="images/email.png" alt="" />
                            <a href="mailto:4asp@pldtdsl.net">4asp@pldtdsl.net</a>
                        </li>
                        <li>
                            <img src="images/call.png" alt="" />
                            <a href="#">8893-1205</a>
                        </li>
                        <li>
                            <img src="images/mobile-phone.png" alt="" />
                            <a href="#">0917 5222 427</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div> 
</div>
@push('custom-js')
<script>
    $(document).ready(activateQuery())

    function activateQuery() {
        $('#btn_signup').on('click', function() {

            $.LoadingOverlay("show", {
                image: 'images/loading1.png'
            },100);

            $('.signup-label').hide()
            $('.spinner-grow').addClass('d-inline-block')

            let params = {
                '_token': '{{ csrf_token() }}',
                // 'name'  : $('#signup_agency').val(),
                'email' : $('#signup_email').val()
            }

            fetch("/register", {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify(params)
            })
            .then(response => response.json())
            .then((e) => {
                let html = ""

                $('#signup_email').removeClass('is-invalid')
                $('#invalid-email').hide()

                if (e.status_code == 400 || e.status_code == 409) {
                    $.LoadingOverlay("hide");

                    $.each(e.message, function(a,b) {
                        if (e.header == "Duplicate") {
                            html += "Email address already registered. Are you trying to <a href='/login'>sign in</a>?"
                        } else {
                            html += b
                        }
                    })

                    $('#invalid-email').html(html).show()
                    $('#signup_email').addClass('is-invalid')

                    // Swal.fire({
                    //     icon: 'error',
                    //     title: 'Oops...',
                    //     html: html,
                    //     confirmButtonText: 'Close',
                    //     confirmButonColor: 'btn-primary'
                    // })

                    $('.signup-label').show()
                    $('.spinner-grow').removeClass('d-inline-block')
                }

                if (e.status_code == 200) {
                    $.LoadingOverlay("hide");

                    $.each(e.message, function(a,b) {
                        html += "<ul class='list-group'>"
                        html += "<li class='list-group-item'>"+b+"</li>"
                        html += "</ul>"
                    })

                    // Swal.fire({
                    //     icon: 'success',
                    //     title: e.header,
                    //     html: html,
                    //     confirmButtonText: 'Close',
                    //     confirmButonColor: 'btn-primary'
                    // })

                    localStorage.setItem("email", params.email)
                    setTimeout(function() {
                        location.href = "/register-email-sent"
                    }, 100)
                }
            })
            .catch((error) => {
            })
        })

        $('#btn_signin').on('click', function() {
            location.href = "/login"
        })

    }
</script>
@endpush
@endsection