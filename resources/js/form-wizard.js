$(document).ready(function () {
    let stepper = document.querySelector('#stepper');
    let stepperInstace = new MStepper(stepper, {
        // options
        firstActive: 0, // this is the default
        // Allow navigation by clicking on the next and previous steps on linear steppers.
        stepsNavigation: false,
    });

    $(".flatpickr").flatpickr();

    if ($(window).width() < 700) {
        Swal.fire({
            type: 'warning',
            title: "Ooops! Uploading large file sizes works best on Desktop",
            text: "Access enter.kidlatawards.com.ph on desktop for a better experience completing your submission."
        });
    }

    $('#stepper').on('steperror', function () {
        Swal.fire({
            imageUrl: "/images/Error.svg",
            imageWidth: 100,
            imageHeight: 124,
            title: "Ooops! Looks like you missed something important.",
            text: "Kindly fill up all the required fields. Thank you!"
        });
    });

    $('#stepper').on('stepchange', function () {
        resizeStepper();
        let active = $(".stepper .active").index();
        if (active == 3) {
            $('.label-credits-container').empty();
            $("input:text[name='credit_name[]']").each(function (index) {
                let name = $(this).val();
                let role = $("input:text[name='credit_role[]']").eq(index).val();
                if (name != '') {
                    $('.label-credits-container').append(`
                      <div class="row">
                        <div class="col s6">
                          <p>` + name + `</p>
                        </div>
                        <div class="col">
                          <p>` + role + `</p>
                        </div>
                      </div>`);
                }
            });
        }
        if (active == 5) {
            $('.label-key-requirements').empty();
            $('.label-supporting-requirements').empty();
            $('.label-reference').empty();
            $("input:text[name='files_name[]']").each(function (index) {
                let name = $(this).val();
                let type = $("input:hidden[name='file_type[]']").eq(index).val();
                if (name != '') {
                    if (type == 'key') {
                        $('.label-key-requirements').append(`
                          <div class="row">
                            <div class="col s12">
                              <label>` + name + `</label>
                            </div>
                          </div>`);
                    } else {
                        $('.label-supporting-requirements').append(`
                          <div class="row">
                            <div class="col s12">
                              <label>` + name + `</label>
                            </div>
                          </div>`);
                    }
                }
            });
            $("input:text[name='reference[]']").each(function (index) {
                let link = $(this).val();
                if (link != '') {
                    $('.label-reference').append(`
                      <div class="row">
                        <div class="col s12">
                          <label><a href=` + link + ` target="_blank" class="pointer">` + link + `</a></label>
                        </div>
                      </div>`);
                }
            });
        }
    });

    $(document).on('click', '.delete-node', function () {
        $(this).parent().remove();
    });

    $(document).on('change', 'form input', function () {
        let me = $(this);
        let name = me.attr("name");
        let val = me.val();
        if (name != 'credit_name[]' &&
            name != 'credit_role[]' &&
            name != 'files[]' &&
            name != 'files_name[]' &&
            name != 'client_cert' &&
            name != 'media' &&
            name != 'asc_cert') {
            $(".label__" + name).text(val);
        }
        if (name == 'sub_category') {
            let catID = me.data('category-id');
            $(".label__category").text(me.data('category-name'));
            $(".label__sub_category").text(me.data('sub-name'));
            $('input[name="category"]').val(catID);
            $('.guidelines div').addClass('hidden');
            $('.cat-' + catID).removeClass('hidden');
        }
        if (name == 'files[]') {
            let filesize = 0;
            $('input[name="files[]"]').each(function () {
                if ($(this).get(0).files.length > 0) {
                    filesize += $(this)[0].files[0].size;
                }
                console.log(filesize);
                if (filesize > 1073741824) { // 1GB
                    Swal.fire({
                        type: 'warning',
                        title: "Ooops! Failed attaching file",
                        text: "You have exceeded the 1GB Filesize limit."
                    });
                    me.val('');
                    me.parent().next().find('input[name="files_name[]"]').val('');
                }
            });
        }
    });

    $('.credits__add-more').on('click', function () {
        if ($('.credits-container').children().length < 20) {
            $('.credits-container').append(`
        <div class="row">
          <div class="col field s6 inline">
            <div class="input-field w-100">
              <input type="text" name="credit_name[]" class="validate">
            </div>
          </div>
          <div class="col field s6 inline">
            <div class="input-field w-100">
              <input type="text" name="credit_role[]" class="validate">
            </div>
          </div>
          <a class="btn-floating btn-large waves-effect waves-light red delete-node"><i class="material-icons">delete</i></a>
        </div>
      `);
        } else {
            $(document).ready(function () {
                Swal.fire({
                    imageUrl: "/images/Error.svg",
                    imageWidth: 100,
                    imageHeight: 124,
                    title: "Oops!",
                    text: "You already reached the limit."
                });
            });
        }
    });

    $('.key-files__add-more').on('click', function () {
        if ($('.key-files-container').children().length < 10) {
            $('.key-files-container').append(`
        <div class="file-field input-field">
          <div class="btn pink">
            <span>Click to Attach File</span>
            <input type="hidden" name="pre_file[]" value="no">
            <input type="hidden" name="f_id[]" value="">
            <input type="hidden" name="file_type[]" value="key">
            <input class="test" type="file" name="files[]" accept=".doc,.docx,application/msword,application/vnd.openxmlformats-officedocument.wordprocessingml.document,.jpg,.jpeg,.png,.pdf,.mp3,.mp4,.avi,.flv,.wmv,.mov">
          </div>
          <div class="file-path-wrapper">
            <input class="file-path validate" name="files_name[]" type="text">
          </div>
          <a class="btn-floating btn-large waves-effect waves-light red delete-node"><i class="material-icons">delete</i></a>
        </div>
      `);
        } else {
            $(document).ready(function () {
                Swal.fire({
                    imageUrl: "/images/Error.svg",
                    imageWidth: 100,
                    imageHeight: 124,
                    title: "Oops!",
                    text: "You already reached the limit."
                });
            });
        }
    });

    $('.supporting-files__add-more').on('click', function () {
        if ($('.supporting-files-container').children().length < 3) {
            $('.supporting-files-container').append(`
        <div class="file-field input-field">
          <div class="btn pink">
            <span>Click to Attach File</span>
            <input type="hidden" name="pre_file[]" value="no">
            <input type="hidden" name="f_id[]" value="">
            <input type="hidden" name="file_type[]" value="supporting">
            <input type="file" name="files[]" accept=".doc,.docx,application/msword,application/vnd.openxmlformats-officedocument.wordprocessingml.document,.jpg,.jpeg,.png,.pdf">
          </div>
          <div class="file-path-wrapper">
            <input class="file-path validate" name="files_name[]" type="text">
          </div>
          <a class="btn-floating btn-large waves-effect waves-light red delete-node"><i class="material-icons">delete</i></a>
        </div>
      `);
        } else {
            $(document).ready(function () {
                Swal.fire({
                    imageUrl: "/images/Error.svg",
                    imageWidth: 100,
                    imageHeight: 124,
                    title: "Oops!",
                    text: "You already reached the limit."
                });
            });
        }
    });

    $('.reference__add-more').on('click', function () {
        if ($('.reference-container').children().length < 5) {
            $('.reference-container').append(`
        <div class="file-field input-field">
          <div class="input-field">
            <input type="text" name="reference[]">
          </div>
          <a class="btn-floating btn-large waves-effect waves-light red delete-node"><i class="material-icons">delete</i></a>
        </div>
      `);
        } else {
            $(document).ready(function () {
                Swal.fire({
                    imageUrl: "/images/Error.svg",
                    imageWidth: 100,
                    imageHeight: 124,
                    title: "Oops!",
                    text: "You already reached the limit."
                });
            });
        }
    });


    $('.edit-step').on('click', function () {
        let step = $(this).data('step');
        stepperInstace.openStep(step, stepper);
    });

    $(".search-category").on("keyup", function () {
        let value = $(this).val().toLowerCase();
        $("input[name='sub_category']").filter(function () {
            $(this).next().toggle($(this).next().text().toLowerCase().indexOf(value) > -1)
        });
    });


    function resizeStepper() {
        newHeight = 0;
        padding = 150;

        $('#stepper').find('.step.active').find('.step-content > div').each(function () {
            newHeight += parseInt($(this).css('height'));
        });

        newHeight += padding;

        $('#stepper').animate({
                height: newHeight
            },
            300);
    }
    resizeStepper();
});
