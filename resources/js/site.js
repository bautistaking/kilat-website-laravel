window.$ = window.jQuery = require('jquery');
// require('materialize-css');
window.flatpickr = require("flatpickr");
require('materialize-css/dist/js/materialize.min.js');
window.Swal = require('sweetalert2');

document.addEventListener('DOMContentLoaded', function () {
    let nav = document.querySelectorAll('.sidenav');
    let navInstances = M.Sidenav.init(nav);

    let modals = document.querySelectorAll('.modal');
    let modalsInstances = M.Modal.init(modals);

    let dropdown = document.querySelectorAll('.dropdown-trigger');
    let dropdown2 = document.querySelectorAll('.dropdown2-trigger');
    let ddOptions = { hover: true, constrainWidth: false, gutter: 0, belowOrigin: false };
    let dropdownInstances = M.Dropdown.init(dropdown);
    let dropdownInstances2 = M.Dropdown.init(dropdown2,ddOptions);

    let select = document.querySelectorAll('select');
    let selectInstances = M.FormSelect.init(select);

    $('.invoice-request').click(function () {
        $('.default-actions').addClass('hidden');
        $('.request-actions').removeClass('hidden');
        $('.uid-cb').removeClass('hidden');
        $(".confirm-request").attr("disabled", true);
    });

    $('.nav-signin').click(function () {
        $('.sign-in').removeClass('hidden');
        $('.sign-up').addClass('hidden');
    });

    $('.nav-signup').click(function () {
        $('.sign-in').addClass('hidden');
        $('.sign-up').removeClass('hidden');
    });

    $('.cancel-request').click(function () {
        $('.default-actions').removeClass('hidden');
        $('.request-actions').addClass('hidden');
        $(".uid-cb input:checkbox").prop('checked', false);
        $('.uid-cb').addClass('hidden');
    });

    $(document).on('submit', 'form:not(.prevent-loading)', function () {
        $('.loading-section').removeClass('hidden');
    });

    $(document).on('change', '.uid-cb', function () {
        let len = $(".uid-cb > [type='checkbox']:checked:not(:disabled)").length;
        console.log(len);
        if (len > 0) {
            $(".confirm-request").attr("disabled", false);
        } else {
            $(".confirm-request").attr("disabled", true);
        }

    });

    $('.submit').click(function(){
        const form = $(this).next()
        Swal.fire({
            title: "Are you sure you want to proceed with this action?",
            imageUrl: "/images/question.svg",
            imageWidth: 100,
            imageHeight: 100,
            showCancelButton: true,}).
          then((confirm) => {
            if(confirm.value === true)  {
              form.submit();
            } else {
              return false;
            }
        });
    });

    $('.submit-score').click(function(){
        const form = $(this).next()
        Swal.fire({
            title: "Are you sure you submit these scores?",
            imageUrl: "/images/question.svg",
            imageWidth: 100,
            imageHeight: 100,
            showCancelButton: true,}).
          then((confirm) => {
            if(confirm.value === true)  {
              form.submit();
            } else {
              return false;
            }
        });
    });

    $('.files').click(function(){
        const source = $(this).data('url');
        const type = $(this).data('type');
        let append;
        switch (type) {
            case "Image":
                append = `
                        <img src="` + source + `" class="mw-100 mh-800px">
                        <div class="font -xs-small">
                            <a href="` + source + `" class="preview-link" target="_blank"><i>*Click here to view / download the file if the preview is not working properly</i></a>
                        </div>`;
                break;
            case "Video":
                append = `
                        <video controls class="mw-100 mh-800px">
                            <source src="` + source + `" type="video/mp4">
                        </video>
                        <div class="font -xs-small">
                            <a href="` + source + `" class="preview-link" target="_blank"><i>*Click here to view / download the file if the preview is not working properly</i></a>
                        </div>`;
                break;
            case "Audio":
                append = `
                        <audio controls class="mw-100 mh-800px">
                            <source src="` + source + `" type="audio/mpeg">
                        </audio>
                        <div class="font -xs-small">
                            <a href="` + source + `" class="preview-link" target="_blank"><i>*Click here to view / download the file if the preview is not working properly</i></a>
                        </div>`;
                break;
            default:
                append = `
                        <div class="no-preview">
                            <div>
                            <i class="material-icons">broken_image</i> <br>
                            No Preview Available
                            </div>
                        </div>
                        <div class="font -xs-small">
                            <a href="` + source + `" class="preview-link" target="_blank"><i>*Click here to view / download the file if the preview is not working properly</i></a>
                        </div>`;
                break;
        }
        $('.main-file').html(append);
    });



    $('.confirm-request').click(function () {
        let ids = [];
        $('.uid-cb input:checkbox:checked:not(:disabled)').each(function () {
            ids.push($(this).val());
        });
        $('.loading-section').removeClass('hidden');
        $.ajax({
            type: "POST",
            url: '/requestInvoice',
            headers: {
                'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
            },
            data: {
                ids: ids,
            },
            success: function (data) {
                if (data.status == true) {
                    $('.loading-section').addClass('hidden');
                    Swal.fire({
                        title: "Almost there! Your selected entries are now pending payment.",
                        text: data.name + ", check your e-mail for payment instructions. After payment, 4AS Admin will verify your submission before counting it as a valid entry.",
                        allowOutsideClick: false,
                        type: "success"
                    }).then(function () {
                        location.reload();
                    });
                } else {
                    $('.loading-section').addClass('hidden');
                    $(document).ready(function () {
                        Swal.fire({
                            imageUrl: "/images/Error.svg",
                            imageWidth: 100,
                            imageHeight: 124,
                            title: "Oops!",
                            text: "Something went wrong. Please try again."
                        });
                    });
                }
            }
        });
    });
});