$(document).ready(function () {
  $('.dt').DataTable({
		"aaSorting": []
	});

  $('.entries-filter').on('change', function() {
		const url = $(this).find(':selected').data('url');
		if (url) {
			window.location = url;
		}
		return false;
	});
});