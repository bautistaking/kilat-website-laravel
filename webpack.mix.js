const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.options({ uglify: { compress: true }});
mix.disableSuccessNotifications();
mix.js('resources/js/site.js', 'public/js')
   .js('resources/js/form-wizard.js', 'public/js')
   .js('resources/js/admin.js', 'public/admin/js')
   .sass('resources/sass/site.scss', 'public/css')
   .sass('resources/sass/admin.scss', 'public/admin/css')
   .sass('resources/sass/vendor.scss', 'public/css').version();
