<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
// New Route
Route::get('/', 'Site\HomeController@index');
Route::get('/register', 'Site\UserController@getRegister');
Route::post('/register', 'Site\UserController@postRegister');
Route::get('/register-email-sent', 'Site\UserController@registerEmailSent');

Route::get('/login', 'Site\UserController@getLogin');
Route::post('/login', 'Site\UserController@postLogin');
Route::get('/login-email-sent', 'Site\UserController@loginEmailSent');
// End New Route


// Route::get('/home/{modal?}', 'Site\HomeController@index');
// Route::get('error', ['as' => 'error', 'uses' => function () {
//     return view('error.error');
// }]);

// Route::post('/signUp', 'Site\UserController@signUp');
Route::get('/authenticate/{token}/{email}', 'Site\UserController@authenticate');
Route::get('/logout', 'Site\UserController@logout');

Route::get('/admin/login', 'Admin\EntryController@adminLogin');

// Route::get('admin/login', function () {
//     if (!Auth::check()) {
//         return view('admin.login');
//     } else {
//         return redirect('/admin/entries');
//     }
// });

Route::post('admin/login/attempt', 'Admin\EntryController@login');


Route::group(['middleware' => ['auth']], function () {

    Route::get('/dashboard', 'Site\EntryController@dashboard');
    Route::post('/user/agreePolicy', 'Site\UserController@agreePolicy');

    Route::group(['middleware' => ['time']], function () {
        Route::post('/entry/clone', 'Site\EntryController@clone');
        Route::get('/entry/edit/{uid}', 'Site\EntryController@editEntry');
        Route::post('/edit-entry', 'Site\EntryController@edit');
        Route::post('/entry/delete', 'Site\EntryController@delete');
        Route::post('/request-invoice', 'Site\EntryController@postRequestInvoice');
    });

    // Route::get('/dashboard/form/{uid?}/{edit?}', 'Site\EntryController@form');
    Route::get('/dashboard/success/{uid}', 'Site\EntryController@thankyou');
    Route::post('/preview-entry', 'Site\EntryController@getPreview');
    Route::post('/preview-entry-links', 'Site\EntryController@getPreviewLinks');
    Route::post('/preview-credits', 'Site\EntryController@getCredits');
    Route::post('/minutes-passed', 'Site\EntryController@minutesPassed');
    Route::get('/submit-entry', 'Site\EntryController@submitEntry');
    Route::post('/submit-entry', 'Site\EntryController@postSubmitEntry');

    // autosave feature
    Route::post('/submit-category', 'Site\EntryController@submitCategory');
    Route::post('/submit-campaign', 'Site\EntryController@submitCampaign');
    Route::post('/submit-file', 'Site\EntryController@submitFile');
    Route::post('/submit-written', 'Site\EntryController@submitWritten');
    Route::post('/submit-agency', 'Site\EntryController@submitAgency');
    Route::post('/submit-review', 'Site\EntryController@submitReview');
    Route::get('/get-files/{id}/{type}', 'Site\EntryController@getFiles');
    Route::get('/delete-file/{id}/{file}', 'Site\EntryController@deleteFile');
    // end autosave feature

    // Route::post('/submit/entry', 'Site\EntryController@submit');
    Route::get('/my-invoice', 'Site\EntryController@myInvoice');
    Route::get('/invoice-email-sent', 'Site\EntryController@invoiceEmailSent');
    // Route::get('/request-invoice', 'Site\UserController@getRequestInvoice');
    Route::get('/my-entries', 'Site\EntryController@myEntries');
    Route::get('/view/entry/{uid}', 'Site\EntryController@view');
    Route::get('/user/file/{id}', 'Site\EntryController@downloadFile');
    Route::get('/user/certificate/{uid}/{field}', 'Site\EntryController@certs');

    // JUDGES ROUTES
    Route::get('juror/dashboard', 'Site\JudgeController@index');
    Route::get('judge/view-round-1/category/{cat_id}/subcategory/{sub_id}', 'Site\JudgeController@viewRound1');
    Route::post('judge/submit-vote-round1', 'Site\JudgeController@submitVoteRound1');

    Route::get('judge/review-round-1', 'Site\JudgeController@reviewRound1');
    Route::get('judge/success-round-1', 'Site\JudgeController@successRound1');

    Route::get('judge/view-round-2/category/{cat_id}/subcategory/{sub_id}', 'Site\JudgeController@viewRound2');
    Route::get('judge/review-round-2', 'Site\JudgeController@reviewRound2');
    Route::get('judge/success-round-2', 'Site\JudgeController@successRound2');

    Route::post('/preview-files', 'Site\EntryController@previewFiles');
    Route::post('/submit-links', 'Site\EntryController@submitLinks');

});

// ADMIN ROUTES

Route::group(['prefix' => 'admin', 'middleware' => ['admin']], function () {
    Route::get('entries/{filter?}', 'Admin\EntryController@index');
    Route::get('/download-entries', 'Admin\EntryController@downloadEntries');
    Route::post('/request-invoice-solo', 'Site\EntryController@postRequestInvoiceSolo');
    Route::get('download/file/{id}', 'Admin\EntryController@downloadFile');
    Route::get('view/entry/{uid}', 'Admin\EntryController@viewEntry');
    Route::get('certificate/{uid}/{field}', 'Admin\EntryController@certs');
    Route::get('/interim-export', 'Site\EntryController@export');
    Route::post('/update/payment', 'Admin\EntryController@updatePayment');
    Route::post('/update/status', 'Admin\EntryController@updateStatus');
    Route::get('/credits-export', 'Admin\EntryController@exportCredits');

    Route::get('jurors', 'Admin\JudgeController@index');
    Route::post('juror/send-email', 'Admin\JudgeController@sendEmail');
    Route::post('juror/send-email-all', 'Admin\JudgeController@sendEmailAll');
    Route::post('juror/send-email2', 'Admin\JudgeController@sendEmail2');
    Route::post('juror/add', 'Admin\JudgeController@add');
    // Route::post('jurors/add/post', 'Admin\JudgeController@addPost');
    Route::get('/juror/{id}', 'Admin\JudgeController@getJurorById');
    Route::post('juror/edit', 'Admin\JudgeController@edit');
    Route::post('juror/delete', 'Admin\JudgeController@delete');

    Route::post('/expiry-status', 'Admin\ExpiryStatusController@setExpiry');
    Route::get('admin-users', 'Admin\AdminUserController@index');
    Route::post('admin-user/add', 'Admin\AdminUserController@add');
    // Route::post('admin-user/add/post', 'Admin\AdminUserController@addPost');
    Route::get('/admin-id/{id}', 'Admin\AdminUserController@getAdminById');
    Route::post('admin-user/edit', 'Admin\AdminUserController@edit');
    Route::post('admin-user/delete', 'Admin\AdminUserController@delete');

    Route::get('votes/{id?}', 'Admin\VotesController@index');
    Route::post('votes/updateEntries', 'Admin\VotesController@updateEntries');

    Route::get('preview/{category}/{id?}', 'Admin\JudgeController@entryPreview');
    Route::get('summary/{category?}', 'Admin\JudgeController@summary');

    Route::get('/scores', 'Admin\ScoreController@index');

    Route::get('/download-score-summary-round1', 'Admin\ScoreController@downloadScoreSummaryRound1');
    Route::get('/download-score-all-round1', 'Admin\ScoreController@downloadScoreAllRound1');
    Route::get('/download-score-summary-round2', 'Admin\ScoreController@downloadScoreSummaryRound2');
    Route::get('/download-score-all-round2', 'Admin\ScoreController@downloadScoreAllRound2');
    
    Route::get('/view/score/{uid}', 'Admin\ScoreController@viewScore');

    Route::get('/round2-entries', 'Admin\EntryController@round2Entries');
    Route::post('/import-entries', 'Admin\EntryController@importEntries');

    Route::get('/export-round2', 'Admin\EntryController@exportRound2');
});

Route::group(['prefix' => 'judge', 'middleware' => ['judge']], function () {
    // old routes
    Route::get('/dashboard/instructions', 'Site\JudgeController@instructions');
    Route::get('/dashboard/list', 'Site\JudgeController@list');
    Route::get('/vote/{id}', 'Site\JudgeController@vote');
    Route::get('/file/{id}', 'Site\JudgeController@file');
    Route::post('/vote-round1', 'Site\JudgeController@voteRound1');
    Route::post('/vote-status-round1', 'Site\JudgeController@voteStatusRound1');
    Route::post('/submit-allvote-round1', 'Site\JudgeController@submitAllVoteRound1');

    Route::post('/vote/saveScore', 'Site\JudgeController@submitScore');
    Route::post('/vote/completeJudging', 'Site\JudgeController@completeJudging');
});

Route::get('/admin/voteResults/{id?}', 'Admin\VotesController@export');