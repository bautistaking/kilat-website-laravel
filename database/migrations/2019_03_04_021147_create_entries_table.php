<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEntriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('entries', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('user_id')->nullable()->unsigned();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->string('uid')->unique();
            $table->integer('category')->nullable();
            $table->integer('sub_category')->nullable();
            $table->enum('type', ['SINGLE', 'CAMPAIGN']);
            $table->text('entry_type_list', 191)->nullable();
            $table->string('entry_name')->nullable();
            $table->string('ad_details')->nullable();
            $table->string('advertiser')->nullable();
            $table->string('brand')->nullable();
            $table->string('agency')->nullable();
            $table->date('date_publication')->nullable();

            // new file migration
            $table->text('concept_board_dropzone')->nullable();
            $table->text('case_study_video_dropzone')->nullable();
            $table->text('written_case_study_dropzone')->nullable();
            $table->text('asc_clearance_dropzone')->nullable();
            $table->text('media_certification_dropzone')->nullable();
            $table->text('client_certification_dropzone')->nullable();
            $table->text('film_dropzone')->nullable();
            $table->text('posters_dropzone')->nullable();
            $table->text('audio_dropzone')->nullable();
            $table->text('demo_film_dropzone')->nullable();
            $table->text('english_translation_dropzone')->nullable();
            $table->text('photo_piece_dropzone')->nullable();
            $table->text('entry_dropzone')->nullable();
            // end file migration

            $table->string('member')->default('No');
            $table->string('agency_contact')->nullable();
            $table->string('position')->nullable();
            $table->string('phone_number')->nullable();
            $table->string('email')->nullable();
            $table->string('payment_status')->default('Unpaid');
            $table->string('status')->default('Pending');
            $table->string('fee')->default('0');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('entries');
    }
}
