/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "/";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 1);
/******/ })
/************************************************************************/
/******/ ({

/***/ "./resources/js/form-wizard.js":
/*!*************************************!*\
  !*** ./resources/js/form-wizard.js ***!
  \*************************************/
/*! no static exports found */
/***/ (function(module, exports) {

$(document).ready(function () {
  var stepper = document.querySelector('#stepper');
  var stepperInstace = new MStepper(stepper, {
    // options
    firstActive: 0,
    // this is the default
    // Allow navigation by clicking on the next and previous steps on linear steppers.
    stepsNavigation: false
  });
  $(".flatpickr").flatpickr();

  if ($(window).width() < 700) {
    Swal.fire({
      type: 'warning',
      title: "Ooops! Uploading large file sizes works best on Desktop",
      text: "Access enter.kidlatawards.com.ph on desktop for a better experience completing your submission."
    });
  }

  $('#stepper').on('steperror', function () {
    Swal.fire({
      imageUrl: "/images/Error.svg",
      imageWidth: 100,
      imageHeight: 124,
      title: "Ooops! Looks like you missed something important.",
      text: "Kindly fill up all the required fields. Thank you!"
    });
  });
  $('#stepper').on('stepchange', function () {
    resizeStepper();
    var active = $(".stepper .active").index();

    if (active == 3) {
      $('.label-credits-container').empty();
      $("input:text[name='credit_name[]']").each(function (index) {
        var name = $(this).val();
        var role = $("input:text[name='credit_role[]']").eq(index).val();

        if (name != '') {
          $('.label-credits-container').append("\n                      <div class=\"row\">\n                        <div class=\"col s6\">\n                          <p>" + name + "</p>\n                        </div>\n                        <div class=\"col\">\n                          <p>" + role + "</p>\n                        </div>\n                      </div>");
        }
      });
    }

    if (active == 5) {
      $('.label-key-requirements').empty();
      $('.label-supporting-requirements').empty();
      $('.label-reference').empty();
      $("input:text[name='files_name[]']").each(function (index) {
        var name = $(this).val();
        var type = $("input:hidden[name='file_type[]']").eq(index).val();

        if (name != '') {
          if (type == 'key') {
            $('.label-key-requirements').append("\n                          <div class=\"row\">\n                            <div class=\"col s12\">\n                              <label>" + name + "</label>\n                            </div>\n                          </div>");
          } else {
            $('.label-supporting-requirements').append("\n                          <div class=\"row\">\n                            <div class=\"col s12\">\n                              <label>" + name + "</label>\n                            </div>\n                          </div>");
          }
        }
      });
      $("input:text[name='reference[]']").each(function (index) {
        var link = $(this).val();

        if (link != '') {
          $('.label-reference').append("\n                      <div class=\"row\">\n                        <div class=\"col s12\">\n                          <label><a href=" + link + " target=\"_blank\" class=\"pointer\">" + link + "</a></label>\n                        </div>\n                      </div>");
        }
      });
    }
  });
  $(document).on('click', '.delete-node', function () {
    $(this).parent().remove();
  });
  $(document).on('change', 'form input', function () {
    var me = $(this);
    var name = me.attr("name");
    var val = me.val();

    if (name != 'credit_name[]' && name != 'credit_role[]' && name != 'files[]' && name != 'files_name[]' && name != 'client_cert' && name != 'media' && name != 'asc_cert') {
      $(".label__" + name).text(val);
    }

    if (name == 'sub_category') {
      var catID = me.data('category-id');
      $(".label__category").text(me.data('category-name'));
      $(".label__sub_category").text(me.data('sub-name'));
      $('input[name="category"]').val(catID);
      $('.guidelines div').addClass('hidden');
      $('.cat-' + catID).removeClass('hidden');
    }

    if (name == 'files[]') {
      var filesize = 0;
      $('input[name="files[]"]').each(function () {
        if ($(this).get(0).files.length > 0) {
          filesize += $(this)[0].files[0].size;
        }

        console.log(filesize);

        if (filesize > 1073741824) {
          // 1GB
          Swal.fire({
            type: 'warning',
            title: "Ooops! Failed attaching file",
            text: "You have exceeded the 1GB Filesize limit."
          });
          me.val('');
          me.parent().next().find('input[name="files_name[]"]').val('');
        }
      });
    }
  });
  $('.credits__add-more').on('click', function () {
    if ($('.credits-container').children().length < 20) {
      $('.credits-container').append("\n        <div class=\"row\">\n          <div class=\"col field s6 inline\">\n            <div class=\"input-field w-100\">\n              <input type=\"text\" name=\"credit_name[]\" class=\"validate\">\n            </div>\n          </div>\n          <div class=\"col field s6 inline\">\n            <div class=\"input-field w-100\">\n              <input type=\"text\" name=\"credit_role[]\" class=\"validate\">\n            </div>\n          </div>\n          <a class=\"btn-floating btn-large waves-effect waves-light red delete-node\"><i class=\"material-icons\">delete</i></a>\n        </div>\n      ");
    } else {
      $(document).ready(function () {
        Swal.fire({
          imageUrl: "/images/Error.svg",
          imageWidth: 100,
          imageHeight: 124,
          title: "Oops!",
          text: "You already reached the limit."
        });
      });
    }
  });
  $('.key-files__add-more').on('click', function () {
    if ($('.key-files-container').children().length < 10) {
      $('.key-files-container').append("\n        <div class=\"file-field input-field\">\n          <div class=\"btn pink\">\n            <span>Click to Attach File</span>\n            <input type=\"hidden\" name=\"pre_file[]\" value=\"no\">\n            <input type=\"hidden\" name=\"f_id[]\" value=\"\">\n            <input type=\"hidden\" name=\"file_type[]\" value=\"key\">\n            <input class=\"test\" type=\"file\" name=\"files[]\" accept=\".doc,.docx,application/msword,application/vnd.openxmlformats-officedocument.wordprocessingml.document,.jpg,.jpeg,.png,.pdf,.mp3,.mp4,.avi,.flv,.wmv,.mov\">\n          </div>\n          <div class=\"file-path-wrapper\">\n            <input class=\"file-path validate\" name=\"files_name[]\" type=\"text\">\n          </div>\n          <a class=\"btn-floating btn-large waves-effect waves-light red delete-node\"><i class=\"material-icons\">delete</i></a>\n        </div>\n      ");
    } else {
      $(document).ready(function () {
        Swal.fire({
          imageUrl: "/images/Error.svg",
          imageWidth: 100,
          imageHeight: 124,
          title: "Oops!",
          text: "You already reached the limit."
        });
      });
    }
  });
  $('.supporting-files__add-more').on('click', function () {
    if ($('.supporting-files-container').children().length < 3) {
      $('.supporting-files-container').append("\n        <div class=\"file-field input-field\">\n          <div class=\"btn pink\">\n            <span>Click to Attach File</span>\n            <input type=\"hidden\" name=\"pre_file[]\" value=\"no\">\n            <input type=\"hidden\" name=\"f_id[]\" value=\"\">\n            <input type=\"hidden\" name=\"file_type[]\" value=\"supporting\">\n            <input type=\"file\" name=\"files[]\" accept=\".doc,.docx,application/msword,application/vnd.openxmlformats-officedocument.wordprocessingml.document,.jpg,.jpeg,.png,.pdf\">\n          </div>\n          <div class=\"file-path-wrapper\">\n            <input class=\"file-path validate\" name=\"files_name[]\" type=\"text\">\n          </div>\n          <a class=\"btn-floating btn-large waves-effect waves-light red delete-node\"><i class=\"material-icons\">delete</i></a>\n        </div>\n      ");
    } else {
      $(document).ready(function () {
        Swal.fire({
          imageUrl: "/images/Error.svg",
          imageWidth: 100,
          imageHeight: 124,
          title: "Oops!",
          text: "You already reached the limit."
        });
      });
    }
  });
  $('.reference__add-more').on('click', function () {
    if ($('.reference-container').children().length < 5) {
      $('.reference-container').append("\n        <div class=\"file-field input-field\">\n          <div class=\"input-field\">\n            <input type=\"text\" name=\"reference[]\">\n          </div>\n          <a class=\"btn-floating btn-large waves-effect waves-light red delete-node\"><i class=\"material-icons\">delete</i></a>\n        </div>\n      ");
    } else {
      $(document).ready(function () {
        Swal.fire({
          imageUrl: "/images/Error.svg",
          imageWidth: 100,
          imageHeight: 124,
          title: "Oops!",
          text: "You already reached the limit."
        });
      });
    }
  });
  $('.edit-step').on('click', function () {
    var step = $(this).data('step');
    stepperInstace.openStep(step, stepper);
  });
  $(".search-category").on("keyup", function () {
    var value = $(this).val().toLowerCase();
    $("input[name='sub_category']").filter(function () {
      $(this).next().toggle($(this).next().text().toLowerCase().indexOf(value) > -1);
    });
  });

  function resizeStepper() {
    newHeight = 0;
    padding = 150;
    $('#stepper').find('.step.active').find('.step-content > div').each(function () {
      newHeight += parseInt($(this).css('height'));
    });
    newHeight += padding;
    $('#stepper').animate({
      height: newHeight
    }, 300);
  }

  resizeStepper();
});

/***/ }),

/***/ 1:
/*!*******************************************!*\
  !*** multi ./resources/js/form-wizard.js ***!
  \*******************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! C:\Users\kctanjuan\Desktop\ebdigital\kidlat_awards\resources\js\form-wizard.js */"./resources/js/form-wizard.js");


/***/ })

/******/ });