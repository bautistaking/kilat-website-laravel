(function() {
    var elements = document.querySelectorAll('[data-kb]'),
        scope = {};
    elements.forEach(function(element) {
        //execute scope setter
        // console.log('Element is: '+element.type)
        if(element.type === 'date' ||
           element.type === 'text'|| 
           element.type === 'email'|| 
           element.type === 'number'|| 
           element.type === 'textarea' || 
           element.type === 'select-one'){
            var propToBind = element.getAttribute('data-kb');
            addScopeProp(propToBind);
            element.oninput = function(){
                scope[propToBind] = element.value;
            }
        };

        //bind prop to elements
        function addScopeProp(prop){
            //add property if needed
            if(!scope.hasOwnProperty(prop)){
                //value to populate with newvalue
                var value;
                Object.defineProperty(scope, prop, {
                    set: function (newValue) {
                        value = newValue;
                        elements.forEach(function(element){
                            //change value to binded elements
                            if(element.getAttribute('data-kb') === prop){
                                if(element.type && (
                                    element.type === 'date' ||
                                    element.type === 'text' ||
                                    element.type === 'email'|| 
                                    element.type === 'number'||
                                    element.type === 'textarea' ||
                                    element.type === 'select-one')){
                                    element.value = newValue;
                                }
                                else if(!element.type){
                                    element.innerHTML = newValue;
                                }
                            }
                        });
                    },
                    get: function(){
                        return value;
                    },
                    enumerable: true
                });
            }
        }
    });
})();